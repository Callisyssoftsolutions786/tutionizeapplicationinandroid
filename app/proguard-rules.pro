# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /home/preusst/SDKs/Android-SDK/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:
# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
#-dontwarn okio.**
#-dontwarn retrofit.**
#-keep class retrofit.** { *; }
#-keepclassmembers,allowobfuscation interface * {
#    @retrofit.http.** <methods>;
#}

#-dontwarn com.squareup.okhttp.**
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
-dontwarn com.googlecode.mp4parser.**
-dontwarn rx.**
-dontwarn okio.**
-dontwarn com.squareup.okhttp.**
-dontwarn retrofit.**
-keepattributes *Annotation*,Signature
-keep class com.example.MyClass
-keep class com.example.MyClassToo
-keep class com.example.** { *; }
-keep class retrofit.** { *; }
-keep class com.example.model.** { *; }




