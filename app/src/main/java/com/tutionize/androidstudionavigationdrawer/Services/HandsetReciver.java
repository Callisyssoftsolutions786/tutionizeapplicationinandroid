package com.tutionize.androidstudionavigationdrawer.Services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class HandsetReciver extends BroadcastReceiver
{
    @Override
    public void onReceive(Context context, Intent intent)
    {
  String action =  intent.getAction();

        assert action != null;
        if (action.equals(Intent.ACTION_HEADSET_PLUG)){

         int State=    intent.getIntExtra("state",-1);
         switch (State)
          {
              case 0 :
                  Toast.makeText(context, "Unpluged", Toast.LENGTH_SHORT).show();
                  break;
              case 1 :
                  Toast.makeText(context, "Plugged", Toast.LENGTH_SHORT).show();
                  break;
              default:
                  break;
          }

        }
    }
}
