package com.tutionize.androidstudionavigationdrawer.Services;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.telephony.TelephonyManager;

import com.tutionize.androidstudionavigationdrawer.Activity_Screen.Home_screen;
import com.tutionize.androidstudionavigationdrawer.Constant.AppConst;
import com.tutionize.androidstudionavigationdrawer.R;

import java.util.Objects;

import static android.app.NotificationManager.IMPORTANCE_HIGH;

public class Class_join_services extends Service {
    private String room;
    private String type, calledid;
    private static int START_SERVICE=0;


    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        String CHANNEL_ONE_ID = "com.tutonize.androidstudionav";
        String CHANNEL_ONE_NAME = "Channel One";
        NotificationChannel notificationChannel = null;
        try {
            if(intent.getExtras()!=null)
            {
                room = intent.getExtras().getString("roomname");
                type = intent.getExtras().getString("type");
                calledid = intent.getExtras().getString("calledid");
            }

        }catch (Exception ignored){}
        switch (Objects.requireNonNull(intent.getAction())) {
            case AppConst.ACTION.STARTFOREGROUND_ACTION:

                setSTART_SERVICE(1);
                Intent notificationIntent = new Intent(this, Home_screen.class);
                notificationIntent.setAction(AppConst.ACTION.MAIN_ACTION);
                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);


                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
                Intent previousIntent = new Intent(this, Class_join_services.class);
                previousIntent.setAction(AppConst.ACTION.PREV_ACTION);
                PendingIntent ppreviousIntent = PendingIntent.getService(this, 0, previousIntent, 0);


                Intent nextIntent = new Intent(this, Class_join_services.class);
                nextIntent.setAction(AppConst.ACTION.NEXT_ACTION);
                nextIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                PendingIntent pnextIntent = PendingIntent.getService(this, 0, nextIntent, 0);

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    notificationChannel = new NotificationChannel(CHANNEL_ONE_ID,
                            CHANNEL_ONE_NAME, IMPORTANCE_HIGH);
                    notificationChannel.enableLights(true);
                    notificationChannel.setLightColor(Color.RED);
                    notificationChannel.setShowBadge(true);
                    notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
                    NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                    assert manager != null;
                    manager.createNotificationChannel(notificationChannel);
                }


                Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.logo);
                Notification notification = new NotificationCompat.Builder(this)
                        .setContentTitle("Tuitionize Application")
                        .setTicker("Tuitionize Application")
                        .setContentText("Your Class in Available")
                        .setSmallIcon(R.drawable.logo)
                        .setLargeIcon(Bitmap.createScaledBitmap(icon, 128, 128, false))
                        .setContentIntent(pendingIntent)
                        .setOngoing(true)
                        .setChannelId(CHANNEL_ONE_ID)
                        .addAction(R.drawable.leaveclass, "LEAVE", ppreviousIntent)
                        .addAction(R.drawable.joinclass, "JOIN AGAIN", pnextIntent).build();
                startForeground(AppConst.NOTIFICATION_ID.FOREGROUND_SERVICE, notification);
                break;
            case AppConst.ACTION.PREV_ACTION:
                stopForeground(true);
                stopSelf();
                break;
            case AppConst.ACTION.NEXT_ACTION:
                TelephonyManager telephonyManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    //return;
                }
                assert telephonyManager != null;
                @SuppressLint("HardwareIds")
                String deviceId = telephonyManager.getDeviceId();
                String type = this.type;
                String calltype = "Video";
                String teacherimage = "TeacherImage";
                String teachername = "TeacherName";
                if (room != null && type != null && deviceId != null && calltype != null) {

                    Intent intentactivity = new Intent(this, Home_screen.class);
                    intentactivity.putExtra("msg1", "serviceIntent");
                    intentactivity.putExtra("room1", room);

                    intentactivity.putExtra("teachername1", teachername);
                    intentactivity.putExtra("teacherimage1", teacherimage);

                    intentactivity.putExtra("calltype1", calltype);
                    intentactivity.putExtra("callid1", calledid);

                    intentactivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intentactivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    this.startActivity(intentactivity);
                    stopForeground(true);
                    stopSelf();
                    setSTART_SERVICE(0);
                    //GoToClass(room,type.concat(deviceId),calltype,teacherimage,teachername);
                }

                break;
            case AppConst.ACTION.STOPFOREGROUND_ACTION:
                stopForeground(true);
                stopSelf();
                setSTART_SERVICE(0);

                break;
        }

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public static int getSTART_SERVICE() {
        return START_SERVICE;
    }

    public static void setSTART_SERVICE(int START_SERVICE) {
        Class_join_services.START_SERVICE = START_SERVICE;
    }

}
