package com.tutionize.androidstudionavigationdrawer.Services;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.tutionize.androidstudionavigationdrawer.Activity_Screen.Home_screen;
import com.tutionize.androidstudionavigationdrawer.Activity_Screen.MyAlertDialog;
import com.tutionize.androidstudionavigationdrawer.Constant.AppConst;
import com.tutionize.androidstudionavigationdrawer.Fragment.Fragment_Start_Class;
import com.tutionize.androidstudionavigationdrawer.Fragment.Live_Video_Screen;
import com.tutionize.androidstudionavigationdrawer.Fragment.TeacherMainActivity;
import com.tutionize.androidstudionavigationdrawer.Fragment.Teacher_Detail_Screen;
import com.tutionize.androidstudionavigationdrawer.Sharepool.MySharePrafranceClass;
import com.tutionize.androidstudionavigationdrawer.command.ConstantCommand;

import org.json.JSONArray;

import java.util.Objects;

import cz.msebera.android.httpclient.Header;

/**
 * Created by android on 12/18/17.
 */

public class MyReciver extends BroadcastReceiver {
    protected String[] listdata;
    protected String[] login;
    private Context context;

    @Override
    public void onReceive(Context context, Intent intent)
    {
        this.context = context;
        String  msg = Objects.requireNonNull(intent.getExtras()).getString("msg");
        String room = intent.getExtras().getString("room");
        String teachername = intent.getExtras().getString("teachername");
        String teacherimage = intent.getExtras().getString("teacherimage");
        String calltype = intent.getExtras().getString("calltype");
        String callid = intent.getExtras().getString("callid");
        sharedata(context);
        if (TeacherMainActivity.teacherMainActivity!=null) {
            TeacherMainActivity.teacherMainActivity.setNotification(true);
        }
        assert msg != null;
        if (msg.contains("is requesting you")){
            if (TeacherMainActivity.teacherMainActivity!=null){
                TeacherMainActivity.teacherMainActivity.Requestlist(TeacherMainActivity.Teacherid);
            }
        }
        if (msg.contains("Your request is decline by the")){
            if (Teacher_Detail_Screen.teacher_detail_screen!= null){
                Teacher_Detail_Screen.teacher_detail_screen.hire_button.setVisibility(View.VISIBLE);
            }
        }
        if (msg.contains("Your request is decline by the")){
            if (Teacher_Detail_Screen.teacher_detail_screen!= null){
                Teacher_Detail_Screen.teacher_detail_screen.hire_button.setVisibility(View.VISIBLE);
            }
        }
        if (calltype !=null)
        {
            if (calltype.equals(ConstantCommand.callType_Audio)){}
            else if(calltype.equals(ConstantCommand.callType_MuteAudio))
            {
                room=null;
                if (Live_Video_Screen.live_video_screen!=null) {
                    Live_Video_Screen.live_video_screen.MuteLocalpaticipate();
                }
            }
            else if(calltype.equals(ConstantCommand.callType_UnMuteAudio)){
                room=null;
                if (Live_Video_Screen.live_video_screen!=null)
                {   Live_Video_Screen.live_video_screen.UnMuteLocalpaticipate();}
            }
            else if(calltype.equals(ConstantCommand.callType_Accepted))
            { room=null;
              if (Live_Video_Screen.live_video_screen!=null) {
                    Live_Video_Screen.live_video_screen.CallTimeStart();
                }
            }
            else if(calltype.equals(ConstantCommand.callType_End))
            { room=null;
              if (Class_join_services.getSTART_SERVICE()==1)
              {
                  Intent stopIntent = new Intent(context,Class_join_services.class);
                  stopIntent.setAction(AppConst.ACTION.STOPFOREGROUND_ACTION);
                  context.startService(stopIntent);
                  return;
              }
                if (listdata[2].equals(ConstantCommand.Profile_Type_T))
                {

                    if (Live_Video_Screen.live_video_screen!=null)
                    {
                        Live_Video_Screen.live_video_screen.Endfragment();
                    }
                  else
                    {
                        if (Fragment_Start_Class.fragment_start_class!=null)
                        {
                            Fragment_Start_Class.fragment_start_class.closefragment();
                        }
                    }
                }
                else {
                    if (Fragment_Start_Class.fragment_start_class!=null)
                    {
                        Fragment_Start_Class.fragment_start_class.closefragment();
                    }
                }
            }
            else
                {
                    String userid  = login[0];
                    if (userid !=null) {
                        Save_Notification(callid, msg, room,userid,AppConst.constant.concat("/add_notification"));
                    }
                }
        }
        else {
            String userid  = login[0];
            if (userid !=null) {
                Save_Notification(callid, msg,room,userid,AppConst.constant.concat("/add_notification"));
            }
            }
        if (room!=null)
        {
            Intent intentactivity=new Intent(context, Home_screen.class);
            intentactivity.putExtra("msg1", msg);
            intentactivity.putExtra("room1", room);
            intentactivity.putExtra("teachername1", teachername);
            intentactivity.putExtra("teacherimage1", teacherimage);
            intentactivity.putExtra("calltype1", calltype);
            intentactivity.putExtra("callid1", callid);
            intentactivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intentactivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intentactivity);
        }
        else if(calltype !=null)
        {
            if(calltype.equals(ConstantCommand.callType_MuteAudio)){}
            if(calltype.equals(ConstantCommand.callType_UnMuteAudio)){}
            if(calltype.equals(ConstantCommand.callType_Accepted)){

            }
        }
        else
        {

            String userid  = login[0];
            if (userid !=null) {
                Save_Notification(callid, msg,"",userid,AppConst.constant.concat("/add_notification"));
            }
            Intent intent1 = new Intent(context, MyAlertDialog.class);
            intent1.putExtra("msg1", msg);
            intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
//                    |Intent.FLAG_ACTIVITY_CLEAR_TASK
//                    |Intent.FLAG_ACTIVITY_CLEAR_TASK
            );
           context.startActivity(intent1);
        }

    }

    private void sharedata(Context context){
        listdata= MySharePrafranceClass.LoginDataGetSharePrefrance(context);
        login = MySharePrafranceClass.GetSharePrefrance(context);
    }


    public void Save_Notification(String callid,String msg,String room,String userid,String Url){
        RequestParams params = new RequestParams();
        params.put("caller_id",callid);
        params.put("notification",msg);
        params.put("room_id", room);
        params.put("user_id",userid);
        AsyncHttpClient client = new AsyncHttpClient();
        client.post(Url,params,new JsonHttpResponseHandler(){

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);
               // Home_screen.ShowToast("Your Notification is Succesfully added");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
               // Home_screen.ShowToast("Your Notification adding Problem ");
            }
        });
    }
}
