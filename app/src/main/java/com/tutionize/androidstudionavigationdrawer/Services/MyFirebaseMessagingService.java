package com.tutionize.androidstudionavigationdrawer.Services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.tutionize.androidstudionavigationdrawer.Activity_Screen.Home_screen;
import com.tutionize.androidstudionavigationdrawer.R;
import com.tutionize.androidstudionavigationdrawer.Sharepool.MySharePrafranceClass;

import static android.app.NotificationManager.IMPORTANCE_HIGH;

/**
 * Created by android on 11/24/17.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgService";
    private String[] list = MySharePrafranceClass.GetSharePrefrance(getApplication());
    private String id = null;


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if(remoteMessage.getData().size()>0)
          {
//              if (list[0]!=null)
//              {
                  String messageBody=remoteMessage.getData().get("message");
                  String roomid = remoteMessage.getData().get("roomname");
                  String teachername = remoteMessage.getData().get("teacher_name");
                  String teacherimage = remoteMessage.getData().get("teacher_image");
                  String calltype = remoteMessage.getData().get("call_type");
                  String callid = remoteMessage.getData().get("caller_id");
                  
                  Intent intent = new Intent(getApplicationContext(),MyReciver.class);
                  intent.putExtra("msg",messageBody);
                  intent.putExtra("callid",callid);
                  intent.putExtra("calltype",calltype);

                  if (roomid!=null&&teacherimage!=null&&teachername!=null){
                      intent.putExtra("room",roomid);
                      intent.putExtra("teachername",teachername);
                      intent.putExtra("teacherimage",teacherimage);
                  }

                  sendBroadcast(intent);
                  sendNotification(messageBody,calltype);
              //}
          }
    }
    private void sendNotification(String messageBody,String Calltype) {

        String CHANNEL_ONE_ID = "com.tutonize.androidstudionav";
        String CHANNEL_ONE_NAME = "Channel One";
        NotificationChannel notificationChannel = null;

        if (Calltype!=null) {}
        else {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                notificationChannel = new NotificationChannel(CHANNEL_ONE_ID,CHANNEL_ONE_NAME, IMPORTANCE_HIGH);
                notificationChannel.enableLights(true);
                notificationChannel.setLightColor(Color.RED);
                notificationChannel.setShowBadge(true);
                notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
                NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                assert manager != null;
                manager.createNotificationChannel(notificationChannel);
            }

            Bitmap icon = BitmapFactory.decodeResource(getResources(),R.drawable.logo);
            Intent intent = new Intent(this, Home_screen.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setContentTitle("Tuitionize Application")
                    .setTicker("Tuitionize Application")
                    .setLargeIcon(Bitmap.createScaledBitmap(icon, 128, 128, false))
                    .setSmallIcon(R.drawable.logo)
                    .setContentText(messageBody)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setChannelId(CHANNEL_ONE_ID)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            assert notificationManager != null;
            notificationManager.notify(0, notificationBuilder.build());
        }
    }
}