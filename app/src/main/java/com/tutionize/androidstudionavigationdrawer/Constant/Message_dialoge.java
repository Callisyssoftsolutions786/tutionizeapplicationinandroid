package com.tutionize.androidstudionavigationdrawer.Constant;

import android.app.Activity;
import android.content.DialogInterface;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;

import com.tutionize.androidstudionavigationdrawer.R;

import java.util.Objects;
public class Message_dialoge
{
    private  static Message_dialoge message_dialoge;
    private static AlertDialog alertDialog;
    private AlertDialog Exitalert;
    private Ringtone ringtone;
    private Message_dialoge(){}

    public static Message_dialoge getInstance(){
        if (message_dialoge==null){
            message_dialoge =  new Message_dialoge();
        }
        return message_dialoge;
    }

    public void  messagedialog(Activity context,String Message){
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        ringtone = RingtoneManager.getRingtone(context, notification);
        ringtone.play();
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setIcon(R.drawable.logo);
        builder.setTitle(Message);
        builder.setCancelable(false);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                context.finish();
                ringtone.stop();
            }
        });
        alertDialog = builder.create();
        Window window = alertDialog.getWindow();
        assert window != null;
        window.setGravity(Gravity.TOP);
        Objects.requireNonNull(alertDialog.getWindow()).getAttributes().windowAnimations = R.style.DialogAnimation;
        try {
            alertDialog.show();
        }
        catch (WindowManager.BadTokenException ignored){}
        catch (IllegalArgumentException ignored){}
        final Handler handler  = new Handler();
        final Runnable runnable = () -> {
            if (alertDialog.isShowing())
            {
                ringtone.stop();
                alertDialog.dismiss();
                context.finish();
            }
        };
        alertDialog.setOnDismissListener(dialog -> handler.removeCallbacks(runnable));
        handler.postDelayed(runnable, 5000);
    }

    public void exitfromApplication(Activity activity) throws WindowManager.BadTokenException {
        if (Exitalert!=null){
            if (Exitalert.isShowing()){Exitalert.dismiss();}
        }
        AlertDialog.Builder dialog = new AlertDialog.Builder(activity, R.style.AppTheme);
        dialog.setCancelable(false);
        dialog.setIcon(R.drawable.logo);
        dialog.setTitle("Tuitionize");
        dialog.setMessage("Do you want to Exit ?");
        dialog.setPositiveButton("Exit", (dialog1, id) -> {
            dialog1.dismiss();
            activity.finish();
        })
                .setNegativeButton("Cancel ", (dialog12, which) -> {
                    Exitalert.dismiss();
                    dialog12.dismiss();
                });
            Exitalert = dialog.create();
            if (!Exitalert.isShowing()) {

                Exitalert.show();
            }

        }
}

