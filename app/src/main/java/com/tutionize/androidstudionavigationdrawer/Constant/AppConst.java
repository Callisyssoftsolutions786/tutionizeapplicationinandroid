package com.tutionize.androidstudionavigationdrawer.Constant;


/**
 * Created by Vishal Sandhu on 04-09-2017.
 */

public class AppConst {

    public static String constant="https://tuitionize.com/demo/webservices/user";

    public static String Ap_image_constant = "https://tuitionize.com/demo/assets/images/";

    public static String photoconstant="https://tuitionize.com/demo/uploads/profile_pic/";

    public static String batchphotoconstant="https://tuitionize.com/demo/uploads/batch/";

    public static String videoconstant="https://tuitionize.com/demo/assets/video/";

    public static final String analiyic_id = "UA-120014023-1";

    public interface NOTIFICATION_ID
    {
        int FOREGROUND_SERVICE = 101;
    }
    public interface ACTION {
        String STARTFOREGROUND_ACTION = "com.example.androidstudionavigationdrawer.action.startforeground";
        String STOPFOREGROUND_ACTION = "com.example.androidstudionavigationdrawer.action.stopforeground";
        String MAIN_ACTION = "com.example.androidstudionavigationdrawer.action.main";
        String PREV_ACTION = "com.truiton.foregroundservice.action.prev";
        String PLAY_ACTION = "com.truiton.foregroundservice.action.play";
        String NEXT_ACTION = "com.truiton.foregroundservice.action.next";
    }
    public static final String about_us = "Tuitionize Ltd is a registered company in England and Wales under UK’s Companies House. Our aim is to make everyone expert in everything by making ways of obtaining knowledge smarter.\n" +
            "Tuitionize, the teaching and learning platform is the result of two years of extensive research and coding by a very dedicated and trained IT professional team. The app is specifically designed to make live tuitions pleasant and stimulating experience for everyone in every subject. With unbelievable low prices we are connecting learners with rated teachers all over the globe live.\n\n" +
            "At Tuitionize we believe if knowledge is the key to all powers, then everyone has to own it. We believed, in order to get this key of all powers, it is necessary to have a smart, creative and yet a simplistic  system which will make knowledge accessible as well as affordable  to everyone in the world having access to internet. Tuitionize tends to revolutionise education by making everyone expert in everything.\n\n" +
            "We love unconventional, unique and creative ways of education and thats why, we are different from others; and love to remain different!\n" +
            "Explore and enjoy our app\n" +
            "\n" +
            "\n";
}
