package com.tutionize.androidstudionavigationdrawer.Constant;

import java.util.ArrayList;
import java.util.List;

public class LiveData {

    public static List<String> getCountry(){
        ArrayList<String> country =  new ArrayList<>();
        // A
        country.add("Afghanistan");country.add("Albania");country.add("Algeria");
        country.add("Andorra");country.add("Angola");country.add("Antigua and Barbuda");
        country.add("Argentina");country.add("Armenia");country.add("Aruba");country.add("Australia");
        country.add("Austria");country.add("Azerbaijan");
        // B
        country.add("Bahamas");country.add("Bahrain");country.add("Bangladesh");
        country.add("Barbados");country.add("Belarus");country.add("Belgium");
        country.add("Belize");country.add("Benin");country.add("Bhutan");country.add("Bolivia");
        country.add("Bosnia and Herzegovina");country.add("Botswana");
        country.add("Brazil");country.add("Brunei");country.add("Bulgaria");country.add("Burkina Faso");
        country.add("Burma");country.add("Burundi");
        // C
        country.add("Cambodia");country.add("Cameroon");country.add("Canada");country.add("Cabo Verde");
        country.add("Central African Republic");country.add("Chad");country.add("Chile");
        country.add("China");country.add("Colombia");country.add("Comoros");country.add("Congo");
        country.add("Costa Rica");country.add("Cote d'Ivoire");country.add("Croatia");country.add("Cuba");
        country.add("Curacao");country.add("Cyprus");country.add("Czechia");
        // D
        country.add("Denmark");country.add("Djibouti");country.add("Dominica");country.add("Dominican Republic");
        // E
        country.add("Ecuador");country.add("Egypt");country.add("El Salvador");
        country.add("Equatorial Guinea");country.add("Eritrea");country.add("Estonia");country.add("Eswatini");country.add("Ethiopia");
        // F
        country.add("Fiji");country.add("Finland");country.add("France");
        // G
        country.add("Gabon");country.add("Gambia");country.add("Georgia");country.add("Germany");country.add("Ghana");country.add("Greece");
        country.add("Grenada");country.add("Guatemala");country.add("Guinea");country.add("Guinea-Bissau");country.add("Guyana");
        // H
        country.add("Haiti");country.add("Holy See");country.add("Honduras");
        country.add("Hong Kong");country.add(" Hungary");
        // I
        country.add("Iceland");country.add("India");country.add("Indonesia");country.add("Iran");
        country.add("Iraq");country.add("Ireland");country.add("Israel");country.add("Italy");
        // J
        country.add("Jamaica");country.add("Japan");country.add("Jordan");
        // K
        country.add("Kazakhstan");country.add("Kenya");country.add("Kiribati");country.add("Korea, North");
        country.add("Korea, South");country.add("Kosovo");country.add("Kuwait");country.add("Kyrgyzstan");
        // L
        country.add("Laos");country.add("Latvia");country.add("Lebanon");country.add("Lesotho");
        country.add("Liberia");country.add("Libya");country.add("Liechtenstein");country.add("Lithuania");
        country.add("Luxembourg");country.add("");country.add("");
        // M
        country.add("Macau");country.add("Macedonia");country.add("Madagascar");country.add("Malawi");
        country.add("Malaysia");country.add("Maldives");country.add("Mali");country.add("Malta");
        country.add("Marshall Islands");country.add("Mauritania");country.add("Mauritius");country.add("Mexico");
        country.add("Micronesia");country.add("Moldova");country.add("Monaco");country.add("Mongolia");country.add("Montenegro");
        country.add("Morocco");country.add("Mozambique");
        // N
        country.add("Namibia");country.add("Nauru");country.add("Nepal");country.add("Netherlands");country.add("New Zealand");
        country.add("Nicaragua");country.add("Niger");country.add("Nigeria");country.add("North Korea");country.add("Norway");
        // O
        country.add("Oman");
        // P
        country.add("Pakistan");country.add("Palau");country.add("Palestinian Territories");country.add("Panama");country.add("Papua New Guinea");
        country.add("Paraguay");country.add("Peru");country.add("Philippines");country.add("Poland");country.add("Portugal");
        // Q
        country.add("Qatar");
        // R
        country.add("Romania");country.add("Russia");country.add("Rwanda");
        // S
        country.add("Saint Kitts and Nevis");country.add("Saint Lucia");country.add("Saint Vincent and the Grenadines");country.add("Samoa");
        country.add("San Marino");country.add("Sao Tome and Principe");country.add("Saudi Arabia");country.add("Senegal");country.add("Serbia");
        country.add("Seychelles");country.add("Sierra Leone");country.add("Singapore");country.add("Sint Maarten");country.add("Slovakia");country.add("Slovenia");
        country.add("Solomon Islands");country.add("Somalia");country.add("South Africa");country.add("South Korea");country.add("South Sudan");
        country.add("Spain");country.add("Sri Lanka");country.add("Sudan");country.add("Suriname");country.add("Swaziland (See Eswatini)");
        country.add("Sweden");country.add("Switzerland");country.add("Syria");
        // T
        country.add("Taiwan");country.add("Tajikistan");country.add("Tanzania");country.add("Thailand");country.add("Timor-Leste");
        country.add("Togo");country.add("Tonga");country.add("Trinidad and Tobago");country.add("Tunisia");country.add("Turkey");
        country.add("Turkmenistan");country.add("Tuvalu");
        // U
        country.add("Uganda");country.add("Ukraine");country.add("United Arab Emirates");country.add("United Kingdom");
        country.add("Uruguay");country.add("Uzbekistan");
        //  V
        country.add("Vanuatu");country.add("Venezuela");country.add("Vietnam");
        // Y
        country.add("Yemen");
        // Z
        country.add("Zambia");
        country.add("Zimbabwe");

        return country;
    }
}
