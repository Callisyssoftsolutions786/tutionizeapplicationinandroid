package com.tutionize.androidstudionavigationdrawer.Constant;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Picture_method {
    private static final int MEDIA_TYPE_IMAGE = 1;
    private static final int MEDIA_TYPE_VIDEO = 2;
    private String current;
    private static Picture_method picture_method;
    private Picture_method(){}


   public static Picture_method getInstance(){
       if (picture_method==null){
           picture_method =  new Picture_method();
       }
       return picture_method;
   }


    public String getPathFromUri(Uri uri, Activity activity) {

        String[] projection = {MediaStore.Images.Media.DATA};
        @SuppressLint("Recycle")
        Cursor cursor = activity.getContentResolver().query(uri, projection, null, null, null);

        assert cursor != null;
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();

        return cursor.getString(column_index);

    }


    public  File getOutputMediaFile(int type) {

        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/.Studentlife/");


        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {

                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }
        current = mediaFile.getAbsolutePath();
        setCurrent(current);
        return mediaFile;
    }

    public String getCurrent(){
       return current;
    }
    private void setCurrent(String current){
       this.current = current;
    }


    public File getDestinationPath(Activity activity){

        File f = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + activity.getPackageName() + "/tuitionize/media");
        if (! f.mkdirs() || f.isDirectory())
        {

        }
        return f;
    }


}
