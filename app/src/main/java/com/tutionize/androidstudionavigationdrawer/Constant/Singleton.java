package com.tutionize.androidstudionavigationdrawer.Constant;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.tutionize.androidstudionavigationdrawer.Api_Interface.All_APIs;
import com.tutionize.androidstudionavigationdrawer.Fragment.Teacher_Detail_Screen;
import com.tutionize.androidstudionavigationdrawer.R;

import java.util.ArrayList;
import java.util.Objects;

import retrofit.RestAdapter;

public class Singleton {
    private RestAdapter restAdapter;
    private ProgressDialog progressDialog;
    private static Singleton singleton;
    private All_APIs all_apIs;

    private Singleton(){}


    public static Singleton getInstance(){
        if (singleton==null){
            singleton =  new Singleton();
        }
        return singleton;
    }
//=========================RestAdapter Class ==============


    private RestAdapter getRestAdapter(){
       if (restAdapter==null){
           restAdapter = new RestAdapter.Builder().setEndpoint(AppConst.constant).build();
       }
       return restAdapter;
    }


//========================= Api interface =====================


    public All_APIs getApi()
    {

        if (all_apIs==null){

          all_apIs  =   getRestAdapter().create(All_APIs.class);
        }

        return all_apIs;
    }

// ================================= Progress Dialog =========


    @SuppressLint("ResourceAsColor")
    public ProgressDialog Progress(Activity activity){

        if (progressDialog==null)
        {
             ProgressBar spinner = new android.widget.ProgressBar(activity, null, android.R.attr.progressBarStyle);
             progressDialog = new ProgressDialog(activity,R.style.AppCompatAlertDialogStyle);
             progressDialog.setMessage("Loading...");
             progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
             progressDialog.setIndeterminate(true);
             Objects.requireNonNull(progressDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
             spinner.getIndeterminateDrawable().setColorFilter(R.color.Toolbarcolor, PorterDuff.Mode.LIGHTEN);
             progressDialog.setCancelable(false);
            progressDialog.addContentView(new ProgressBar(activity),new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        }

       return progressDialog;
    }

    //=========================== Fragment Mehod =============


    public   void CallFragment(Fragment fragment, Activity activity,String tag)
    {
        try {


        FragmentManager fragmentManager = ((FragmentActivity) activity).getSupportFragmentManager();
        fragmentManager.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                .replace(R.id.container,fragment)//.commit();
                .addToBackStack(tag).commit();

        }catch (IllegalArgumentException ignored){}
        catch (Exception ignored){}
    }


    public void callfrontclass(Activity activity,String id, String name, String bio, String Qualification,
                                String Image, String teacher_course, String fee, String review, String languages,String country, ArrayList<String> Student, ArrayList<String> PandingStudent) {
        Bundle bundle = new Bundle();
        Fragment mFragment = null;
        Class fragmentClass = Teacher_Detail_Screen.class;
        try {
            mFragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        FragmentManager fragmentManager = ((FragmentActivity) activity).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        bundle.putString("teacher_id", id);
        bundle.putString("teacher_name", name);
        bundle.putString("teacher_bio", bio);
        bundle.putString("teacher_qualification", Qualification);
        bundle.putString("teacher_image", Image);
        bundle.putString("teacher_course",teacher_course);
        bundle.putString("fee",fee);
        bundle.putString("review",review);
        bundle.putString("languages", languages);
        bundle.putString("country", country);
        bundle.putStringArrayList("Student",Student);
        bundle.putStringArrayList("PandingStudent",PandingStudent);
        assert mFragment != null;
        mFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.container, mFragment).addToBackStack("jnsbnbs").commit();

    }
}
