package com.tutionize.androidstudionavigationdrawer.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by android on 11/30/17.
 */

public class All_Batch_modle {
    @SerializedName("success")
    @Expose
    private String  success;
    @SerializedName("result")
    @Expose
    private List<All_Batch_Result_modle> result;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<All_Batch_Result_modle> getResult() {
        return result;
    }

    public void setResult(List<All_Batch_Result_modle> result) {
        this.result = result;
    }
}
