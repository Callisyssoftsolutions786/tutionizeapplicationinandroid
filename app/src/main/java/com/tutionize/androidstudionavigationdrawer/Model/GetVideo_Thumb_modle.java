package com.tutionize.androidstudionavigationdrawer.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by android on 11/20/17.
 */

public class GetVideo_Thumb_modle {
    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("userid")
    @Expose
    private String userid;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("video_title")
    @Expose
    private String video_title;

    @SerializedName("video_url")
    @Expose
    private String video_url;

    @SerializedName("video_description")
    @Expose
    private String video_description;

    @SerializedName("teacher")
    @Expose
    private String teacher;

    @SerializedName("thumb")
    @Expose
    private String thumb;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getVideo_title() {
        return video_title;
    }

    public void setVideo_title(String video_title) {
        this.video_title = video_title;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public String getVideo_description() {
        return video_description;
    }

    public void setVideo_description(String video_description) {
        this.video_description = video_description;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }
}
