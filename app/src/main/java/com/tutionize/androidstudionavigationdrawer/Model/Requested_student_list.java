package com.tutionize.androidstudionavigationdrawer.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by android on 12/8/17.
 */

public class Requested_student_list {
    @SerializedName("success")
    @Expose
     private String success;
    @SerializedName("result")
    @Expose
     private List<Requested_student_result> result;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<Requested_student_result> getResult() {
        return result;
    }

    public void setResult(List<Requested_student_result> result) {
        this.result = result;
    }
}
