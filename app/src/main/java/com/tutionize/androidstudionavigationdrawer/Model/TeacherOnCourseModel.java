
package com.tutionize.androidstudionavigationdrawer.Model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TeacherOnCourseModel {

    @SerializedName("success")
    @Expose
    private Integer success;

    @SerializedName("msg")
    @Expose
    private String msg;

    @SerializedName("result")
    @Expose
    private List<Result> result = null;

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }
    public String getMsg() {return msg;}

    public void setMsg(String msg) {this.msg = msg;}

}
