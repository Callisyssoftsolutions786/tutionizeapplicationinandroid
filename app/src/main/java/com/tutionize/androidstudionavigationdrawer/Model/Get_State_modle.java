package com.tutionize.androidstudionavigationdrawer.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by user on 27/12/17.
 */

public class Get_State_modle {

@SerializedName("success")
    @Expose
    private String success;
@SerializedName("result")
@Expose
    private List<State_Result_modle> result;


    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<State_Result_modle> getResult() {
        return result;
    }

    public void setResult(List<State_Result_modle> result) {
        this.result = result;
    }
}
