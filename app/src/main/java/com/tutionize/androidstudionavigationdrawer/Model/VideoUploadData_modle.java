package com.tutionize.androidstudionavigationdrawer.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by android on 11/15/17.
 */

public class VideoUploadData_modle {

    @SerializedName("video_id")
    @Expose
      private String video_id;
    @SerializedName("user_id")
    @Expose
      private String user_id;
    @SerializedName("success")
    @Expose
      private String success;
    @SerializedName("message")
    @Expose
      private String message;

    public String getVideo_id() {
        return video_id;
    }

    public void setVideo_id(String video_id) {
        this.video_id = video_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
