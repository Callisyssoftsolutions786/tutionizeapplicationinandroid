package com.tutionize.androidstudionavigationdrawer.Model;

import com.twilio.video.Participant;
import com.twilio.video.VideoTrack;

/**
 * Created by android on 11/14/17.
 */

public class TwelioView_Model {
    private Participant paticipant;
    private VideoTrack videoTrack;

   public TwelioView_Model(VideoTrack videoTrack,Participant paticipant){

       this.paticipant=paticipant;
       this.videoTrack=videoTrack;
   }

    public Participant getPaticipant() {
        return paticipant;
    }

    public void setPaticipant(Participant paticipant) {
        this.paticipant = paticipant;
    }

    public VideoTrack getVideoTrack() {
        return videoTrack;
    }

    public void setVideoTrack(VideoTrack videoTrack) {
        this.videoTrack = videoTrack;
    }
}
