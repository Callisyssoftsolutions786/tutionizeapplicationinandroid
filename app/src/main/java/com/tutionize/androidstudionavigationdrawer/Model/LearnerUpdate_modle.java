package com.tutionize.androidstudionavigationdrawer.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by android on 10/11/17.
 */

public class LearnerUpdate_modle {
    @SerializedName("response")
    @Expose
    private String response;
    @SerializedName("success")
    @Expose
    private String success;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }
}
