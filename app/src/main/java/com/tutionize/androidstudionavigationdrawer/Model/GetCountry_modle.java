package com.tutionize.androidstudionavigationdrawer.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by user on 26/12/17.
 */

public class GetCountry_modle {

    @SerializedName("success")
            @Expose
    String success;
    @SerializedName("result")
            @Expose
    List<GetCountryResult> result;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<GetCountryResult> getResult() {
        return result;
    }

    public void setResult(List<GetCountryResult> result) {
        this.result = result;
    }
}
