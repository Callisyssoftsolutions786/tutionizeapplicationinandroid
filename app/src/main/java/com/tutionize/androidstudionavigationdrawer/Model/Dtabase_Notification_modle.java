package com.tutionize.androidstudionavigationdrawer.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 22/12/17.
 */

public class Dtabase_Notification_modle {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("user_id")
    @Expose
    private String user_id;
    @SerializedName("notification")
    @Expose
    private String notification;
    @SerializedName("notification_time")
    @Expose
    private String notification_time;
    @SerializedName("room_id")
    @Expose
    private String room_id;
    @SerializedName("caller_id")
    @Expose
    private String caller_id;


    public String getCaller_id() {return caller_id; }
    public void setCaller_id(String caller_id) {this.caller_id = caller_id; }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getUser_id() {
        return user_id;
    }
    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
    public String getNotification() {
        return notification;
    }
    public void setNotification(String notification) {
        this.notification = notification;
    }
    public String getNotification_time() {
        return notification_time;
    }
    public void setNotification_time(String notification_time)
    {
        this.notification_time = notification_time;
    }
    public String getRoom_id() {
        return room_id;
    }
    public void setRoom_id(String room_id) {
        this.room_id = room_id;
    }
}
