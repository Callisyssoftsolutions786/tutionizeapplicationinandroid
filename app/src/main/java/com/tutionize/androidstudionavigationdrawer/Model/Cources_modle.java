package com.tutionize.androidstudionavigationdrawer.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Cources_modle {
    @SerializedName("data")
    @Expose
    List<CourseData> data;

    public List<CourseData> getData() {
        return data;
    }

    public void setData(List<CourseData> data) {
        this.data = data;
    }
}
