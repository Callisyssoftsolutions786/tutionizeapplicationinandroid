package com.tutionize.androidstudionavigationdrawer.Model;

/**
 * Created by user on 31/1/18.
 */

public class UnseletedStudent {
    private String id;
    private String first;
    private String last;
    private String email;
    private String image;
    private String thumb;
    private String country;
    private String type;

    public  UnseletedStudent(String id,String first,String last, String email, String image, String thumb, String country, String type){
        this.setId(id);
        this.setFirst(first);
        this.setLast(last);
        this.setEmail(email);
        this.setImage(image);
        this.setThumb(thumb);
        this.setCountry(country);
        this.setType(type);
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getLast() {
        return last;
    }

    public void setLast(String last) {
        this.last = last;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
