
package com.tutionize.androidstudionavigationdrawer.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TeacherDetailModle
{

    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("result")
    @Expose
    private List<TeacherDetailResult> result = null;
    @SerializedName("studentlist")
    @Expose
    private List<StudentListOfTeacherDetail> studentlist = null;
    @SerializedName("studentpendinglist")
    @Expose
    private List<PandingStudentListofTeacherDetail> studentpendinglist = null;


    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMsg() {return msg;}

    public void setMsg(String msg) {this.msg = msg;}

    public List<TeacherDetailResult> getResult() {
        return result;
    }

    public void setResult(List<TeacherDetailResult> result) {
        this.result = result;
    }

    public List<StudentListOfTeacherDetail> getStudentlist() {
        return studentlist;
    }

    public void setStudentlist(List<StudentListOfTeacherDetail> studentlist) {
        this.studentlist = studentlist;
    }
    public List<PandingStudentListofTeacherDetail> getStudentpendinglist() {
        return studentpendinglist;
    }
    public void setStudentpendinglist(List<PandingStudentListofTeacherDetail> studentpendinglist) {
        this.studentpendinglist = studentpendinglist;
    }

}
