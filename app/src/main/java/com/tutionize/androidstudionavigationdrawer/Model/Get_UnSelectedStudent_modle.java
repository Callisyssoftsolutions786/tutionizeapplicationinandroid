package com.tutionize.androidstudionavigationdrawer.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by user on 27/12/17.
 */

public class Get_UnSelectedStudent_modle {

    @SerializedName("success")
    @Expose
    private  String success;
    @SerializedName("result")
    @Expose
    private List<Get_UnSelectedSudentResult> result;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<Get_UnSelectedSudentResult> getResult() {
        return result;
    }

    public void setResult(List<Get_UnSelectedSudentResult> result) {
        this.result = result;
    }
}
