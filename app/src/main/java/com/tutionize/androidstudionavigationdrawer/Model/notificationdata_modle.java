package com.tutionize.androidstudionavigationdrawer.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class notificationdata_modle {

   @SerializedName("data")
   @Expose
   private List<Dtabase_Notification_modle> data;
   @SerializedName("msg")
   @Expose
   private sucess msg;


   public List<Dtabase_Notification_modle> getData() {
      return data;
   }

   public void setData(List<Dtabase_Notification_modle> data) {
      this.data = data;
   }

   public sucess getMsg() {
      return msg;
   }

   public void setMsg(sucess msg) {
      this.msg = msg;
   }

   public class sucess {
      @SerializedName("success")
      @Expose
      private  String success;

      public String getSuccess() {
         return success;
      }

      public void setSuccess(String success) {
         this.success = success;
      }
   }
}
