package com.tutionize.androidstudionavigationdrawer.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by android on 11/20/17.
 */

public class GetVideo_model {
    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("thumb")
    @Expose
    private List<GetVideo_Thumb_modle> thumb = null;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<GetVideo_Thumb_modle> getThumb() {
        return thumb;
    }

    public void setThumb(List<GetVideo_Thumb_modle> thumb) {
        this.thumb = thumb;
    }
}
