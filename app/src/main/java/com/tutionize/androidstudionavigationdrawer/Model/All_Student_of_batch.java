package com.tutionize.androidstudionavigationdrawer.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by android on 12/5/17.
 */

public class All_Student_of_batch {



    @SerializedName("success")
    @Expose
    private String success;

    @SerializedName("result")
    @Expose
    private List<All_Student_of_batch_result> result;
    public String getSuccess() {
        return success;
    }
    public void setSuccess(String success) {
        this.success = success;
    }
    public List<All_Student_of_batch_result> getResult() {
        return result;
    }
    public void setResult(List<All_Student_of_batch_result> result) {
        this.result = result;
    }
}
