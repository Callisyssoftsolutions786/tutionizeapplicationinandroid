package com.tutionize.androidstudionavigationdrawer.Lacture;

import android.content.Context;
import android.support.v4.util.Pair;
import android.widget.Toast;

import com.twilio.video.Camera2Capturer;
import com.twilio.video.CameraCapturer;
import com.twilio.video.ScreenCapturer;
import com.twilio.video.VideoCapturer;

import org.webrtc.Camera2Enumerator;


/**
 * Created by Vishal Sandhu on 11/3/17.
 */

public class CameraCapture {


    private CameraCapturer   camera1Capture;
    private Camera2Capturer camera2Capturer;

    private Pair<CameraCapturer.CameraSource,String>frontCameraPair;
    private Pair<CameraCapturer.CameraSource,String>backCameraPair;
    private Context context;

    private final Camera2Capturer.Listener camera2Listner = new Camera2Capturer.Listener() {
        @Override
        public void onFirstFrameAvailable() {

        }

        @Override
        public void onCameraSwitched(String newCameraId) {
        }

        @Override
        public void onError(Camera2Capturer.Exception camera2CapturerException) {
            Toast.makeText(context, ""+camera2CapturerException.getMessage(), Toast.LENGTH_SHORT).show();
        }
    };

     private final ScreenCapturer.Listener screencapture =  new ScreenCapturer.Listener() {
        @Override
        public void onScreenCaptureError(String errorDescription) {

        }

        @Override
        public void onFirstFrameAvailable() {

        }
    };

    public CameraCapture(Context context,CameraCapturer.CameraSource source){
        this.context=context;

             if (Camera2Capturer.isSupported(context))
             {
                 setCameraPairs(context);
                 camera2Capturer=new Camera2Capturer(context,getCameraId(source),camera2Listner);
             }
             else {
                 camera1Capture= new CameraCapturer(context,source);
             }
    }

    public CameraCapturer.CameraSource getCameraSource(){

        if (usingCamera1()){
            return  camera1Capture.getCameraSource();
        }else {
            return getCameraSource(camera2Capturer.getCameraId());
        }
    }

   public void switchCamera(){
        if (usingCamera1()){
            camera1Capture.switchCamera();
        }else {
            CameraCapturer.CameraSource cameraSource = getCameraSource(camera2Capturer.getCameraId());
            if (cameraSource == CameraCapturer.CameraSource.FRONT_CAMERA){
                assert backCameraPair.second != null;
                camera2Capturer.switchCamera(backCameraPair.second);
            }else {
                assert frontCameraPair.second != null;
                camera2Capturer.switchCamera(frontCameraPair.second);
            }
        }
   }

  public VideoCapturer getVideoCapturer(){
       if (usingCamera1()){
           return camera1Capture;
       }else {
           return camera2Capturer;
       }
}

    //____________________________________  hellper Method  _____________________


    private CameraCapturer.CameraSource getCameraSource(String cameraId) {

        assert frontCameraPair.second != null;
        if (frontCameraPair.second.equals(cameraId)){
            return frontCameraPair.first;
        }else
            {
            return backCameraPair.first;
        }
    }


    private boolean usingCamera1() {
        return camera1Capture !=null;
    }


    private String getCameraId(CameraCapturer.CameraSource source) {

        if (frontCameraPair.first==source){
            return frontCameraPair.second;
        }else {
            return backCameraPair.second;
        }
    }
    private void setCameraPairs(Context context) {
        Camera2Enumerator camera2Enumerator = new Camera2Enumerator(context);
        for (String CameraId : camera2Enumerator.getDeviceNames()){
            if (camera2Enumerator.isFrontFacing(CameraId)){
                frontCameraPair=new Pair<>(CameraCapturer.CameraSource.FRONT_CAMERA,CameraId);
            }
            if (camera2Enumerator.isBackFacing(CameraId)){
                backCameraPair=new Pair<>(CameraCapturer.CameraSource.BACK_CAMERA,CameraId);
            }
        }
    }
}
