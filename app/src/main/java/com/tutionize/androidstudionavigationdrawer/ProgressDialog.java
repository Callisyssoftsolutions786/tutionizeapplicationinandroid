package com.tutionize.androidstudionavigationdrawer;

import android.app.Activity;

import com.eminayar.panter.PanterDialog;
import com.kaopiz.kprogresshud.KProgressHUD;

public class ProgressDialog {
    private static ProgressDialog progressDialog;


   public static ProgressDialog getInstance(){

       if (progressDialog==null){
           progressDialog =  new ProgressDialog();
       }

       return progressDialog;
   }

   public  KProgressHUD getKNProgressDialog(Activity activity)
   {
       new KProgressHUD(activity);
       return KProgressHUD.create(activity)
               .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
               .setBackgroundColor(activity.getResources().getColor(R.color.Toolbarcolor))
               .setCancellable(false)
               .setAnimationSpeed(2)
               .setDimAmount(0.5f);

   }


   public KProgressHUD getKNVideoProgressDialog(Activity activity){
       new KProgressHUD(activity);
       return KProgressHUD.create(activity)
               .setStyle(KProgressHUD.Style.ANNULAR_DETERMINATE)
               .setBackgroundColor(activity.getResources().getColor(R.color.Toolbarcolor))
               .setLabel("Please wait")
               .setCancellable(true)
               .setAnimationSpeed(2)
               .setDimAmount(0.5f)
               .setMaxProgress(100)
               .show();
   }

    public void  ShowMessagetoUser(String message,Activity activity) {
        PanterDialog dialog = new PanterDialog(activity);
        dialog.setHeaderBackground(R.drawable.pattern_bg_orange)
                .setHeaderLogo(R.drawable.logo)
                .setTitle("Forgot Password")
                .setPositive("I GOT IT", v -> {
                    dialog.dismiss();
                })
                .setMessage(message)
                .isCancelable(false);

        dialog.show();

    }



}
