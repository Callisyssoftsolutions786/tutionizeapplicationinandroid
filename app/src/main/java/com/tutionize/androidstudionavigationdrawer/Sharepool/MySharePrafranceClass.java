package com.tutionize.androidstudionavigationdrawer.Sharepool;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Ravi on 8/3/2017.
 */

public class MySharePrafranceClass {

    private static final String MyPREFERENCES = "MyPrefs";
    private static final String id = "key";
    private static final String name="name";
    private static final String userimage="image";
    private static final String[] list = new String[6];
    private static final String MyLoginPREFERENCES = "MyloginPrefs";
    private static final String Email = "Emailkey";
    private static final String Password="passwordkey";
    private static final String Type="type";
    private static final String Country = "Country";
    private static final String City="city";
    private static final String teacherDetail ="teacherDetail";
    private static final String[] loginlist = new String[6];




    public static void ShareTeacherDetailEdit( Activity activity, String Detail) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(MyPREFERENCES,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(teacherDetail,Detail);

        editor.apply();
    }

    public static void Share( Activity activity,String userid,String username,String image) {
       SharedPreferences sharedPreferences = activity.getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(id,userid);
        editor.putString(name,username);
        editor.putString(userimage,image);
        editor.apply();
    }
    public static void LoginDataShare(Context activity, String loginemail, String loginpassword,String type,String country,String city) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(MyLoginPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Email,loginemail);
        editor.putString(Password,loginpassword);
        editor.putString(Type,type);
        editor.putString(Country,country);
        editor.putString(City,city);
        editor.apply();
    }

    public static void ClearEditTeacherDetail(Activity activity) {
        SharedPreferences sharedPreferences = activity.getPreferences(Context.MODE_PRIVATE);
        ShareTeacherDetailEdit(activity,null);
        sharedPreferences.edit().clear().apply();
    }

    public static void ClearAll(Activity activity) {
       SharedPreferences sharedPreferences = activity.getPreferences(Context.MODE_PRIVATE);
        Share(activity,null,null,null);
        sharedPreferences.edit().clear().apply();
    }
    public static void ClearAllLoginData(Activity activity) {
        SharedPreferences sharedPreferences = activity.getPreferences(Context.MODE_PRIVATE);
        LoginDataShare(activity,null,null,null,null,null);
        sharedPreferences.edit().clear().apply();
    }

    public static String GetTeacherDetailEdit(Activity activity)
    {
        SharedPreferences sharedPreferences=activity.getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
        String value = (sharedPreferences.getString(teacherDetail, null));
        return value;
    }


    public static String[] GetSharePrefrance(Context activity)
    {
        try {
            SharedPreferences sharedPreferences = activity.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            list[0] = (sharedPreferences.getString(id, null));
            list[1] = (sharedPreferences.getString(name, null));
            list[2] = (sharedPreferences.getString(userimage, null));
            }catch (Exception ignored){}
        return list;
    }


    public static String[] LoginDataGetSharePrefrance(Context activity)
    {
        SharedPreferences sharedPreferences=activity.getSharedPreferences(MyLoginPREFERENCES,Context.MODE_PRIVATE);
        loginlist[0]=(sharedPreferences.getString(Email,null));
        loginlist[1]=(sharedPreferences.getString(Password,null));
        loginlist[2]=(sharedPreferences.getString(Type,null));
        loginlist[3]=(sharedPreferences.getString(Country,null));
        loginlist[4]=(sharedPreferences.getString(City,null));
        return loginlist;
    }
}