package com.tutionize.androidstudionavigationdrawer.Sharepool;

import android.app.Activity;

import com.tutionize.androidstudionavigationdrawer.Constant.AppConst;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

public class Analytic_and_Tracker {

    private static GoogleAnalytics sAnalytics;
    private static Tracker sTracker;

    public static void StartAnalyisation(Activity activity) {
        sAnalytics = GoogleAnalytics.getInstance(activity);
        sAnalytics.setLocalDispatchPeriod(1800);
        sTracker = sAnalytics.newTracker(AppConst.analiyic_id);
        sTracker.enableExceptionReporting(true);
        sTracker.enableAdvertisingIdCollection(true);
        sTracker.enableAutoActivityTracking(true);
    }
}
