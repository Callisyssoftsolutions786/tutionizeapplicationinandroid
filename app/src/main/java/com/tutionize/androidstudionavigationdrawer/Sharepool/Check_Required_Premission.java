package com.tutionize.androidstudionavigationdrawer.Sharepool;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.tutionize.androidstudionavigationdrawer.Fragment.Live_Video_Screen;

public class Check_Required_Premission {
    private Activity activity;
    private static final int STORAGE_PERMISSION_REQUEST_CODE = 1;
    private static final int PHONE_STATE_PERMISSION_REQUEST_CODE = 2;
    public static void CheckPermission(Activity activity) {

        boolean need = false;

        if (!checkPermissionForCameraAndMicrophone(activity)) {
            need = requestPermissionForCameraAndMicrophone(activity);

        }

        if (!checkPremitionForStorageReadWrite(activity)) {
            need = requestPermissionForStorageReadWrite(activity);

        }
        if (!checkPremitionForReadPhoneState(activity)){
          need   =   requestPermissionForReadPhoneState(activity);
        }


        PermississionNeed(need ,activity);
    }


//===========================  Check Permision method -------------------------

    private static boolean checkPermissionForCameraAndMicrophone(Activity activity)
    {
        int resultCamera = ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA);
        int resultMic = ContextCompat.checkSelfPermission(activity, Manifest.permission.RECORD_AUDIO);
        int ReadPhoneState =ContextCompat.checkSelfPermission(activity,Manifest.permission.READ_PHONE_STATE);
        int resultStorageRead=ContextCompat.checkSelfPermission(activity,Manifest.permission.READ_EXTERNAL_STORAGE);
        int resultStorageWrite=ContextCompat.checkSelfPermission(activity,Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return  ReadPhoneState== PackageManager.PERMISSION_GRANTED &&
                resultStorageRead == PackageManager.PERMISSION_GRANTED &&
                resultStorageWrite == PackageManager.PERMISSION_GRANTED &&
                resultCamera == PackageManager.PERMISSION_GRANTED &&
                resultMic == PackageManager.PERMISSION_GRANTED;
    }
    private  static boolean checkPremitionForReadPhoneState(Activity activity){
        int ReadPhoneState =ContextCompat.checkSelfPermission(activity,Manifest.permission.READ_PHONE_STATE);
        return ReadPhoneState==PackageManager.PERMISSION_GRANTED;}

    private static boolean checkPremitionForStorageReadWrite(Activity activity){
        int resultStorageRead=ContextCompat.checkSelfPermission(activity,Manifest.permission.READ_EXTERNAL_STORAGE);
        int resultStorageWrite=ContextCompat.checkSelfPermission(activity,Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return resultStorageRead == PackageManager.PERMISSION_GRANTED &&
                resultStorageWrite == PackageManager.PERMISSION_GRANTED;}


    private static boolean requestPermissionForCameraAndMicrophone(Activity activity) {
        boolean need = false;
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CAMERA)
                ||
                ActivityCompat.shouldShowRequestPermissionRationale(activity,Manifest.permission.RECORD_AUDIO)
                ||
                ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.READ_PHONE_STATE)
                ||
                ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                ||
                ActivityCompat.shouldShowRequestPermissionRationale(activity,Manifest.permission.READ_EXTERNAL_STORAGE))

        {
            need = true;

        } else {
            ActivityCompat.requestPermissions(activity,
                    new String[]{
                    Manifest.permission.CAMERA,
                            Manifest.permission.RECORD_AUDIO,Manifest.permission.READ_PHONE_STATE,
                            Manifest.permission.READ_EXTERNAL_STORAGE
                            ,Manifest.permission.WRITE_EXTERNAL_STORAGE},Live_Video_Screen.CAMERA_MIC_PERMISSION_REQUEST_CODE);
        }

        return need;
    }


    private static boolean requestPermissionForReadPhoneState(Activity activity)
    {
        boolean need = false;
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.READ_PHONE_STATE)){
            need = true;
        }
        else {
            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.READ_PHONE_STATE},PHONE_STATE_PERMISSION_REQUEST_CODE);
        }
        return need;
    }
    private static boolean requestPermissionForStorageReadWrite(Activity activity) {
        boolean need = false;
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                ||
                ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        Manifest.permission.READ_EXTERNAL_STORAGE))
        {
            need =  true;


        } else {
            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},

                    STORAGE_PERMISSION_REQUEST_CODE);
        }
        return need ;
    }

    //==========================  Individual Permission for Speek to text =========

    public static void CheckCameraPermission(Activity activity){

        if (Build.VERSION.SDK_INT>Build.VERSION_CODES.M){

            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA)!= PackageManager.PERMISSION_GRANTED){
                Toast.makeText(activity, "Allow camere permission first from App info", Toast.LENGTH_SHORT).show();
                Intent intent  =  new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" +activity.getPackageName()));
                activity.startActivity(intent);
            }
        }

    }

    public static void checkStorageReadPermition(Activity activity)
    {

        if (Build.VERSION.SDK_INT>Build.VERSION_CODES.M){

            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){
                Toast.makeText(activity, "Allow Storage permission first from App info", Toast.LENGTH_SHORT).show();
                Intent intent  =  new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" +activity.getPackageName()));
                activity.startActivity(intent);
            }
        }
    }
    public static void checkStorageWritePermition(Activity activity)
    {
        if (Build.VERSION.SDK_INT>Build.VERSION_CODES.M){
            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){
                Toast.makeText(activity, "Allow Storage permission first from App info", Toast.LENGTH_SHORT).show();
                Intent intent  =  new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" +activity.getPackageName()));
                activity.startActivity(intent);
            }
        }

    }




    private  static void PermississionNeed(boolean need,Activity activity){
        if (need)
        {
            Toast.makeText(activity, "Allow permission first from App info", Toast.LENGTH_SHORT).show();
            Intent intent  =  new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" +activity.getPackageName()));
            activity.startActivity(intent);
        }

    }

}
