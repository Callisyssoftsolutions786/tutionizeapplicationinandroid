package com.tutionize.androidstudionavigationdrawer.Sharepool;

import java.util.ArrayList;
import java.util.List;

public class Spinner_data {

    public static List<String> getTimeZone()
    {
        List<String> TimeZonelist= new ArrayList<>();
        TimeZonelist.add("Select TimeZone");
        TimeZonelist.add("Australian Central Daylight Savings Time");
        TimeZonelist.add("Australian Central Standard Time");
        TimeZonelist.add("Acre Time");
        TimeZonelist.add("ASEAN Common Time");
        TimeZonelist.add("Australian Central Western Standard Time (unofficial)");
        TimeZonelist.add("Atlantic Daylight Time");
        TimeZonelist.add("Australian Eastern Daylight Savings Time");
        TimeZonelist.add("Australian Eastern Standard Time");
        TimeZonelist.add("Afghanistan Time");
        TimeZonelist.add("Alaska Daylight Time");
        TimeZonelist.add("Alaska Standard Time");
        TimeZonelist.add("Amazon Summer Time (Brazil)[1]");
        TimeZonelist.add("Amazon Time (Brazil)[2]");
        TimeZonelist.add("Armenia Time");
        TimeZonelist.add("Argentina Time");
        TimeZonelist.add("Arabia Standard Time");
        TimeZonelist.add("Atlantic Standard Time");
        TimeZonelist.add("Australian Western Standard Time");
        TimeZonelist.add("Azores Summer Time");
        TimeZonelist.add("Azores Standard Time");
        TimeZonelist.add("Azerbaijan Time");
        TimeZonelist.add("Brunei Time");
        TimeZonelist.add("British Indian Ocean Time");
        TimeZonelist.add("Baker Island Time");
        TimeZonelist.add("Bolivia Time");
        TimeZonelist.add("Brasília Summer Time");
        TimeZonelist.add("Brasilia Time");
        TimeZonelist.add("Bangladesh Standard Time");
        TimeZonelist.add("Bougainville Standard Time[3]");
        TimeZonelist.add("British Summer Time (British Standard Time from Feb 1968 to Oct 1971)");
        TimeZonelist.add("Bhutan Time");
        TimeZonelist.add("Central Africa Time");
        TimeZonelist.add("Cocos Islands Time");
        TimeZonelist.add("Central Daylight Time (North America)");
        TimeZonelist.add("Cuba Daylight Time[4]");
        TimeZonelist.add("Central European Summer Time (Cf. HAEC)");
        TimeZonelist.add("Central European Time");
        TimeZonelist.add("Chatham Daylight Time");
        TimeZonelist.add("Chatham Standard Time");
        TimeZonelist.add("Choibalsan Standard Time");
        TimeZonelist.add("Choibalsan Summer Time");
        TimeZonelist.add("Chamorro Standard Time");
        TimeZonelist.add("Chuuk Time");
        TimeZonelist.add("Clipperton Island Standard Time");
        TimeZonelist.add("Central Indonesia Time");
        TimeZonelist.add("Cook Island Time");
        TimeZonelist.add("Chile Summer Time");
        TimeZonelist.add("Chile Standard Time");
        TimeZonelist.add("Colombia Summer Time");
        TimeZonelist.add("Colombia Time");
        TimeZonelist.add("Central Standard Time (North America)");
        TimeZonelist.add("China Standard Time");
        TimeZonelist.add("Cuba Standard Time");
        TimeZonelist.add("China Time");
        TimeZonelist.add("Cape Verde Time");
        TimeZonelist.add("Central Western Standard Time (Australia) unofficial");
        TimeZonelist.add("Christmas Island Time");
        TimeZonelist.add("Davis Time");
        TimeZonelist.add("Dumont d'Urville Time");
        TimeZonelist.add("AIX-specific equivalent of Central European Time[NB 1]");
        TimeZonelist.add("Easter Island Summer Time");
        TimeZonelist.add("Easter Island Standard Time");
        TimeZonelist.add("East Africa Time");
        TimeZonelist.add("Eastern Caribbean Time (does not recognise DST)");
        TimeZonelist.add("Ecuador Time");
        TimeZonelist.add("Eastern Daylight Time (North America)");
        TimeZonelist.add("Eastern European Summer Time");
        TimeZonelist.add("Eastern European Time");
        TimeZonelist.add("Eastern Greenland Summer Time");
        TimeZonelist.add("Eastern Greenland Time");
        TimeZonelist.add("Eastern Indonesian Time");
        TimeZonelist.add("Eastern Standard Time (North America)");
        TimeZonelist.add("Further-eastern European Time");
        TimeZonelist.add("Fiji Time");
        TimeZonelist.add("Falkland Islands Summer Time");
        TimeZonelist.add("Falkland Islands Time");
        TimeZonelist.add("Fernando de Noronha Time");
        TimeZonelist.add("Galápagos Time");
        TimeZonelist.add("Gambier Islands Time");
        TimeZonelist.add("Georgia Standard Time");
        TimeZonelist.add("French Guiana Time");
        TimeZonelist.add("Gilbert Island Time");
        TimeZonelist.add("Gambier Island Time");
        TimeZonelist.add("Greenwich Mean Time");
        TimeZonelist.add("South Georgia and the South Sandwich Islands Time");
        TimeZonelist.add("Gulf Standard Time");
        TimeZonelist.add("Guyana Time");
        TimeZonelist.add("Hawaii–Aleutian Daylight Time");
        TimeZonelist.add("Heure Avancée d'Europe Centrale French-language name for CEST");
        TimeZonelist.add("Hawaii–Aleutian Standard Time");
        TimeZonelist.add("Hong Kong Time");
        TimeZonelist.add("Heard and McDonald Islands Time");
        TimeZonelist.add("Khovd Summer Time");
        TimeZonelist.add("Khovd Standard Time");
        TimeZonelist.add("Indochina Time");
        TimeZonelist.add("International Day Line West time zone");
        TimeZonelist.add("Israel Daylight Time");
        TimeZonelist.add("Indian Ocean Time");
        TimeZonelist.add("Iran Daylight Time");
        TimeZonelist.add("Irkutsk Time");
        TimeZonelist.add("Iran Standard Time");
        TimeZonelist.add("Indian Standard Time");
        TimeZonelist.add("Irish Standard Time[5]");
        TimeZonelist.add("Israel Standard Time");
        TimeZonelist.add("Israel Standard Time");
        TimeZonelist.add("Japan Standard Time");
        TimeZonelist.add("Kaliningrad Time");
        TimeZonelist.add("Kyrgyzstan Time");
        TimeZonelist.add("Kosrae Time");
        TimeZonelist.add("Krasnoyarsk Time");
        TimeZonelist.add("Korea Standard Time");
        TimeZonelist.add("Lord Howe Standard Time");
        TimeZonelist.add("Lord Howe Summer Time");
        TimeZonelist.add("Line Islands Time");
        TimeZonelist.add("Magadan Time");
        TimeZonelist.add("Marquesas Islands Time");
        TimeZonelist.add("Mawson Station Time");
        TimeZonelist.add("Mountain Daylight Time (North America)");
        TimeZonelist.add("Middle European Time Same zone as CET");
        TimeZonelist.add("Middle European Summer Time Same zone as CEST");
        TimeZonelist.add("Marshall Islands Time");
        TimeZonelist.add("Macquarie Island Station Time");
        TimeZonelist.add("Marquesas Islands Time");
        TimeZonelist.add("Myanmar Standard Time");
        TimeZonelist.add("Moscow Time");
        TimeZonelist.add("Malaysia Standard Time");
        TimeZonelist.add("Mountain Standard Time (North America)");
        TimeZonelist.add("Mauritius Time");
        TimeZonelist.add("Maldives Time");
        TimeZonelist.add("Malaysia Time");
        TimeZonelist.add("New Caledonia Time");
        TimeZonelist.add("Newfoundland Daylight Time");
        TimeZonelist.add("Norfolk Island Time");
        TimeZonelist.add("Nepal Time");
        TimeZonelist.add("Newfoundland Standard Time");
        TimeZonelist.add("Newfoundland Time");
        TimeZonelist.add("Niue Time");
        TimeZonelist.add("New Zealand Daylight Time");
        TimeZonelist.add("New Zealand Standard Time");
        TimeZonelist.add("Omsk Time");
        TimeZonelist.add("Oral Time");
        TimeZonelist.add("Pacific Daylight Time (North America)");
        TimeZonelist.add("Peru Time");
        TimeZonelist.add("Kamchatka Tim");
        TimeZonelist.add("Papua New Guinea Time");
        TimeZonelist.add("Phoenix Island Time");
        TimeZonelist.add("Philippine Time");
        TimeZonelist.add("Pakistan Standard Time");
        TimeZonelist.add("Saint Pierre and Miquelon Daylight Time");
        TimeZonelist.add("Saint Pierre and Miquelon Standard Time");
        TimeZonelist.add("Pohnpei Standard Time");
        TimeZonelist.add("Pacific Standard Time (North America)");
        TimeZonelist.add("Philippine Standard Time");
        TimeZonelist.add("Paraguay Summer Time[6]");
        TimeZonelist.add("Paraguay Time[7]");
        TimeZonelist.add("Réunion Time");
        TimeZonelist.add("Rothera Research Station Time");
        TimeZonelist.add("Sakhalin Island Time");
        TimeZonelist.add("Samara Time");
        TimeZonelist.add("South African Standard Time");
        TimeZonelist.add("Solomon Islands Time");
        TimeZonelist.add("Seychelles Time");
        TimeZonelist.add("Samoa Daylight Time");
        TimeZonelist.add("Singapore Time");
        TimeZonelist.add("Sri Lanka Standard Time");
        TimeZonelist.add("Srednekolymsk Time");
        TimeZonelist.add("Suriname Time");
        TimeZonelist.add("Samoa Standard Time");
        TimeZonelist.add("Singapore Standard Time");
        TimeZonelist.add("Showa Station Time");
        TimeZonelist.add("Tahiti Time");
        TimeZonelist.add("Thailand Standard Time");
        TimeZonelist.add("Indian/Kerguelen");
        TimeZonelist.add("Tajikistan Time");
        TimeZonelist.add("Tokelau Time");
        TimeZonelist.add("Timor Leste Time");
        TimeZonelist.add("Turkmenistan Time");
        TimeZonelist.add("Turkey Time");
        TimeZonelist.add("Tonga Time");
        TimeZonelist.add("Tuvalu Time");
        TimeZonelist.add("Ulaanbaatar Summer Time");
        TimeZonelist.add("Ulaanbaatar Standard Time");
        TimeZonelist.add("Coordinated Universal Time");
        TimeZonelist.add("Uruguay Summer Time");
        TimeZonelist.add("Uruguay Standard Time");
        TimeZonelist.add("Uzbekistan Time");
        TimeZonelist.add("Venezuelan Standard Time");
        TimeZonelist.add("Vladivostok Time");
        TimeZonelist.add("Volgograd Time");
        TimeZonelist.add("Vostok Station Time");
        TimeZonelist.add("Vanuatu Time");
        TimeZonelist.add("Wake Island Time");
        TimeZonelist.add("West Africa Summer Time");
        TimeZonelist.add("West Africa Time");
        TimeZonelist.add("Western European Summer Time");
        TimeZonelist.add("Western European Time");
        TimeZonelist.add("Western Indonesian Time");
        TimeZonelist.add("Western Standard Time");
        TimeZonelist.add("Yakutsk Time");
        TimeZonelist.add("Yekaterinburg Time");
        return TimeZonelist;
    }


    public static List<String> getQualification() {

        List<String> qualifictionlist = new ArrayList<>();

        qualifictionlist.add("Select Qualification");
        qualifictionlist.add("Prof.");
        qualifictionlist.add("Dr.");
        qualifictionlist.add("Master");
        qualifictionlist.add("Graduate");
        qualifictionlist.add("12 Grade");
        qualifictionlist.add("Hafiz");
        qualifictionlist.add("Mufti");
        qualifictionlist.add("Maulana");
        qualifictionlist.add("Shiekh.");
        qualifictionlist.add("MD");
        qualifictionlist.add("Teacher");
        qualifictionlist.add("Head-Teacher");

        return qualifictionlist;
    }

    public static List<String> getGenderList()
    {
        List<String> genderlist= new ArrayList<>();
        genderlist.add("Select Gender");
        genderlist.add("Male");
        genderlist.add("Female");
        return genderlist;
    }
}
