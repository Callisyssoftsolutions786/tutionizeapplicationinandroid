package com.tutionize.androidstudionavigationdrawer.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.rey.material.widget.RelativeLayout;
import com.rey.material.widget.TextView;
import com.tutionize.androidstudionavigationdrawer.Adapter.Adapter_All_Hire_Student;
import com.tutionize.androidstudionavigationdrawer.Adapter.Adapter_Batch_Student;
import com.tutionize.androidstudionavigationdrawer.Adapter.Adapter_NewBatchSelectedStudent;
import com.tutionize.androidstudionavigationdrawer.Adapter.Adapter_NewBatchStudent;
import com.tutionize.androidstudionavigationdrawer.Adapter.adapter_NewBatchUnSeletedStudent;
import com.tutionize.androidstudionavigationdrawer.Constant.Singleton;
import com.tutionize.androidstudionavigationdrawer.Model.All_Student_of_batch;
import com.tutionize.androidstudionavigationdrawer.Model.All_Student_of_batch_result;
import com.tutionize.androidstudionavigationdrawer.Model.Get_UnSelectedStudent_modle;
import com.tutionize.androidstudionavigationdrawer.Model.Get_UnSelectedSudentResult;
import com.tutionize.androidstudionavigationdrawer.Model.HireStudentList_Result;
import com.tutionize.androidstudionavigationdrawer.Model.HireStudentList_modle;
import com.tutionize.androidstudionavigationdrawer.Model.SeletedStudent;
import com.tutionize.androidstudionavigationdrawer.Model.UnseletedStudent;
import com.tutionize.androidstudionavigationdrawer.ProgressDialog;
import com.tutionize.androidstudionavigationdrawer.R;
import com.tutionize.androidstudionavigationdrawer.Sharepool.MySharePrafranceClass;
import com.tutionize.androidstudionavigationdrawer.Sharepool.NetworkStateCheck;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by android on 12/4/17.
 */

public class Fragment_Student_List extends Fragment  {
    private String[] list;
    private TextView list_item;
    private List<Get_UnSelectedSudentResult>StudentList=new ArrayList<>();
    private List<HireStudentList_Result>hiredStudentList=new ArrayList<>();
    private List<UnseletedStudent>collectionhiredStudentList;
    private LinearLayoutManager layoutManager;
    private RecyclerView StudentRecycle;
    private RelativeLayout acback_arrow_about;
    private Adapter_All_Hire_Student mTeacher_list;
    private Adapter_NewBatchStudent NewBatchStudent;
    private String batchid;
    private String typeoflist;
    private ArrayList<SeletedStudent> SelectedStudent;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

         final View view = inflater.inflate(R.layout.fragment_student_list, container, false);
         list= MySharePrafranceClass.GetSharePrefrance(getActivity());

        Bundle mBundle = getArguments();

        if (!(mBundle == null)) {


            batchid = mBundle.getString("batchid");
            typeoflist = mBundle.getString("typeoflist");
            String teacherid = mBundle.getString("teacherid");
        }

            SelectedStudent = Selectbatch_fragment.getSelectedStudentId();
            collectionhiredStudentList=Selectbatch_fragment.getUnselectedStudentId();

        FindView(view);
        Clickable();
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener((v, keyCode, event) -> {
            if( keyCode == KeyEvent.KEYCODE_BACK )
            {
                assert getFragmentManager() != null;
                getFragmentManager().popBackStackImmediate();
                return true;
            }
            return false;
        });

        list_item.setText(typeoflist);
        return view;
    }

    private  void Clickable(){
        acback_arrow_about.setOnClickListener(v-> Objects.requireNonNull(getActivity()).onBackPressed());
    }
    private void FindView(View view) {

                list_item = view.findViewById(R.id.list_item);
                StudentRecycle= view.findViewById(R.id.Student_list_recycle);
                 acback_arrow_about= view.findViewById(R.id.acback_arrow_about);

        if (typeoflist != null) {
            if (typeoflist.equals("Remove List")) {
                if (batchid != null) {
                    AllBatchStudent(list[0],batchid);
                } else {
                    if (!SelectedStudent.isEmpty()){
                        selectedBectchStudentAdapter();
                    }
                }
            } else {
                if (batchid != null) {
                    UnAddBatchStudent(batchid,list[0]);
                } else {
                    if (SelectedStudent==null||SelectedStudent.isEmpty()){
                        HireStudent(list[0]);

                    }else {

                        SetnewUnseletedStudentAdapter();

                    }
                }
            }
        }
    }

    @SuppressLint("ResourceAsColor")
    private void UnAddBatchStudent(String batchid , String teacherid)
    {
        if (NetworkStateCheck.isNetworkAvaliable(Objects.requireNonNull(getActivity()))) {
            KProgressHUD knprogress = ProgressDialog.getInstance().getKNProgressDialog(getActivity());
            knprogress.show();
            Singleton.getInstance().getApi().UnselectedBatchStudent(batchid, teacherid, new Callback<Get_UnSelectedStudent_modle>() {
                @Override
                public void success(Get_UnSelectedStudent_modle get_unSelectedStudent_modle, Response response) {
                    if (get_unSelectedStudent_modle.getSuccess().equals("1"))
                    {
                        StudentList = get_unSelectedStudent_modle.getResult();
                        if (StudentList != null) {
                            if (StudentList.isEmpty()) {
                                Toast.makeText(getActivity(), "don't have a student ", Toast.LENGTH_SHORT).show();

                                return;
                            } else {
                                for (int i = 0; i < StudentList.size(); i++) {
                                    if (StudentList.get(i) == null) {
                                        StudentList.remove(i);
                                    }
                                }
                            }
                        }
                        StudentRecycle.setVisibility(View.VISIBLE);
                        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                        StudentRecycle.setLayoutManager(layoutManager);
                        mTeacher_list = new Adapter_All_Hire_Student(getActivity(), StudentList);
                        StudentRecycle.setAdapter(mTeacher_list);
                        mTeacher_list.notifyDataSetChanged();
                        knprogress.dismiss();
                    }else {
                        knprogress.dismiss();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    knprogress.dismiss();
                }
            });
        }else {
            Toast.makeText(getContext(), "Internet connection problem", Toast.LENGTH_SHORT).show();
        }
    }
    private void AllBatchStudent(String teacherid, String batchid) {
        if (NetworkStateCheck.isNetworkAvaliable(Objects.requireNonNull(getActivity()))) {

            KProgressHUD knprogress = ProgressDialog.getInstance().getKNProgressDialog(getActivity());
            knprogress.show();
            Singleton.getInstance().getApi().AllBatchStudent(teacherid, batchid, new Callback<All_Student_of_batch>() {
                @Override
                public void success(All_Student_of_batch all_student_of_batch, Response response) {
                    if (all_student_of_batch.getSuccess().equals("1"))
                    {

                        List<All_Student_of_batch_result> StudentofBatch = all_student_of_batch.getResult();

                        StudentRecycle.setVisibility(View.VISIBLE);

                        layoutManager = new LinearLayoutManager(getActivity(),
                                LinearLayoutManager.VERTICAL, false);
                        StudentRecycle.setLayoutManager(layoutManager);

                        Adapter_Batch_Student batchlistAdapter = new Adapter_Batch_Student(getActivity(),StudentofBatch);
                        StudentRecycle.setAdapter(batchlistAdapter);
                        batchlistAdapter.notifyDataSetChanged();

                    }
                    knprogress.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    knprogress.dismiss();
                }
            });
        }else {
            Toast.makeText(getContext(), "Internet connection problem", Toast.LENGTH_SHORT).show();
        }
    }

    private void selectedBectchStudentAdapter(){
        StudentRecycle.setVisibility(View.VISIBLE);
        layoutManager= new LinearLayoutManager(getActivity(),
                LinearLayoutManager.VERTICAL, false);
        StudentRecycle.setLayoutManager(layoutManager);
        Adapter_NewBatchSelectedStudent adapter_NewBatchSelectedStudent = new Adapter_NewBatchSelectedStudent(getActivity(), SelectedStudent);

        StudentRecycle.setAdapter(adapter_NewBatchSelectedStudent);
        adapter_NewBatchSelectedStudent.notifyDataSetChanged();
    }

    @SuppressLint("ResourceAsColor")
    private void HireStudent(String teacherid){
        if (NetworkStateCheck.isNetworkAvaliable(Objects.requireNonNull(getActivity())))
        {
            KProgressHUD knprogress = ProgressDialog.getInstance().getKNProgressDialog(getActivity());
            knprogress.show();

            Singleton.getInstance().getApi().HireStudent(teacherid, new Callback<HireStudentList_modle>() {
                @Override
                public void success(HireStudentList_modle hireStudentList_modle, Response response) {
                    if (hireStudentList_modle.getSuccess().equals("1")) {
                        hiredStudentList = hireStudentList_modle.getResult();
                        if (hiredStudentList == null || hiredStudentList.isEmpty()) {
                            Singleton.getInstance().Progress(getActivity()).dismiss();
                            Toast.makeText(getActivity(), " No data ", Toast.LENGTH_SHORT).show();
                            return;
                        } else {
                            for (int i = 0; i < hiredStudentList.size(); i++) {
                                if (hiredStudentList.get(i) == null) {
                                    hiredStudentList.remove(i);
                                }
                            }
                        }
                        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                        StudentRecycle.setLayoutManager(layoutManager);
                        NewBatchStudent = new Adapter_NewBatchStudent(getActivity(), hiredStudentList);
                        StudentRecycle.setAdapter(NewBatchStudent);
                        NewBatchStudent.notifyDataSetChanged();
                        knprogress.dismiss();

                    } else {
                        knprogress.dismiss();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    knprogress.dismiss();
                }
            });
        }else {
            Toast.makeText(getContext(), "Internet connection problem", Toast.LENGTH_SHORT).show();
        }
    }

    private void SetnewUnseletedStudentAdapter(){
        layoutManager= new LinearLayoutManager(getActivity(),
                LinearLayoutManager.VERTICAL, false);
        StudentRecycle.setLayoutManager(layoutManager);
        adapter_NewBatchUnSeletedStudent adapterNewBatchUnSeletedStudent = new adapter_NewBatchUnSeletedStudent(getActivity(), collectionhiredStudentList);

        StudentRecycle.setAdapter(adapterNewBatchUnSeletedStudent);
        adapterNewBatchUnSeletedStudent.notifyDataSetChanged();
    }
}
