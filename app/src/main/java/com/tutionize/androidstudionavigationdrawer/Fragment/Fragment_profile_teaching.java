package com.tutionize.androidstudionavigationdrawer.Fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.tutionize.androidstudionavigationdrawer.Activity_Screen.Home_screen;
import com.tutionize.androidstudionavigationdrawer.Api_Interface.All_APIs;
import com.tutionize.androidstudionavigationdrawer.Constant.AppConst;
import com.tutionize.androidstudionavigationdrawer.Constant.LiveData;
import com.tutionize.androidstudionavigationdrawer.Constant.Singleton;
import com.tutionize.androidstudionavigationdrawer.Model.Cources_modle;
import com.tutionize.androidstudionavigationdrawer.Model.CourseData;
import com.tutionize.androidstudionavigationdrawer.Model.GetCountryResult;
import com.tutionize.androidstudionavigationdrawer.Model.State_Result_modle;
import com.tutionize.androidstudionavigationdrawer.Model.TeacherUpdate_modle;
import com.tutionize.androidstudionavigationdrawer.ProgressDialog;
import com.tutionize.androidstudionavigationdrawer.R;
import com.tutionize.androidstudionavigationdrawer.Sharepool.MySharePrafranceClass;
import com.tutionize.androidstudionavigationdrawer.Sharepool.NetworkStateCheck;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

import static com.tutionize.androidstudionavigationdrawer.R.layout.support_simple_spinner_dropdown_item;


/*
 *
 * Created by Vishal Sandhu on 01-03-2017.
 */
public class Fragment_profile_teaching extends Fragment {
    View rootView;
    private Spinner tp_qualifiction,tp_Course,tp_gender;
    private  AutoCompleteTextView tp_city , tp_country;
    private  List<CourseData> courcedata = new ArrayList<>();
    private List<String> qualifictionlist = new ArrayList<>();
    private List<String> genderlist = new ArrayList<>();
    private List<GetCountryResult> Countrylist = new ArrayList<>();
    private List<State_Result_modle> Citylist= new ArrayList<>();
    private List<String> CountryNamelist = new ArrayList<>();
    private List<String> CityNamelist= new ArrayList<>();
    private List<String> courselist= new ArrayList<>();
   // private ;
   // private ;
    private ArrayAdapter<String> spinnerCourseAdapter;
    private Button Submit_teacherProfile;
    private EditText tp_name,tp_Bio,tp_course_price;
    private String[] list;
    private TypedFile tp_file;
    @SuppressLint("StaticFieldLeak")
    private static Activity activity;
    private String  Teachername,Teacherid,Teachercourase,Teacheramount,Teachercity,Teacherqualification,Teachergender,TeacherBioData,TeacherCountry;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_profile_teaching, container, false);
        activity = getActivity();
        Cource();
         FindView(rootView);
         GetEditData();
         ShareElement();
         AddElement();
         setAdapterlist();
         setData();
         Clickable();
         AapterItemSelect();
         return rootView;
    }

    private void setData() {
        tp_name.setText(Teachername);
        tp_Bio.setText(TeacherBioData);
        tp_gender.setSelection(genderlist.indexOf(Teachergender));
        tp_qualifiction.setSelection(qualifictionlist.indexOf(Teacherqualification));
        tp_city.setText(Teachercity);
        tp_country.setText(TeacherCountry);
        tp_Bio.setText(TeacherBioData);
        tp_course_price.setText(Teacheramount);
    }

    private void GetEditData() {
        Bundle bundle = this.getArguments();
        if (bundle != null)
        {
              Teachername= bundle.getString("teachername");
              Teacherid= bundle.getString("teacherid");
              Teachercourase= bundle.getString("teachercourse");
              Teacheramount= bundle.getString("teacheramount");
              Teachercity= bundle.getString("teachercity");
              Teacherqualification= bundle.getString("teacherqualification");
              Teachergender= bundle.getString("teachergender");
              TeacherBioData=bundle.getString("techerbio");
              TeacherCountry=bundle.getString("techercountry");
        }
    }

    private void AapterItemSelect() {

        tp_country.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }


    private void FindView(View rootView) {

        tp_name= rootView.findViewById(R.id.tp_edt_name);
        tp_course_price= rootView.findViewById(R.id.tp_course_price);
        tp_gender= rootView.findViewById(R.id.tp_edt_gender);
        tp_Course= rootView.findViewById(R.id.tp_edt_Course);

        tp_Bio= rootView.findViewById(R.id.tp_edt_bio);
        tp_city= rootView.findViewById(R.id.tp_edt_City);
        tp_country= rootView.findViewById(R.id.tp_edt_contry);
        tp_qualifiction= rootView.findViewById(R.id.tp_edt_qualification);

        Submit_teacherProfile= rootView.findViewById(R.id.tp_btn_submit_teacher_profile);
       // Submit_teacherProfile.setVisibility(View.GONE);

    }

    private void Clickable() {
        Submit_teacherProfile.setOnClickListener(view -> {

                String userid=list[0];
                String ed_image = list[2];
                String ed_Username=tp_name.getText().toString();
                String ed_Gender= tp_gender.getSelectedItem().toString();

                String ed_bio= tp_Bio.getText().toString();
                String ed_city =tp_city.getText().toString();
                String ed_country=tp_country.getText().toString();
                String ed_course=tp_Course.getSelectedItem().toString();
                String ed_qualifiction=tp_qualifiction.getSelectedItem().toString();
                String course_price =tp_course_price.getText().toString();


            if(ed_Username.equals("")){
                Toast.makeText(activity, "Enter name", Toast.LENGTH_SHORT).show();

            }else if(ed_Gender.equals("Gender")){Toast.makeText(activity, "Enter Gender", Toast.LENGTH_SHORT).show();
            }else if (ed_city.equals("Select Course")){Toast.makeText(activity, "Enter City", Toast.LENGTH_SHORT).show();
            }else if (ed_country.equals("Select Country")){Toast.makeText(activity, "Enter Country", Toast.LENGTH_SHORT).show();
            }else if (ed_course.equals("Select Course")){Toast.makeText(activity, "Enter Course", Toast.LENGTH_SHORT).show();
            }else if(ed_qualifiction.equals("Qualifiction")){Toast.makeText(activity, "Enter Qualification", Toast.LENGTH_SHORT).show();
            }else if (ed_bio.equals("")){Toast.makeText(activity, "Enter Biographic", Toast.LENGTH_SHORT).show();
            }else {
                updateTeacherProfile(userid,ed_Username,ed_Gender,tp_file,ed_city,ed_country,ed_qualifiction,ed_bio,ed_course,course_price, ed_image);
            }
        });
    }

    @SuppressLint("ResourceAsColor")
    private void updateTeacherProfile(String userid, String ed_username, String ed_gender, TypedFile file, String ed_city,
                                      String ed_country, String ed_qualifiction, String ed_bio, String ed_course, String course_price, String ed_image) {
        String coursevalidid = "";
        for(int i = 0; i<courcedata.size();i++ ){

            if (courcedata.get(i).getCourse_category().equals(ed_course)){
                coursevalidid = courcedata.get(i).getId();
            }
        }

        if (NetworkStateCheck.isNetworkAvaliable(Objects.requireNonNull(getActivity())))
        {
            KProgressHUD knprogress = ProgressDialog.getInstance().getKNProgressDialog(getActivity());
            knprogress.show();

            RestAdapter rest = new RestAdapter.Builder().setEndpoint(AppConst.constant).build();
            All_APIs api = rest.create(All_APIs.class);
            api.updateTeacherProfile(userid, ed_username, ed_gender, ed_city, ed_country, ed_qualifiction, ed_bio,
                    file, coursevalidid, course_price, new Callback<TeacherUpdate_modle>() {
                        @Override
                        public void success(TeacherUpdate_modle teacherUpdate_modle, Response response) {
                            if (teacherUpdate_modle.getSuccess().equals("1")) {
                                MySharePrafranceClass.ClearAll(Objects.requireNonNull(getActivity()));
                                Toast.makeText(activity, "Profile updated successfully", Toast.LENGTH_SHORT).show();
                                tp_Bio.setText("");
                                tp_name.setText("");
                                tp_qualifiction.setSelection(0);
                                tp_Course.setSelection(0);
                                tp_gender.setSelection(0);
                                Home_screen.home_screen.txt_UserName.setText(ed_username);
                                MySharePrafranceClass.Share(Objects.requireNonNull(getActivity()), userid, ed_username, ed_image);

                                knprogress.dismiss();
                                activity.onBackPressed();
                            } else {
                                knprogress.dismiss();
                                Toast.makeText(activity, "" + teacherUpdate_modle.getResponse(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            knprogress.dismiss();
                            Toast.makeText(activity, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
        }else {
            Toast.makeText(activity, "Internet connection problrem", Toast.LENGTH_SHORT).show();
        }
    }

  //----------------------------------------helper Method--------------------------------------



    private void ShareElement() {
        list= MySharePrafranceClass.GetSharePrefrance(activity);
        String[] loginlist = MySharePrafranceClass.LoginDataGetSharePrefrance(activity);
        tp_name.setText(list[1]);
    }



    private void AddElement() {
        CityNamelist.add("Select Region");

        qualifictionlist.add("Select Qualification");
        qualifictionlist.add("Prof.");
        qualifictionlist.add("Dr.");
        qualifictionlist.add("Master");
        qualifictionlist.add("Graduate");
        qualifictionlist.add("12 grade.");
        qualifictionlist.add("Hafiz");
        qualifictionlist.add("Mufti");
        qualifictionlist.add("Maulana");
        qualifictionlist.add("Shiekh.");
        qualifictionlist.add("MD");
        qualifictionlist.add("Teacher");
        qualifictionlist.add("Head-Teacher");

        genderlist.add("Gender");
        genderlist.add("Male");
        genderlist.add("Female");
    }

    private void setAdapterlist() {

        ArrayAdapter<String> spinnerQualifictionAdapter = new ArrayAdapter<>(activity, support_simple_spinner_dropdown_item, qualifictionlist);
        spinnerQualifictionAdapter.setDropDownViewResource(support_simple_spinner_dropdown_item);
        tp_qualifiction.setAdapter(spinnerQualifictionAdapter);

        ArrayAdapter<String> spinnerGenderAdapter = new ArrayAdapter<>(activity, support_simple_spinner_dropdown_item, genderlist);
        spinnerGenderAdapter.setDropDownViewResource(support_simple_spinner_dropdown_item);
        tp_gender.setAdapter(spinnerGenderAdapter);

        ArrayAdapter<String> spinnerCountryAdapter = new ArrayAdapter<>(activity, support_simple_spinner_dropdown_item,LiveData.getCountry());
        spinnerCountryAdapter.setDropDownViewResource(support_simple_spinner_dropdown_item);
        tp_country.setAdapter(spinnerCountryAdapter);

        ArrayAdapter<String> spinnerCityAdapter = new ArrayAdapter<>(activity, support_simple_spinner_dropdown_item,CityNamelist);
        spinnerCityAdapter.setDropDownViewResource(support_simple_spinner_dropdown_item);
        tp_city.setAdapter(spinnerCityAdapter);

        spinnerCourseAdapter = new ArrayAdapter<>(activity,support_simple_spinner_dropdown_item,courselist);
        spinnerCourseAdapter.setDropDownViewResource(support_simple_spinner_dropdown_item);
        tp_Course.setAdapter(spinnerCourseAdapter);
    }


    private void Cource() {

        if (NetworkStateCheck.isNetworkAvaliable(Objects.requireNonNull(getActivity()))) {

            Singleton.getInstance().getApi().getCourse(new Callback<Cources_modle>() {
                @Override
                public void success(Cources_modle cources_modle, Response response) {
                    courcedata = cources_modle.getData();
                    courselist.add("Select Course");

                    for (CourseData e : courcedata) {
                        courselist.add(e.getCourse_category());
                    }
                    if (spinnerCourseAdapter != null) {
                        spinnerCourseAdapter.notifyDataSetChanged();
                    }

                    if (Teachercourase != null) {
                        for (CourseData e : courcedata) {
                            if (e.getId().equals(Teachercourase)) {
                                String corsename = e.getCourse_category();
                                tp_Course.setSelection(courselist.indexOf(corsename));
                            }
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {

                }
            });
        }else {
            Toast.makeText(activity, "Internet connection problem", Toast.LENGTH_SHORT).show();
        }
}
}
