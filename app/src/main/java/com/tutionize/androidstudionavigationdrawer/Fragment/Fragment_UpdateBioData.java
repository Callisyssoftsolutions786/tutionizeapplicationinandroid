package com.tutionize.androidstudionavigationdrawer.Fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.rey.material.widget.LinearLayout;
import com.tutionize.androidstudionavigationdrawer.Activity_Screen.Home_screen;
import com.tutionize.androidstudionavigationdrawer.BuildConfig;
import com.tutionize.androidstudionavigationdrawer.Constant.AppConst;
import com.tutionize.androidstudionavigationdrawer.Constant.Picture_method;
import com.tutionize.androidstudionavigationdrawer.R;
import com.tutionize.androidstudionavigationdrawer.Sharepool.Check_Required_Premission;
import com.tutionize.androidstudionavigationdrawer.Sharepool.MySharePrafranceClass;

import org.json.JSONArray;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Objects;

import cz.msebera.android.httpclient.Header;
import retrofit.mime.TypedFile;


/**
 * Created by android on 10/18/17.
 */

public class Fragment_UpdateBioData extends Fragment {
    private String StrUrl;
    private AlertDialog alertDialog;
    private String teacherid;
    private String[] list;
    private String txt_name,txt_Discription,txt_bio_edt_prize,txt_title;
    private EditText edit_name;
    private EditText edit_Discription;
    private EditText edit_title;
    private Button btn_UploadVideo;
    private Button btn_Submit;
    @SuppressLint("StaticFieldLeak")
    private static VideoView Upload_VideoView;
    public static File Update_Bio_file_teacherprofile;
    public static final int CAMERA_CODE=1;
    public static final int GALLERY_CODE=0;
    public static final int MEDIA_TYPE_VIDEO = 2;
    @SuppressLint("StaticFieldLeak")
    protected static Activity activity;
    public static Uri Update_Bio_photofile_teacherprofile;
    public static String current;
    private static String SelectedVideoPath = "";
    private TypedFile videofileTypeFile;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.update_biodata, container, false);
        activity = getActivity();
        findView(rootView);
        ShareElement();
        ClickAble();
        return rootView;
    }
    @SuppressLint("SetTextI18n")
    private void ClickAble() {
        btn_Submit.setOnClickListener(view -> {
            txt_Discription = edit_Discription.getText().toString();
            txt_name = edit_name.getText().toString();
            txt_title = edit_title.getText().toString();
            String VideoPath = getSelectedVideoPath();
            teacherid=list[0];
            if (txt_name.length()== 0)
            {
                Toast.makeText(getActivity(), "Enter Name", Toast.LENGTH_SHORT).show();
            }
            else if (txt_title.equals(""))
            {
                Toast.makeText(getActivity(), "Enter Title", Toast.LENGTH_SHORT).show();
            }
            else if (txt_Discription.equals(""))
            {
                Toast.makeText(getActivity(), "Enter Discription", Toast.LENGTH_SHORT).show();
            }
            else if (VideoPath.equals(""))
            {
               Toast.makeText(getActivity(), "Please Select Video ", Toast.LENGTH_SHORT).show();
            }
            else
                {
                  videofileTypeFile = new TypedFile("video/*", new File(VideoPath));
                  StrUrl = AppConst.constant.concat("/upload_video");
                    String videofilePath = getSelectedVideoPath();


//                    String filePath = null;
//                    try {
//                        filePath = SiliCompressor.with(getActivity()).compressVideo(videofilePath,getDestinationPath().getPath());
//                    } catch (URISyntaxException e) {
//                        e.printStackTrace();
//                    }
//                    assert filePath != null;
//                    File sourceFile = new File(filePath);

                    File sourceFile = new File(videofilePath);

                    UploadVideo(teacherid,txt_title,txt_Discription,sourceFile,StrUrl);
                }
        });

        btn_UploadVideo.setOnClickListener(view-> {
            Check_Required_Premission.CheckCameraPermission(getActivity());
            Check_Required_Premission.checkStorageReadPermition(getActivity());
            Check_Required_Premission.checkStorageWritePermition(getActivity());
            VideoDialog();
        });
    }

    @SuppressLint("SetTextI18n")
    private void VideoDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Objects.requireNonNull(getActivity()));
        LayoutInflater inflater = getActivity().getLayoutInflater();
        @SuppressLint("InflateParams")
        View dialogView = inflater.inflate(R.layout.choose_image_option, null);
        LinearLayout layout_Camera = dialogView.findViewById(R.id.layout_Camera);
        LinearLayout layout_gallery = dialogView.findViewById(R.id.layout_gallery);
        LinearLayout layout_Cancel = dialogView.findViewById(R.id.layout_Cancel);
        TextView dialog_titel = (TextView)dialogView.findViewById(R.id.dialog_title);
        dialog_titel.setText("Upload Video");


        layout_Camera.setOnClickListener(v->{
            Intent cameraintent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
            if(cameraintent.resolveActivity(getActivity().getPackageManager())!=null) {
                Update_Bio_file_teacherprofile = null;
                try {

                    Update_Bio_file_teacherprofile = Picture_method.getInstance().getOutputMediaFile(MEDIA_TYPE_VIDEO);
                    current = Picture_method.getInstance().getCurrent();
                } catch (Exception ex)
                {

                    return;
                }

                if (Update_Bio_file_teacherprofile != null) {


                    Update_Bio_photofile_teacherprofile = FileProvider.getUriForFile(getActivity(),
                            BuildConfig.APPLICATION_ID + ".provider",
                            Update_Bio_file_teacherprofile);
                    cameraintent.putExtra(MediaStore.EXTRA_OUTPUT, Update_Bio_photofile_teacherprofile);
                    getActivity().startActivityForResult(cameraintent, CAMERA_CODE);


                }
            }
            alertDialog.dismiss();
        });


        layout_gallery.setOnClickListener(v->{
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
            getActivity().startActivityForResult(intent,GALLERY_CODE);
            alertDialog.dismiss();
        });


        layout_Cancel.setOnClickListener(v-> alertDialog.dismiss());

        dialogBuilder.setView(dialogView);
        alertDialog = dialogBuilder.create();
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams lp =  Objects.requireNonNull(alertDialog.getWindow()).getAttributes();
        lp.gravity = Gravity.TOP;
        lp.windowAnimations = R.style.dialog_animation;
        lp.width = LinearLayout.LayoutParams.FILL_PARENT;
        alertDialog.show();

    }
//------------------------------------------------------helper Method-----------------------------------------


    public String getSelectedVideoPath() {
        return SelectedVideoPath;
    }

    public  static  void setSelectedVideoPath(String selectedVideoPath) {
        Upload_VideoView.setVisibility(View.VISIBLE);
        SelectedVideoPath = selectedVideoPath;
        getSize(selectedVideoPath);
        try
        {
            MediaController mediacontroller = new MediaController(activity);
            mediacontroller.setAnchorView(Upload_VideoView);
            Upload_VideoView.setMediaController(mediacontroller);
            Upload_VideoView.setVideoPath(SelectedVideoPath);
            Upload_VideoView.requestFocus();
            Upload_VideoView.start();

        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }

        Upload_VideoView.requestFocus();
        Upload_VideoView.setOnPreparedListener(mp -> Upload_VideoView.start());
    }

    private static void getSize(String selectedVideoPath) {

        File file = new File(selectedVideoPath);
        if (!file.exists() || !file.isFile()) {
            String size  =  file.length() /(1024*1024)+" mb";
            Toast.makeText(activity, size, Toast.LENGTH_SHORT).show();
        }
    }
    private void findView(View rootView) {
        edit_Discription = rootView.findViewById(R.id.bio_edt_video_discription);
        edit_title= rootView.findViewById(R.id.bio_edt_Title);
        edit_name= rootView.findViewById(R.id.bio_edt_name);
        btn_UploadVideo= rootView.findViewById(R.id.bio_btn_upload);
        Upload_VideoView= rootView.findViewById(R.id.bio_vidio_txt);
        btn_Submit= rootView.findViewById(R.id.bio_btn_submit_teacher_profile);
        EditText bio_edt_prize = rootView.findViewById(R.id.bio_edt_prize);

    }
    private void ShareElement() {
        list= MySharePrafranceClass.GetSharePrefrance(getActivity());
        String username = list[1];
        if (!(username==null)){
            edit_name.setText(username);
        }else {
            edit_name.setText("");
        }
    }
    private File getDestinationPath(){

        File f = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + Objects.requireNonNull(getActivity()).getPackageName() + "/media/videos");
        if (f.mkdirs()) {
            f.isDirectory();
        }
        return f;
    }


      void UploadVideo(String teacherid, String txt_title, String txt_Discription, File sourceFile, String strUrl)
      {
          RequestParams params = new RequestParams();
          try {

              params.put("teacherid", teacherid);
              params.put("title", txt_title);
              params.put("desc", txt_Discription);
              params.put("video", sourceFile);

          } catch (FileNotFoundException ignored) {
          }
          AsyncHttpClient client = new AsyncHttpClient();
          client.post(strUrl, params, new JsonHttpResponseHandler()
          {
              KProgressHUD pd;

              @Override
              public void onStart() {
                  pd = com.tutionize.androidstudionavigationdrawer.ProgressDialog.getInstance().getKNVideoProgressDialog(getActivity());
                  pd.show();
              }

              @Override
              public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                  super.onSuccess(statusCode, headers, response);
                  Home_screen.ShowToast("Video uploaded Successfully");
              }

              @Override
              public void onProgress(long bytesWritten, long totalSize) {
                  super.onProgress(bytesWritten, totalSize);

                 Integer progress =  (int) ((bytesWritten / (float) totalSize) * 100);
                  pd.setProgress(progress);
                  pd.setDetailsLabel(progress+"/"+100+"%");
                  Log.d("Uploading Progress  :  ",""+bytesWritten);
              }

              @Override
              public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                  super.onFailure(statusCode, headers, responseString, throwable);
                  Home_screen.ShowToast("Video uploaded Error : " +responseString);
              }
              @Override
              public void onFinish() {
                  pd.dismiss();
              }
          });
      }


}
