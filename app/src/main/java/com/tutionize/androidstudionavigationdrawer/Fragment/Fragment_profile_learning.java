package com.tutionize.androidstudionavigationdrawer.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.tutionize.androidstudionavigationdrawer.Activity_Screen.Home_screen;
import com.tutionize.androidstudionavigationdrawer.Constant.LiveData;
import com.tutionize.androidstudionavigationdrawer.Constant.Singleton;
import com.tutionize.androidstudionavigationdrawer.Model.Get_State_modle;
import com.tutionize.androidstudionavigationdrawer.Model.LearnerUpdate_modle;
import com.tutionize.androidstudionavigationdrawer.Model.State_Result_modle;
import com.tutionize.androidstudionavigationdrawer.ProgressDialog;
import com.tutionize.androidstudionavigationdrawer.R;
import com.tutionize.androidstudionavigationdrawer.Sharepool.MySharePrafranceClass;
import com.tutionize.androidstudionavigationdrawer.Sharepool.NetworkStateCheck;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

import static com.tutionize.androidstudionavigationdrawer.R.layout.support_simple_spinner_dropdown_item;

/**
 * Created by hp on 01-03-2017.
 */
public class Fragment_profile_learning extends Fragment
{
    private Spinner up_edt_gender, up_edt_country;
    private AutoCompleteTextView up_ed_state;
    private String txt_area,txt_name,txt_gender,txt_country,txt_pin,txt_state,txtuser,txt_email,txt_image;
    private List<String> genderlist = new ArrayList<>();
    private TypedFile photo;
    private String UserName, userEmail,usergender,userArea,usercity,userpin,userCountry;
    private EditText up_edt_name, up_edt_email, up_edt_pin, up_edt_area;
    private Button up_txt_SUBMIT;
    private String[] list;
    private List<State_Result_modle> Citylist= new ArrayList<>();
    private List<String> CityNamelist= new ArrayList<>();
    private  ArrayAdapter<String> spinnerCityAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.fragment_profile_learning, container, false);
        findView(rootView);
        GetEditData();
        ShareElement();
        AddElement();
        setAdapterlist();
        setData();
        ClickAble();
       // AapterItemSelect();
        return rootView;
    }
    private void setData()
    {
        up_edt_name.setText(UserName);
        up_edt_email.setText(userEmail);
        up_edt_pin.setText(userpin);
        up_edt_area.setText(userArea);
        up_edt_gender.setSelection(genderlist.indexOf(usergender));
        up_ed_state.setText(usercity);

    }
    private void GetEditData() {
        Bundle bundle = this.getArguments();
        if (bundle != null) {

            UserName= bundle.getString("username");
            userEmail= bundle.getString("userEmail");
            usergender= bundle.getString("usergender");
            userArea= bundle.getString("userArea");
            usercity= bundle.getString("usercity");
            userpin= bundle.getString("userpin");
            userCountry=bundle.getString("userCountry");

        }
    }
    private void AapterItemSelect()
    {
//        up_edt_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
//        {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
//                String Country = adapterView.getItemAtPosition(position).toString();
//                int ejectPosition = position-1;
//                if (position==0){
//                    CityNamelist.clear();
//                    CityNamelist.add("Select Rigion");
//                    if (spinnerCityAdapter !=null){
//                        spinnerCityAdapter.notifyDataSetChanged();
//                    }
//
//                }else {
//                    if (Countrylist!=null)
//                    {
//                        if (!Countrylist.isEmpty()) {
//                            try {
//                                GetCountryResult vishal = Countrylist.get(ejectPosition);
//                                ListofState(vishal.getCode());
//                            } catch (Exception ignored) {
//                            }
//                        }
//                    }
//                }
//            }
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView){}
//        });
    }
    private void ListofState(String code) {
        if (!CityNamelist.isEmpty())
        {
            Citylist.clear();
            CityNamelist.clear();
        }
       Singleton.getInstance().getApi().getState(code, new Callback<Get_State_modle>() {
            @Override
            public void success(Get_State_modle get_state_modle, Response response) {
                Citylist=get_state_modle.getResult();

                for (int index=0;index<Citylist.size();index++)
                {
                    CityNamelist.add(Citylist.get(index).getRegion());
                }
                if (usercity!=null)
                {
                    up_ed_state.setText(usercity);
                    usercity =  null;
                }
                spinnerCityAdapter = new ArrayAdapter<>(Objects.requireNonNull(getActivity()),support_simple_spinner_dropdown_item,CityNamelist);
                spinnerCityAdapter.setDropDownViewResource(support_simple_spinner_dropdown_item);
                up_ed_state.setAdapter(spinnerCityAdapter);
            }
            @Override
            public void failure(RetrofitError error) {

            }
        });
    }
    private void ShareElement()
    {

        list= MySharePrafranceClass.GetSharePrefrance(Objects.requireNonNull(getActivity()));
        String[] loginlist = MySharePrafranceClass.LoginDataGetSharePrefrance(getActivity());

        String username=list[1];
        String userEmail= loginlist[0];

        if (!(username==null)){
            if (!(username.equals(""))){
                up_edt_name.setText(username);
            }
        }

        if (!(userEmail==null)){
            if (!(userEmail.equals(""))){
                up_edt_email.setText(userEmail);
            }
        }
    }
    private boolean validate()
    {
        boolean valid = true;

        if (txt_name.isEmpty())
        {
            up_edt_name.setError("Enter Valid Name");
            valid=false;
        }
        if (txt_pin.isEmpty())
        {
            up_edt_pin.setError("Enter Valid Pin");
            valid=false;
        }

        if (txt_area.isEmpty())
        {
            up_edt_area.setError("Enter Valid Area");
            valid=false;
        }

        if (txt_gender.equals("Select Gender"))
        {
            up_edt_area.setError("Enter Valid Gender");
            valid=false;
        }
        if (txt_country.equals("Select Country")||txt_country==null)
        {
            up_edt_area.setError("Enter Valid Country");
            valid=false;
        }
        if (txt_state.equals("Select Region")||txt_state==null)
        {
            up_edt_area.setError("Enter Valid Region");
            valid=false;
        }
        return valid;
    }
    private void ClickAble() {
        up_txt_SUBMIT.setOnClickListener(view -> {

            txt_area=up_edt_area.getText().toString();
            txt_name=up_edt_name.getText().toString();
            txt_gender=up_edt_gender.getSelectedItem().toString();
            txt_country=up_edt_country.getSelectedItem().toString();
            txt_pin=up_edt_pin.getText().toString();
            txt_state=up_ed_state.getText().toString();
            txtuser = list[0];
            txt_image = list[2];
            txt_email=up_edt_email.getText().toString();

            if (!validate())
            {
                Toast.makeText(getActivity(), " Your Request Has Failed", Toast.LENGTH_SHORT).show();
            }
            else
                {
                updateLearner(txtuser, txt_name, txt_email, txt_gender,txt_state,txt_country,"", txt_area,txt_pin,txt_image);
            }

        });
    }
    @SuppressLint("ResourceAsColor")
    private void updateLearner(String txt_userid, String txt_name, String txt_email, String txt_gender, String txt_state, String txt_country, String txt_street, String txt_area, String txt_pin, String txt_image) {

        if (NetworkStateCheck.isNetworkAvaliable(Objects.requireNonNull(getActivity()))) {
            KProgressHUD knprogress = ProgressDialog.getInstance().getKNProgressDialog(getActivity());
            knprogress.show();
            Singleton.getInstance().getApi().UpdateUserProfile(txt_userid, txt_name, txt_email, txt_gender, txt_state, txt_country, photo,
                    txt_street, txt_area, txt_state, txt_pin, new Callback<LearnerUpdate_modle>() {
                        @Override
                        public void success(LearnerUpdate_modle learnerUpdate_modle, Response response) {
                            if (learnerUpdate_modle.getSuccess().equals("1")) {

                                MySharePrafranceClass.ClearAll(Objects.requireNonNull(getActivity()));

                                up_edt_name.setText("");
                                up_edt_email.setText("");
                                up_edt_pin.setText("");
                                up_edt_area.setText("");
                                up_edt_gender.setSelection(0);
                                up_edt_country.setSelection(0);
                                Home_screen.home_screen.txt_UserName.setText(txt_name);

                                MySharePrafranceClass.Share(getActivity(), txt_userid, txt_name, txt_image);
                                knprogress.dismiss();
                                Toast.makeText(getActivity(), "Profile updated successfully", Toast.LENGTH_SHORT).show();
                                Objects.requireNonNull(getActivity()).onBackPressed();

                            } else {
                                knprogress.dismiss();
                                Toast.makeText(getActivity(), "" + learnerUpdate_modle.getResponse(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        @Override
                        public void failure(RetrofitError error) {
                            knprogress.dismiss();
                        }
                    });
        }else {
            Toast.makeText(getContext(), "Internet connection problem", Toast.LENGTH_SHORT).show();
        }
    }
    private void findView(View rootView) {
        up_edt_email= rootView.findViewById(R.id.up_ed_email);
        up_edt_area= rootView.findViewById(R.id.up_ed_area);
        up_edt_name= rootView.findViewById(R.id.up_ed_username);
        up_edt_gender= rootView.findViewById(R.id.up_ed_gender);
        up_edt_country= rootView.findViewById(R.id.up_ed_country);
        up_ed_state = rootView.findViewById(R.id.up_ed_state);
        up_edt_pin= rootView.findViewById(R.id.up_ed_pincode);
        up_txt_SUBMIT= rootView.findViewById(R.id.up_btn_submit_student_profile);
    }

    //-------------------helper method------------------------------

    private void AddElement() {
        CityNamelist.add("Select Region");
        genderlist.add("Gender");
        genderlist.add("Male");
        genderlist.add("Female");
    }
    private void setAdapterlist() {
        ArrayAdapter<String> spinnerGenderAdapter = new ArrayAdapter<>(Objects.requireNonNull(getActivity()), support_simple_spinner_dropdown_item, genderlist);
        spinnerGenderAdapter.setDropDownViewResource(support_simple_spinner_dropdown_item);
        up_edt_gender.setAdapter(spinnerGenderAdapter);

        ArrayAdapter<String> spinnerCountryAdapter = new ArrayAdapter<>(getActivity(),support_simple_spinner_dropdown_item,LiveData.getCountry());
        spinnerCountryAdapter.setDropDownViewResource(support_simple_spinner_dropdown_item);
        up_edt_country.setAdapter(spinnerCountryAdapter);

        spinnerCityAdapter = new ArrayAdapter<>(getActivity(),support_simple_spinner_dropdown_item,CityNamelist);
        spinnerCityAdapter.setDropDownViewResource(support_simple_spinner_dropdown_item);
        up_ed_state.setAdapter(spinnerCityAdapter);
    }

}

