package com.tutionize.androidstudionavigationdrawer.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.tutionize.androidstudionavigationdrawer.R;

import java.util.Objects;

/**
 * Created by android on 10/25/17.
 */

public class Fragment_PayPal extends Fragment {

    private TextView paymentId,paymentStatus,paymentTime,paymentamount;
    private String id,status,createtime,amount,StudentID,TeacherID;
    @SuppressLint("SetTextI18n")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_paypal, container, false);
        findView(rootView);
        cliclAble();

        Bundle mBundle = getArguments();

        if (!(mBundle == null)) {
            id         = mBundle.getString("id");
            status     = mBundle.getString("status");
            createtime = mBundle.getString("createtime");
            amount     = mBundle.getString("amount");

            StudentID = mBundle.getString("Studentid");
            TeacherID = mBundle.getString("teacherid");
            String course = mBundle.getString("Course");

            paymentId.setText(id);
            paymentamount.setText("$ "+amount);
            paymentTime.setText(createtime);
            paymentStatus.setText(status);

            if (status.equals("approved")){
               //
                }
                else {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(Objects.requireNonNull(getContext()));
                alertDialog.setTitle("PayPal Payment");
                alertDialog.setCancelable(false);
                alertDialog.setMessage("Payment is not Approved Try Again");
                alertDialog.setPositiveButton("Yes", (dialogInterface, i) -> {

                    dialogInterface.dismiss();
                    Toast.makeText(getContext(), "You clicked on YES",
                            Toast.LENGTH_SHORT).show();

                });
            alertDialog.setNegativeButton("No", (dialogInterface, i) -> Toast.makeText(getContext(), "You clicked on No",Toast.LENGTH_SHORT).show());
                alertDialog.show();
            }

        }
        return  rootView;
    }

    private void findView(View rootView) {
        paymentId= (TextView)rootView.findViewById(R.id.paymentid);
        paymentStatus= (TextView)rootView.findViewById(R.id.payment_status);
        paymentTime = (TextView)rootView.findViewById(R.id.payment_Time);
        paymentamount = (TextView)rootView.findViewById(R.id.payment_amount);
    }
    private void cliclAble() {}
    


}
