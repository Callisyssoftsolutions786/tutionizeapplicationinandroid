package com.tutionize.androidstudionavigationdrawer.Fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.EditText;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.tutionize.androidstudionavigationdrawer.Adapter.adapter_course_learning;
import com.tutionize.androidstudionavigationdrawer.Constant.Singleton;
import com.tutionize.androidstudionavigationdrawer.Model.Result;
import com.tutionize.androidstudionavigationdrawer.Model.TeacherOnCourseModel;
import com.tutionize.androidstudionavigationdrawer.ProgressDialog;
import com.tutionize.androidstudionavigationdrawer.R;
import com.tutionize.androidstudionavigationdrawer.Sharepool.NetworkStateCheck;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * Created by hp on 01-03-2017.
 */
public class Fragment_course_learning extends Fragment
{ private View rootView;
    private EditText seachbox;
    private FloatingActionButton DataSearch_mic;
    private RecyclerView course_learning;
    private adapter_course_learning mTeacher_list_Adapter;
    private List<Result> teacherlist = new ArrayList<>();
    private SpeechRecognizer speechRecognizer;
    private Intent speechRecognizerintent;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_course_learning, container, false);
         CheckRecordAudioPermission(getActivity());
          initialize();
          RecognizedSpeach();
          Bundledata();
          ValidListner();
        return rootView;
    }

    private void RecognizedSpeach() {

        speechRecognizer= SpeechRecognizer.createSpeechRecognizer(getActivity());
        speechRecognizerintent =  new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        speechRecognizerintent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        speechRecognizerintent.putExtra(RecognizerIntent.EXTRA_LANGUAGE,Locale.ENGLISH);

        speechRecognizer.setRecognitionListener(new RecognitionListener() {
            @Override
            public void onReadyForSpeech(Bundle params) {
                Toast.makeText(getContext(), "Ready to Speak", Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onBeginningOfSpeech() {}
            @Override
            public void onRmsChanged(float rmsdB) {}
            @Override
            public void onBufferReceived(byte[] buffer) {}
            @Override
            public void onEndOfSpeech() {
                Toast.makeText(getActivity(), "End to speek", Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onError(int error) {
                Toast.makeText(getActivity(), "Error is speek - " +error, Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onResults(Bundle results) {
                ArrayList<String>  match = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                if (match!=null){
                    seachbox.setText(match.get(0));
                }
            }
            @Override
            public void onPartialResults(Bundle partialResults) {
                Toast.makeText(getActivity(), ""+partialResults, Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onEvent(int eventType, Bundle params) {}
        });

    }

    @SuppressLint("ClickableViewAccessibility")
    private void ValidListner() {

        seachbox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(mTeacher_list_Adapter!=null) {mTeacher_list_Adapter.getFilter().filter(s);}
            }
            @Override
            public void afterTextChanged(Editable s) {}});


        DataSearch_mic.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()){

                    case MotionEvent.ACTION_DOWN:
                        seachbox.setText("");
                        seachbox.setHint("Listening...");
                        speechRecognizer.startListening(speechRecognizerintent);
                        break;

                    case MotionEvent.ACTION_UP:
                        speechRecognizer.stopListening();
                        seachbox.setHint("Search...");
                        break;

                }

                return true;
            }
        });
    }

    private void Bundledata() {
        Bundle mBundle = getArguments();
        if (!(mBundle == null))
        {
            String course = mBundle.getString("course");
            Teacheroncourse(course);
        }
    }
    @SuppressLint("ResourceAsColor")
    private void Teacheroncourse(String course) {
        if (NetworkStateCheck.isNetworkAvaliable(Objects.requireNonNull(getActivity()))) {
            KProgressHUD knprogress = ProgressDialog.getInstance().getKNProgressDialog(getActivity());
            knprogress.show();
            Singleton.getInstance().getApi().TeacherOnCourse(course, new Callback<TeacherOnCourseModel>() {
                @Override
                public void success(TeacherOnCourseModel teacherOnCourseModel, Response response) {
                    if (teacherOnCourseModel.getSuccess() == 1) {
                        knprogress.dismiss();
                        teacherlist = teacherOnCourseModel.getResult();

                        if (teacherlist != null) {
                            if (!teacherlist.isEmpty()) {
                                LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                                course_learning.setLayoutManager(layoutManager);
                                mTeacher_list_Adapter = new adapter_course_learning(getActivity(), teacherlist);
                                try {
                                    LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getContext(), R.anim.layout_animation);
                                    course_learning.setLayoutAnimation(animation);
                                } catch (NullPointerException ignored) {
                                } catch (Exception ignored) {
                                }
                                course_learning.setAdapter(mTeacher_list_Adapter);
                            } else {
                                Toast.makeText(getActivity(), "Currently don't have any teacher", Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            Toast.makeText(getActivity(), "Currently don't have any teacher", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        knprogress.dismiss();
                    }
                }
                @Override
                public void failure(RetrofitError error) {
                    knprogress.dismiss();
                    Toast.makeText(getActivity(), "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }else {
            Toast.makeText(getContext(), "Internet coonection problem", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void onDestroy() {
        if (speechRecognizer!=null){
            speechRecognizer.destroy();
        }
        super.onDestroy();
    }

    public void initialize() {
        DataSearch_mic =(FloatingActionButton)rootView.findViewById(R.id.DataSearch_mic);
        course_learning = (RecyclerView) rootView.findViewById(R.id.rcy_course_learning);
        course_learning.setHasFixedSize(true);
        seachbox= (EditText) rootView.findViewById(R.id.datasearch_box);
    }

    public void CheckRecordAudioPermission(Activity activity){

        if (Build.VERSION.SDK_INT>Build.VERSION_CODES.M)
        {
            if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getActivity()), Manifest.permission.RECORD_AUDIO)!= PackageManager.PERMISSION_GRANTED){
                Intent intent  =  new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,Uri.parse("package:" +getActivity().getPackageName()));
                  getActivity().startActivity(intent);
            }
        }
    }
}
