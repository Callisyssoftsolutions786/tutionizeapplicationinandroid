package com.tutionize.androidstudionavigationdrawer.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.tutionize.androidstudionavigationdrawer.Constant.Singleton;
import com.tutionize.androidstudionavigationdrawer.Model.ChangePassword_modle;
import com.tutionize.androidstudionavigationdrawer.ProgressDialog;
import com.tutionize.androidstudionavigationdrawer.R;
import com.tutionize.androidstudionavigationdrawer.Sharepool.MySharePrafranceClass;

import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by android on 10/13/17.
 */

public class Fragment_Change_Password extends Fragment {
    private Button Chp_bt_submit;
    private EditText Chp_edt_NewPassword,Chp_edt_OldPassword;
    private String[] list,logindata;
    private String txt_newpassword ,txt_oldpassword;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_change_password, container, false);
        findView(rootView);
        ShareElement();
        ClickAble();
        return rootView;
    }

    private void ClickAble() {
        Chp_bt_submit.setOnClickListener((View view) -> {
                 txt_newpassword=Chp_edt_NewPassword.getText().toString();
                 txt_oldpassword= Chp_edt_OldPassword.getText().toString();

                 if (!validate()){
                     Toast.makeText(getActivity(), "Your Request Change Password Has Failed ", Toast.LENGTH_SHORT).show();
                 }else {
                     String userid=list[0];

                     String email = logindata[0];
                     String Type = logindata[2];
                     String Country = logindata[3];
                     String City = logindata[4];

                     ChangePassword(userid,email,txt_newpassword,Type,Country,City);}
        });
    }

    @SuppressLint("ResourceAsColor")
    private void ChangePassword(String userid, String email,String txt_newpassword,String Type,
                                String Country, String City) {
        KProgressHUD knprogress = ProgressDialog.getInstance().getKNProgressDialog(getActivity());
        knprogress.show();
        Singleton.getInstance().getApi().ChangePassword(userid, txt_newpassword, new Callback<ChangePassword_modle>() {
            @Override
            public void success(ChangePassword_modle changePassword_modle, Response response) {
                if(changePassword_modle.getSuccess().equals("1")){

                    MySharePrafranceClass.ClearAllLoginData(Objects.requireNonNull(getActivity()));
                    knprogress.dismiss();
                    Chp_edt_NewPassword.setText("");
                    Chp_edt_OldPassword.setText("");

                    MySharePrafranceClass.LoginDataShare(getActivity(),email,txt_newpassword,Type,Country,City);

                    Toast.makeText(getActivity(), "Password Has been Changed Successfull", Toast.LENGTH_SHORT).show();

                    getActivity().onBackPressed();
                }else {
                    knprogress.dismiss();
                    Toast.makeText(getActivity(), ""+changePassword_modle.getResponse(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                knprogress.dismiss();
                Toast.makeText(getActivity(), ""+error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void findView(View rootView) {
        Chp_bt_submit=(Button) rootView.findViewById(R.id.chp_bt_changePassword_submit);
        Chp_edt_NewPassword=(EditText)rootView.findViewById(R.id.chp_edt_new_password);
        Chp_edt_OldPassword=(EditText)rootView.findViewById(R.id.chp_edt_old_password);
    }

    //------------------------------------helper method------------------------------------

    private boolean validate(){
        boolean valid =true;
        if (txt_oldpassword.isEmpty()||! txt_oldpassword.equals(logindata[1])){
            Chp_edt_OldPassword.setError("Enter Valid Old Password");
            valid=false;
        }
        if (txt_newpassword.isEmpty()){
            Chp_edt_NewPassword.setError("Enter Valid New Password");
            valid=false;
        }
        if (txt_oldpassword.equals(txt_newpassword)){
            Chp_edt_NewPassword.setError("Choose Diffrent Password");
            valid=false;
        }

        return valid;
    }

    private void ShareElement() {
        logindata=MySharePrafranceClass.LoginDataGetSharePrefrance(getActivity());

        list= MySharePrafranceClass.GetSharePrefrance(getActivity());


        String password=logindata[1];
        if (!(password==null)){
            Chp_edt_OldPassword.setText(password);
        }
        else {
            Chp_edt_OldPassword.setText("");
        }

    }
}
