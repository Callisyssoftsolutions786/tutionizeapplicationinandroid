package com.tutionize.androidstudionavigationdrawer.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.tutionize.androidstudionavigationdrawer.Constant.Singleton;
import com.tutionize.androidstudionavigationdrawer.Model.ContactUs_Modle;
import com.tutionize.androidstudionavigationdrawer.R;
import com.tutionize.androidstudionavigationdrawer.Sharepool.NetworkStateCheck;

import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * Created by android on 10/12/17.
 */

public class Fragment_WriteUs extends Fragment{
    private Button bt_write_submit;
    private EditText edt_write_email,edt_write_name,edt_write_message,edt_write_subject;
    private String txt_write_email,txt_write_name,txt_write_msg,txt_write_subject;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_write_us, container, false);
        findView(rootView);
        ClickAble();
        return rootView;
    }

    private void ClickAble() {
        bt_write_submit.setOnClickListener((View view) ->{

                txt_write_email   = edt_write_email.getText().toString();
                txt_write_name    = edt_write_name.getText().toString();
                txt_write_msg     = edt_write_message.getText().toString();
                txt_write_subject = edt_write_subject.getText().toString();

                if (!Validate()){
                    Toast.makeText(getActivity(), "Your Request has Failed", Toast.LENGTH_SHORT).show();
                }
                else {
                    ContactUs(txt_write_name, txt_write_email,txt_write_subject,txt_write_msg);
                }});
    }



    private void ContactUs(String name, String email, String subject, String msg)
    {
        if(NetworkStateCheck.isNetworkAvaliable(Objects.requireNonNull(getActivity()))) {
            Singleton.getInstance().getApi().contactUs(name, email, subject, msg, new Callback<ContactUs_Modle>() {
                @Override
                public void success(ContactUs_Modle contactUs_modle, Response response) {
                    if (contactUs_modle.getSuccess().equals("1")) {
                        Toast.makeText(getActivity(), "" + contactUs_modle.getMessage(), Toast.LENGTH_SHORT).show();
                        edt_write_email.setText("");
                        edt_write_message.setText("");
                        edt_write_subject.setText("");
                        edt_write_name.setText("");
                    } else {
                        Toast.makeText(getActivity(), "" + contactUs_modle.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Toast.makeText(getActivity(), "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }else {
            Toast.makeText(getContext(), "Internet connection problem", Toast.LENGTH_SHORT).show();
        }
    }
    private void findView(View rootView) {
        bt_write_submit=(Button) rootView.findViewById(R.id.btn_writeus_submit);
        edt_write_email=(EditText)rootView.findViewById(R.id.txt_writeUS_email);
        edt_write_message=(EditText)rootView.findViewById(R.id.txt_writeUs_message);
        edt_write_name=(EditText)rootView.findViewById(R.id.txt_writeUs_name);
        edt_write_subject=(EditText)rootView.findViewById(R.id.txt_writeUS_subject);
    }


    //================================================helper Method +========================================================



    private boolean Validate(){
        boolean valid= true;

        if (txt_write_name.isEmpty()){
            edt_write_name.setError("Enter Valid Name");
            valid=false;
        }
        if (txt_write_email.isEmpty()||!Patterns.EMAIL_ADDRESS.matcher(txt_write_email).matches()){
            edt_write_email.setError("Enter Valid Email");
            valid=false;
        }
        if (txt_write_subject.isEmpty()){
            edt_write_subject.setError("Enter Valid Subject");
            valid=false;
        }
        if (txt_write_msg.isEmpty()){
            edt_write_message.setError("Enter Valid Message");
            valid=false;
        }
        return valid;
    }


}
