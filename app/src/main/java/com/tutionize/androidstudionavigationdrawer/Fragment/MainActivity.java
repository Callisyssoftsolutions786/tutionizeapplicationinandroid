package com.tutionize.androidstudionavigationdrawer.Fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.tutionize.androidstudionavigationdrawer.Adapter.CourceAdapter;
import com.tutionize.androidstudionavigationdrawer.Constant.Message_dialoge;
import com.tutionize.androidstudionavigationdrawer.Constant.Singleton;
import com.tutionize.androidstudionavigationdrawer.Model.Cources_modle;
import com.tutionize.androidstudionavigationdrawer.Model.CourseData;
import com.tutionize.androidstudionavigationdrawer.ProgressDialog;
import com.tutionize.androidstudionavigationdrawer.R;
import com.tutionize.androidstudionavigationdrawer.Sharepool.NetworkStateCheck;

import java.util.List;
import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;



public class MainActivity extends Fragment
{
    private RecyclerView courserecycle;
    @SuppressLint("StaticFieldLeak")
    private static Activity activity;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_main, container, false);
        activity  = getActivity();
        touchlistner(view);
        Cource();
        FINDVIEWBYID(view);
        return view;
    }

    private void touchlistner(View view) {
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener((v, keyCode, event) -> {
            if (keyCode== KeyEvent.KEYCODE_BACK)
            {
                try {

                    Message_dialoge.getInstance().exitfromApplication(getActivity());
                }
                catch (WindowManager.BadTokenException ignored){}
                catch (Exception ignored){}
                return true;
            }

            return false;
        });
    }

    private void Cource() {

        if (NetworkStateCheck.isNetworkAvaliable(Objects.requireNonNull(getActivity()))) {
            KProgressHUD knprogress = ProgressDialog.getInstance().getKNProgressDialog(getActivity());
            knprogress.show();
            Singleton.getInstance().getApi().getCourse(new Callback<Cources_modle>()
            {
                @Override
                public void success(Cources_modle cources_modle, Response response) {
                    knprogress.dismiss();
                    List<CourseData> courcedata = cources_modle.getData();
                    if (courcedata!=null)
                    {
                        int resId = R.anim.layout_animation;
                        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(activity, resId);
                        LinearLayoutManager layoutManager1 = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
                        courserecycle.setLayoutManager(layoutManager1);
                        CourceAdapter courceAdapter = new CourceAdapter(activity,courcedata);
                        courserecycle.setAdapter(courceAdapter);
                        courserecycle.setLayoutAnimation(animation);
                        courceAdapter.notifyDataSetChanged();
                    }
                    else {
                        Toast.makeText(getActivity(), "Currently no course available", Toast.LENGTH_SHORT).show();
                    }
                }
                @Override
                public void failure(RetrofitError error) {

                    knprogress.dismiss();
                }
            });
        }else {
            Toast.makeText(activity, "Internet connection problem", Toast.LENGTH_SHORT).show();
        }
        }

        private void FINDVIEWBYID (View view)
        {
            courserecycle = view.findViewById(R.id.courserecycle);
        }

}
