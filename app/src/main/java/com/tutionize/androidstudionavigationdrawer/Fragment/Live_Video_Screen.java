package com.tutionize.androidstudionavigationdrawer.Fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioAttributes;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.tutionize.androidstudionavigationdrawer.Activity_Screen.Home_screen;
import com.tutionize.androidstudionavigationdrawer.Adapter.Adapter_live_video_user;
import com.tutionize.androidstudionavigationdrawer.Constant.AppConst;
import com.tutionize.androidstudionavigationdrawer.Constant.Singleton;
import com.tutionize.androidstudionavigationdrawer.Lacture.CameraCapture;
import com.tutionize.androidstudionavigationdrawer.Model.Start_class_modle;
import com.tutionize.androidstudionavigationdrawer.Model.TeacherReview_Modle;
import com.tutionize.androidstudionavigationdrawer.Model.TwelioView_Model;
import com.tutionize.androidstudionavigationdrawer.R;
import com.tutionize.androidstudionavigationdrawer.Services.Class_join_services;
import com.tutionize.androidstudionavigationdrawer.Sharepool.MySharePrafranceClass;
import com.tutionize.androidstudionavigationdrawer.Sharepool.NetworkStateCheck;
import com.tutionize.androidstudionavigationdrawer.command.ConstantCommand;
import com.tutionize.androidstudionavigationdrawer.widget.MoveableVideoview;
import com.twilio.video.AudioTrack;
import com.twilio.video.CameraCapturer;
import com.twilio.video.ConnectOptions;
import com.twilio.video.LocalAudioTrack;
import com.twilio.video.LocalParticipant;
import com.twilio.video.LocalVideoTrack;
import com.twilio.video.Participant;
import com.twilio.video.Room;
import com.twilio.video.RoomState;
import com.twilio.video.TwilioException;
import com.twilio.video.Video;
import com.twilio.video.VideoConstraints;
import com.twilio.video.VideoDimensions;
import com.twilio.video.VideoRenderer;
import com.twilio.video.VideoTrack;
import com.twilio.video.VideoView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Vishal sandhu on 10-02-2017.
 */
public class  Live_Video_Screen extends android.support.v4.app.Fragment {

    public static final int CAMERA_MIC_PERMISSION_REQUEST_CODE = 1;
    static final int MIN_WIDTH = 100;
    private FloatingActionButton voulume_up_action_fab, GoVideo, volume_control ,StudentControl,Camera_control,Video_control;
    private VideoView Thumb_two_Video, Primary_VideoView;
    private MoveableVideoview Thumb_vidios,thumbnail_vidio_teacher;
    private Button LeaveClass,videoview_msg;
    private ImageView Studentvolume_control;
    private RecyclerView mRecyclerView_online;
    private View rootView;
    private Handler handler = new Handler();
    private boolean StudentMute=true,audioMute=true,videoMute=true;
    private LocalAudioTrack localAudioTrack;
    private LocalVideoTrack localVideoTrack;
    private VideoTrack primaryVideoTracker;
    private VideoTrack TeacherprimaryVideoTracker;
    private Participant primaryPaticipant;
    private Participant TeacherprimaryPaticipant;
    private VideoRenderer localVideoView;
    private Room room;
    private int previousAudioMode;
    private boolean previousMicrophoneMute;
    private LocalParticipant localParticipant;
    private CameraCapture cameraCapture;
    private AudioManager audioManager;
    private boolean disconnectedFromOnDistroy;
    private Adapter_live_video_user madapter;
    private Participant TeacherPaticipant;
    private CircleImageView AudioCallerImage;
    private FloatingActionButton AudioCallButtonEnd;
    private TextView AudiocallerName;
    private RelativeLayout AudioCallScreen;
    private String rate;
    private  TextView callTime;
    private LinearLayout VideoCallScreen,student_layout;
    public String acesstoken,calltype,teacherimage,callid,AllStudent,teachername;
    private String roomName;
    private List<TwelioView_Model> twelioView_models = new ArrayList<>();
    @SuppressLint("StaticFieldLeak")
    public  static Live_Video_Screen live_video_screen;
    private String[] listlist,list;
    private int hours=0, seconds =0,  minutes=0;
    private String roomid;
    private ScaleGestureDetector SGD;

    public Live_Video_Screen() {
    }


    @SuppressLint("RestrictedApi")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        Home_screen.home_screen.toolbar.setVisibility(View.GONE);
        Home_screen.home_screen.drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        try {
            rootView = inflater.inflate(R.layout.live_video_screen, container, false);
        }catch (Exception e){
            Log.getStackTraceString(e);
        }
        live_video_screen=this;
        BundleData();
        initialize();
        InitilizeRecycleView();
        Clickable();

        if (!checkPermissionForCameraAndMicrophone()){
            requestPermissionForCameraAndMicrophone();
        } else {
            createAudioAndVideoTracker();
        }

if (teacherimage!=null) {
    String pic = AppConst.photoconstant.concat(teacherimage);
    Glide.with(this).load(pic).asBitmap().into(new SimpleTarget<Bitmap>(150,
            50) {
        @Override
        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
            Drawable drawable = new BitmapDrawable(Objects.requireNonNull(getActivity()).getResources(), resource);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                AudioCallScreen.setBackground(drawable);
            }
        }
    });
}
        if (calltype!=null){

            if (calltype.equals(ConstantCommand.callType_Video))
            {
                //GoVideo.setVisibility(View.GONE);
                String type=listlist[2];

                if (!type.equals(ConstantCommand.Profile_Type_T)){

//                    if (localVideoTrack!=null)
//                    {
//                        Video_control.setImageDrawable(ContextCompat.getDrawable
//                                (Objects.requireNonNull(getActivity()),R.drawable.videocamoff));
//                    }

                }

                AudioCallScreen.setVisibility(View.GONE);
                VideoCallScreen.setVisibility(View.VISIBLE);
            }
            else if (calltype.equals(ConstantCommand.callType_Audio))
            {
               // GoVideo.setVisibility(View.GONE);
                String type=listlist[2];
                if (localVideoTrack!=null)
                {
                    Video_control.setImageDrawable(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()),R.drawable.videocamoff));
                    //setVideoMute(false);
                    localVideoTrack.enable(false);
                }
                VideoCallScreen.setVisibility(View.GONE);
                AudioCallScreen.setVisibility(View.VISIBLE);

                if (teacherimage!=null) {
                    String pic = AppConst.photoconstant.concat(teacherimage);
                    Glide.with(getActivity()).load(pic).into(AudioCallerImage);
                } else {
                    Glide.with(getActivity()).load(R.drawable.userimagewhite).into(AudioCallerImage);
                }

                if (teachername!=null){
                    AudiocallerName.setText(teachername);
                }
                if (!type.equals(ConstantCommand.Profile_Type_T)){
                    CallTimeStart();
                }
            }
            else if (calltype.equals(ConstantCommand.callType_Video_1)){

                String type=listlist[2];
                if (localVideoTrack!=null)
                {
                    Video_control.setImageDrawable(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()),R.drawable.videocamoff));
                    //setVideoMute(false);
                    localVideoTrack.enable(false);
                }
                VideoCallScreen.setVisibility(View.GONE);
                AudioCallScreen.setVisibility(View.VISIBLE);

                if (teacherimage!=null) {
                    String pic = AppConst.photoconstant.concat(teacherimage);
                    Glide.with(getActivity()).load(pic)
                            .into(AudioCallerImage);
                } else {
                    Glide.with(getActivity()).load(R.drawable.userimagewhite)
                            .into(AudioCallerImage);
                }

                if (teachername!=null){
                    AudiocallerName.setText(teachername);
                }
                if (!type.equals(ConstantCommand.Profile_Type_T)){
                    CallTimeStart();
                }

            }
        }
        TouchListner_and_keyEvent(rootView);

        connectToRoom(getRoomName());

        String type=listlist[2];
        if (type.equals(ConstantCommand.Profile_Type_T)){
        }
        else {
            //StudentControl.setVisibility(View.GONE);
            Studentvolume_control.setVisibility(View.GONE);
        }
        return rootView;
    }

    private void BundleData() {
        Bundle mBundle = getArguments();
        if (!(mBundle == null))
        {
            String token = mBundle.getString("Token");
            roomid = mBundle.getString("Room");
            calltype = mBundle.getString("calltype");
            teacherimage=mBundle.getString("teacherimage");
            teachername=mBundle.getString("teachername");
            callid = mBundle.getString("callid");
            AllStudent = mBundle.getString("AllStudent");
            setAcesstoken(token);
            setRoomName(roomid);
        }
    }

    public void  CallTimeStart()
    {
        GoVideo.clearAnimation();
        AudioCallButtonEnd.clearAnimation();
        voulume_up_action_fab.clearAnimation();
        AudiocallerName.clearAnimation();

        hours=0;
        minutes=0;
        seconds=0;
        new Thread(() -> {
            while(seconds < 60)
            {
                seconds +=1;
                try{
                    Thread.sleep(1000);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }
                handler.post(new Runnable() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void run()
                    {
                        callTime.setText(" "+hours + " : " + minutes + " : " + seconds+" " );
                        if(seconds == 60){
                            minutes+=1;
                            seconds=0;
                            if (minutes==60)
                            {
                                hours+=1;
                                seconds=0;
                                minutes=60;
                            }
                        }
                    }
                });
            }
        }).start();
    }

    //---------------------------Click Able Variable ..............................

    private void Clickable()
    {
        GoVideo.setOnClickListener(view -> {
                AudioCallScreen.setVisibility(View.GONE);
                VideoCallScreen.setVisibility(View.VISIBLE);
                if (localVideoTrack!=null)
                {
                    localVideoTrack.enable(true);
                }
        });



        AudioCallButtonEnd.setOnClickListener((View v)->{
             if (listlist[2]!=null&&listlist[2].equals(ConstantCommand.Profile_Type_T))
             {
                 SetClassAddress(AllStudent,roomid,list[1],list[2],ConstantCommand.callType_End,list[0]);
             }
             else {
                 SetClassAddress(callid,roomid,list[1],list[2],ConstantCommand.callType_End,list[0]);
             }
            if (room != null) {
                ClosefragmentAudioCall();
                room.disconnect();
            }

        });

        Studentvolume_control.setOnClickListener(v->{
            String type=listlist[2];
            if (type!=null&&type.equals(ConstantCommand.Profile_Type_T))
            {

                if (isStudentMute())
                {
                    Studentvolume_control.setImageDrawable(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()),R.drawable.studentvoice_off ));
                    setStudentMute(false);
                    MuteAllStudent();
                }
                else
                    {
                    Studentvolume_control.setImageDrawable(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()),R.drawable.studentvoice_on));
                    setStudentMute(true);
                    UnMuteAllStudent();
                }
            }
        });
        volume_control.setOnClickListener((View view)->
        {
            if (localAudioTrack != null) {
                boolean enable = !localAudioTrack.isEnabled();
                localAudioTrack.enable(enable);
                int icon = enable ?
                        R.drawable.mic_on :
                        R.drawable.mic_off;
                volume_control.setImageDrawable(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()),icon));
            }
        });

        Camera_control.setOnClickListener((View view) ->{
                if (cameraCapture != null)
                {
                    CameraCapturer.CameraSource cameraSource = cameraCapture.getCameraSource();
                    cameraCapture.switchCamera();
                    if (Thumb_vidios.getVisibility() == View.VISIBLE) {
                        Thumb_vidios.setMirror(cameraSource == CameraCapturer.CameraSource.BACK_CAMERA);
                    }
                    else
                        {
                        Primary_VideoView.setMirror(cameraSource == CameraCapturer.CameraSource.BACK_CAMERA);
                        }
                }
        });

        LeaveClass.setOnClickListener((View view)-> {



            String type=listlist[2];

          if (type.equals(ConstantCommand.Profile_Type_T))
          {

              if (calltype.equals(ConstantCommand.callType_Audio) | calltype.equals(ConstantCommand.callType_Video_1))
              {
                  SetClassAddress(callid,roomName,list[1],list[2],ConstantCommand.callType_End,list[0]);
              }
              else {
                  SetClassAddress(AllStudent,roomid,list[1],list[2],ConstantCommand.callType_End,list[0]);
              }

              closefragment();
          }
          else
              {
                  if (calltype.equals(ConstantCommand.callType_Video))
                  {
                      String[] loginlist = MySharePrafranceClass.LoginDataGetSharePrefrance(Objects.requireNonNull(getActivity()));
                      Intent startIntent = new Intent(getActivity(), Class_join_services.class);
                      startIntent.setAction(AppConst.ACTION.STARTFOREGROUND_ACTION);
                      startIntent.putExtra("roomname", roomid);
                      startIntent.putExtra("type", loginlist[2]);
                      startIntent.putExtra("calledid", callid);
                      if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.O) {
                          getActivity().startForegroundService(startIntent);
                      }else {
                          getActivity().startService(startIntent);
                      }
                  }
                  else {
                      SetClassAddress(callid,roomName,list[1],list[2],ConstantCommand.callType_End,list[0]);
                  }
                  if(room!=null){
                      room.disconnect();
                  }

                  giveRating();
              }
        });

        Video_control.setOnClickListener((View view) -> {
            if (localVideoTrack!= null) {
                boolean enable = !localVideoTrack.isEnabled();
                localVideoTrack.enable(enable);
                int icon = enable ?
                        R.drawable.videocamon :
                        R.drawable.videocamoff;
                Video_control.setImageDrawable(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()),
                        icon));
            }
        });
        voulume_up_action_fab.setOnClickListener(v->{
            if (audioManager.isSpeakerphoneOn()) {
                audioManager.setSpeakerphoneOn(false);
                voulume_up_action_fab.setImageResource(R.drawable.ic_phonelink_ring_white_24dp);
            }
            else {
                voulume_up_action_fab.setImageResource(R.drawable.ic_speaker_phone_black_24dp);
            }
        });
        StudentControl.setOnClickListener((View v)->{

            if (student_layout.getVisibility() == View.GONE){
                student_layout.setVisibility(View.VISIBLE);
            }else {
                student_layout.setVisibility(View.GONE);
            }
//
//            if (isFullScreenClick())
//            {
//                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,0,1.9f);
//                video_container.setLayoutParams(params);
//                setFullScreenClick(false);
//            }
//            else {
//                setFullScreenClick(true);
//                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,0,2.7f);
//                video_container.setLayoutParams(params);
//            }
//

        });
    }

    private void UnMuteAllStudent()
    {
        SetClassAddress(AllStudent,"","","","UnMuteAudio",list[0]);
    }

    private void MuteAllStudent()
    {
        SetClassAddress(AllStudent,"","","","MuteAudio",list[0]);
    }

    private void SetClassAddress(String student, String roomId,String teacher_name,String img_name, String Calltype,String callid) {
      try {
       Singleton.getInstance().getApi().SetClassAddress(student,roomId,teacher_name,img_name,Calltype,callid,new Callback<Start_class_modle>() {
            @Override
            public void success(Start_class_modle start_class_modle, Response response) {
                if (start_class_modle.getSuccess().equals("1"))
                {
                }
            }
            @Override
            public void failure(RetrofitError error) {

            }

        });
      }catch (Exception ignored){}
    }

        @Override
    public void onDestroy() {
        if (audioManager!=null){
            {
                if (!audioManager.isSpeakerphoneOn()) {
                    audioManager.setSpeakerphoneOn(true);
                }
            }

            if (room != null && room.getState() != RoomState.DISCONNECTED) {
                if (localParticipant!=null)
                {
                      if (localParticipant.getIdentity().contains(ConstantCommand.Profile_Type_T)){
                          if (room != null)
                          {
                              closefragment();
                              room.disconnect();
                              disconnectedFromOnDistroy = true;
                          }
                } else {

                      room.disconnect();
                      disconnectedFromOnDistroy = true;
                }}
            }
            if (localAudioTrack != null) {

                localAudioTrack.release();
                localAudioTrack = null;
            }

            if (localVideoTrack != null) {

                localVideoTrack.release();
                localVideoTrack = null;
            }

        }
        super.onDestroy();
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == CAMERA_MIC_PERMISSION_REQUEST_CODE) {
            boolean cameraAndMicPermissionGranted = true;

            for (int grantResult : grantResults) {
                cameraAndMicPermissionGranted &= grantResult == PackageManager.PERMISSION_GRANTED;
            }
            if (cameraAndMicPermissionGranted) {
                createAudioAndVideoTracker();
            } else {
                Toast.makeText(getActivity(), "permissions_needed", Toast.LENGTH_SHORT).show();
            }
        }
    }
    @Override
    public void onPause() {
        if (localVideoTrack != null) {
            if (localParticipant != null) {
                localParticipant.removeVideoTrack(localVideoTrack);
            }
            localVideoTrack.release();
            localVideoTrack = null;
        }
        super.onPause();
    }
    @Override
    public void onResume() {
        super.onResume();
        if (localVideoTrack == null && checkPermissionForCameraAndMicrophone()) {
            localVideoTrack = LocalVideoTrack.create(Objects.requireNonNull(getContext()), true, cameraCapture.getVideoCapturer());
            localVideoTrack.addRenderer(localVideoView);
            if (localParticipant != null) {
                localParticipant.addVideoTrack(localVideoTrack);
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    public void initialize()
    {
        Animation animation= AnimationUtils.loadAnimation(getContext(),R.anim.vibrate);

        SGD =  new ScaleGestureDetector(getActivity(),new Scale_listner());
        listlist= MySharePrafranceClass. LoginDataGetSharePrefrance(Objects.requireNonNull(getActivity()));
        list = MySharePrafranceClass.GetSharePrefrance(getActivity());
        callTime = rootView.findViewById(R.id.callTime);
        AudioCallerImage = rootView.findViewById(R.id.Live_class_image);
        AudioCallButtonEnd= rootView.findViewById(R.id.Live_class_Decline);
        AudioCallButtonEnd.setAnimation(animation);


        AudiocallerName= rootView.findViewById(R.id.Live_class_teacher_name);
        AudiocallerName.setAnimation(animation);


        AudioCallScreen= rootView.findViewById(R.id.PhoneCall);
        RelativeLayout phonecallbackgroundimage = rootView.findViewById(R.id.phonecallbackgroundimage);
        VideoCallScreen= rootView.findViewById(R.id.VideoCall);
       // LinearLayout studentView = rootView.findViewById(R.id.student);
        LinearLayout visible_vidioView = rootView.findViewById(R.id.Layout_video_view);

        student_layout =  rootView.findViewById(R.id.student_layout);

        Primary_VideoView = rootView.findViewById(R.id.Primary_video);
        FrameLayout video_container = rootView.findViewById(R.id.vidio_container);
        Thumb_vidios = rootView.findViewById(R.id.thumbnail_vidio_view);
        thumbnail_vidio_teacher = rootView.findViewById(R.id.thumbnail_vidio_view_teacher);
        Studentvolume_control = rootView.findViewById(R.id.Live_vidio_StudentvolumeControl);


        //------ floating Button ---------;


        volume_control = rootView.findViewById(R.id.Live_vidio_volumeControl);
        volume_control.show();

        Camera_control = rootView.findViewById(R.id.Live_vidio_cameraControl);
        Camera_control.show();

        Video_control = rootView.findViewById(R.id.LiveVideo_VideoControl);
        Video_control.show();

        StudentControl = rootView.findViewById(R.id.LiveVideo_StudentControl);
        StudentControl.show();

        GoVideo = rootView.findViewById(R.id.LiveVideo_Go_Video);
        GoVideo.show();
        GoVideo.setAnimation(animation);

        voulume_up_action_fab = rootView.findViewById(R.id.voulume_up_action_fab);
        voulume_up_action_fab.show();
        voulume_up_action_fab.setAnimation(animation);


        LeaveClass = rootView.findViewById(R.id.live_vidio_leave_class);
        videoview_msg = rootView. findViewById(R.id.videoview_msg);
        mRecyclerView_online = rootView.findViewById(R.id.Rec_online_user);

        if (calltype.equals(ConstantCommand.callType_Audio)){
            getActivity().setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);
            audioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
            assert audioManager != null;
            audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
            audioManager.setSpeakerphoneOn(true);

        }
        else {
            getActivity().setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);
            audioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
            assert audioManager != null;
            audioManager.setSpeakerphoneOn(true);
        }
    }

    private void InitilizeRecycleView() {
        mRecyclerView_online.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView_online.setLayoutManager(layoutManager);
        madapter = new Adapter_live_video_user(getActivity(), twelioView_models);
        mRecyclerView_online.setAdapter(madapter);
    }

    public void connectToRoom(String roomName)
    {
        configureAudio(true);
        ConnectOptions.Builder connectOptionsBuilder = new ConnectOptions.Builder(getAcesstoken()).roomName(roomName);
        if (localAudioTrack != null) {
            connectOptionsBuilder.audioTracks(Collections.singletonList(localAudioTrack));
        }
        if (localVideoTrack != null) {
            connectOptionsBuilder.videoTracks(Collections.singletonList(localVideoTrack));
        }
        room = Video.connect(Objects.requireNonNull(getContext()), connectOptionsBuilder.build(), roomListener());
    }
    private Room.Listener roomListener() {
        return new Room.Listener() {

            @Override
            public void onConnected(Room room) {


                localParticipant = room.getLocalParticipant();

                List<Participant> Connectedpaticipants = new ArrayList<>();

                for (Participant participant : room.getParticipants())
                {
                    if (participant.getIdentity().contains(ConstantCommand.Profile_Type_S))
                    {
                      // nothing to do...!
                    }
                    else if(participant.getIdentity().contains(ConstantCommand.Profile_Type_T)){

                    }
                    else {
                        participant.getIdentity().concat(ConstantCommand.Profile_Type_T);}
                        Connectedpaticipants.add(participant);
                }
                AddParticipant(Connectedpaticipants);
            }
            @Override
            public void onConnectFailure(Room room, TwilioException twilioException) {
                Toast.makeText(getActivity(), "" + twilioException.getMessage(), Toast.LENGTH_SHORT).show();
                configureAudio(false);
                initialize();
            }

            @Override
            public void onDisconnected(Room room, TwilioException twilioException) {
                localParticipant = null;


                Live_Video_Screen.this.room = null;

                if (!disconnectedFromOnDistroy) {
                    configureAudio(false);
                    moveLocalViewToPrimeryView();
                }
            }
            @Override
            public void onParticipantConnected(Room room, Participant participant) {
                addParticipant(participant);
            }
            @Override
            public void onParticipantDisconnected(Room room, Participant participant) {
                removeParticipant(participant);
            }
            @Override
            public void onRecordingStarted(Room room) {
            }
            @Override
            public void onRecordingStopped(Room room) {}
        };
    }
    public void createAudioAndVideoTracker()
    {

        try {
            cameraCapture = new CameraCapture(getContext(), getAvailableCameraSource());
            VideoConstraints constraints = new VideoConstraints.Builder()
                    .maxFps(40)
                    .minFps(10)
                    .maxVideoDimensions(VideoDimensions.HD_960P_VIDEO_DIMENSIONS)
                    .build();


            localAudioTrack = LocalAudioTrack.create(Objects.requireNonNull(getContext()), true);
            localVideoTrack = LocalVideoTrack.create(getContext(), true, cameraCapture.getVideoCapturer(), constraints);

            if (calltype.equals(ConstantCommand.callType_Audio)){

                if (localVideoTrack!=null){
                    localVideoTrack.enable(false);
                }

            }

            if (calltype.equals(ConstantCommand.callType_Video_1)){
                if (localVideoTrack!=null){
                    localVideoTrack.enable(false);
                }
            }

            Primary_VideoView.setMirror(true);
            localVideoTrack.addRenderer(Primary_VideoView);
            localVideoView = Primary_VideoView;

        }catch (Exception ignored){}
    }

    //______________________________Hellper method__________________________


    private void ClosefragmentAudioCall(){
        Intent intent = new Intent(getActivity(), Home_screen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);

    }

    private void closefragment() {

        if (room != null) {

            Intent intent = new Intent(getActivity(), Home_screen.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            room.disconnect();
        }else {
            Intent intent = new Intent(getActivity(), Home_screen.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }
    private  void giveRating()
    {
        final Dialog dialog = new Dialog(Objects.requireNonNull(getActivity()));
        dialog.setContentView(R.layout.custome_alert_dialog);
        Objects.requireNonNull(dialog.getWindow()).getAttributes().windowAnimations = R.style.DialogAnimation;
        RatingBar rating = dialog.findViewById(R.id.ratingBar);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        Button btnDismiss = dialog.getWindow().findViewById(R.id.dismiss);
        btnDismiss.setOnClickListener(v -> {
            rate = String.valueOf(rating.getRating());
            RateAPi(rate,callid);
            closefragment();
            dialog.dismiss();
        });
        try {
            dialog.show();
        }catch (WindowManager.BadTokenException ignored){}

    }

    private void RateAPi(String rating, String teacherid)
    {
        if (NetworkStateCheck.isNetworkAvaliable(Objects.requireNonNull(getActivity()))) {
            Singleton.getInstance().getApi().StudentReview(rating, teacherid, new Callback<TeacherReview_Modle>() {
                @Override
                public void success(TeacherReview_Modle teacherReview_modle, Response response) {
                    String status = teacherReview_modle.getSuccess();
                    try {
                        if (status.equals("1")) {
                            Toast.makeText(getActivity(), "" + teacherReview_modle.getMessage(), Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getActivity(), "" + teacherReview_modle.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception ignored) {
                    }
                }
                @Override
                public void failure(RetrofitError error) {
                }
            });
        }else {
            Toast.makeText(getContext(), "Internet connection problem", Toast.LENGTH_SHORT).show();
        }
    }

public void MuteLocalpaticipate()
{
    String type = listlist[2];
    if (type.equals(ConstantCommand.Profile_Type_T)) {
    } else {
        if (localAudioTrack != null) {
            localAudioTrack.enable(false);
        }
    }
}
 public void UnMuteLocalpaticipate(){
        String type =   listlist[2];
        if (type.equals(ConstantCommand.Profile_Type_T))
        {}else {
            if(localAudioTrack!=null)
            {localAudioTrack.enable(true);}
        }
    }
    public void Endfragment() {
        if (room!=null){
            room.disconnect();
            Intent intent = new Intent(getActivity(), Home_screen.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public void setAcesstoken(String acesstoken) {
        this.acesstoken = acesstoken;
    }

    public String getAcesstoken() {
        return acesstoken;
    }

    public boolean isStudentMute() {
        return StudentMute;
    }

    public boolean isAudioMute() {
        return audioMute;
    }

    public void setAudioMute(boolean audioMute) {
        this.audioMute = audioMute;
    }


    public boolean isVideoMute() {
        return videoMute;
    }

    public void setVideoMute(boolean videoMute) {
        this.videoMute = videoMute;
    }

    public void setStudentMute(boolean studentMute) {
        StudentMute = studentMute;
    }


    private void AddParticipant(List<Participant> participants) {

        for (int index = 0; index < participants.size(); index++) {
            addParticipant(participants.get(index));
        }
    }

    private void addParticipant(Participant participant) {


        String participantIdentity = participant.getIdentity();
        if (participant.getVideoTracks().size() > 0)
        {

            addParticipantVidio(participant.getVideoTracks().get(0), participant);

        }

        participant.setListener(participantListner());
    }

    private Participant.Listener participantListner() {
        return new Participant.Listener() {
            @Override
            public void onAudioTrackAdded(Participant participant, AudioTrack audioTrack) {

            }

            @Override
            public void onAudioTrackRemoved(Participant participant, AudioTrack audioTrack) {
            }

            @Override
            public void onVideoTrackAdded(Participant participant, VideoTrack videoTrack) {

                addParticipantVidio(videoTrack, participant);

            }

            @Override
            public void onVideoTrackRemoved(Participant participant, VideoTrack videoTrack) {
            //
                removeParticipantVideo(videoTrack);

            }

            @Override
            public void onAudioTrackEnabled(Participant participant, AudioTrack audioTrack) {

            }

            @Override
            public void onAudioTrackDisabled(Participant participant, AudioTrack audioTrack) {

            }

            @Override
            public void onVideoTrackEnabled(Participant participant, VideoTrack videoTrack) {

                if (TeacherprimaryPaticipant!=null&&primaryPaticipant.getIdentity().equals(participant.getIdentity()))
                   {
                    videoview_msg.setVisibility(View.GONE);
                    }

                if (primaryPaticipant!= null&&primaryPaticipant.getIdentity().equals(participant.getIdentity())) {
                    videoview_msg.setVisibility(View.GONE);

                }

            }

            @SuppressLint("SetTextI18n")
            @Override
            public void onVideoTrackDisabled(Participant participant, VideoTrack videoTrack) {

                if (TeacherprimaryPaticipant!=null&&primaryPaticipant.getIdentity().equals(participant.getIdentity())){

                    videoview_msg.setVisibility(View.VISIBLE);
                    videoview_msg.setText("Teacher has  Stop Sharing Screen");
                }
                if (primaryPaticipant!= null&&primaryPaticipant.getIdentity().equals(participant.getIdentity())) {
                    videoview_msg.setVisibility(View.VISIBLE);
                    videoview_msg.setText("Teacher VideoView is Disable");

                }

            }
        };
    }

    private void addParticipantVidio(VideoTrack videoTrack, Participant participant) {

        if (participant.getIdentity().contains(ConstantCommand.Profile_Type_T) || ! participant.getIdentity().contains(ConstantCommand.Profile_Type_S)){
            moveLocalViewToThumbnailView();
            primaryVideoTracker = videoTrack;
            primaryPaticipant = participant;
            Primary_VideoView.setMirror(false);
            primaryVideoTracker.addRenderer(Primary_VideoView);
        }

        else {

            if (localParticipant.getIdentity().contains(ConstantCommand.Profile_Type_T)){

                twelioView_models.add(new TwelioView_Model(videoTrack, participant));
                madapter.notifyDataSetChanged();
            }
            else {
                twelioView_models.add(new TwelioView_Model(videoTrack, participant));
                madapter.notifyDataSetChanged();
            }
        }

    }

    private void moveLocalViewToThumbnailView() {
        if (Thumb_vidios.getVisibility() == View.GONE)
        {
            Thumb_vidios.setVisibility(View.VISIBLE);
            localVideoTrack.removeRenderer(Primary_VideoView);
            localVideoTrack.addRenderer(Thumb_vidios);
            localVideoView = Thumb_vidios;

            Thumb_vidios.setMirror(cameraCapture.getCameraSource() == CameraCapturer.CameraSource.FRONT_CAMERA);

        }else {
            moveTeacherViewToThumbnailView();
        }
    }

    private void moveTeacherViewToThumbnailView() {
        if (thumbnail_vidio_teacher.getVisibility() == View.GONE)
        {
            TeacherprimaryVideoTracker = primaryVideoTracker;
            TeacherprimaryPaticipant = primaryPaticipant;
            thumbnail_vidio_teacher.setVisibility(View.VISIBLE);
            primaryVideoTracker.removeRenderer(Primary_VideoView);
            primaryVideoTracker.addRenderer(thumbnail_vidio_teacher);
            thumbnail_vidio_teacher.setMirror(cameraCapture.getCameraSource() == CameraCapturer.CameraSource.FRONT_CAMERA);
        }
    }

    private void movePrimaryViewToListView() {
        if (primaryVideoTracker != null || primaryPaticipant != null) {

            assert primaryVideoTracker != null;
            primaryVideoTracker.removeRenderer(Primary_VideoView);

            twelioView_models.add(new TwelioView_Model(primaryVideoTracker, primaryPaticipant));
            madapter.notifyDataSetChanged();
        }

    }

    private void removeParticipant(Participant participant) {

        if (participant.getIdentity().contains(ConstantCommand.Profile_Type_T)||! participant.getIdentity().contains(ConstantCommand.Profile_Type_S))
        {
           if (TeacherprimaryPaticipant!= null&&
                   TeacherprimaryPaticipant.getIdentity().equals(participant.getIdentity()))
           {
                 giveRating();
                return;
            }
            else if (TeacherprimaryPaticipant!=null && primaryPaticipant.getIdentity().equals(participant.getIdentity())){
               removeParticipantVideo(participant.getVideoTracks().get(0));
               moveTeacherThumbnilViewToPrimeryView();

           }else if (TeacherprimaryVideoTracker== null|| !participant.getIdentity().contains(ConstantCommand.Profile_Type_S) ){
               giveRating();
               return;
           }
            return;
        }

        if (calltype.equals(ConstantCommand.callType_Audio)){
            if (room != null) {
                ClosefragmentAudioCall();
                room.disconnect();
                return;
            }
            return;
        }

         if (!twelioView_models.isEmpty()) {

            int temp = 0;

            for (int index = 0; index < twelioView_models.size(); index++) {
                if (twelioView_models.get(index).getPaticipant().getSid().equals(participant.getSid())) {

                    temp = index;
                    twelioView_models.remove(temp);
                    madapter.notifyDataSetChanged();
                    madapter.notifyItemRemoved(temp);
                    madapter.notifyItemRangeChanged(temp, twelioView_models.size());
                    break;
                }

            }
        }
        else if (localParticipant.getSid().equals(participant.getSid())) {
            return;

        }
        else if (primaryPaticipant.getSid().equals(participant.getSid())) {

            if (!twelioView_models.isEmpty()) {

                removeParticipantVideo(participant.getVideoTracks().get(0));

                Participant Paticipantindex = twelioView_models.get(0).getPaticipant();
                VideoTrack videoTrackindex = twelioView_models.get(0).getVideoTrack();
                primaryVideoTracker = videoTrackindex;
                primaryPaticipant = Paticipantindex;
                Primary_VideoView.setMirror(false);
                primaryVideoTracker.addRenderer(Primary_VideoView);
                twelioView_models.remove(0);
                madapter.notifyDataSetChanged();
            }
            else {
                removeParticipantVideo(participant.getVideoTracks().get(0));
                moveLocalViewToPrimeryView();

            }
        }
    }


    private void removeParticipantVideo(VideoTrack videoTrack) {

try {

    if (primaryVideoTracker.getTrackId().equals(videoTrack.getTrackId())) {
        videoTrack.removeRenderer(Primary_VideoView);
    } else if (localVideoTrack.getTrackId().equals(videoTrack.getTrackId())) {
    } else if (TeacherprimaryVideoTracker.getTrackId().equals(videoTrack.getTrackId())) {
        videoTrack.removeRenderer(thumbnail_vidio_teacher);
    } else if (!twelioView_models.isEmpty()) {
        int temp = 0;
        for (int index = 0; index < twelioView_models.size(); index++) {
            if (twelioView_models.get(index).getVideoTrack().getTrackId()
                    .equals(videoTrack.getTrackId())) {
                temp = index;
                break;
            }
        }
        twelioView_models.remove(temp);
        madapter.notifyDataSetChanged();
    }
}catch (NullPointerException ignored){}
catch (Exception ignored){}
    }


    private void moveTeacherThumbnilViewToPrimeryView() {
        if (thumbnail_vidio_teacher.getVisibility() == View.VISIBLE) {
            thumbnail_vidio_teacher.setVisibility(View.GONE);

            if (TeacherprimaryVideoTracker != null)
            {
                TeacherprimaryVideoTracker.removeRenderer(thumbnail_vidio_teacher);
                TeacherprimaryVideoTracker.addRenderer(Primary_VideoView);

                  primaryVideoTracker = TeacherprimaryVideoTracker;
                  primaryPaticipant = TeacherprimaryPaticipant;

                TeacherprimaryVideoTracker = null;
                TeacherprimaryPaticipant = null;
            }
            Primary_VideoView.setMirror(cameraCapture.getCameraSource() == CameraCapturer.CameraSource.FRONT_CAMERA);
        }
    }

    private void moveLocalViewToPrimeryView() {
        if (Thumb_vidios.getVisibility() == View.VISIBLE) {
            Thumb_vidios.setVisibility(View.GONE);
            if (localVideoTrack != null) {
                localVideoTrack.removeRenderer(Thumb_vidios);
                localVideoTrack.addRenderer(Primary_VideoView);
            }
            localVideoView = Primary_VideoView;
            Primary_VideoView.setMirror(cameraCapture.getCameraSource() == CameraCapturer.CameraSource.FRONT_CAMERA);
        }
    }

    private void configureAudio(boolean b) {
        if (b) {
            previousAudioMode = audioManager.getMode();
            requestAudioFocus();
            audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
            previousMicrophoneMute = audioManager.isMicrophoneMute();
            audioManager.setMicrophoneMute(false);

        } else {
            audioManager.setMode(previousAudioMode);
            audioManager.abandonAudioFocus(null);
            audioManager.setMicrophoneMute(previousMicrophoneMute);
        }
    }

    private void requestAudioFocus() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            AudioAttributes playBackAttribute = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_VOICE_COMMUNICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                    .build();
            AudioFocusRequest focusRequest =  new AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN_TRANSIENT)
                    .setAudioAttributes(playBackAttribute)
                    .setAcceptsDelayedFocusGain(true)
                    .setOnAudioFocusChangeListener(focusChange -> {})
                    .build();
            audioManager.requestAudioFocus(focusRequest);

        } else
            {
              audioManager.requestAudioFocus(null, AudioManager.STREAM_VOICE_CALL,
                    AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
        }
    }


    private CameraCapturer.CameraSource getAvailableCameraSource() {
        return (CameraCapturer.isSourceAvailable(CameraCapturer.CameraSource.FRONT_CAMERA)) ?
                (CameraCapturer.CameraSource.FRONT_CAMERA) :
                (CameraCapturer.CameraSource.BACK_CAMERA);
    }


    private boolean checkPermissionForCameraAndMicrophone() {
        int resultCamera = ContextCompat.checkSelfPermission(Objects.requireNonNull(getContext()), Manifest.permission.CAMERA);

        int resultMic = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.RECORD_AUDIO);

        return resultCamera == PackageManager.PERMISSION_GRANTED &&
                resultMic == PackageManager.PERMISSION_GRANTED;
    }


    private void requestPermissionForCameraAndMicrophone() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(Objects.requireNonNull(getActivity()), Manifest.permission.CAMERA)
                ||
                ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                        Manifest.permission.RECORD_AUDIO))
        {
            checkStorageReadPermition(getActivity());
            Toast.makeText(getContext(), "permissions_needed", Toast.LENGTH_LONG).show();

        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO},

                    CAMERA_MIC_PERMISSION_REQUEST_CODE);
        }
    }


    public static void checkStorageReadPermition(Activity activity){

        if (Build.VERSION.SDK_INT>Build.VERSION_CODES.M){

            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){
                Toast.makeText(activity, "Allow Camera & Mic permission first from App info", Toast.LENGTH_SHORT).show();
                Intent intent  =  new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" +activity.getPackageName()));
                activity.startActivity(intent);
            }
        }
    }
    @SuppressLint("ClickableViewAccessibility")
    private  void TouchListner_and_keyEvent(View rootView )
    {
        rootView.setFocusableInTouchMode(true);
        rootView.requestFocus();
        rootView.setOnKeyListener((View view, int keyCode, KeyEvent keyEvent)-> {
            if (keyCode==KeyEvent.KEYCODE_BACK){
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Objects.requireNonNull(getContext()));
                alertDialogBuilder.setPositiveButton("Leave Class",
                        (dialogInterface, i) -> {
                            if (room != null)
                            {
                                room.disconnect();
                            }
                        });
                alertDialogBuilder.setNegativeButton("Cancel", (dialogInterface, i) -> dialogInterface.dismiss());
                return true;
            }
            return false;
        });


        rootView.setOnTouchListener((v, event) -> {

            SGD.onTouchEvent(event);
            return true;
        });

    }

    class Scale_listner implements ScaleGestureDetector.OnScaleGestureListener{
        private int cW, cH;
        @Override
        public boolean onScale(ScaleGestureDetector detector) {

            cH*=detector.getScaleFactor();
            cW*=detector.getScaleFactor();
            if (cW < MIN_WIDTH)
            { // limits width
                cW = Primary_VideoView.getWidth();
                cH = Primary_VideoView.getHeight();
            }
            Primary_VideoView.getHolder().setFixedSize(cW,cH);
            Primary_VideoView.getLayoutParams().height = cH;
            Primary_VideoView.getLayoutParams().width = cW;
            return true;
        }

        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {
            cW = Primary_VideoView.getWidth();
            cH = Primary_VideoView.getHeight();
            return true;
        }

        @Override
        public void onScaleEnd(ScaleGestureDetector detector) {

        }
    }

}