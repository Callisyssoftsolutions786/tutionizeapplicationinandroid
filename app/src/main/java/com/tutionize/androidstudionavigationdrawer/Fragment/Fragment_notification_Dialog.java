package com.tutionize.androidstudionavigationdrawer.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.Toast;

import com.tutionize.androidstudionavigationdrawer.Adapter.AdapterNoutification;
import com.tutionize.androidstudionavigationdrawer.Constant.Singleton;
import com.tutionize.androidstudionavigationdrawer.Model.Dtabase_Notification_modle;
import com.tutionize.androidstudionavigationdrawer.Model.delete_notification_modle;
import com.tutionize.androidstudionavigationdrawer.Model.notificationdata_modle;
import com.tutionize.androidstudionavigationdrawer.R;
import com.tutionize.androidstudionavigationdrawer.Sharepool.MySharePrafranceClass;
import com.tutionize.androidstudionavigationdrawer.Sharepool.NetworkStateCheck;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * Created by user on 22/12/17.
 */

@SuppressLint("ValidFragment")
public class
Fragment_notification_Dialog extends DialogFragment {

    private ImageView notification_back;
    private RecyclerView notificationRecycle;
    private AdapterNoutification adapterNoutification;
    private List<Dtabase_Notification_modle> notificationlist = new ArrayList<>();
    @SuppressLint("StaticFieldLeak")
    public static Fragment_notification_Dialog fragment_notification_Dialog;
    @SuppressLint("ValidFragment")
    public Fragment_notification_Dialog(){
        super();
    }

    @SuppressLint("ResourceAsColor")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.notification_fragment_out,container);
        String[] list = MySharePrafranceClass.GetSharePrefrance(this.getActivity());
        String userid = list[0];
        findView(view);
        Notification(userid);
        Objects.requireNonNull(this.getDialog().getWindow()).requestFeature(Window.FEATURE_NO_TITLE);
        fragment_notification_Dialog=this;
        onclick();
        return view;
    }

    private void Notification(String userid) {

        if (NetworkStateCheck.isNetworkAvaliable(Objects.requireNonNull(getActivity()))) {
            Singleton.getInstance().getApi().getNotification(userid, new Callback<notificationdata_modle>() {
                @Override
                public void success(notificationdata_modle notificationdata_modle, Response response) {
                    if (notificationdata_modle.getMsg().getSuccess().equals("1")) {
                        notificationlist = notificationdata_modle.getData();
                        adapter();
                    } else {
                        Toast.makeText(Fragment_notification_Dialog.this.getActivity(), "Some Error", Toast.LENGTH_SHORT).show();
                    }
                }
                @Override
                public void failure(RetrofitError error) {
                }
            });
        }else {
            Toast.makeText(getContext(), "Internet connection problem", Toast.LENGTH_SHORT).show();
        }
    }
    private void onclick() {
        notification_back.setOnClickListener(v-> this.getDialog().dismiss());
    }
    private void findView(View view) {

        notificationRecycle= view.findViewById(R.id.notification_dialog_recycle);
        notification_back = view.findViewById(R.id.notification_back);
    }
    private void adapter(){
        try {
            int resId = R.anim.layout_animation;
            LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getContext(), resId);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            notificationRecycle.setLayoutManager(linearLayoutManager);
            adapterNoutification = new AdapterNoutification(this.getActivity(), notificationlist);
            notificationRecycle.setAdapter(adapterNoutification);
            notificationRecycle.setLayoutAnimation(animation);
            adapterNoutification.notifyDataSetChanged();
        }catch(Exception ignored){}

        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0,ItemTouchHelper.LEFT|ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction)
            {
                try {
                    String id = notificationlist.get(viewHolder.getAdapterPosition()).getId();
                    deletenotification(id,viewHolder);
                }catch (Exception ignored){}
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(notificationRecycle);
    }
    private void deletenotification(String id, RecyclerView.ViewHolder viewHolder) {
        if (NetworkStateCheck.isNetworkAvaliable(Objects.requireNonNull(getActivity()))) {
            Singleton.getInstance().getApi().deletenotification(id, new Callback<delete_notification_modle>() {
                @Override
                public void success(delete_notification_modle delete_notification_modle, Response response) {
                    if (delete_notification_modle.getSuccess().equals("1")) {
                        int position = viewHolder.getAdapterPosition();
                        try {
                            notificationlist.remove(position);
                            adapterNoutification.notifyItemChanged(position);
                            adapterNoutification.notifyItemRangeChanged(position, notificationlist.size());
                            adapterNoutification.notifyDataSetChanged();
                        }
                        catch (Exception ignored) {}
                    }  //Toast.makeText(getActivity(), " Some Error ", Toast.LENGTH_SHORT).show();

                }
                @Override
                public void failure(RetrofitError error) {
                   // Toast.makeText(getActivity(), " Some Error " + error, Toast.LENGTH_SHORT).show();
                }
            });
        }else {
            Toast.makeText(getContext(), "Internet connection problem", Toast.LENGTH_SHORT).show();
        }
    }
}
