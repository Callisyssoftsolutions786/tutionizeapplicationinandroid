package com.tutionize.androidstudionavigationdrawer.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.tutionize.androidstudionavigationdrawer.Constant.Singleton;
import com.tutionize.androidstudionavigationdrawer.Model.ChangeEmail_modle;
import com.tutionize.androidstudionavigationdrawer.ProgressDialog;
import com.tutionize.androidstudionavigationdrawer.R;
import com.tutionize.androidstudionavigationdrawer.Sharepool.MySharePrafranceClass;
import com.tutionize.androidstudionavigationdrawer.Sharepool.NetworkStateCheck;

import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * Created by android on 10/12/17.
 */

public class Fragment_Change_Email extends Fragment {
    private Button bt_ChangeEmail;
    private EditText edt_New_email, edt_Old_email;
    private String[] list,logindata;
    private String txt_NewEmail,txt_OldEmail;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       View rootView = inflater.inflate(R.layout.fragment_change_email, container, false);
        list= MySharePrafranceClass.GetSharePrefrance(getActivity());
        findView(rootView);
        ShareElement();
        ClickAble();
        return rootView;
    }
    private void ClickAble() {
        bt_ChangeEmail.setOnClickListener((View view)-> {

                txt_NewEmail  = edt_New_email.getText().toString();
                txt_OldEmail  = edt_Old_email.getText().toString();

            if (!validate()){
                    Toast.makeText(getActivity(), "Your Request Change Email Has Failed ", Toast.LENGTH_SHORT).show();
                }else {

                    String userid=list[0];
                    String password = logindata[1];
                    String Type = logindata[2];
                    String Country = logindata[3];
                    String City = logindata[4];
                    ChangeEmail(userid,txt_NewEmail,password, Type,Country, City);}
            });
    }

    @SuppressLint("ResourceAsColor")
    private void ChangeEmail(String userid, String txt_newEmail, String password,String Type,
                             String Country, String City) {
        if (NetworkStateCheck.isNetworkAvaliable(Objects.requireNonNull(getActivity()))) {
            KProgressHUD knprogress = ProgressDialog.getInstance().getKNProgressDialog(getActivity());
            knprogress.show();

            Singleton.getInstance().getApi().ChangeEmail(userid, txt_newEmail, new Callback<ChangeEmail_modle>() {
                @Override
                public void success(ChangeEmail_modle changeEmail_modle, Response response) {
                    if (changeEmail_modle.getSuccess().equals("1")) {

                        MySharePrafranceClass.ClearAllLoginData(Objects.requireNonNull(getActivity()));

                        edt_Old_email.setText("");
                        edt_New_email.setText("");
                        knprogress.dismiss();
                        MySharePrafranceClass.LoginDataShare(getActivity(), txt_newEmail, password, Type, Country, City);
                        Toast.makeText(getActivity(), "" + changeEmail_modle.getResponse(), Toast.LENGTH_SHORT).show();
                        getActivity().onBackPressed();

                    } else {
                        knprogress.dismiss();
                        Toast.makeText(getActivity(), "" + changeEmail_modle.getResponse(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    knprogress.dismiss();
                    Toast.makeText(getActivity(), "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }else {
            Toast.makeText(getContext(), "Internet connection problem", Toast.LENGTH_SHORT).show();
        }
    }

    private void findView(View rootView) {
        bt_ChangeEmail=(Button) rootView.findViewById(R.id.ce_bt_changeemail_submit);
        edt_New_email=(EditText)rootView.findViewById(R.id.ce_edt_changeEmail_New);
        edt_Old_email=(EditText)rootView.findViewById(R.id.ce_edt_changeEmail_Old);
    }
    //------------------------------------helper method------------------------------------

    private boolean validate()
    {
        boolean valid=true;
        if (txt_OldEmail.isEmpty()||!Patterns.EMAIL_ADDRESS.matcher(txt_OldEmail).matches()||!txt_OldEmail.equals(logindata[0]))
        {
            edt_Old_email.setError("Enter Valid Old Email");
            valid=false;
        }
        if (txt_NewEmail.isEmpty()||!Patterns.EMAIL_ADDRESS.matcher(txt_NewEmail).matches()){
            edt_New_email.setError("Enter Valid New  Email");
            valid=false;
        }
        if (txt_OldEmail.equals(txt_NewEmail))
        {
            edt_New_email.setError("Choose Diffrent Email");
            valid=false;
        }
        return valid;
    }
    private void ShareElement() {
        logindata=MySharePrafranceClass.LoginDataGetSharePrefrance(Objects.requireNonNull(getContext()));
        list= MySharePrafranceClass.GetSharePrefrance(getActivity());
        String email=logindata[0];
        if (!(email==null)){
            edt_Old_email.setText(email);
        }
        else {
            edt_Old_email.setText("");
        }
    }

}
