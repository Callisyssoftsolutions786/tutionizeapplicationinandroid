package com.tutionize.androidstudionavigationdrawer.Fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.tutionize.androidstudionavigationdrawer.Constant.AppConst;
import com.tutionize.androidstudionavigationdrawer.R;



/**
 * Created by android on 11/20/17.
 */

public class Fragment_video_lacture extends Fragment {
    private TextView txt_videotitle,txt_videodescription,txt_teachername;
    private VideoView VideoLecture;
    private ProgressDialog pDialog;
    private FrameLayout framelayout;
    private ProgressBar progressVideoStremming;

    @SuppressLint("ResourceAsColor")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_video_lacture, container, false);
        findView(rootView);
        clickable();

        Bundle mBundle = getArguments();
        if (!(mBundle == null)) {


            String video_id = mBundle.getString("video_id");
            String video_title = mBundle.getString("video_title");
            String video_url = mBundle.getString("video_url");
            String video_description = mBundle.getString("video_description");
            String teacher_name = mBundle.getString("teacher_name");

            if (teacher_name !=null){
                if (!(teacher_name.equals(""))){
                    txt_teachername.setText(teacher_name);
                }
            }
            if (video_description !=null){
                if (!(video_description.equals(""))){
                    txt_videodescription.setText(video_description);
                }
            }
            if (video_title !=null){
                if (!(video_title.equals(""))){
                    txt_videotitle.setText(video_title);
                }
            }
            if (video_url !=null){
                if (!(video_url.equals(""))){
                    String VideoURL= AppConst.videoconstant.concat(video_url);

                    progressVideoStremming.setVisibility(View.VISIBLE);

                    try {

                        MediaController mediacontroller = new MediaController(getActivity());
                        //mediacontroller.setLayoutParams();
                        mediacontroller.setAnchorView(VideoLecture);

                        Uri video = Uri.parse(VideoURL);
                        VideoLecture.setMediaController(mediacontroller);
                        VideoLecture.setVideoURI(video);

                    } catch (Exception e) {
                        Log.e("Error", e.getMessage());
                        e.printStackTrace();
                    }
                    VideoLecture.requestFocus();
                    VideoLecture.setOnPreparedListener(mp -> {

                        progressVideoStremming.setVisibility(View.GONE);
                        VideoLecture.start();
                    });
                }
            }
        }
        return rootView;
    }

    @SuppressLint("ClickableViewAccessibility")
    private void clickable() {
        framelayout.setOnTouchListener((v, event) -> false);
    }
    private void findView(View rootView)
    {
        progressVideoStremming  = (ProgressBar)rootView.findViewById(R.id.progressVideoStremming);
        framelayout=(FrameLayout)rootView.findViewById(R.id.framelayout);
        txt_videotitle= (TextView) rootView.findViewById(R.id.video_lacture_video_title);
        txt_videodescription= (TextView) rootView.findViewById(R.id.video_lacture_discription);
        txt_teachername = (TextView) rootView.findViewById(R.id.video_lacture_teacher_name);
        VideoLecture=(VideoView)rootView.findViewById(R.id.video_lac_videoView);
    }
}
