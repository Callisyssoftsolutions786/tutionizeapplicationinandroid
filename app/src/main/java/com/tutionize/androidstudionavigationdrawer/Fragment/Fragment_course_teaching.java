package com.tutionize.androidstudionavigationdrawer.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.tutionize.androidstudionavigationdrawer.Adapter.Adapter_All_Active_Student;
import com.tutionize.androidstudionavigationdrawer.Constant.Singleton;
import com.tutionize.androidstudionavigationdrawer.Model.HireStudentList_Result;
import com.tutionize.androidstudionavigationdrawer.Model.HireStudentList_modle;
import com.tutionize.androidstudionavigationdrawer.ProgressDialog;
import com.tutionize.androidstudionavigationdrawer.R;
import com.tutionize.androidstudionavigationdrawer.Sharepool.MySharePrafranceClass;
import com.tutionize.androidstudionavigationdrawer.Sharepool.NetworkStateCheck;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by hp on 01-03-2017.
 */
public class Fragment_course_teaching extends Fragment
{
    private View rootView;
    private RecyclerView course_teaching;
    private LinearLayoutManager layoutManager;
    private Adapter_All_Active_Student mTeacher_list;
    private List<HireStudentList_Result> StudentList=new ArrayList<>();
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_course_teaching, container, false);
        initialize();
        Shareable();

        return rootView;
    }

    private void Shareable() {
        String[] list = MySharePrafranceClass.GetSharePrefrance(getActivity());
        String teacher= list[0];
        HireStudent(teacher);
    }

    @SuppressLint("ResourceAsColor")
    private void HireStudent(String teacherid){
        if (NetworkStateCheck.isNetworkAvaliable(Objects.requireNonNull(getActivity())))
        {
            KProgressHUD knprogress = ProgressDialog.getInstance().getKNProgressDialog(getActivity());
            knprogress.show();
            Singleton.getInstance().getApi().HireStudent(teacherid, new Callback<HireStudentList_modle>() {
                @Override
                public void success(HireStudentList_modle hireStudentList_modle, Response response) {
                    if (hireStudentList_modle.getSuccess().equals("1")) {
                        StudentList = hireStudentList_modle.getResult();

                        if (StudentList != null) {
                            layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                            course_teaching.setLayoutManager(layoutManager);
                            mTeacher_list = new Adapter_All_Active_Student(getActivity(), StudentList);
                            course_teaching.setAdapter(mTeacher_list);
                            mTeacher_list.notifyDataSetChanged();
                        } else {
                            Toast.makeText(getActivity(), "No one hired you", Toast.LENGTH_SHORT).show();
                        }
                        knprogress.dismiss();

                    } else {
                        knprogress.dismiss();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    knprogress.dismiss();
                    Toast.makeText(getActivity(), "" + error, Toast.LENGTH_SHORT).show();
                }
            });
        }else {
            Toast.makeText(getContext(), "Internet connection problem", Toast.LENGTH_SHORT).show();
        }
    }

    public void initialize() {

        course_teaching = (RecyclerView)rootView.findViewById(R.id.rcy_course_teaching);
        course_teaching.setHasFixedSize(true);
    }

}
