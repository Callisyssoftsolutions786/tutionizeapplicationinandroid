package com.tutionize.androidstudionavigationdrawer.Fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.eminayar.panter.PanterDialog;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.rey.material.app.BottomSheetDialog;
import com.tutionize.androidstudionavigationdrawer.Activity_Screen.Home_screen;
import com.tutionize.androidstudionavigationdrawer.Activity_Screen.Registration_Screen;
import com.tutionize.androidstudionavigationdrawer.Adapter.Adapter_Teacher_detail;
import com.tutionize.androidstudionavigationdrawer.Constant.AppConst;
import com.tutionize.androidstudionavigationdrawer.Constant.Singleton;
import com.tutionize.androidstudionavigationdrawer.Model.GetVideo_model;
import com.tutionize.androidstudionavigationdrawer.Model.HireTeacher_modle;
import com.tutionize.androidstudionavigationdrawer.Model.LoginModel;
import com.tutionize.androidstudionavigationdrawer.Model.PaymentProff_Modle;
import com.tutionize.androidstudionavigationdrawer.ProgressDialog;
import com.tutionize.androidstudionavigationdrawer.R;
import com.tutionize.androidstudionavigationdrawer.Sharepool.MySharePrafranceClass;
import com.tutionize.androidstudionavigationdrawer.Sharepool.NetworkStateCheck;
import com.tutionize.androidstudionavigationdrawer.command.ConstantCommand;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by hp on 10-02-2017.
 */
public class Teacher_Detail_Screen extends Fragment {

    private static final int REQUEST_CODE_FUTURE_PAYMENT = 786;
    public static final int PAYPAL_REQUEST_CODE = 123;
    private List<com.tutionize.androidstudionavigationdrawer.Model.GetVideo_Thumb_modle> GetVideo_Thumb_modle = new ArrayList<>();
    private EditText Email,Password;
    public TextView hire_button;
    private TextView txt_teacherName,txt_review,txt_teacherBio, txt_teacherQualifiction,txt_languages;
    private ImageView img_teacherImage;

    private String teacher_course;
    private String[] list;
    private String teacherid,teacher_fee,teacher_image;

    // Sandbox  Client Id
   //private final String clientId =
   //         "AZD3q3uT7otKvvckfiGhBc-6WPpNs4XOXbiJgqgvLzW5ZttbQI4-1_OYy3LFanlKlP6sC0TNdx8DtnIT";

    private RecyclerView mRecyclerView_teacher_video;
    private RelativeLayout mLayout_hire, layout_classhere;
    private boolean hireCurrentStudent=false;


    private PayPalConfiguration mconfigration;
    @SuppressLint("StaticFieldLeak")
    public static Teacher_Detail_Screen teacher_detail_screen;
    private ArrayList<String> Student;
    private ArrayList<String> PandingStudent;

    public Teacher_Detail_Screen() {
    }


    @SuppressLint("SetTextI18n")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.teacher_detail_screen, container, false);
        configPaypal();
        initialize(rootView);
        clickable(rootView);
        teacher_detail_screen = this;
        Bundle mBundle = getArguments();
        if (!(mBundle == null))
        {
            teacherid = mBundle.getString("teacher_id");
            String teacher_name = mBundle.getString("teacher_name");
            String teacher_bio = mBundle.getString("teacher_bio");
            String teacher_Qualification = mBundle.getString("teacher_qualification");
            teacher_image = mBundle.getString("teacher_image");
            teacher_course = mBundle.getString("teacher_course");
            Student=mBundle.getStringArrayList("Student");
            PandingStudent= mBundle.getStringArrayList("PandingStudent");
            teacher_fee = mBundle.getString("fee");
            String review = mBundle.getString("review");
            String languages = mBundle.getString("languages");
            String country = mBundle.getString("country");
            if (!(teacher_image == null)) {
                if (!(teacher_image.equals(""))) {
                    String pic = AppConst.photoconstant.concat(teacher_image);
                    Glide.with(getActivity()).load(pic).into(img_teacherImage);
                } else {
                    Glide.with(getActivity()).load(R.drawable.userimagewhite).into(img_teacherImage);
                }
            } else {
                Glide.with(getActivity()).load(R.drawable.userimagewhite).into(img_teacherImage);
            }
            txt_teacherName.setText(teacher_name);
            txt_teacherBio.setText(teacher_bio);
            txt_teacherQualifiction.setText(teacher_Qualification);
            if (review !=null){
                txt_review.setText(review);
            }
            else {
                txt_review.setText("1.5");
            }
            if (languages !=null&& !languages.equals("")){
                txt_languages.setText(languages);
                if (country!=null && !country.equals(""))
                {
                    txt_languages.setText(languages+" , "+country);
                }
                else {
                    txt_languages.setText(languages+" , "+"No country");
                }
            }
            else {
                txt_languages.setText("No language");
                if (country!=null&&!country.equals(""))
                {
                    txt_languages.setText("No language , "+country);
                }
                else {
                    txt_languages.setText("No language , "+"No country");
                }
            }

        }
        if (teacherid != null) {
            if (!(teacherid.equals("")))
            {
                if (teacherid.equals(list[0]))
                {
                    mLayout_hire.setVisibility(View.GONE);
                }
                getTeacherVideo(teacherid);
            }
        }

        //========================================================

       if (list[0]!=null) {
            String userid=list[0];

          for (String e : Student) {

                     if(userid.equals(e)){
                         mLayout_hire.setVisibility(View.GONE);
                     }
             }
             for (String S : PandingStudent) {

               if(userid.equals(S))
               {
                   setHireCurrentStudent(true);
                   hire_button.setText("Complete Your Payment");

               }
           }

        }

        //====================================================

        return rootView;
    }


    @Override
    public void onDestroy() {
        Objects.requireNonNull(getActivity()).stopService(new Intent(getActivity(),PayPalService.class));
        super.onDestroy();
    }


    public boolean isHireCurrentStudent() {
        return hireCurrentStudent;
    }

    public void setHireCurrentStudent(boolean hireCurrentStudent) {
        this.hireCurrentStudent = hireCurrentStudent;
    }

    private void StartBottemDeletemenuMenu()
    {
        int styleId = R.style.MyAlertDialogStyle1;
        BottomSheetDialog dialogView = new BottomSheetDialog(getActivity());
        int view = R.layout.login_register_dialog;
        dialogView.applyStyle(styleId)
                .contentView(view)
                .heightParam(ViewGroup.LayoutParams.WRAP_CONTENT)
                .inDuration(300)
                .cancelable(false);

        Email = dialogView.findViewById(R.id.dialog_txt_email_login);
        Password = dialogView.findViewById(R.id.dialog_txt_password_login);
        com.rey.material.widget.ImageView Close_dialog = dialogView.findViewById(R.id.Close_dialog);
        com.rey.material.widget.Button LoginButton = dialogView.findViewById(R.id.dialog_btn_login);
        Close_dialog.setOnClickListener(v-> dialogView.dismiss());

        LoginButton.setOnClickListener(v-> {
            String txt_email = Email.getText().toString();
            String txt_password = Password.getText().toString();
            if (txt_email.trim().equals("")) {
                Toast.makeText(getActivity(), "Enter Email Address", Toast.LENGTH_SHORT).show();
            } else if (txt_password.trim().equals("")) {
                Toast.makeText(getActivity(), "Enter Password", Toast.LENGTH_SHORT).show();
            }
            else {
                User_login(txt_email, txt_password, dialogView);
            }
        });
        com.rey.material.widget.Button RegisterButton = dialogView.findViewById(R.id.dialog_btn_register1);

        RegisterButton.setOnClickListener(v -> {
            Intent intent = new Intent(Objects.requireNonNull(getActivity()).getApplication(), Registration_Screen.class);
            startActivity(intent);
            dialogView.dismiss();
        });

        dialogView.show();
    }

    private void clickable(View view) {
        mLayout_hire.setOnClickListener(view1 -> {
            if (list[0]==null)
            {
                StartBottemDeletemenuMenu();

            } else {
                if (hireCurrentStudent) {
                    Pay(view1);
                }
                else
                    {
                        HireTeacher(teacherid, list[0]);
                    }
            }
        });

    }
    private void HireTeacher(String teacherid, String s) {
        if (NetworkStateCheck.isNetworkAvaliable(Objects.requireNonNull(getActivity()))) {
            Singleton.getInstance().getApi().HireTeacher(teacherid, s, new Callback<HireTeacher_modle>() {
                @Override
                public void success(HireTeacher_modle hireTeacher_modle, Response response) {
                    if (hireTeacher_modle.getSuccess().equals("1")) {
                        mLayout_hire.setVisibility(View.GONE);
                        Toast.makeText(getContext(), "" + hireTeacher_modle.getMessage(), Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(getContext(), "" + hireTeacher_modle.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                }
            });
        }else {
            Toast.makeText(getActivity(), "Intenet connection problem", Toast.LENGTH_SHORT).show();
        }
    }


    private void User_login(final String getemail, final String getpassword, BottomSheetDialog dialogView)
    {

        if (NetworkStateCheck.isNetworkAvaliable(Objects.requireNonNull(getActivity()))) {

            KProgressHUD kpprogress = ProgressDialog.getInstance().getKNProgressDialog(getActivity());
            kpprogress.show();
            Singleton.getInstance().getApi().userlogin(getemail, getpassword, new Callback<LoginModel>() {
                @Override
                public void success(LoginModel loginModel, Response response) {

                    String status = loginModel.getSuccess();
                    String message = loginModel.getMessage();

                    if (status.equalsIgnoreCase("1")) {
                        String userid = loginModel.getUserid();
                        String username = loginModel.getName();
                        String photo = loginModel.getThumb();

                        String Type = loginModel.getType();
                        String contry = loginModel.getCountry();
                        String city = loginModel.getCity();

                        MySharePrafranceClass.LoginDataShare(Objects.requireNonNull(getActivity()), getemail, getpassword, Type, contry, city);// save email and password in share prefrace
                        MySharePrafranceClass.Share(getActivity(), userid, username, photo);// save login id,name,photo in share prefrace


                        Intent intent = new Intent(getActivity().getApplication(), Home_screen.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        startActivity(intent);
                        getActivity().finish();

                        dialogView.dismiss();

                        Toast.makeText(getActivity(), "" + message, Toast.LENGTH_SHORT).show();

                    } else {

                        Toast.makeText(getActivity(), "" + message, Toast.LENGTH_SHORT).show();
                    }
                    kpprogress.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    kpprogress.dismiss();
                    Toast.makeText(getActivity(), "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }else {
            Toast.makeText(getActivity(), "Internet connection problem", Toast.LENGTH_SHORT).show();
        }
    }

    private void getTeacherVideo(String teacherid) {
        if (NetworkStateCheck.isNetworkAvaliable(Objects.requireNonNull(getActivity()))) {
            Singleton.getInstance().getApi().getTeacherVideo(teacherid, new Callback<GetVideo_model>() {
                @Override
                public void success(GetVideo_model getVideo_model, Response response) {
                    if (getVideo_model.getSuccess().equals("1")) {
                        GetVideo_Thumb_modle = getVideo_model.getThumb();
                        mRecyclerView_teacher_video.setHasFixedSize(true);
                        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                        mRecyclerView_teacher_video.setLayoutManager(layoutManager);
                        Adapter_Teacher_detail mTeacher_detail = new Adapter_Teacher_detail(getActivity(), GetVideo_Thumb_modle);
                        mRecyclerView_teacher_video.setAdapter(mTeacher_detail);
                    }
                }
                @Override
                public void failure(RetrofitError error) {
//                Toast.makeText(getActivity(), "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }else {
            Toast.makeText(getActivity(), "Internet connection problem", Toast.LENGTH_SHORT).show();
        }
    }

    public void initialize(View view) {
        txt_languages = view.findViewById(R.id.teacher_detail_languages);
        txt_teacherBio = view.findViewById(R.id.teacher_detail_teacher_Bio);
        txt_review = view.findViewById(R.id.teacher_detail_text_number);
        txt_teacherName = view.findViewById(R.id.teacher_detail_text_name);
        txt_teacherQualifiction = view.findViewById(R.id.teacher_detail_Qualification_txt);
        img_teacherImage = view.findViewById(R.id.teacher_detail_teacherImage);
        mRecyclerView_teacher_video = view.findViewById(R.id.rcy_teacher_video);
        mLayout_hire = view.findViewById(R.id.layout_hire_him);
        hire_button= view.findViewById(R.id.hire_button);
        list = MySharePrafranceClass.GetSharePrefrance(Objects.requireNonNull(getActivity()));
        String[] loginlist = MySharePrafranceClass.LoginDataGetSharePrefrance(Objects.requireNonNull(getContext()));
        if (list[0] !=null) {
            if (loginlist[2]!=null) {
             if (loginlist[2].equals("Teacher")) {
                 mLayout_hire.setVisibility(View.GONE);
             }
            }
        }
    }
    private void Pay(View view) {
        String value;

        if (teacher_fee.equals("0")){
            value = "1";

        }else {
            value = teacher_fee;
        }
        PayPalPayment payment = new PayPalPayment(new BigDecimal(value),"USD","Tuitionize",PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intent = new Intent(getActivity(), PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, mconfigration);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);
        Objects.requireNonNull(getActivity()).startActivityForResult(intent, PAYPAL_REQUEST_CODE);
    }
    public void getPaymentData(Intent data){
        String studentid = list[0];
        PaymentConfirmation confirmation = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
        if (confirmation!=null){
            JSONObject object = confirmation.toJSONObject();
            try
            {
                JSONObject response = object.getJSONObject("response");

                String id = response.getString("id");
                String status = response.getString("state");
                String createTime = response.getString("create_time");

                ProgressBotemSheet(id,status,createTime,""+teacher_fee,studentid,teacher_course);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }
    }

    private void ProgressBotemSheet(String id, String status, String createTime, String amout, String studentid, String teacher_course) {
        paymentProff(id,studentid,teacherid,createTime,status, teacher_course,amout);
    }
    @SuppressLint("SetTextI18n")
    private void Botemsheetready(String payment_id, String student_id, String teacher_id, String payment_time, String payment_status, String course, String payment_amount) {
        mLayout_hire.setVisibility(View.GONE);
        int styleId = R.style.MyAlertDialogStyle1;
        BottomSheetDialog mDialog = new BottomSheetDialog(getActivity());
        int view = R.layout.fragment_paypal;

        mDialog.applyStyle(styleId).contentView(view).heightParam(ViewGroup.LayoutParams.WRAP_CONTENT)
                .inDuration(300)
                .cancelable(false);
        com.rey.material.widget.TextView paymentId = mDialog.findViewById(R.id.paymentid);
        com.rey.material.widget.TextView paymentStatus = mDialog.findViewById(R.id.payment_status);
        com.rey.material.widget.TextView paymentTime = mDialog.findViewById(R.id.payment_Time);
        com.rey.material.widget.TextView paymentamount = mDialog.findViewById(R.id.payment_amount);
        com.rey.material.widget.ImageView cross = mDialog.findViewById(R.id.Close_payment_dialog);

        CircleImageView paystudentimage = mDialog.findViewById(R.id.paystudentimage);
        CircleImageView payteacherImage = mDialog.findViewById(R.id.payteacherImage);


        cross.setOnClickListener(v->{
            mDialog.dismiss();
        });
        if (teacher_image != null)
        {
            if (!teacher_image.equals(""))
            {
                String pic = AppConst.photoconstant.concat(teacher_image);
                Glide.with(getActivity()).load(pic).asGif().into(payteacherImage);
            } else {
                Glide.with(getActivity()).load(R.drawable.userimagewhite).into(payteacherImage);
            }
        }
        else {
            Glide.with(getActivity()).load(R.drawable.userimagewhite).into(payteacherImage);
        }

        if (list[2] != null)
        {
            if (!list[2].equals(""))
            {
                String pic = AppConst.photoconstant.concat(list[2]);
                Glide.with(getActivity()).load(pic).asGif().into(paystudentimage);
            } else {
                Glide.with(getActivity()).load(R.drawable.userimagewhite).into(paystudentimage);
            }
        }
        else {
            Glide.with(getActivity()).load(R.drawable.userimagewhite).into(paystudentimage);
        }
        paymentId.setText(payment_id);
        paymentStatus.setText(payment_status);
        paymentTime.setText(payment_time);
        paymentamount.setText("$"+payment_amount);
        mDialog.show();
    }

    private void configPaypal()
    {
         String clientId = "AZlzFqnZ0QrhWOvxhNv8WSKKzqeg0--wThCIT8wbKPAgB-MjUijQxsBDvA8nSeBT5mKypKJBKGnNAU1o";

        String merchant_Name = "Tuitionize";
        mconfigration = new PayPalConfiguration().environment(PayPalConfiguration.ENVIRONMENT_PRODUCTION).clientId(clientId)
                .merchantName(merchant_Name).merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
                .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));
        Intent m_services = new Intent(getActivity(), PayPalService.class);
        m_services.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION,mconfigration);
        Objects.requireNonNull(getActivity()).startService(m_services);
    }

    @SuppressLint("ResourceAsColor")
    private void paymentProff(String payment_id, String student_id, String teacher_id, String payment_time, String payment_status, String course, String payment_amount){
        if (NetworkStateCheck.isNetworkAvaliable(Objects.requireNonNull(getActivity()))) {
            KProgressHUD knprogress = ProgressDialog.getInstance().getKNProgressDialog(getActivity());
            knprogress.show();
            Singleton.getInstance().getApi().paymenyproff(payment_id, student_id, teacher_id, payment_time, payment_status, course, payment_amount, new Callback<PaymentProff_Modle>() {
                @Override
                public void success(PaymentProff_Modle paymentProff_modle, Response response)
                {
                    if (paymentProff_modle.getSuccess().equals("1")) {
                        knprogress.dismiss();
                        Botemsheetready(payment_id,student_id,teacher_id,payment_time,payment_status,course,payment_amount);
                    } else {
                        Toast.makeText(getActivity(), ""+paymentProff_modle.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    knprogress.dismiss();

                    new PanterDialog(getActivity())
                            .setHeaderBackground(R.drawable.pattern_bg_orange)
                            .setHeaderLogo(R.drawable.logo)
                            .setTitle("Panding Process")
                            .setPositive("I GOT IT & TRY AGAIN", v -> {
                                paymentProff(payment_id, student_id, teacher_id, payment_time, payment_status, course, payment_amount);
                            })
                            .setMessage(ConstantCommand.PANDING_PAYMENT_MSG)
                            .isCancelable(false)
                            .show();
                    }
            });
        }else {
            Toast.makeText(getContext(), "Internet connection problem", Toast.LENGTH_SHORT).show();
        }
    }
}
