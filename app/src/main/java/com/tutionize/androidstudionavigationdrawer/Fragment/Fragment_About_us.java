package com.tutionize.androidstudionavigationdrawer.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tutionize.androidstudionavigationdrawer.Constant.AppConst;
import com.tutionize.androidstudionavigationdrawer.R;

/**
 * Created by android on 10/13/17.
 */

public class Fragment_About_us extends Fragment {
    private TextView txt_About_Discription;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_about_us, container, false);
        findView(rootView);
        TextEdit();
        return rootView;
    }

    @SuppressLint("SetTextI18n")
    private void TextEdit() {
        txt_About_Discription.setText(AppConst.about_us);
    }

    private void findView(View rootView) {
        txt_About_Discription= rootView.findViewById(R.id.txt_about_Discription);
    }
}
