package com.tutionize.androidstudionavigationdrawer.Fragment;

import android.annotation.SuppressLint;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.rey.material.widget.Button;
import com.rey.material.widget.ImageView;
import com.rey.material.widget.LinearLayout;
import com.rey.material.widget.RelativeLayout;
import com.rey.material.widget.TextView;
import com.tutionize.androidstudionavigationdrawer.Activity_Screen.Home_screen;
import com.tutionize.androidstudionavigationdrawer.BuildConfig;
import com.tutionize.androidstudionavigationdrawer.Constant.AppConst;
import com.tutionize.androidstudionavigationdrawer.Constant.Picture_method;
import com.tutionize.androidstudionavigationdrawer.Constant.Singleton;
import com.tutionize.androidstudionavigationdrawer.Model.EditBAtch_modle;
import com.tutionize.androidstudionavigationdrawer.Model.InserBatch_modle;
import com.tutionize.androidstudionavigationdrawer.Model.SeletedStudent;
import com.tutionize.androidstudionavigationdrawer.Model.UnseletedStudent;
import com.tutionize.androidstudionavigationdrawer.ProgressDialog;
import com.tutionize.androidstudionavigationdrawer.R;
import com.tutionize.androidstudionavigationdrawer.Sharepool.Check_Required_Premission;
import com.tutionize.androidstudionavigationdrawer.Sharepool.MySharePrafranceClass;
import com.tutionize.androidstudionavigationdrawer.Sharepool.NetworkStateCheck;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

import static com.tutionize.androidstudionavigationdrawer.R.layout.support_simple_spinner_dropdown_item;

/**
 * Created by android on 11/28/17.
 */

public class Selectbatch_fragment extends Fragment {
    private static final int MEDIA_TYPE_IMAGE = 1;
    public  static final int GALLERYCODE = 250;
    public  static final int CAMERACODE = 150;
    public  static String navigation_current;
    public  static Selectbatch_fragment selectbatch_fragment;
    private String SelectedImagePath = "",SelectedStudent="";
    private String BatchName,BatchStartTime,BatchEndTime,duration,ImagePath;
    private String batchid,starttime,teacherid;
    private int end_mHour, end_mMinute, Stat_mHour,Start_mMinute;
    private RelativeLayout BatchImage_layout;
    private Spinner Spiner_duration;
    private EditText Edit_BAtch_name;
    private Button Batch_save_button;
    private ImageView icon_removeStudent_list,icon_student_List;
    private TextView Batch_Start_Time,Batch_End_Time;
    private String[] list;
    private File file_navigation;
    private Uri photofile_navigation;
    public CircleImageView BatchImage;
    private TypedFile fileTypeFile;
    private List<String> Duration = new ArrayList<>();
    public static ArrayList<SeletedStudent> selectedStudentId = new ArrayList<>();
    public static ArrayList<UnseletedStudent> UnselectedStudentId = new ArrayList<>();
    private ArrayAdapter<String> spinnerDurationAdapter;
    private AlertDialog alertDialog;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View view=inflater.inflate
                (R.layout.selectbatch_fragment,container,false);
        list= MySharePrafranceClass.GetSharePrefrance(getActivity());
        Check_Required_Premission.CheckCameraPermission(getActivity());
        FindView(view);
        Clickable();
        AddElement();
        setAdapterlist();

        selectbatch_fragment=this;

        Bundle mBundle = getArguments();

        if (!(mBundle == null))
        {
             String batchName = mBundle.getString("batchName");
             batchid = mBundle.getString("batchid");
             starttime = mBundle.getString("starttime");
             String endtime = mBundle.getString("endtime");
             teacherid = mBundle.getString("teacherid");
             String durationedit = mBundle.getString("duration");
             String image = mBundle.getString("image");

            if (!(image == null)) {
                if (!(image.equals(""))) {
                    String pic = AppConst.batchphotoconstant.concat(image);
                    Glide.with(getActivity()).load(pic).into(BatchImage);
                } else {
                    Glide.with(getActivity()).load(R.drawable.userimagewhite).into(BatchImage);
                }
            } else {
                Glide.with(getActivity()).load(R.drawable.userimagewhite)
                        .into(BatchImage);
            }

            if (starttime!=null&& endtime !=null&& batchName !=null&& durationedit !=null)
            {

                Batch_Start_Time.setText(starttime);
                Batch_End_Time.setText(endtime);
                Edit_BAtch_name.setText(batchName);
                Spiner_duration.setSelection(spinnerDurationAdapter.getPosition(durationedit));
            }

        }
        return view;
    }

    private void AddElement() {
        Duration.add("1 Day");
        Duration.add("2 Day");
        Duration.add("3 Day");
        Duration.add("4 Day");
        Duration.add("5 Day");
        Duration.add("6 Day");
        Duration.add("7 Day");
        Duration.add("15 Day");
        Duration.add("22 Day");
        Duration.add("30 Day");
        Duration.add("60 Day");
        Duration.add("90 Day");
    }

   private void setAdapterlist(){
       spinnerDurationAdapter = new ArrayAdapter<String>(Objects.requireNonNull(getActivity()),R.layout.spinner_item,Duration);
       spinnerDurationAdapter.setDropDownViewResource(support_simple_spinner_dropdown_item);
       Spiner_duration.setAdapter(spinnerDurationAdapter);
    }

    public static ArrayList<SeletedStudent> getSelectedStudentId() {
        return selectedStudentId;
    }

    public static ArrayList<UnseletedStudent> getUnselectedStudentId() {
        return UnselectedStudentId;
    }

    public static void AddSelectedStudent(SeletedStudent gsggs){
        Selectbatch_fragment.selectedStudentId.add(gsggs);
    }

    public static void AddUnSelectedStudent(UnseletedStudent gsggs){
        Selectbatch_fragment.UnselectedStudentId.add(gsggs);
    }

    private boolean validate(){
       boolean valid = true;
       if (BatchName.isEmpty()){
           Edit_BAtch_name.setError("Enter Valid Name");
           valid=false;
       }
    if (BatchStartTime.equals("Start")){
        Batch_Start_Time.setError("Enter Valid Start Time");
        valid=false;
    }
    if (BatchEndTime.equals("End")){
        Batch_End_Time.setError("Enter Valid End Time");
        valid=false;
    }

       return valid;
   }
    @SuppressLint({"ResourceAsColor", "SetTextI18n"})
    private void Clickable() {

        Batch_save_button.setOnClickListener(v -> {

            BatchName = Edit_BAtch_name.getText().toString();
            BatchStartTime=Batch_Start_Time.getText().toString();
            BatchEndTime=Batch_End_Time.getText().toString();
            ImagePath = getSelectedImagePath();
            duration=Spiner_duration.getSelectedItem().toString();

            if (!validate()){

                Toast.makeText(getActivity(), "Your Request Has Failed", Toast.LENGTH_SHORT).show();
            }
            else {
                if (batchid != null)
                {
                    fileTypeFile = new TypedFile("image/*", new File(ImagePath));
                    EditBatch(batchid,BatchName,BatchStartTime,BatchEndTime,duration,fileTypeFile);
                }
                else {
                    fileTypeFile = new TypedFile("image/*", new File(ImagePath));

                    if(!selectedStudentId.isEmpty()){
                        for (int i = 0 ; i<selectedStudentId.size(); i++){
                             if (i==0)
                             {
                                 SelectedStudent = selectedStudentId.get(i).getId();
                             }
                             else
                                 {
                                 SelectedStudent = SelectedStudent.concat(",").concat(selectedStudentId.get(i).getId());
                             }
                        }
                    }


                    InserBatch(list[0],BatchStartTime,duration,BatchEndTime,BatchName,
                            fileTypeFile,SelectedStudent);
                }
            }
        });

        BatchImage_layout.setOnClickListener(v -> {

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Objects.requireNonNull(getActivity()));
            LayoutInflater inflater = getActivity().getLayoutInflater();
            @SuppressLint("InflateParams")
            View dialogView = inflater.inflate(R.layout.choose_image_option, null);

            LinearLayout layout_Camera = dialogView.findViewById(R.id.layout_Camera);
            LinearLayout layout_gallery = dialogView.findViewById(R.id.layout_gallery);
            LinearLayout layout_Cancel = dialogView.findViewById(R.id.layout_Cancel);
            TextView dialog_titel = dialogView.findViewById(R.id.dialog_title);

               dialog_titel.setText("Batch Photo");
            layout_Camera.setOnClickListener(v13 -> {
                Intent cameraintent = new Intent("android.media.action.IMAGE_CAPTURE");
                if (cameraintent.resolveActivity(getActivity().getPackageManager()) != null) {
                    file_navigation = null;
                    try {
                        file_navigation = Picture_method.getInstance().getOutputMediaFile(MEDIA_TYPE_IMAGE);
                        navigation_current =  Picture_method.getInstance().getCurrent();
                    }
                    catch (Exception ex) {
                        return;
                    }
                    if (photofile_navigation != null)
                    {
                        photofile_navigation = FileProvider.getUriForFile(getActivity(),
                                BuildConfig.APPLICATION_ID + ".provider", file_navigation);
                        cameraintent.putExtra(MediaStore.EXTRA_OUTPUT, photofile_navigation);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            cameraintent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        }
                        else {
                            List<ResolveInfo> resInfoList = getActivity().getPackageManager().queryIntentActivities(cameraintent, PackageManager.MATCH_DEFAULT_ONLY);
                            for (ResolveInfo resolveInfo : resInfoList) {
                                String packageName = resolveInfo.activityInfo.packageName;
                                getActivity().grantUriPermission(packageName,
                                        photofile_navigation,
                                        Intent.FLAG_GRANT_WRITE_URI_PERMISSION
                                                | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            }
                        }
                        getActivity().startActivityForResult(cameraintent, CAMERACODE);
                    }

                }
                alertDialog.dismiss();


            });

            layout_gallery.setOnClickListener(v1 -> {
                Intent intent = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                getActivity().startActivityForResult(intent, GALLERYCODE);
                alertDialog.dismiss();

            });

            layout_Cancel.setOnClickListener(v12 -> alertDialog.cancel());
            dialogBuilder.setView(dialogView);
            alertDialog = dialogBuilder.create();
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            WindowManager.LayoutParams lp = Objects.requireNonNull(alertDialog.getWindow()).getAttributes();
            lp.gravity = Gravity.TOP;
            lp.windowAnimations = R.style.dialog_animation;
            lp.width = LinearLayout.LayoutParams.FILL_PARENT;
            alertDialog.show();

         });

        icon_removeStudent_list.setOnClickListener(v -> callfrontclass(batchid, "Remove List", teacherid));



        icon_student_List.setOnClickListener((View v)->
                callfrontclass(batchid," Students List",teacherid)
        );

        Batch_Start_Time.setOnClickListener(v -> {
            final Calendar c = Calendar.getInstance();
            Stat_mHour = c.get(Calendar.HOUR_OF_DAY);
            Start_mMinute = c.get(Calendar.MINUTE);
            @SuppressLint("SetTextI18n")
            TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),R.style.DialogTheme,
                    (view, hourOfDay, minute) -> Batch_Start_Time.setText(hourOfDay + " : " + minute), Stat_mHour, Start_mMinute, false);

            timePickerDialog.show();
        });


        Batch_End_Time.setOnClickListener((View v)-> {
                final Calendar c = Calendar.getInstance();
                end_mHour = c.get(Calendar.HOUR_OF_DAY);
                end_mMinute = c.get(Calendar.MINUTE);
                @SuppressLint("SetTextI18n")
                TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),R.style.DialogTheme,
                        (view, hourOfDay, minute) -> Batch_End_Time.setText(hourOfDay + " : " + minute), end_mHour, end_mMinute,false);
                timePickerDialog.show();
        });
    }
    private void FindView(View view) {

        Spiner_duration= view.findViewById(R.id.Spiner_duration);

        RelativeLayout batch_Time = view.findViewById(R.id.Batch_Time);
        Batch_Start_Time = view.findViewById(R.id.Batch_Start_Time);
        Batch_End_Time = view.findViewById(R.id.Batch_End_Time);
        BatchImage_layout= view.findViewById(R.id.BatchImage_layout);
        Edit_BAtch_name = view.findViewById(R.id.BatchName);
        Batch_save_button = view.findViewById(R.id.Batch_save_button);
        icon_student_List = view.findViewById(R.id.icon_student_List);
        icon_removeStudent_list = view.findViewById(R.id.icon_removeStudent_list);
        BatchImage= view.findViewById(R.id.batch_image_view);
    }
    @SuppressLint("ResourceAsColor")
    private void InserBatch(String s, String txt_startTime, String duration, String txt_endTime,
                            String BatchName, TypedFile photo, String students_ids) {
         if (NetworkStateCheck.isNetworkAvaliable(Objects.requireNonNull(getActivity()))) {
             KProgressHUD knprogress = ProgressDialog.getInstance().getKNProgressDialog(getActivity());
             knprogress.show();
             Singleton.getInstance().getApi().InserBatch(s, txt_startTime, txt_endTime, BatchName, duration, photo, students_ids, new Callback<InserBatch_modle>() {
                 @SuppressLint("SetTextI18n")
                 @Override
                 public void success(InserBatch_modle inserBatch_modle, Response response) {
                     if (inserBatch_modle.getSuccess().equals("1")) {
                         knprogress.dismiss();
                         Edit_BAtch_name.setText("");
                         Batch_Start_Time.setText("Start");
                         Batch_End_Time.setText("End");
                         Intent intent = new Intent(getActivity(), Home_screen.class);
                         startActivity(intent);
                         Objects.requireNonNull(getActivity()).finish();
                     } else {
                         knprogress.dismiss();
                         Toast.makeText(getActivity(), "" + inserBatch_modle.getMessage(), Toast.LENGTH_SHORT).show();
                     }
                 }

                 @Override
                 public void failure(RetrofitError error) {
                     knprogress.dismiss();
                     Toast.makeText(getActivity(), "" + error, Toast.LENGTH_SHORT).show();
                 }
             });
         }else {
             Toast.makeText(getActivity(), "Internet connection problem" , Toast.LENGTH_SHORT).show();
         }

    }
    @SuppressLint("ResourceAsColor")
    private  void EditBatch(String Batchid, String BatchName, String txt_startTime, String txt_endTime, String duration, TypedFile photo){
        TypedFile photoImage = null;
        if(photo.file().exists()){
            photoImage = photo;
        }

        if (NetworkStateCheck.isNetworkAvaliable(Objects.requireNonNull(getActivity()))) {
            KProgressHUD knprogress = ProgressDialog.getInstance().getKNProgressDialog(getActivity());
            knprogress.show();
            Singleton.getInstance().getApi().EditBatch(Batchid, BatchName, txt_startTime, txt_endTime, duration, photoImage, new Callback<EditBAtch_modle>() {
                @Override
                public void success(EditBAtch_modle editBAtch_modle, Response response) {
                    if (editBAtch_modle.getSuccess().equals("1")) {
                        knprogress.dismiss();
                        Toast.makeText(getActivity(), "" + editBAtch_modle.getResult(), Toast.LENGTH_SHORT).show();
                        assert getFragmentManager() != null;
                        getFragmentManager().popBackStackImmediate();

                    } else {
                        knprogress.dismiss();
                        Toast.makeText(getActivity(), "" + editBAtch_modle.getResult(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    knprogress.dismiss();
                    Toast.makeText(getActivity(), "" + error, Toast.LENGTH_SHORT).show();

                }
            });
        }else {
            Toast.makeText(getActivity(), "Internet connection problem", Toast.LENGTH_SHORT).show();
        }
    }

    public void callfrontclass(String batchid,String typeoflist, String teacherid) {
        Bundle bundle = new Bundle();
        Fragment mFragment = null;
        Class fragmentClass = Fragment_Student_List.class;
        try {
            mFragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        FragmentManager fragmentManager = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        bundle.putString("batchid", batchid);
        bundle.putString("teacherid", teacherid);
        bundle.putString("typeoflist", typeoflist);

        assert mFragment != null;
        mFragment.setArguments(bundle);

        fragmentTransaction.replace(R.id.container, mFragment).addToBackStack("jnsbnbs").commit();

    }

    //==========================helper Method================================


    public  String getBatchid() {
        return batchid;
    }

    public  String getStarttime() {
        return starttime;
    }


    public String getSelectedImagePath() {
        return SelectedImagePath;
    }

    public void setSelectedImagePath(String selectedImagePath)
    { SelectedImagePath = selectedImagePath; }
}
