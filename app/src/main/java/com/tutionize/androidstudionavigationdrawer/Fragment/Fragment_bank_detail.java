package com.tutionize.androidstudionavigationdrawer.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.tutionize.androidstudionavigationdrawer.Constant.Singleton;
import com.tutionize.androidstudionavigationdrawer.Model.BankDetail_Modle;
import com.tutionize.androidstudionavigationdrawer.ProgressDialog;
import com.tutionize.androidstudionavigationdrawer.R;
import com.tutionize.androidstudionavigationdrawer.Sharepool.MySharePrafranceClass;
import com.tutionize.androidstudionavigationdrawer.Sharepool.NetworkStateCheck;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.tutionize.androidstudionavigationdrawer.R.layout.support_simple_spinner_dropdown_item;


/**
 * Created by android on 10/18/17.
 */

public class Fragment_bank_detail extends Fragment {

    private String[] list;
    private EditText ed_AccountName, ed_Account_numer, ed_Bank_Name, ed_bank_OtherCode, Edit_Code;
    private Button btn_Bank_submit;
    private Spinner Bd_codeSelecter;
    private List<String> codedata = new ArrayList<>();
    private String txt_AccountNumber, txt_bankName, txt_bankAccountName, other_code, Typecode, seletedType;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_bank_detail, container, false);
        list = MySharePrafranceClass.GetSharePrefrance(Objects.requireNonNull(getActivity()));
        findView(rootView);
        AddElement();
        ClickAble();
        setAdapterValue();
        return rootView;
    }

    private void setAdapterValue() {
        final ArrayAdapter<String> SpinnerCodeAdapter = new ArrayAdapter<>(
                Objects.requireNonNull(getActivity()), support_simple_spinner_dropdown_item, codedata);
        SpinnerCodeAdapter.setDropDownViewResource(support_simple_spinner_dropdown_item);
        Bd_codeSelecter.setAdapter(SpinnerCodeAdapter);
    }

    private void AddElement() {
        codedata.add("Bank Code");
        codedata.add("SWIFT Code");
        codedata.add("IFSC Code");
    }

    private void ClickAble() {
        btn_Bank_submit.setOnClickListener(view -> {
            String userid = list[0];
            txt_AccountNumber = ed_Account_numer.getText().toString();
            txt_bankName = ed_Bank_Name.getText().toString();
            txt_bankAccountName = ed_AccountName.getText().toString();
            other_code = ed_bank_OtherCode.getText().toString();

            seletedType = Bd_codeSelecter.getSelectedItem().toString();
            Typecode = Edit_Code.getText().toString();

            if (!validate()){
                Toast.makeText(getActivity(), "SignUp has Failed", Toast.LENGTH_SHORT).show();
            }else{
                String Swiftcode="";
                String ICFScode="";

                if (seletedType.equals("SWIFT Code")){

                     Swiftcode =Typecode;

                }
                else {
                    ICFScode =Typecode;
                }

                AddDetail(userid, txt_AccountNumber, txt_bankAccountName, Swiftcode, ICFScode, other_code);
            }


            if (txt_AccountNumber.equals("")) {
                Toast.makeText(getActivity(), "Enter Account Number ", Toast.LENGTH_SHORT).show();
            } else if (txt_bankAccountName.equals("")) {
                Toast.makeText(getActivity(), "Enter Account Name", Toast.LENGTH_SHORT).show();
            } else if (txt_bankName.equals("")) {
                Toast.makeText(getActivity(), "Enter Bank Name ", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity(), "BAnk Detatil submit", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @SuppressLint("ResourceAsColor")
    private void AddDetail(String userid, String txt_accountNumber, String txt_bankAccountName, String swiftcode, String icfScode, String other_code) {
        if (NetworkStateCheck.isNetworkAvaliable(Objects.requireNonNull(getActivity()))) {
            KProgressHUD knprogress = ProgressDialog.getInstance().getKNProgressDialog(getActivity());
            knprogress.show();
            Singleton.getInstance().getApi().AddBankDetail(userid, txt_accountNumber, txt_bankAccountName, swiftcode, icfScode, other_code, new Callback<BankDetail_Modle>() {
                @Override
                public void success(BankDetail_Modle bankDetail_modle, Response response) {
                    if (bankDetail_modle.getSuccess().equals("1")) {
                        ed_Account_numer.setText("");
                        ed_Bank_Name.setText("");
                        ed_AccountName.setText("");
                        ed_bank_OtherCode.setText("");
                        Edit_Code.setText("");
                        knprogress.dismiss();
                        Toast.makeText(getActivity(), "" + bankDetail_modle.getMessage(), Toast.LENGTH_SHORT).show();
                        Objects.requireNonNull(getActivity()).onBackPressed();
                    } else {
                        knprogress.dismiss();
                        Toast.makeText(getActivity(), "" + bankDetail_modle.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    knprogress.dismiss();
                    Toast.makeText(getActivity(), "" + error, Toast.LENGTH_SHORT).show();

                }
            });
        }else {
            Toast.makeText(getContext(), "Internet connection problem", Toast.LENGTH_SHORT).show();
        }
    }
    private void findView(View rootView) {
        ed_AccountName =(EditText)rootView.findViewById(R.id.bD_edt_AccountNAme);
        ed_Account_numer=(EditText)rootView.findViewById(R.id.bD_edt_AccountNumber);
        ed_Bank_Name=(EditText)rootView.findViewById(R.id.bD_edt_bank_Name);
        ed_bank_OtherCode=(EditText)rootView.findViewById(R.id.bD_edt_bank_OtherCode);
        Edit_Code=(EditText)rootView.findViewById(R.id.Bd_txt_Edit_Code);
        Bd_codeSelecter =(Spinner)rootView.findViewById(R.id.Bd_codeSelecter);
        btn_Bank_submit=(Button)rootView.findViewById(R.id.bD_btn_Bank_submit);
    }
    public boolean validate() {
        boolean valid = true;
        if (txt_AccountNumber.isEmpty() || txt_AccountNumber.length() > 18) {
            ed_Account_numer.setError("Please Enter Valid Account Number");
            valid = false;
        }
        if (txt_bankAccountName.isEmpty()) {
            ed_AccountName.setError("Please Enter Valid Account Name");
            valid = false;
        }
        if (txt_bankName.isEmpty()) {
            ed_Bank_Name.setError("Please Enter Valid Bank Name");
            valid = false;
        }
        if (seletedType.equals("Bank Code")) {
            Edit_Code.setError("Please Choose Valid Code Type");
            valid = false;
        }
        if (Typecode.isEmpty()){
            Edit_Code.setError("Please Enter Valid Code");
         valid = false;
       }

       return valid;

    }

}
