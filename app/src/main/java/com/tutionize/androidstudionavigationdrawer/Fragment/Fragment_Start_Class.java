package com.tutionize.androidstudionavigationdrawer.Fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.tutionize.androidstudionavigationdrawer.Activity_Screen.Home_screen;
import com.tutionize.androidstudionavigationdrawer.Constant.AppConst;
import com.tutionize.androidstudionavigationdrawer.Constant.Singleton;
import com.tutionize.androidstudionavigationdrawer.Model.GetToken_model;
import com.tutionize.androidstudionavigationdrawer.Model.Start_class_modle;
import com.tutionize.androidstudionavigationdrawer.R;
import com.tutionize.androidstudionavigationdrawer.Sharepool.MySharePrafranceClass;
import com.tutionize.androidstudionavigationdrawer.Sharepool.NetworkStateCheck;
import com.tutionize.androidstudionavigationdrawer.command.ConstantCommand;
import com.tutionize.androidstudionavigationdrawer.command.FragmentStartClassCommand;
import com.tutionize.androidstudionavigationdrawer.databinding.FragmentStartClassBinding;

import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;



/**
 * Created by user on 20/12/17.
 */

public class Fragment_Start_Class extends Fragment {

    private FragmentStartClassBinding fragmentStartClassBinding;
    private String Roomid,teacherimage,teachername,callid,calltype;
    private String[] loginlist,list;
    private Ringtone ringtone;
    @SuppressLint("StaticFieldLeak")
    public static Fragment_Start_Class fragment_start_class;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Home_screen.home_screen.toolbar.setVisibility(View.GONE);
        Home_screen.home_screen.drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        fragmentStartClassBinding = DataBindingUtil.inflate(inflater,R.layout.fragment_start_class,container,false);
        loginlist= MySharePrafranceClass.LoginDataGetSharePrefrance(Objects.requireNonNull(getActivity()));
        list=MySharePrafranceClass.GetSharePrefrance(getActivity());
        
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
        ringtone= RingtoneManager.getRingtone(getActivity(), notification);

        PerFormAction(fragmentStartClassBinding);
        SetAnimationOnButton(fragmentStartClassBinding);

        ShareElement();
        fragment_start_class=this;
        Bundle mBundle = getArguments();

        BundlePerformance(fragmentStartClassBinding,mBundle);

        return fragmentStartClassBinding.getRoot();
    }

    private void BundlePerformance(FragmentStartClassBinding fragmentStartClassBinding, Bundle mBundle) {

        if (!(mBundle == null))
        {
            Roomid = mBundle.getString("Roomid");
            teacherimage = mBundle.getString("teacherimage");
            teachername = mBundle.getString("teachername");
            calltype=mBundle.getString("calltype");
            callid=mBundle.getString("callid");
            if (teacherimage.equals("TeacherImage")&&teachername.equals("TeacherName")){ directcall();
            }
            if (teacherimage!=null) {
                String pic = AppConst.photoconstant.concat(teacherimage);
                Glide.with(this).load(pic).asBitmap().into(new SimpleTarget<Bitmap>(150,
                        50) {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        Drawable drawable = new BitmapDrawable(Objects.requireNonNull(getActivity()).getResources(), resource);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            fragmentStartClassBinding.videolecturelayout.setBackground(drawable);
                        }
                    }
                });
            }


            if (teacherimage != null && teachername != null)
            {
                fragmentStartClassBinding.classTeacherName.setText(teachername);
                if (!(teacherimage.equals(""))) {
                    String pic = AppConst.photoconstant.concat(teacherimage);
                    Glide.with(Objects.requireNonNull(getActivity())).load(pic)
                            .into(fragmentStartClassBinding.classImage);
                } else {
                    Glide.with(Objects.requireNonNull(getActivity())).load(R.drawable.userimagewhite).into(fragmentStartClassBinding.classImage);
                }
            } else {
                Glide.with(Objects.requireNonNull(getActivity())).load(R.drawable.userimagewhite).into(fragmentStartClassBinding.classImage);
            }
        }
    }
    private void SetAnimationOnButton(FragmentStartClassBinding fragmentStartClassBinding) {
        Animation animation= AnimationUtils.loadAnimation(getContext(),R.anim.vibrate);
        fragmentStartClassBinding.classAccepted.show();
        fragmentStartClassBinding.classAccepted.setAnimation(animation);
        fragmentStartClassBinding.classDecline.show();
        fragmentStartClassBinding.classDecline.setAnimation(animation);
        fragmentStartClassBinding.classTeacherName.setAnimation(animation);
    }

    private void PerFormAction(FragmentStartClassBinding fragmentStartClassBinding) {

        fragmentStartClassBinding.setStartClassCommand(new FragmentStartClassCommand() {
            @Override
            public void ClassDecline() {
                switch (calltype) {
                    case ConstantCommand.callType_Audio:
                        SetClassAddress(callid, Roomid, list[1], list[2], ConstantCommand.callType_End, list[0]);
                        closefragment();
                        if (Roomid != null) {
                            Roomid = null;
                        }
                        break;
                    case ConstantCommand.callType_Video_1:
                        SetClassAddress(callid, Roomid, list[1], list[2], ConstantCommand.callType_End, list[0]);
                        closefragment();
                        if (Roomid != null) {
                            Roomid = null;
                        }
                        break;
                    default:
                        closefragment();
                        if (Roomid != null) {
                            Roomid = null;
                        }
                        break;
                }

            }

            @Override
            public void ClassAccept() {


                if (calltype.equals(ConstantCommand.callType_Audio))
                {
                    SetClassAddress(callid, Roomid, list[1], list[2], ConstantCommand.callType_Accepted, list[0]);
                }
                else if (calltype.equals(ConstantCommand.callType_Video_1)){
                    SetClassAddress(callid, Roomid, list[1], list[2], ConstantCommand.callType_Accepted, list[0]);
                }


                TelephonyManager telephonyManager = (TelephonyManager)
                        Objects.requireNonNull(getActivity()).getSystemService(Context.TELEPHONY_SERVICE);
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getActivity(), "Allow MicroPhone permission first from App info", Toast.LENGTH_SHORT).show();

                    return;
                }

                assert telephonyManager != null;
                @SuppressLint("HardwareIds")
                String deviceId = telephonyManager.getDeviceId();
                String type = loginlist[2];

                if (Roomid!=null && type!=null && deviceId!=null && calltype!=null&&callid!=null) {

                    GoToClass(Roomid,type.concat(deviceId),calltype,teacherimage,teachername,callid);
                }

            }
        });
    }

    private void ShareElement()
    {
        Objects.requireNonNull(getActivity())
                .getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                | WindowManager.LayoutParams.FLAG_FULLSCREEN
                | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON);
    }

    @Override
    public void onStart() {
        ringtone.play();
        super.onStart();

    }
    @Override
    public void onDestroy() {
        ringtone.stop();
        super.onDestroy();
    }

    public void closefragment(){

        try {
            ringtone.stop();
            Home_screen.home_screen.toolbar.setVisibility(View.VISIBLE);
            Intent intent = new Intent(getActivity(), Home_screen.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
        catch (Exception ignored){
        }
    }
    @SuppressLint("ResourceAsColor")
    private void GoToClass(final String roomidentity, String userIdentity, String calltype, String teacherimage, String teachername,String callid) {
        if (NetworkStateCheck.isNetworkAvaliable(Objects.requireNonNull(getActivity())))
        {
            Singleton.getInstance().getApi().getToken(roomidentity, userIdentity, new Callback<GetToken_model>() {
                @Override
                public void success(GetToken_model getToken_model, Response response) {
                    if (getToken_model.getResult().equals("")) {
                        Singleton.getInstance().Progress(getActivity()).dismiss();
                    } else {
                        String Token = getToken_model.getResult();
                        callfrontclass2(Token, roomidentity, calltype, teacherimage, teachername, callid);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Toast.makeText(getActivity(), "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }else {
            Toast.makeText(getActivity(), "Internet connection problem", Toast.LENGTH_SHORT).show();
        }
    }

    public void callfrontclass2(String token, String room ,String calltype,String teacherimage,String teachername ,String callid) {

        ringtone.stop();
        Home_screen.home_screen.toolbar.setVisibility(View.VISIBLE);

        Bundle bundle = new Bundle();
        Fragment mFragment = null;
        Class fragmentClass = Live_Video_Screen.class;
        try {
            mFragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        FragmentManager fragmentManager = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        bundle.putString("Room", room);
        bundle.putString("Token", token);
        bundle.putString("calltype",calltype);
        bundle.putString("teacherimage",teacherimage);
        bundle.putString("teachername",teachername);
        bundle.putString("callid",callid);



        assert mFragment != null;
        mFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.container, mFragment).addToBackStack(null).commit();

    }
    private void SetClassAddress(String student, String roomId,String teacher_name,String img_name, String Calltype,String callid){
        Singleton.getInstance().getApi().SetClassAddress(student, roomId,teacher_name,img_name,Calltype,callid ,new Callback<Start_class_modle>() {
            @Override
            public void success(Start_class_modle start_class_modle, Response response)
            {
            }
            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

   private void directcall()
   {
       TelephonyManager telephonyManager = (TelephonyManager)
               Objects.requireNonNull(getActivity()).getSystemService(Context.TELEPHONY_SERVICE);
       if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED)
       {
           return;
       }
       assert telephonyManager != null;
       @SuppressLint("HardwareIds")
       String deviceId = telephonyManager.getDeviceId();
       String type = loginlist[2];
       if (Roomid!=null && type!=null && deviceId!=null && calltype!=null&&callid!=null)
       {
           GoToClass(Roomid,type.concat(deviceId),calltype,teacherimage,teachername,callid);
       }
   }

}
