package com.tutionize.androidstudionavigationdrawer.Fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.rey.material.widget.ImageView;
import com.rey.material.widget.LinearLayout;
import com.rey.material.widget.RelativeLayout;
import com.tutionize.androidstudionavigationdrawer.Adapter.Adapter_AllBatch;
import com.tutionize.androidstudionavigationdrawer.Adapter.Adapter_Request_Student;
import com.tutionize.androidstudionavigationdrawer.Constant.Message_dialoge;
import com.tutionize.androidstudionavigationdrawer.Constant.Singleton;
import com.tutionize.androidstudionavigationdrawer.Model.All_Batch_Result_modle;
import com.tutionize.androidstudionavigationdrawer.Model.All_Batch_modle;
import com.tutionize.androidstudionavigationdrawer.Model.Requested_student_list;
import com.tutionize.androidstudionavigationdrawer.Model.Requested_student_result;
import com.tutionize.androidstudionavigationdrawer.ProgressDialog;
import com.tutionize.androidstudionavigationdrawer.R;
import com.tutionize.androidstudionavigationdrawer.Sharepool.MySharePrafranceClass;
import com.tutionize.androidstudionavigationdrawer.Sharepool.NetworkStateCheck;

import java.util.List;
import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 *
 * Created by Vishal Sandhu on 11/24/17.
 *
 */

public class TeacherMainActivity extends Fragment
{
    private RecyclerView Hire_Student_ListView,Hire_Request_ListView;
    private ImageView AddBatch,Teacher_main_view_request;
    float startDegress =-90;
    float endDegress = 0;
    private static boolean Notification = false;
    public static String Teacherid;
    //private Adapter_Request_Student requestedadapter;
    private RelativeLayout New_RequestMenu;
    private LinearLayout New_RequestMenu_recycle;
    private List<Requested_student_result> StudentRequest;
    @SuppressLint("StaticFieldLeak")
    public static TeacherMainActivity teacherMainActivity;
    @SuppressLint("StaticFieldLeak")
    private static Activity getActivity;
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.teacher_main_activity, container, false);
        String[] list = MySharePrafranceClass.GetSharePrefrance(Objects.requireNonNull(getActivity()));
        if (getActivity==null)
        {
            getActivity = getActivity();
        }
        touchlistner(view);
        FINDVIEWBYID(view);
        CLICKLISTINER();
        Teacherid= list[0];
        AllBatch(Teacherid);
        Requestlist(Teacherid);
        teacherMainActivity=this;
        return view;
}
    private void touchlistner(View view)
    {
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener((v, keyCode, event) -> {
            if (keyCode== KeyEvent.KEYCODE_BACK)
            {
                try {
                    Message_dialoge.getInstance().exitfromApplication(getActivity());
                    }
                    catch (WindowManager.BadTokenException ignored){}
                return true;
            }
            return false;
        });
    }
    public void Requestlist(String teacherid) {
        if (NetworkStateCheck.isNetworkAvaliable(getActivity)) {
            Singleton.getInstance().getApi().Requestlist(teacherid, new Callback<Requested_student_list>() {
                @Override
                public void success(Requested_student_list requested_student_list, Response response) {
                    if (requested_student_list.getSuccess().equals("1")) {
                        StudentRequest = requested_student_list.getResult();
                        if (StudentRequest != null && !StudentRequest.isEmpty()) {
                            animateCollapse();
                            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity,LinearLayoutManager.VERTICAL, false);
                            Hire_Request_ListView.setLayoutManager(layoutManager);
                            Adapter_Request_Student requestedadapter = new Adapter_Request_Student(getActivity, StudentRequest,
                                    New_RequestMenu, New_RequestMenu_recycle, Teacher_main_view_request);
                            Hire_Request_ListView.setAdapter(requestedadapter);
                            requestedadapter.notifyDataSetChanged();
                        }
                    } else {
                        Toast.makeText(getActivity, "Request Api  Else ", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                }
            });
        }else {
            Toast.makeText(getActivity, "Internet connection problem", Toast.LENGTH_SHORT).show();
        }
    }
    @SuppressLint("ResourceAsColor")
    private void AllBatch(String teacherid)
    {
        if (NetworkStateCheck.isNetworkAvaliable(getActivity)){
            KProgressHUD knprogress = ProgressDialog.getInstance().getKNProgressDialog(getActivity());
            knprogress.show();
            Singleton.getInstance().getApi().AllBatch(teacherid, new Callback<All_Batch_modle>() {
            @Override
            public void success(All_Batch_modle all_batch_modle, Response response) {
                if(all_batch_modle.getSuccess().equals("1"))
                {
                    List<All_Batch_Result_modle>Batchlist = all_batch_modle.getResult();
                    if (Batchlist!=null) {
                        CallBatchAdapter(Batchlist);
                    }
                    knprogress.dismiss();
                    }
                else {
                    knprogress.dismiss();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                knprogress.dismiss();
                if (error.toString().contains("JsonSyntaxException:")){
                    Toast.makeText(getActivity, "Don't have any current batch ", Toast.LENGTH_SHORT).show();

                }else {
                    Toast.makeText(getActivity, "" + error, Toast.LENGTH_SHORT).show();
                }
            }
        });
        }else {
            Toast.makeText(getActivity, "Internet connection problem", Toast.LENGTH_SHORT).show();
        }
    }
    private void CLICKLISTINER() {
        Teacher_main_view_request.setOnClickListener(v->{
            if (New_RequestMenu.getVisibility()==View.VISIBLE){ animationExpand();}
            else {Requestlist(Teacherid);animateCollapse();
            }});

        AddBatch.setOnClickListener(v -> {
            Selectbatch_fragment.selectedStudentId.clear();
            Selectbatch_fragment.UnselectedStudentId.clear();
            Singleton.getInstance().CallFragment(new Selectbatch_fragment(),getActivity(), "Select batch");
       });

    }
    public void animateCollapse() {
        if (New_RequestMenu.getVisibility()==View.GONE) {
            startDegress += 180;
            endDegress += 180;
            RotateAnimation anim = new RotateAnimation(startDegress, endDegress, RotateAnimation.RELATIVE_TO_SELF,
                    0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
            anim.setInterpolator(new LinearInterpolator());
            anim.setRepeatCount(0);
            anim.setFillAfter(true);
            anim.setDuration(200);
            Teacher_main_view_request.startAnimation(anim);
            New_RequestMenu.setVisibility(View.VISIBLE);
            New_RequestMenu_recycle.setVisibility(View.VISIBLE);
        }
    }

    private void animationExpand(){

        if (New_RequestMenu.getVisibility()==View.VISIBLE) {
            startDegress += 180;
            endDegress += 180;
            RotateAnimation anim = new RotateAnimation(startDegress, endDegress, RotateAnimation.RELATIVE_TO_SELF,
                    0.5f,RotateAnimation.RELATIVE_TO_SELF, 0.5f);
            anim.setInterpolator(new LinearInterpolator());
            anim.setRepeatCount(0);
            anim.setFillAfter(true);
            anim.setDuration(200);
            Teacher_main_view_request.startAnimation(anim);
            New_RequestMenu.setVisibility(View.GONE);
            New_RequestMenu_recycle.setVisibility(View.GONE);
        }
    }
    private void FINDVIEWBYID(View view) {
        Teacher_main_view_request = view.findViewById(R.id.Teacher_main_view_request);
        AddBatch= view.findViewById(R.id.Teacher_main_Add_Batch);
        Hire_Request_ListView= view.findViewById(R.id.Teacher_main_Req_list );
        Hire_Student_ListView= view.findViewById(R.id.Teacher_main_HireStudent_List);
        New_RequestMenu= view.findViewById(R.id.menu);
        New_RequestMenu_recycle= view.findViewById(R.id.recycle_layout);
    }
    public void setNotification(boolean notification)
    {
        if (notification)
        {
            // Requestlist(Teacherid);
        }
    }
    private void CallBatchAdapter(List<All_Batch_Result_modle> batchlist)
    {
        int resId = R.anim.layout_animation;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(teacherMainActivity.getActivity(), resId);


        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL, false);
        Hire_Student_ListView.setLayoutManager(layoutManager1);
        Adapter_AllBatch mBatch_list = new Adapter_AllBatch(getActivity(), batchlist);
        Hire_Student_ListView.setAdapter(mBatch_list);
        Hire_Student_ListView.setLayoutAnimation(animation);
        mBatch_list.notifyDataSetChanged();
    }

}
