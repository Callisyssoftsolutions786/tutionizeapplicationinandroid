package com.tutionize.androidstudionavigationdrawer.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.tutionize.androidstudionavigationdrawer.Constant.Singleton;
import com.tutionize.androidstudionavigationdrawer.Model.ChangeName_modle;
import com.tutionize.androidstudionavigationdrawer.ProgressDialog;
import com.tutionize.androidstudionavigationdrawer.R;
import com.tutionize.androidstudionavigationdrawer.Sharepool.MySharePrafranceClass;
import com.tutionize.androidstudionavigationdrawer.Sharepool.NetworkStateCheck;

import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
/**
 * Created by android on 10/12/17.
 */

public class Fragment_Change_Name extends Fragment{
    private Button bt_ChangeName;
    private EditText edt_New_Name,edt_Old_Name;
    private String[] list;
    private String txt_Oldname,txt_New_name;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_change_name, container, false);
        list= MySharePrafranceClass.GetSharePrefrance(getActivity());
        findView(rootView);
        Shareable();
        ClickAble();
        return rootView;
    }

    private void ClickAble() {
        bt_ChangeName.setOnClickListener((View view)-> {
                 txt_Oldname   =  edt_Old_Name.getText().toString();
                 txt_New_name  =  edt_New_Name.getText().toString();

                 if (!validate()){
                     Toast.makeText(getActivity(), "Your Request Change Name Has Failed ", Toast.LENGTH_SHORT).show();
                 }else {
                     String userid=list[0];
                     String UserImage  = list[2];
                     ChangeName(userid,txt_New_name,UserImage);
                 }

            });
    }

    @SuppressLint("ResourceAsColor")
    private void ChangeName(String userid, String txt_new_name , String UserImage) {
        if (NetworkStateCheck.isNetworkAvaliable(Objects.requireNonNull(getActivity()))) {
            KProgressHUD knprogress = ProgressDialog.getInstance().getKNProgressDialog(getActivity());
            knprogress.show();
            Singleton.getInstance().getApi().ChangeName(userid, txt_new_name, new Callback<ChangeName_modle>() {
                @Override
                public void success(ChangeName_modle changeName_modle, Response response) {
                    if (changeName_modle.getSuccess().equals("1")) {
                        MySharePrafranceClass.ClearAll(Objects.requireNonNull(getActivity()));
                        edt_Old_Name.setText("");
                        edt_New_Name.setText("");
                        knprogress.dismiss();
                        MySharePrafranceClass.Share(getActivity(), userid, txt_new_name, UserImage);
                        Toast.makeText(getActivity(), "" + changeName_modle.getResponse(), Toast.LENGTH_SHORT).show();
                        getActivity().onBackPressed();

                    } else {
                        knprogress.dismiss();
                        Toast.makeText(getActivity(), "" + changeName_modle.getResponse(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    knprogress.dismiss();
                    Toast.makeText(getActivity(), "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }else {
            Toast.makeText(getContext(), "Internet connection problem", Toast.LENGTH_SHORT).show();
        }
    }

    private void findView(View rootView) {
        bt_ChangeName=(Button) rootView.findViewById(R.id.cn_bt_changename_submit);
        edt_New_Name=(EditText)rootView.findViewById(R.id.cn_edt_changename_new);
        edt_Old_Name=(EditText)rootView.findViewById(R.id.cn_edt_changename_old);
    }
    //=====================================================Helper Method============================================

    private boolean validate(){
        boolean valid =true;
        if (txt_Oldname.isEmpty()||!txt_Oldname.equals(list[1])){
            edt_Old_Name.setError("Enter Valid Old Name");
            valid=false;
        }
        if (txt_New_name.isEmpty()){
            edt_New_Name.setError("Enter Valid New Name");
            valid=false;
        }
        if (txt_Oldname.equals(txt_New_name)){
            edt_New_Name.setError("Choose Diffrent Name");
            valid=false;
        }

        return valid;
    }

    private void Shareable(){
        String username=list[1];
        if(username !=null){edt_Old_Name.setText(username);}else{
            edt_Old_Name.setText("");
        }
    }
}
