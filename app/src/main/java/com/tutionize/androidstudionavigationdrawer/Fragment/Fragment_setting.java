package com.tutionize.androidstudionavigationdrawer.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.tutionize.androidstudionavigationdrawer.Constant.Singleton;
import com.tutionize.androidstudionavigationdrawer.Model.TeacherDetailModle;
import com.tutionize.androidstudionavigationdrawer.Model.TeacherDetailResult;
import com.tutionize.androidstudionavigationdrawer.Model.UserDetail_modle;
import com.tutionize.androidstudionavigationdrawer.R;
import com.tutionize.androidstudionavigationdrawer.Sharepool.MySharePrafranceClass;
import com.tutionize.androidstudionavigationdrawer.Sharepool.NetworkStateCheck;

import java.util.List;
import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * Created by android on 10/11/17.
 */

public class Fragment_setting extends Fragment {
    private LinearLayout but_ChangeEmail , btn_Chnage_password , btn_editProfile ,  btn_changename,

            btn_writeUS,btn_aboutUs,btn_reminder,btn_bankDetail,btn_UpdateProfile;

    private static String type;
    private String[] logindata;
    private String UserName, userEmail,usergender,userArea,usercity,userpin,userCountry;

    private String Teacher_name,Teacher_id,teacher_course,amount,teacherCity,teacherQualification,teacherGender,teacherbiography,teachercountry;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.setting_option_screen, container, false);
        logindata= MySharePrafranceClass.LoginDataGetSharePrefrance(Objects.requireNonNull(getActivity()));
        String type  =  logindata[2];
        String[] list = MySharePrafranceClass.GetSharePrefrance(getActivity());
        String Userid= list[0];
        if (type!=null) {
            if (type.equals("Teacher")) {

                TeacherDetail(Userid);
            } else {

                Studentdetail(Userid);
            }
        }
        findView(view);
        Shareable();
        ClickAble();
        return view;
    }

    private void Studentdetail(String userid) {
        if(NetworkStateCheck.isNetworkAvaliable(Objects.requireNonNull(getActivity()))) {
            Singleton.getInstance().getApi().Studentdetail(userid, new Callback<UserDetail_modle>() {
                @Override
                public void success(UserDetail_modle userDetail_modle, Response response) {
                    if (userDetail_modle.getSuccess().equals("1")) {
                        userArea = userDetail_modle.getArea();
                        usercity = userDetail_modle.getCity();
                        userEmail = userDetail_modle.getEmail();
                        usergender = userDetail_modle.getGender();
                        UserName = userDetail_modle.getFirst();
                        userpin = userDetail_modle.getPin();
                        userCountry = userDetail_modle.getCountry();
                    } else {
                        Toast.makeText(getActivity(), "" + userDetail_modle.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                @Override
                public void failure(RetrofitError error) {
                }
            });
        }else {
            Toast.makeText(getContext(), "Internet connection", Toast.LENGTH_SHORT).show();
        }
    }
    private void Shareable()
    {
        type=logindata[2];
        if (type != null)
        {
            if (type.equals("Teacher"))
            {
                btn_UpdateProfile.setVisibility(View.VISIBLE);
            }
            else
                {
                btn_bankDetail.setVisibility(View.GONE);
                btn_UpdateProfile.setVisibility(View.GONE);
                }
        }
    }


    private void ClickAble() {

        if (logindata[0] != null) {

            but_ChangeEmail.setOnClickListener(view -> CallFragment(new Fragment_Change_Email(),"shdhhbhhjdhvdhvdhhdes"));
            btn_Chnage_password.setOnClickListener(view -> CallFragment(new Fragment_Change_Password(), "shdhhbhhjdhbdkjdbbjbdhhkjhbhdjbjbds"));
            btn_aboutUs.setOnClickListener(view -> CallFragment(new Fragment_About_us(), "sdhhkjnncjnjnjdnjckjkjksjnckjjdnkwowonhbhdjbjbds"));
            btn_changename.setOnClickListener(view -> CallFragment(new Fragment_Change_Name(), "shdhhbhhjdhjbjbdbsbvdhvdhhdes"));
            btn_editProfile.setOnClickListener(view -> {

                if (type.equals("Teacher"))
                {
                    CallEditFragment(new Fragment_profile_teaching(),"djffksjdks",Teacher_name,
                            Teacher_id,teacher_course,amount,teacherCity,teacherQualification,teacherGender,
                            teacherbiography,teachercountry);
                }
                else
                    {
                        CallEditFragment2(new Fragment_profile_learning(),UserName,userEmail,usergender,userArea,usercity
                                ,userpin,userCountry,"ghvdvvd");

                    }
            });

            btn_UpdateProfile.setOnClickListener(view -> CallFragment(new Fragment_UpdateBioData(), "update bio"));
            btn_bankDetail.setOnClickListener(view -> CallFragment(new Fragment_bank_detail(), "bank detail"));
        }else {

            CallFragment(new MainActivity(), "MainActivity");
        }
    }

    private  void CallFragment(Fragment fragment, String tag)
    {
        FragmentManager fragmentManager = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                .replace(R.id.container,fragment)
                .addToBackStack(tag).commit();
    }

    private  void CallEditFragment(Fragment fragment, String tag,String Teachername,String Teacherid,String  teachercourse,String Amount,String TeacherCity,String TeacherQualification,String TeacherGender,String Bio,String Techercountry){

        FragmentManager fragmentManager = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);

        Bundle bundle=new Bundle();
        bundle.putString("teachername",Teachername);
        bundle.putString("teacherid",Teacherid);
        bundle.putString("teachercourse",teachercourse);
        bundle.putString("teacheramount",Amount);
        bundle.putString("teachercity",TeacherCity);
        bundle.putString("teacherqualification",TeacherQualification);
        bundle.putString("teachergender",TeacherGender);
        bundle.putString("techerbio",Bio);
        bundle.putString("techercountry",Techercountry);
        fragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.container, fragment).addToBackStack(tag).commit();
    }


    private  void CallEditFragment2(Fragment fragment, String username,String userEmail,String usergender,String  userArea,
                                    String usercity,String userpin,String userCountry,String tag){

        FragmentManager fragmentManager = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);

        Bundle bundle=new Bundle();
        bundle.putString("username",username);
        bundle.putString("userEmail",userEmail);
        bundle.putString("usergender",usergender);
        bundle.putString("userArea",userArea);
        bundle.putString("usercity",usercity);
        bundle.putString("userpin",userpin);
        bundle.putString("userCountry",userCountry);
        fragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.container, fragment).addToBackStack(tag).commit();
    }



    private void findView(View view) {

        but_ChangeEmail=(LinearLayout) view.findViewById(R.id.st_bt_ChangeEmail);
        btn_Chnage_password=(LinearLayout) view.findViewById(R.id.st_bt_ChangePassword);
        btn_editProfile=(LinearLayout) view.findViewById(R.id.st_bt_EditProfile);
        btn_changename=(LinearLayout) view.findViewById(R.id.st_bt_Changename);
        btn_aboutUs=(LinearLayout) view.findViewById(R.id.st_bt_Aboutus);
        btn_UpdateProfile=(LinearLayout) view.findViewById(R.id.st_bt_updateprofile);
        btn_bankDetail=(LinearLayout) view.findViewById(R.id.st_bt_Bank_detail);

    }
    @SuppressLint("ResourceAsColor")
    private void TeacherDetail(final String teacherid) {
        if (NetworkStateCheck.isNetworkAvaliable(Objects.requireNonNull(getActivity()))) {
            Singleton.getInstance().getApi().TeacherDetail(teacherid, new Callback<TeacherDetailModle>() {
                @Override
                public void success(TeacherDetailModle teacherDetailModle, Response response) {
                    if (teacherDetailModle.getSuccess() == 1) {

                        List<TeacherDetailResult> teacherdetail = teacherDetailModle.getResult();
                        Teacher_name = teacherdetail.get(0).getFirst();
                        Teacher_id = teacherdetail.get(0).getId();
                        teacher_course = teacherdetail.get(0).getCourses();
                        amount = teacherdetail.get(0).getCourseprice();
                        teacherCity = teacherdetail.get(0).getCity();
                        teacherQualification = teacherdetail.get(0).getQualification();
                        teacherGender = teacherdetail.get(0).getGender();
                        teacherbiography = teacherdetail.get(0).getBio();
                        teachercountry = teacherdetail.get(0).getCountry();
                    } else {
                        Toast.makeText(getActivity(), "" + teacherDetailModle.getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                }
            });
        }else {
            Toast.makeText(getContext(), "Internet connection problem", Toast.LENGTH_SHORT).show();
        }
        }
}
