package com.tutionize.androidstudionavigationdrawer.Api_Interface;

import com.tutionize.androidstudionavigationdrawer.Model.AddStudentToBatch_modle;
import com.tutionize.androidstudionavigationdrawer.Model.All_Batch_modle;
import com.tutionize.androidstudionavigationdrawer.Model.All_Student_of_batch;
import com.tutionize.androidstudionavigationdrawer.Model.BankDetail_Modle;
import com.tutionize.androidstudionavigationdrawer.Model.ChangeEmail_modle;
import com.tutionize.androidstudionavigationdrawer.Model.ChangeName_modle;
import com.tutionize.androidstudionavigationdrawer.Model.ChangePassword_modle;
import com.tutionize.androidstudionavigationdrawer.Model.ContactUs_Modle;
import com.tutionize.androidstudionavigationdrawer.Model.Cources_modle;
import com.tutionize.androidstudionavigationdrawer.Model.DeleteStudentToBatch;
import com.tutionize.androidstudionavigationdrawer.Model.Delete_batch_modle;
import com.tutionize.androidstudionavigationdrawer.Model.EditBAtch_modle;
import com.tutionize.androidstudionavigationdrawer.Model.ForgetModel;
import com.tutionize.androidstudionavigationdrawer.Model.GetCountry_modle;
import com.tutionize.androidstudionavigationdrawer.Model.GetProfilePic_modle;
import com.tutionize.androidstudionavigationdrawer.Model.GetToken_model;
import com.tutionize.androidstudionavigationdrawer.Model.GetVideo_model;
import com.tutionize.androidstudionavigationdrawer.Model.Get_State_modle;
import com.tutionize.androidstudionavigationdrawer.Model.Get_UnSelectedStudent_modle;
import com.tutionize.androidstudionavigationdrawer.Model.HireStudentList_modle;
import com.tutionize.androidstudionavigationdrawer.Model.HireTeacher_modle;
import com.tutionize.androidstudionavigationdrawer.Model.InserBatch_modle;
import com.tutionize.androidstudionavigationdrawer.Model.LearnerUpdate_modle;
import com.tutionize.androidstudionavigationdrawer.Model.LoginModel;
import com.tutionize.androidstudionavigationdrawer.Model.PaymentProff_Modle;
import com.tutionize.androidstudionavigationdrawer.Model.RegisterModel;
import com.tutionize.androidstudionavigationdrawer.Model.Requested_student_list;
import com.tutionize.androidstudionavigationdrawer.Model.Start_class_modle;
import com.tutionize.androidstudionavigationdrawer.Model.Student_Notification_modle;
import com.tutionize.androidstudionavigationdrawer.Model.TeacherDetailModle;
import com.tutionize.androidstudionavigationdrawer.Model.TeacherOnCourseModel;
import com.tutionize.androidstudionavigationdrawer.Model.TeacherReview_Modle;
import com.tutionize.androidstudionavigationdrawer.Model.TeacherUpdate_modle;
import com.tutionize.androidstudionavigationdrawer.Model.UploadProfilePic_modle;
import com.tutionize.androidstudionavigationdrawer.Model.UserDetail_modle;
import com.tutionize.androidstudionavigationdrawer.Model.VideoUploadData_modle;
import com.tutionize.androidstudionavigationdrawer.Model.delete_notification_modle;
import com.tutionize.androidstudionavigationdrawer.Model.notificationdata_modle;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.mime.TypedFile;

/**
 * Created by NAVJOT SINGH on 02-09-2017.
 */

public interface All_APIs
{

    //======== course APi ==


    @POST("/get_courses")
    void getCourse(Callback<Cources_modle> response);


//    *****************************************  REGISTER USER   API  *****************************************************

    @Multipart
    @POST("/register")
    void userRegisteration(@Part("name") String name,
                           @Part("email") String email,
                           @Part("password") String password,
                           @Part("type") String type,
                           Callback<RegisterModel> response);

    //    *****************************************LOGIN USER*****************************************************

    @Multipart
    @POST("/signin")
    void userlogin(@Part("email") String email,
                   @Part("password") String password,
                   Callback<LoginModel> response);

    //    *****************************************forgetpassword********************************

    @Multipart
    @POST("/forget_password")
    void forgetpassword(@Part("email") String email,
                        Callback<ForgetModel> responce);

//    *****************************************Update Teacher profile***************************

    @Multipart
    @POST("/teacherupdateallfield")
     void updateTeacherProfileallfield(
                          @Part("user_id") String user_id,
                          @Part("username")String username,
                          @Part("gender") String gender,
                          @Part("state") String state,
                          @Part("city") String city,
                          @Part("country") String country,
                          @Part("qualification") String qualification,
                          @Part("bio") String bio,
                          @Part("photo") TypedFile photo,
                          @Part("cource_id") String course,
                          @Part("course_price")String course_price,
                          @Part("phone") String phone,
                          @Part("subject") String subject,
                          @Part("timezone") String timezone,
                          @Part("expirties") String expirties,
                          @Part("language") String language,
                          Callback<TeacherUpdate_modle> res);


    //************************************ Update Teaher Profile ******************************

    @Multipart
    @POST("/teacherupdate")
    void updateTeacherProfile(
            @Part("user_id") String user_id,
            @Part("username")String username,
            @Part("gender") String gender,
            @Part("city") String city,
            @Part("country") String country,
            @Part("qualification") String qualification,
            @Part("bio") String bio,
            @Part("photo") TypedFile photo,
            @Part("cource_id") String course,
            @Part("course_price")String course_price,
            Callback<TeacherUpdate_modle> res);

    //    ***************************************** Contact us *****************************************************

    @Multipart
    @POST("/contactus")
    void contactUs(@Part("name")String name,
                   @Part("email")String email,
                   @Part("subject") String subject,
                   @Part("msg") String msg, Callback<ContactUs_Modle> res);

    //    *****************************************Student Update *****************************************************

    @Multipart
    @POST("/studentupdate")
    void UpdateUserProfile(@Part("user_id")String user_id, @Part("username")String username, @Part("email")String email,
                           @Part("gender")String gender, @Part("city")String city, @Part("country")String country,
                           @Part("photo")TypedFile photo, @Part("street")String street , @Part("area")String area ,
                           @Part("state")String state , @Part("pin")String pin, Callback<LearnerUpdate_modle>res);


    //    *****************************************Change Name *****************************************************

    @Multipart
    @POST("/chnagename")
    void ChangeName(@Part("user_id") String user_id,
                    @Part("username") String username, Callback<ChangeName_modle> res);

    //    *****************************************Change Email *****************************************************

    @Multipart
    @POST("/chnageemail")
    void ChangeEmail(@Part("user_id") String user_id,
                    @Part("email") String email, Callback<ChangeEmail_modle> res);


    //    *****************************************Change Password  *****************************************************

    @Multipart
    @POST("/chnagepass")
    void ChangePassword(@Part("user_id") String user_id,
                    @Part("pass") String pass, Callback<ChangePassword_modle> res);



    //    *****************************************Upload ProfilePic   *****************************************************


    @Multipart
    @POST("/updateprofilepic")
    void UploadProfilePic(@Part("user_id")String user_id,
                          @Part("photo") TypedFile photo,Callback<UploadProfilePic_modle> res);


    //    ******************** Teacher On based on Course id  *************************************


    @Multipart
    @POST("/serachteacheroncourse")
    void TeacherOnCourse(@Part("id")String id,
                          Callback<TeacherOnCourseModel> res);


    //    ************* Teacher Detail on Based  of ID   ****************************************

    @Multipart
    @POST("/teacherdetails")
    void TeacherDetail(@Part("userid")String userid,
                         Callback<TeacherDetailModle> res);


     //    *************************    get User Profile Pic on based of userid   *****************************************

    @Multipart
    @POST("/getprofilepic")
    void GetProfilePic(@Part("user_id")String userid,
                       Callback<GetProfilePic_modle> res);

    //    *****************************************    get User Twellio  Token  *****************************************************

    @Multipart
    @POST("/twiliotoken")
    void getToken(@Part("roomname")String roomname ,
                  @Part("identity")String identity ,
                  Callback<GetToken_model> res);

    //    **********************  Upload  teacher Video   *****************************************************

    @Multipart
    @POST("/upload_video")
     void  UploadVideo(@Part("teacherid")String teacherid,
                               @Part("title")String title,
                               @Part("desc")String desc,
                               @Part("courseprice")String courseprice,
                               @Part("video")TypedFile video,
                               Callback<VideoUploadData_modle> res);



    //    ******************  Get  teacher  Videos on based of Teacher id  *********************



    @Multipart
    @POST("/get_video")
    void getTeacherVideo(@Part("teacherid")String teacherid,
                          Callback<GetVideo_model> res);





    //    **************** Hire Teacher  *************************************

    @Multipart
    @POST("/hire_teacher")
    void HireTeacher(@Part("teacher")String teacher,
                     @Part("student")String student,
                         Callback<HireTeacher_modle> res);



    // ***************** All student whose  hired of completed  Teacher payment  ***************

    @Multipart
    @POST("/hirestudentlist")
    void HireStudent(@Part("teacher")String teacher,
                     Callback<HireStudentList_modle> res);


    //    ****************** Unselected  batch Student   *****************************

    @Multipart
    @POST("/allteacheractivestudentlist")
    void UnselectedBatchStudent(@Part("batch")String batch,
                                @Part("teacher")String teacher,
                                 Callback<Get_UnSelectedStudent_modle> res);




    //    *****************************************  Create Teacher Batch  *****************************************************

    @Multipart
    @POST("/insertbatch")
    void InserBatch(@Part("teacher")String teacher,
                    @Part("time")String time,
                    @Part("endtime")String endtime,
                    @Part("batchname")String batchname,
                    @Part("duration")String duration,
                    @Part("photo")TypedFile photo,
                    @Part("students_ids")String students_ids,
                    Callback<InserBatch_modle> res);


    //    *****************************************  Edit Teacher Batch  *****************************************************

    @Multipart
    @POST("/updatebatch")
    void EditBatch(@Part("batchid")String batchid,
                    @Part("name")String name,
                    @Part("start_time")String start_time,
                    @Part("end_time")String end_time,
                    @Part("duration")String duration,
                    @Part("photo")TypedFile photo,
                    Callback<EditBAtch_modle> res);

    //    ***************************************** Batch  List   *****************************************************

    @Multipart
    @POST("/allbatch")
    void AllBatch(@Part("teacherid")String teacherid,
                    Callback<All_Batch_modle> res);



    //    ***************************************** Add sududent into Batch   *****************************************************

    @Multipart
    @POST("/addstudenttobatch")
    void AddStudentToBatch(@Part("teacher")String teacherid,
                           @Part("student")String student,
                           @Part("batch")String batch,
                            Callback<AddStudentToBatch_modle> res);

    //    ***************************************** ReMOVE student from Batch   *****************************************************

    @Multipart
    @POST("/removestudenttobatch")
    void RemoveStudentToBatch(@Part("teacher")String teacherid,
                           @Part("student")String student,
                           @Part("batch")String batch,
                           Callback<DeleteStudentToBatch> res);

    //    *********************  All Batch Student    **************************

    @Multipart
    @POST("/allstudentbatchlist")
    void AllBatchStudent(@Part("teacher")String teacherid,
                         @Part("batch")String batch,
                         Callback<All_Student_of_batch> res);

    //    *************** Firebase Token submit to database  **************************

    @Multipart
    @POST("/submittoken")
    void submittoken(@Part("user_id")String user_id,
                         @Part("token")String token,
                         @Part("type")String type,
                         Callback<DeleteStudentToBatch> res);


    //    *****************  Student Requests   ***************************

    @Multipart
    @POST("/requestlist")
    void Requestlist(@Part("teacher")String teacher,
                     Callback<Requested_student_list> res);



    //    ********************** Accept Request 0f student ****************************


    @Multipart
    @POST("/addnotifytostudent")
    void AcceptRequest(@Part("teacher_id")String teacher_id,
                     @Part("student_id")String student_id,
                     Callback<Student_Notification_modle> res);

    //    ***************************************** Reject Request 0f student *****************************************************


    @Multipart
    @POST("/studentreject")
    void RejectRequest(@Part("teacher_id")String teacher_id,
                         @Part("student_id")String student_id,
                         Callback<Student_Notification_modle> res);


//    *****************************************Delete Batch *****************************************************


    @Multipart
    @POST("/deletebatch")
    void DeleteBatch(@Part("batchid")String batchid,
                       Callback<Delete_batch_modle> res);


   //   ***************************************** video and Audio Call API  *****************************************************


    @Multipart
    @POST("/addnotifytoallstudent")
    void SetClassAddress(@Part("student_id")String student_id,
                         @Part("room_name")String room_name,
                         @Part("teacher_name")String teacher_name,
                         @Part("img_name")String img_name,
                         @Part("call_type")String call_type,
                         @Part("caller_id")String caller_id,
                         Callback<Start_class_modle> res);

    //   *****************************************get Country List *****************************************************

    @GET("/countrylist")
    void getCountry(Callback<GetCountry_modle>res);


    //   *****************************************get State List *****************************************************

    @Multipart
    @POST("/statelist")
    void getState(@Part("country_code")String country_code,
                  Callback<Get_State_modle> res);



    //   ******************Submit  Payment proof after payment *********************************

    @Multipart
    @POST("/paymentdetailsofteacher")
    void paymenyproff(@Part("payment_id")String payment_id,
                       @Part("student_id")String student_id,
                       @Part("teacher_id")String teacher_id,
                       @Part("payment_time")String payment_time,
                       @Part("payment_status")String payment_status,
                       @Part("course")String course,
                       @Part("payment_amount")String payment_amount,
                       Callback<PaymentProff_Modle> res);


    //   *****************************************Submit Student rate  for teacher*****************************************************


    @Multipart
    @POST("/teacherrate")
    void StudentReview(@Part("review")String teacherrate,
                       @Part("teacherid")String teacherid,
                       Callback<TeacherReview_Modle> res);


    //   *****************************************Add Bank detail *****************************************************


    @Multipart
    @POST("/register_bank_update")
    void AddBankDetail(@Part("id")String id,
                       @Part("new_account_number")String new_account_number,
                       @Part("account_name")String account_name,
                       @Part("swift_code")String swift_code,
                       @Part("IFSC_code")String IFSC_code,
                       @Part("other_code")String other_code,
                       Callback<BankDetail_Modle> res);


    //   ****************** Update Student Profile ********************

    @Multipart
    @POST("/updateuserdetailsnew")
    void Studentdetail(@Part("id")String id,
                       Callback<UserDetail_modle> res);

    // ===================================== Notification ================================

    @Multipart
    @POST("/get_notification")
    void getNotification(@Part("user_id")String id,
                       Callback<notificationdata_modle> res);

    // =====================================  delete Notification ================================

    @Multipart
    @POST("/delete_notification")
    void deletenotification(@Part("id")String id,
                         Callback<delete_notification_modle> res);


}
