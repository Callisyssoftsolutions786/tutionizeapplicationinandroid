package com.tutionize.androidstudionavigationdrawer.command;

public interface FragmentStartClassCommand {
    public void ClassDecline();
    public void ClassAccept();
}
