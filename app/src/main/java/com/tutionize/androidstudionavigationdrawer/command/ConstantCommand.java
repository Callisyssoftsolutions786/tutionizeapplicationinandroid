package com.tutionize.androidstudionavigationdrawer.command;

public interface ConstantCommand {
    String callType_Audio = "Audio";
    String callType_Video = "Video";
    String callType_Video_1 = "Video1";
    String callType_Accepted = "CallAccepted";

    String callType_End= "CallEnd";
    String callType_MuteAudio = "MuteAudio";
    String callType_UnMuteAudio = "UnMuteAudio";


    String Profile_Type_T = "Teacher";
    String Profile_Type_S = "Student";

    String NOTIFICATION_TOKEN_TYPE =  "1";

    String STUDENT_REGISTATION_MSG = "You have resisted as a student successfully.\n\n"+
            " Please check your email and verify your email address by clicking on verification button.\n"+
            "If you don’t see our email in your inbox, please check your junk mail before you are contacting us.";

    String TEACHER_REGISTATION_MSG = " You have registered as a teacher successfully.\n\n" +
            "Look for the verification email in your inbox and click the link in that email to verify your account.\n" +
            "If you don’t see our email in your inbox, please check your junk mail before you are contacting us.\n";

    String PANDING_PAYMENT_MSG = "You have completed your payment successfully.\n\n" +
            "Due to the information for admin, it is necessary to user check your internet connection and try it again.\n\n";

    String ForGot_PASSWORD_MDG = "Your password has been changed successfully, Please check your email.";
}
