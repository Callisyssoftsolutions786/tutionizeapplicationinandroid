package com.tutionize.androidstudionavigationdrawer.Activity_Screen;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.autofit.et.lib.AutoFitEditText;
import com.eminayar.panter.PanterDialog;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.tutionize.androidstudionavigationdrawer.Constant.Singleton;
import com.tutionize.androidstudionavigationdrawer.Model.RegisterModel;
import com.tutionize.androidstudionavigationdrawer.ProgressDialog;
import com.tutionize.androidstudionavigationdrawer.R;
import com.tutionize.androidstudionavigationdrawer.Sharepool.Chache_Clear;
import com.tutionize.androidstudionavigationdrawer.Sharepool.MySharePrafranceClass;
import com.tutionize.androidstudionavigationdrawer.Sharepool.NetworkStateCheck;
import com.tutionize.androidstudionavigationdrawer.command.ConstantCommand;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * Created by hp on 15-02-2017.
 */
public class Registration_Screen extends Activity {

    private EditText txt_password;
    private AutoFitEditText txt_name,txt_email;
    private String getname,getemail,getpassword;String type;
    private Button btn_register;
    private RadioButton btn_radioStudent;
    private int profile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.registration_screen);
        FINDVIEWBYID();
        CLICKLISTINER();
    }

    private void FINDVIEWBYID() {

        txt_name= (AutoFitEditText) findViewById(R.id.txt_name_register);
        txt_name.setMinTextSize(10f);
        txt_email= (AutoFitEditText) findViewById(R.id.txt_email_register);
        txt_email.setMinTextSize(10f);
        txt_password= (EditText) findViewById(R.id.txt_password_register);
        btn_register= (Button) findViewById(R.id.btn_register);
        RadioButton btn_radioTeacher = (RadioButton) findViewById(R.id.radio_teacher);
        btn_radioStudent=(RadioButton) findViewById(R.id.radio_student);
    }

    private void CLICKLISTINER() {
        btn_register.setOnClickListener((View view)->{
            getname=txt_name.getText().toString();
            getemail=txt_email.getText().toString();
            getpassword=txt_password.getText().toString();
            if (!validate())
            {
                Toast.makeText(Registration_Screen.this, "SignUp has Failed", Toast.LENGTH_SHORT).show();
            }else
                {
                Register_user(getname,getemail,getpassword,type);
            }
        });
    }


    public boolean validate(){
        int userprofile=getUserprofile();
        boolean valid = true;
        if (getname.isEmpty()||getname.length()>32){
            txt_name.setError("Please Enter Valid Name ");
            valid=false;
        }
        if (getemail.isEmpty()||!Patterns.EMAIL_ADDRESS.matcher(getemail).matches()){
            txt_email.setError("Please Enter Valid EMAIL");
            valid=false;
        }
        if (getpassword.isEmpty()||getpassword.length()<6||getpassword.length()>20){
            txt_password.setError("Please Enter Valid PASSWORD");
            valid=false;
        }

        if (userprofile==0){
            type="Student";
        }else{
            type="Teacher";
        }

        if (type.isEmpty()){
            valid=false;
        }

        return valid;
    }

    @SuppressLint("ResourceAsColor")
    private void Register_user(final String getname, final String getemail, String getpassword, String type) {
        if (NetworkStateCheck.isNetworkAvaliable(Registration_Screen.this)) {
            KProgressHUD knprogress = ProgressDialog.getInstance().getKNProgressDialog(Registration_Screen.this);
            knprogress.show();
            Singleton.getInstance().getApi().userRegisteration(getname, getemail, getpassword, type, new Callback<RegisterModel>() {
                @Override
                public void success(RegisterModel registerModel, Response response) {
                    String status = registerModel.getSuccess();
                    Integer userid = registerModel.getUserid();
                    if (status.equalsIgnoreCase("1")) {
                        if (profile == 1) {
                            MySharePrafranceClass.Share(Registration_Screen.this,
                                    userid + "", getname, "");
                            MySharePrafranceClass.ShareTeacherDetailEdit(Registration_Screen.this, "False");
                            knprogress.dismiss();
                            Toast.makeText(Registration_Screen.this, "" + registerModel.getMessage(), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(Registration_Screen.this, Edit_Teacher_Profile.class);
                            intent.putExtra("username", getname);
                            intent.putExtra("userid", userid);
                            startActivity(intent);
                            finish();
                        } else {
                            knprogress.dismiss();
                            ActivateInfoAfterLogin(ConstantCommand.STUDENT_REGISTATION_MSG);
                            Toast.makeText(Registration_Screen.this,
                                    "" + registerModel.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        knprogress.dismiss();
                        Toast.makeText(Registration_Screen.this, "" + registerModel.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    knprogress.dismiss();
                    Toast.makeText(Registration_Screen.this, "User Failed to Register", Toast.LENGTH_SHORT).show();
                }
            });
        }else {
            Toast.makeText(this, "Internet connection problem", Toast.LENGTH_SHORT).show();
        }
    }
    private int getUserprofile(){
         if (btn_radioStudent.isChecked()){
             profile=0;
         }else {
             profile=1;
         }
         return profile;
    }
    public void  ActivateInfoAfterLogin(String message) {

        new PanterDialog(this)
                .setHeaderBackground(R.drawable.pattern_bg_orange)
                .setHeaderLogo(R.drawable.logo)
                .setTitle("Register Successfull")
                .setPositive("I GOT IT", v -> {
                    MySharePrafranceClass.ClearAll(Registration_Screen.this);
                    MySharePrafranceClass.ClearAllLoginData(Registration_Screen.this);
                    MySharePrafranceClass.ClearEditTeacherDetail(Registration_Screen.this);
                    Chache_Clear.deleteCache(Registration_Screen.this);
                    Intent intent=new Intent(Registration_Screen.this,Login_screen.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(intent);
                    finish();
                })
                .setMessage(message)
                .isCancelable(false)
                .show();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent= new Intent(Registration_Screen.this,Login_screen.class);
        startActivity(intent);
        finish();
    }
}
