package com.tutionize.androidstudionavigationdrawer.Activity_Screen;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.view.Window;
import android.view.WindowManager;

import com.tutionize.androidstudionavigationdrawer.Constant.Message_dialoge;

import java.util.Objects;

/**
 * Created by Vishal Sandhu on 12/18/17.
 */

public class MyAlertDialog extends Activity {
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        MyAlertDialog.this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                + WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD|
                + WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED|
                + WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
                requestWindowFeature(Window.FEATURE_NO_TITLE);
         String message = Objects.requireNonNull(getIntent().getExtras()).getString("msg1");
         Message_dialoge.getInstance().messagedialog(MyAlertDialog.this,message);
    }

}
