package com.tutionize.androidstudionavigationdrawer.Activity_Screen;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.eminayar.panter.PanterDialog;
import com.iceteck.silicompressorr.SiliCompressor;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.tutionize.androidstudionavigationdrawer.BuildConfig;
import com.tutionize.androidstudionavigationdrawer.Constant.LiveData;
import com.tutionize.androidstudionavigationdrawer.Constant.Picture_method;
import com.tutionize.androidstudionavigationdrawer.Constant.Singleton;
import com.tutionize.androidstudionavigationdrawer.Model.Cources_modle;
import com.tutionize.androidstudionavigationdrawer.Model.CourseData;
import com.tutionize.androidstudionavigationdrawer.Model.TeacherUpdate_modle;
import com.tutionize.androidstudionavigationdrawer.ProgressDialog;
import com.tutionize.androidstudionavigationdrawer.R;
import com.tutionize.androidstudionavigationdrawer.Sharepool.Chache_Clear;
import com.tutionize.androidstudionavigationdrawer.Sharepool.Check_Required_Premission;
import com.tutionize.androidstudionavigationdrawer.Sharepool.MySharePrafranceClass;
import com.tutionize.androidstudionavigationdrawer.Sharepool.NetworkStateCheck;
import com.tutionize.androidstudionavigationdrawer.Sharepool.Spinner_data;
import com.tutionize.androidstudionavigationdrawer.command.ConstantCommand;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

public class Edit_Teacher_Profile extends AppCompatActivity {
    private TextView txt_titlename;
    private EditText edt_Username,edt_bio,edt_course_price,edt_subjectlist,edt_expirties,edt_languages,edt_phoneNo;
    // edt_bank_detail,edt_statement,edt_review;
    private Button btn_submit_teacher_profile,btn_Upload_profile_pic;
    private Spinner edt_gender, edt_qualification ,edt_Course, edt_contry,edt_timezone;
    private AutoCompleteTextView edt_city,edt_State;
    private ImageView txt_picture;
    private String SelectImage_teacherprofile="";
    private  File file_teacherprofile;
    private Uri photofile_teacherprofile;
    private static  String current;
    private static final int CAMERACODE=10;
    private static final int GALLERYCODE=11;
    private static final int MEDIA_TYPE_IMAGE = 1;
    private String ed_Username,ed_Gender,ed_bio,ed_state , ed_city,ed_country,ed_qualifiction,ed_course,ed_phoneNo,ed_subjectlist,ed_cource_price,ed_timezone ,ed_languages,ed_expirties;
    private TypedFile file;
    private  String userid;

    private  List<CourseData> courcedata = new ArrayList<>();
    private List<String> courselist= new ArrayList<>();
    private List<String> CityNamelist= new ArrayList<>();
    private ArrayAdapter<String> spinnerCourseAdapter;
    private  ArrayAdapter<String> spinnerCityAdapter;
    private AlertDialog alertDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit__teacher__profile);
        Check_Required_Premission.CheckPermission(Edit_Teacher_Profile.this);
        Cource();
        findView();
        Shareable();
        setAdapterlist();
        Clickable();

    }
    @SuppressLint("SetTextI18n")
    private void Clickable() {
       btn_submit_teacher_profile.setOnClickListener((View view)-> {

           ed_Username=edt_Username.getText().toString();
           ed_Gender= edt_gender.getSelectedItem().toString();
           ed_bio= edt_bio.getText().toString();
           ed_city =edt_city.getText().toString();
           ed_state =edt_State.getText().toString();
           ed_country=edt_contry.getSelectedItem().toString();
           ed_qualifiction=edt_qualification.getSelectedItem().toString();
           ed_course=edt_Course.getSelectedItem().toString();
           ed_cource_price = edt_course_price.getText().toString();
           ed_phoneNo =  edt_phoneNo.getText().toString();
           ed_subjectlist = edt_subjectlist.getText().toString();
           ed_expirties =  edt_expirties.getText().toString();
           ed_languages =  edt_languages.getText().toString();

           ed_timezone = edt_timezone.getSelectedItem().toString();

           if (!validate())
           {
               Toast.makeText(Edit_Teacher_Profile.this, "Your Request Has Failed", Toast.LENGTH_SHORT).show();
           }
           else if (SelectImage_teacherprofile.equals(""))
           {
               Toast.makeText(Edit_Teacher_Profile.this, "Please Select Image First", Toast.LENGTH_SHORT).show();
           }
           else {

               String CompressImage = SiliCompressor.with(Edit_Teacher_Profile.this).compress(SelectImage_teacherprofile,Picture_method.getInstance().getDestinationPath(Edit_Teacher_Profile.this), true);
               file = new TypedFile("image/*",new File(CompressImage));
               updateTeacherProfile(userid,ed_Username,ed_Gender,ed_state,file,ed_city,ed_country,ed_qualifiction,ed_bio,ed_course,ed_cource_price
               ,ed_phoneNo, ed_subjectlist,ed_expirties,ed_languages,ed_timezone);
           }
    });

       btn_Upload_profile_pic.setOnClickListener(view -> {
           Check_Required_Premission.CheckCameraPermission(Edit_Teacher_Profile.this);
           Check_Required_Premission.checkStorageReadPermition(Edit_Teacher_Profile.this);
           Check_Required_Premission.checkStorageWritePermition(Edit_Teacher_Profile.this);
           AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Edit_Teacher_Profile.this);
           LayoutInflater inflater = Edit_Teacher_Profile.this.getLayoutInflater();

           @SuppressLint("InflateParams")
           View dialogView = inflater.inflate(R.layout.choose_image_option, null);
           LinearLayout layout_Camera = dialogView.findViewById(R.id.layout_Camera);
           LinearLayout layout_gallery = dialogView.findViewById(R.id.layout_gallery);
           LinearLayout layout_Cancel = dialogView.findViewById(R.id.layout_Cancel);
           TextView dialog_titel = dialogView.findViewById(R.id.dialog_title);

           dialog_titel.setText("Profile Photo");

           layout_Camera.setOnClickListener((View v) ->{


               Intent cameraintent = new Intent("android.media.action.IMAGE_CAPTURE");

               if (cameraintent.resolveActivity(Edit_Teacher_Profile.this.getPackageManager()) != null) {
                   file_teacherprofile = null;
                   try
                   {
                       file_teacherprofile = Picture_method.getInstance().getOutputMediaFile(MEDIA_TYPE_IMAGE);
                       current = Picture_method.getInstance().getCurrent();
                   } catch (Exception ex)
                   {
                       return;
                   }
                   if (file_teacherprofile != null)
                   {
                       photofile_teacherprofile = FileProvider.getUriForFile(Edit_Teacher_Profile.this,
                               BuildConfig.APPLICATION_ID + ".provider", file_teacherprofile);
                       cameraintent.putExtra(MediaStore.EXTRA_OUTPUT, photofile_teacherprofile);
                       if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                       {
                           cameraintent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                       }
                       else {
                           List<ResolveInfo> resInfoList = getPackageManager().queryIntentActivities(cameraintent, PackageManager.MATCH_DEFAULT_ONLY);
                           for (ResolveInfo resolveInfo : resInfoList)
                           {
                               String packageName = resolveInfo.activityInfo.packageName;
                               Edit_Teacher_Profile.this.grantUriPermission(packageName,
                                       photofile_teacherprofile,Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                           }
                       }
                       startActivityForResult(cameraintent,CAMERACODE);
                   }

               }
               alertDialog.dismiss();
           });

           layout_gallery.setOnClickListener((View v)->
           {
               Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
               startActivityForResult(intent, GALLERYCODE);
               alertDialog.dismiss();

           });

           layout_Cancel.setOnClickListener((View v)->{
               alertDialog.cancel();
           });

           dialogBuilder.setView(dialogView);
           alertDialog = dialogBuilder.create();
           alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
           WindowManager.LayoutParams lp =  Objects.requireNonNull(alertDialog.getWindow()).getAttributes();
           lp.gravity = Gravity.TOP;
           lp.windowAnimations = R.style.dialog_animation;
           lp.width = LinearLayout.LayoutParams.FILL_PARENT;
           alertDialog.show();
       });
        }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode==CAMERACODE){
            if (resultCode==RESULT_OK){
                String filePath = current;
                if (filePath != null) {
                    SelectImage_teacherprofile= filePath;
                    txt_picture.setImageURI(Uri.parse(filePath));
                }
                else
                    {
                    Toast.makeText(Edit_Teacher_Profile.this,
                            "Sorry, file path is missing!", Toast.LENGTH_LONG).show();
                }
            }
        }
        else if (requestCode==GALLERYCODE){
            if (resultCode==RESULT_OK)
            {
                Uri gelleryUri = Uri.parse(String.valueOf(Picture_method.getInstance().getPathFromUri(data.getData(), Edit_Teacher_Profile.this)));
                String gallerypath_teacherprofile = gelleryUri.getPath();
                if (gallerypath_teacherprofile !=null)
                {
                    SelectImage_teacherprofile= gallerypath_teacherprofile;
                    txt_picture.setImageURI(Uri.parse(gallerypath_teacherprofile));

                }else{
                    Toast.makeText(Edit_Teacher_Profile.this,
                            "Sorry, file path is missing!", Toast.LENGTH_LONG).show();
                }
            }
        }
    }
    @SuppressLint("ResourceAsColor")
    private void updateTeacherProfile(String userid, String ed_username, String ed_gender,String ed_state, TypedFile s,
                                      String ed_city,String ed_country, String ed_qualifiction, String ed_bio,
                                      String ed_course,String course_price,String ed_phoneNo, String ed_subjectlist,String ed_expirties,String ed_languages,String ed_timezone) {
        String coursevalidid = "";
          for(int i = 0; i<courcedata.size();i++ )
          {
              if (courcedata.get(i).getCourse_category().equals(ed_course)){
                  coursevalidid = courcedata.get(i).getId();
              }
          }
            if (NetworkStateCheck.isNetworkAvaliable(Edit_Teacher_Profile.this)) {
                KProgressHUD knprogress = ProgressDialog.getInstance().getKNProgressDialog(Edit_Teacher_Profile.this);;
                knprogress.show();
                Singleton.getInstance().getApi().updateTeacherProfileallfield(userid, ed_username, ed_gender,ed_state,ed_city, ed_country,
                        ed_qualifiction, ed_bio, s, coursevalidid, course_price, ed_phoneNo,
                        ed_subjectlist, ed_timezone, ed_expirties,ed_languages ,new Callback<TeacherUpdate_modle>() {
                            @Override
                            public void success(TeacherUpdate_modle teacherUpdate_modle, Response response) {

                                if (teacherUpdate_modle.getSuccess().equals("1")) {
                                   // Toast.makeText(Edit_Teacher_Profile.this, "" + teacherUpdate_modle.getResponse(), Toast.LENGTH_SHORT).show();
                                    edt_bio.setText("");
                                    edt_Username.setText("");
                                    knprogress.dismiss();
                                    MySharePrafranceClass.ClearEditTeacherDetail(Edit_Teacher_Profile.this);
                                    ActivateInfoAfterLogin(ConstantCommand.TEACHER_REGISTATION_MSG);
                                } else {
                                    knprogress.dismiss();
                                    Toast.makeText(Edit_Teacher_Profile.this, "" + teacherUpdate_modle.getResponse(), Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                knprogress.dismiss();
                                Toast.makeText(Edit_Teacher_Profile.this, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
            }else {
                Toast.makeText(this, "Internet connection problem", Toast.LENGTH_SHORT).show();
            }
    }


    private void findView()
    {
        edt_course_price = findViewById(R.id.edt_course_price);
        edt_timezone = findViewById(R.id.edt_timezone);
        edt_expirties = findViewById(R.id.edt_expirties);
        edt_languages = findViewById(R.id.edt_languages);
        edt_subjectlist = findViewById(R.id.edt_subjectlist);
        edt_phoneNo  = findViewById(R.id.edt_phoneNo);
        txt_titlename = findViewById(R.id.txt_title);
        edt_Username= findViewById(R.id.edt_username);
        edt_gender= findViewById(R.id.edt_gender);
        edt_bio= findViewById(R.id.edt_bio);
        edt_city= findViewById(R.id.edt_City);
        edt_State= findViewById(R.id.edt_State);
        edt_contry= findViewById(R.id.edt_contry);
        edt_qualification= findViewById(R.id.edt_qualificatio);
        edt_Course= findViewById(R.id.edt_Course);
        btn_submit_teacher_profile= findViewById(R.id.btn_teacher_profile);
       // Objects.requireNonNull(btn_submit_teacher_profile).setVisibility(View.GONE);
        btn_Upload_profile_pic= findViewById(R.id.btn_upload_teacher_picture);
        txt_picture= findViewById(R.id.edt_teacher_picture);

    }

    //-------------------------------------------Hellper Method ----------------------------

    private void Shareable() {
        String[] list = MySharePrafranceClass.GetSharePrefrance(Edit_Teacher_Profile.this);
        String name;
        if (list != null){
            userid = list[0];
            name = list[1];
        }else {
            name = getIntent().getStringExtra("username");
            userid = getIntent().getStringExtra("userid");
            txt_titlename.setText(name);
            edt_Username.setText(name);
        }
        txt_titlename.setText(name);
        edt_Username.setText(name);
    }
    private void setAdapterlist() {

        List<String> TimeZonelist = Spinner_data.getTimeZone();
        final ArrayAdapter<String> spinnertimezonAdapter = new ArrayAdapter<>(
                Edit_Teacher_Profile.this,R.layout.support_simple_spinner_dropdown_item,TimeZonelist);
        spinnertimezonAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        edt_timezone.setAdapter(spinnertimezonAdapter);


        List<String> qualifictionlist = Spinner_data.getQualification();
        final ArrayAdapter<String> spinnerQualifictionAdapter = new ArrayAdapter<>(
                Edit_Teacher_Profile.this, R.layout.support_simple_spinner_dropdown_item,qualifictionlist);
        spinnerQualifictionAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        edt_qualification.setAdapter(spinnerQualifictionAdapter);


        List<String> gender = Spinner_data.getGenderList();
        final ArrayAdapter<String> spinnerGenderAdapter = new ArrayAdapter<>(
                Edit_Teacher_Profile.this,R.layout. support_simple_spinner_dropdown_item,gender);
        spinnerGenderAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        edt_gender.setAdapter(spinnerGenderAdapter);

        spinnerCourseAdapter = new ArrayAdapter<>(Edit_Teacher_Profile.this,R.layout. support_simple_spinner_dropdown_item,courselist);
        spinnerCourseAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        edt_Course.setAdapter(spinnerCourseAdapter);

        ArrayAdapter<String> spinnerCountryAdapter = new ArrayAdapter<>(Edit_Teacher_Profile.this,R.layout. support_simple_spinner_dropdown_item,LiveData.getCountry());
        spinnerCountryAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        edt_contry.setAdapter(spinnerCountryAdapter);


        spinnerCityAdapter = new ArrayAdapter<>(Edit_Teacher_Profile.this,R.layout. support_simple_spinner_dropdown_item,CityNamelist);
        spinnerCityAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        edt_city.setAdapter(spinnerCityAdapter);

    }

    //============================================helper Method================================

//    private void ListofState(String code) {
//
//
//        if (!CityNamelist.isEmpty()){
//            Citylist.clear();
//            CityNamelist.clear();
//        }
//
//         Singleton.getInstance().getApi().getState(code, new Callback<Get_State_modle>() {
//            @Override
//            public void success(Get_State_modle get_state_modle, Response response) {
//                Citylist=get_state_modle.getResult();
//
//                for (int index=0;index<Citylist.size();index++){
//
//                    CityNamelist.add(Citylist.get(index).getRegion());
//                }
//                spinnerCityAdapter = new ArrayAdapter<>(Edit_Teacher_Profile.this,R.layout. support_simple_spinner_dropdown_item,CityNamelist);
//                spinnerCityAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
//                edt_city.setAdapter(spinnerCityAdapter);
//            }
//            @Override
//            public void failure(RetrofitError error) {}
//        });
//    }

    public void  ActivateInfoAfterLogin(String message)
    {
        new PanterDialog(this)
                .setHeaderBackground(R.drawable.pattern_bg_orange)
                .setHeaderLogo(R.drawable.logo)
                .setTitle("Register Successfull")
                .setPositive("I GOT IT", v -> {
                    MySharePrafranceClass.ClearAll(Edit_Teacher_Profile.this);
                    MySharePrafranceClass.ClearAllLoginData(Edit_Teacher_Profile.this);
                    MySharePrafranceClass.ClearEditTeacherDetail(Edit_Teacher_Profile.this);
                    Chache_Clear.deleteCache(Edit_Teacher_Profile.this);
                    Intent intent = new Intent(Edit_Teacher_Profile.this, Login_screen.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(intent);
                    finish();
                })

                .setMessage(message)
                .isCancelable(false)
                .show();
    }
    private boolean validate(){
        boolean valid = true;

        if (ed_Username.isEmpty()){
            edt_Username.setError("Enter Valid User Name");
            valid=false;
        }

        if (ed_Gender.equals("Select Gender")){
            Toast.makeText(Edit_Teacher_Profile.this, "Select Valid Gender", Toast.LENGTH_SHORT).show();
            valid=false;
        }
        if (ed_country.equals("Select Country"))
        {
            Toast.makeText(Edit_Teacher_Profile.this, "Select Valid Country", Toast.LENGTH_SHORT).show();
            valid=false;
        }
        if (ed_city.isEmpty()){
            edt_city.setError("Enter Valid City");
            valid=false;
        }

        if (ed_state.isEmpty()){
            edt_State.setError("Enter Valid City");
            valid=false;
        }
        if (ed_qualifiction.equals("Select Qualification")){
            Toast.makeText(Edit_Teacher_Profile.this, "Select Valid Qualification", Toast.LENGTH_SHORT).show();
            valid=false;
        }
        if (ed_course.equals("Select Course")){
            Toast.makeText(Edit_Teacher_Profile.this, "Enter Valid Course", Toast.LENGTH_SHORT).show();
            valid=false;
        }

        if (ed_bio.isEmpty()||ed_bio.length()<10)
        {
            edt_bio.setError("Enter Valid Biographic");
            valid=false;
        }
        if (ed_cource_price.isEmpty()){
            edt_course_price.setError("Enter Valid Price");
            valid=false;
        }
        if (ed_phoneNo.isEmpty()||ed_phoneNo.length()<10){
            edt_phoneNo.setError("Enter Valid Phone No.");
            valid=false;
        }
        if (ed_subjectlist.isEmpty()){
            edt_subjectlist.setError("Enter Valid Subject/Skills");
            valid=false;
        }
        if (ed_expirties.isEmpty()){
            edt_expirties.setError("Enter Valid Expirties");
            valid=false;
        }

        if (ed_languages.isEmpty()){
            edt_languages.setError("Enter Valid Languages");
            valid=false;
        }


        if (ed_timezone.equals("Select TimeZone")){
            Toast.makeText(Edit_Teacher_Profile.this, "Select Valid TimeZone", Toast.LENGTH_SHORT).show();

            valid=false;
        }
        return valid;
    }

    private void Cource() {

        if (NetworkStateCheck.isNetworkAvaliable(getApplication()))
        {
            Singleton.getInstance().getApi().getCourse(new Callback<Cources_modle>() {
                @Override
                public void success(Cources_modle cources_modle, Response response) {
                    courcedata = cources_modle.getData();
                    courselist.add("Select Course");
                    for (CourseData e : courcedata) {
                        courselist.add(e.getCourse_category());
                    }
                    if (spinnerCourseAdapter != null) {
                        spinnerCourseAdapter.notifyDataSetChanged();
                    }
                }
                @Override
                public void failure(RetrofitError error) {

                }
            });

        }else {
            Toast.makeText(this, "Internet connection problem", Toast.LENGTH_SHORT).show();
        }
    }
}
