package com.tutionize.androidstudionavigationdrawer.Activity_Screen;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.autofit.et.lib.AutoFitEditText;
import com.eminayar.panter.PanterDialog;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.tutionize.androidstudionavigationdrawer.Constant.Singleton;
import com.tutionize.androidstudionavigationdrawer.Model.ForgetModel;
import com.tutionize.androidstudionavigationdrawer.Model.LoginModel;
import com.tutionize.androidstudionavigationdrawer.ProgressDialog;
import com.tutionize.androidstudionavigationdrawer.R;
import com.tutionize.androidstudionavigationdrawer.Sharepool.Chache_Clear;
import com.tutionize.androidstudionavigationdrawer.Sharepool.MySharePrafranceClass;
import com.tutionize.androidstudionavigationdrawer.Sharepool.NetworkStateCheck;
import com.tutionize.androidstudionavigationdrawer.command.ConstantCommand;

import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class Login_screen extends AppCompatActivity {

    private EditText forgetemail,txt_password;
    private AutoFitEditText txt_email ;
    private Button btn_login,forget_Cancel,forgetemail_enter,btn_register;
    private String getemail,getpassword,getforgetemail;
    private LinearLayout txt_forget_password;
    private AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login_screen);
        FINDVIEWBYID();
        ShareElement();
        CLICKLISTINER();
    }

    private void FINDVIEWBYID() {
        txt_email= findViewById(R.id.txt_email_login);
        assert txt_email != null;
        txt_email.setMinTextSize(10f);
        txt_password= findViewById(R.id.txt_password_login);
        btn_login= findViewById(R.id.btn_login);
        btn_register= findViewById(R.id.btn_register1);
        txt_forget_password= findViewById(R.id.txt_forgetpassword);
    }


    private void CLICKLISTINER() {

        btn_login.setOnClickListener((View v) ->{

                getemail = txt_email.getText().toString();
                getpassword = txt_password.getText().toString();
                if (!validate()){
                    Toast.makeText(Login_screen.this, "SignIn Has Failed", Toast.LENGTH_SHORT).show();
                } else {
                    User_login(getemail, getpassword);}
            });

        btn_register.setOnClickListener((View v)-> {

                Intent intent = new Intent(Login_screen.this, Registration_Screen.class);
                startActivity(intent);
                finish();
        });


        txt_forget_password.setOnClickListener((View view)-> {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Login_screen.this);
                LayoutInflater inflater = Login_screen.this.getLayoutInflater();
                @SuppressLint("InflateParams") View dialogView = inflater.inflate(R.layout.forget_password, null);

                forgetemail = dialogView.findViewById(R.id.ed_forgetemail);
                forgetemail_enter = dialogView.findViewById(R.id.btn_forgetemail_enter);

                forgetemail_enter.setOnClickListener((View v) ->{
                        getforgetemail = forgetemail.getText().toString();
                         if (!forgetValidate()) {
                            Toast.makeText(Login_screen.this, "You request has Failed", Toast.LENGTH_SHORT).show();
                        } else {
                            Forget_password(getforgetemail);
                            alertDialog.dismiss();
                        }
                    });

                forget_Cancel = dialogView.findViewById(R.id.btn_forgetemail_cancel);

                forget_Cancel.setOnClickListener((View v)-> alertDialog.cancel());

                dialogBuilder.setView(dialogView);
                alertDialog = dialogBuilder.create();
                alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                WindowManager.LayoutParams lp =  Objects.requireNonNull(alertDialog.getWindow()).getAttributes();
                lp.gravity = Gravity.TOP;
                lp.windowAnimations = R.style.dialog_animation;
                lp.width = LinearLayout.LayoutParams.FILL_PARENT;
                alertDialog.show();
        });

    }

    private void Forget_password(String getemail) {

        if (NetworkStateCheck.isNetworkAvaliable(Objects.requireNonNull(Login_screen.this))) {

            Singleton.getInstance().getApi().forgetpassword(getemail, new Callback<ForgetModel>() {
                @Override
                public void success(ForgetModel forgetModel, Response response) {
                    if (forgetModel.getSuccess().equals("1")) {
                        ForgotpasswordAfterMailsent(ConstantCommand.ForGot_PASSWORD_MDG);
                    } else {
                        Toast.makeText(Login_screen.this, "" + forgetModel.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                }
            });
        }else {
            Toast.makeText(this, "Internet connection problem", Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressLint("ResourceAsColor")
    private void User_login(final String getemail, final String getpassword) {

        if (NetworkStateCheck.isNetworkAvaliable(Objects.requireNonNull(Login_screen.this))) {
            KProgressHUD knprogress = ProgressDialog.getInstance().getKNProgressDialog(Login_screen.this);
            knprogress.show();
            Singleton.getInstance().getApi().userlogin(getemail, getpassword, new Callback<LoginModel>() {
                @Override
                public void success(LoginModel loginModel, Response response) {

                    String status = loginModel.getSuccess();
                    String message = loginModel.getMessage();

                    if (status.equalsIgnoreCase("1")) {
                        MySharePrafranceClass.ClearAllLoginData(Login_screen.this);
                        String userid = loginModel.getUserid();
                        String username = loginModel.getName();
                        String photo = loginModel.getThumb();

                        String Type = loginModel.getType();
                        String contry = loginModel.getCountry();
                        String city = loginModel.getCity();

                        MySharePrafranceClass.LoginDataShare(Login_screen.this, getemail, getpassword, Type, contry, city);// save email and password in share prefrace
                        MySharePrafranceClass.Share(Login_screen.this, userid, username, photo);// save login id,name,photo in share prefrace
                        knprogress.dismiss();
                        Intent intent = new Intent(Login_screen.this, Home_screen.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    } else {
                        knprogress.dismiss();
                        Toast.makeText(Login_screen.this, "" + message, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    knprogress.dismiss();
                    Toast.makeText(Login_screen.this, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
        else
            {
            Toast.makeText(this, "Internet connection problem", Toast.LENGTH_SHORT).show();
            }
    }



    //-----------------------helper method--------------------------------------------------

    @SuppressLint("SetTextI18n")
    private boolean validate(){
        boolean valid=true;
        if (getemail.isEmpty()||!Patterns.EMAIL_ADDRESS.matcher(getemail).matches())
        {
            txt_email.setError("Enter Valid Email");
            valid=false;
        }if (getpassword.isEmpty()||getpassword.length()<6||getpassword.length()>20){
            txt_password.setText("Enter Valid Password");
            valid=false;
        }
        return valid;
    }
    private boolean forgetValidate(){
        boolean valid =true;
        if (getforgetemail.isEmpty()||!Patterns.EMAIL_ADDRESS.matcher(getforgetemail).matches()){
            forgetemail.setError("Enter Valid Email");

           // forgetemail.seter
            valid=false;}
        return valid;
    }
    private void ShareElement() {
        String[] logindata = MySharePrafranceClass.LoginDataGetSharePrefrance(Login_screen.this);
        String email= logindata[0];
        if (!(email==null))
        {
            //txt_email.setText(email);
        }else
            {
            txt_email.setText("");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Chache_Clear.deleteCache(Login_screen.this);
       // Tuitionize.getInstance().clearApplicationData();
    }


    public void  ForgotpasswordAfterMailsent(String message) {
        PanterDialog dialog =   new PanterDialog(this);
        dialog.setHeaderBackground(R.drawable.pattern_bg_orange)
                .setHeaderLogo(R.drawable.logo)
                .setTitle("Forgot Password")
                .setPositive("I GOT IT", v -> {
                    dialog.dismiss();
                })
                .setMessage(message)
                .isCancelable(false);

        dialog.show();

    }
}
