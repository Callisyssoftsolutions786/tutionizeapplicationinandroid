package com.tutionize.androidstudionavigationdrawer.Activity_Screen;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.tutionize.androidstudionavigationdrawer.R;
import com.tutionize.androidstudionavigationdrawer.Sharepool.MySharePrafranceClass;
import com.tutionize.androidstudionavigationdrawer.databinding.ActivitySplashScreenBinding;


public class Splash_screen extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        ActivitySplashScreenBinding activitySplashScreenBinding = DataBindingUtil.setContentView(this,R.layout.activity_splash_screen);
        Animation animation2= AnimationUtils.loadAnimation(this,R.anim.high_light);
        Animation animation1= AnimationUtils.loadAnimation(this,R.anim.pop_enter);
        Animation animation= AnimationUtils.loadAnimation(this,R.anim.vibrate);
        activitySplashScreenBinding.textname.setAnimation(animation1);
        activitySplashScreenBinding.imagelogo.setAnimation(animation2);
        activitySplashScreenBinding.progressbar.setAnimation(animation2);
        CallActivity();
    }
    private void  CallActivity()
    {
        String  Value = MySharePrafranceClass.GetTeacherDetailEdit(Splash_screen.this);
        int SPLASH_TIME_OUT = 3000;
        new Handler().postDelayed(() -> {
            if (Value!= null){
                if (Value.equals("False"))
                {
                    Intent i = new Intent(Splash_screen.this,Edit_Teacher_Profile.class);
                    startActivity(i);
                    finish();
                }
                else
                    {
                        Intent i = new Intent(Splash_screen.this, Home_screen.class);
                        startActivity(i);
                        finish();
                    }
            }
            else
                {
                Intent i = new Intent(Splash_screen.this, Home_screen.class);
                startActivity(i);
                finish();
                }
        }, SPLASH_TIME_OUT);
    }
}
