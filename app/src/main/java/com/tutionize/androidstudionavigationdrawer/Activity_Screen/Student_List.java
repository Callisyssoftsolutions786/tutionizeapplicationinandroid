package com.tutionize.androidstudionavigationdrawer.Activity_Screen;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.tutionize.androidstudionavigationdrawer.Adapter.Adapter_student_list;
import com.tutionize.androidstudionavigationdrawer.R;

/**
 * Created by hp on 01-03-2017.
 */
public class Student_List extends Activity
{
    RecyclerView Recycle_student_list;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.student_list);
        initialize();
    }

    public void initialize() {

        Recycle_student_list = (RecyclerView)findViewById(R.id.rcy_student_list);
        Recycle_student_list.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(Student_List.this, LinearLayoutManager.VERTICAL, false);
        Recycle_student_list.setLayoutManager(layoutManager);
        Adapter_student_list mTeacher_list = new Adapter_student_list(Student_List.this);
        Recycle_student_list.setAdapter(mTeacher_list);


    }


}
