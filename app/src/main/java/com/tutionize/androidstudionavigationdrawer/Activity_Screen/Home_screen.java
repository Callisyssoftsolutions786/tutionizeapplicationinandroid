package com.tutionize.androidstudionavigationdrawer.Activity_Screen;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.FileProvider;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.iceteck.silicompressorr.SiliCompressor;
import com.tutionize.androidstudionavigationdrawer.BuildConfig;
import com.tutionize.androidstudionavigationdrawer.Constant.AppConst;
import com.tutionize.androidstudionavigationdrawer.Constant.Picture_method;
import com.tutionize.androidstudionavigationdrawer.Constant.Singleton;
import com.tutionize.androidstudionavigationdrawer.Fragment.Fragment_Start_Class;
import com.tutionize.androidstudionavigationdrawer.Fragment.Fragment_UpdateBioData;
import com.tutionize.androidstudionavigationdrawer.Fragment.Fragment_WriteUs;
import com.tutionize.androidstudionavigationdrawer.Fragment.Fragment_course_learning;
import com.tutionize.androidstudionavigationdrawer.Fragment.Fragment_course_teaching;
import com.tutionize.androidstudionavigationdrawer.Fragment.Fragment_notification_Dialog;
import com.tutionize.androidstudionavigationdrawer.Fragment.Fragment_setting;
import com.tutionize.androidstudionavigationdrawer.Fragment.MainActivity;
import com.tutionize.androidstudionavigationdrawer.Fragment.Selectbatch_fragment;
import com.tutionize.androidstudionavigationdrawer.Fragment.TeacherMainActivity;
import com.tutionize.androidstudionavigationdrawer.Fragment.Teacher_Detail_Screen;
import com.tutionize.androidstudionavigationdrawer.Model.DeleteStudentToBatch;
import com.tutionize.androidstudionavigationdrawer.Model.Dtabase_Notification_modle;
import com.tutionize.androidstudionavigationdrawer.Model.GetProfilePic_modle;
import com.tutionize.androidstudionavigationdrawer.Model.UploadProfilePic_modle;
import com.tutionize.androidstudionavigationdrawer.R;
import com.tutionize.androidstudionavigationdrawer.Sharepool.Analytic_and_Tracker;
import com.tutionize.androidstudionavigationdrawer.Sharepool.Check_Required_Premission;
import com.tutionize.androidstudionavigationdrawer.Sharepool.MySharePrafranceClass;
import com.tutionize.androidstudionavigationdrawer.Sharepool.NetworkStateCheck;
import com.tutionize.androidstudionavigationdrawer.command.ConstantCommand;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

public class Home_screen extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private static final int CAMERACODE = 110;
    private static final int GALLERYCODE = 220;
    private static final int MEDIA_TYPE_IMAGE = 1;
    public ImageView notificationIcon,cameraimg_icon;
    public ImageView HomeIcon;
    @SuppressLint("StaticFieldLeak")
    public static Home_screen home_screen;
    private Fragment mFragment = null;
    public DrawerLayout drawer;
    private CircleImageView img_userImage;
    private Button notificationCount;
    private TextView txt_Usertype;
    public TextView txt_UserName;
    private String[] list;
    private PhotoView profilelargephotoview;
    private ProgressBar headerprogressBar;
    private Uri photofile_navigation;
    public  Toolbar toolbar;
    private File file_navigation;
    private static String navigation_current;
    private String[] loginlist;
    private MenuItem CourseItem,nav_learner, nav_teacher,signInItem,signout_item, writeUs_item,Invite_item,setting_item;
    private String Roomid,teacherimage,teachername,callid,calltype;
    public static int mNotifCount = 0;
    private List<Dtabase_Notification_modle>notificationlist = new ArrayList<>();
    private AlertDialog alertDialog, alertDialogforviewImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.home_screen);
        Analytic_and_Tracker.StartAnalyisation(Home_screen.this);
        Check_Required_Premission.CheckPermission(Home_screen.this);
        getExtravalues();
        home_screen = this;
        loginlist = MySharePrafranceClass.LoginDataGetSharePrefrance(Home_screen.this);
        list=MySharePrafranceClass.GetSharePrefrance(Home_screen.this);
        SubmitToken();
        FindView();
        SetNavigationCounpond();
        Clickable();
        ShareAble();
    }


    //------------- All Called Method ---------------


    private void OpenImageViewScreen() {
        if (list[0]!=null)
        {
            String  userimage = list[2];
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Home_screen.this, R.style.AppTheme);
            LayoutInflater inflater = Home_screen.this.getLayoutInflater();
            @SuppressLint("InflateParams")
            View dialogView = inflater.inflate(R.layout.view_profile_image, null);

            com.rey.material.widget.ImageView profile_back =  dialogView.findViewById(R.id.profile_back);
            profilelargephotoview =  dialogView.findViewById(R.id.profile_large_photo_view);
            com.rey.material.widget.ImageView  profile_edit = dialogView.findViewById(R.id.profile_edit);

            if(userimage!=null) {
                if (!userimage.equals("")) {
                    String image = AppConst.photoconstant.concat(userimage);
                    Glide.with(this).load(image).into(profilelargephotoview);
                }
            }
            profile_edit.setOnClickListener(v->{
                OpenImageSelectOption();
            });

            profile_back.setOnClickListener(v->{
                alertDialogforviewImage.dismiss();
            });

            dialogBuilder.setView(dialogView);
            alertDialogforviewImage = dialogBuilder.create();
            alertDialogforviewImage.setCancelable(false);
            alertDialogforviewImage.show();
        }
        else
        {
            Toast.makeText(Home_screen.this, "Please Sign-in First", Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressLint("SetTextI18n")
    private void OpenImageSelectOption() {

        if (list[0]!=null)
        {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Home_screen.this);
            LayoutInflater inflater = Home_screen.this.getLayoutInflater();
            @SuppressLint("InflateParams")
            View dialogView = inflater.inflate(R.layout.choose_image_option, null);
            LinearLayout layout_Camera = dialogView.findViewById(R.id.layout_Camera);
            LinearLayout layout_gallery = dialogView.findViewById(R.id.layout_gallery);
            LinearLayout layout_Cancel = dialogView.findViewById(R.id.layout_Cancel);
            TextView dialog_titel = (TextView) dialogView.findViewById(R.id.dialog_title);
            dialog_titel.setText("Profile Photo");
            layout_Camera.setOnClickListener((View v) -> {
                Intent cameraintent = new Intent("android.media.action.IMAGE_CAPTURE");
                if (cameraintent.resolveActivity(Home_screen.this.getPackageManager()) != null) {
                    file_navigation = null;
                    try {
                        file_navigation = Picture_method.getInstance().getOutputMediaFile(MEDIA_TYPE_IMAGE);
                        navigation_current = Picture_method.getInstance().getCurrent();
                    }
                    catch (Exception ex)
                    {
                        return;
                    }
                    if (file_navigation != null) {
                        photofile_navigation = FileProvider.getUriForFile(Home_screen.this,
                                BuildConfig.APPLICATION_ID + ".provider", file_navigation);
                        cameraintent.putExtra(MediaStore.EXTRA_OUTPUT, photofile_navigation);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                        { cameraintent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        }
                        else {
                            List<ResolveInfo> resInfoList = getPackageManager().queryIntentActivities(cameraintent, PackageManager.MATCH_DEFAULT_ONLY);
                            for (ResolveInfo resolveInfo : resInfoList) {
                                String packageName = resolveInfo.activityInfo.packageName;
                                Home_screen.this.grantUriPermission(packageName, photofile_navigation,
                                        Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            }
                        }
                        startActivityForResult(cameraintent, CAMERACODE);
                    }
                }
                alertDialog.dismiss();
            });
                layout_gallery.setOnClickListener((View v) -> {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, GALLERYCODE);
                    alertDialog.dismiss();
                });
                layout_Cancel.setOnClickListener((View v) -> alertDialog.cancel());

            dialogBuilder.setView(dialogView);
            alertDialog = dialogBuilder.create();
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            WindowManager.LayoutParams lp = Objects.requireNonNull(alertDialog.getWindow()).getAttributes();
            lp.gravity = Gravity.TOP;
            lp.windowAnimations = R.style.dialog_animation;
            lp.width = LinearLayout.LayoutParams.FILL_PARENT;
            alertDialog.setCancelable(false);
            alertDialog.show();
        }
        else
            {
                Toast.makeText(Home_screen.this, "Please Sign-in First", Toast.LENGTH_SHORT).show();
            }
    }

    private void SetNavigationCounpond()
    {
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
        NavigationView navigationView = findViewById(R.id.nav_view);
        Objects.requireNonNull(navigationView).setNavigationItemSelectedListener(this);
        Menu menu = navigationView.getMenu();
        nav_learner=menu.findItem(R.id.nav_learner);
        nav_teacher=menu.findItem(R.id.nav_Teacher);
        signInItem=menu.findItem(R.id.nav_signIn);
        CourseItem=menu.findItem(R.id.nav_slideshow);
        setting_item=menu.findItem(R.id.nav_manage);
        Invite_item=menu.findItem(R.id.nav_invitefriend);
        writeUs_item=menu.findItem(R.id.nav_writeus);
        signout_item=menu.findItem(R.id.nav_signOut);
        Drawable mDrawable = navigationView.getBackground();
        View header = navigationView.getHeaderView(0);
        txt_UserName = header.findViewById(R.id.navigation_Username);
        txt_Usertype = header.findViewById(R.id.navigation_Usertype);
        img_userImage = header.findViewById(R.id.navigation_imageView);
        cameraimg_icon =  header.findViewById(R.id.cameraimg_icon);
        headerprogressBar = header.findViewById(R.id.header_prograssBar);
        mDrawable.setAlpha(255);
    }

    private void Clickable() {

        notificationIcon.setOnClickListener((View view) ->{
                    setnotificationcount(0);
                    Fragment_notification_Dialog fragment_notification_Dialog = new Fragment_notification_Dialog();
                    fragment_notification_Dialog.show(getSupportFragmentManager(),"tag");});

        HomeIcon.setOnClickListener((View view) ->{
            Intent intent = new Intent(Home_screen.this,Home_screen.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        });

        img_userImage.setOnClickListener(view -> {

            OpenImageViewScreen();

        });

        cameraimg_icon.setOnClickListener(v->{
            Check_Required_Premission.CheckCameraPermission(Home_screen.this);
            Check_Required_Premission.checkStorageReadPermition(Home_screen.this);
            Check_Required_Premission.checkStorageWritePermition(Home_screen.this);
            OpenImageSelectOption();

        });
    }

    private void SubmitToken() {
         String Userid =  list[0];

         if (Userid!=null){

             FirebaseMessaging.getInstance().subscribeToTopic("news");
             String token = FirebaseInstanceId.getInstance().getToken();
             Singleton.getInstance().getApi().submittoken(Userid, token, ConstantCommand.NOTIFICATION_TOKEN_TYPE, new Callback<DeleteStudentToBatch>() {
                 @Override
                 public void success(DeleteStudentToBatch deleteStudentToBatch, Response response) {
                    // Toast.makeText(Home_screen.this, deleteStudentToBatch.getMessage(), Toast.LENGTH_SHORT).show();
                 }

                 @Override
                 public void failure(RetrofitError error) {
                    // Toast.makeText(Home_screen.this, error.getMessage(), Toast.LENGTH_SHORT).show();

                 }
             });
         }

    }

    @SuppressLint("SetTextI18n")
    private void ShareAble() {
        String userid = list[0];
        String username1 = list[1];
        String userimage = list[2];
        String type = loginlist[2];
           if (userid ==null)
           {
               setting_item.setVisible(false);
               nav_learner.setVisible(false);
               nav_teacher.setVisible(false);
               signout_item.setVisible(false);
               writeUs_item.setVisible(false);
               Invite_item.setVisible(false);
               setting_item.setVisible(false);
               Singleton.getInstance().CallFragment(new MainActivity(),Home_screen.this,"Home");
           }
           else {
                  signInItem.setVisible(false);
                  nav_learner.setVisible(false);

               if (type != null)
               {
                   if (type.equals(ConstantCommand.Profile_Type_T)) {
                       Singleton.getInstance().CallFragment(new TeacherMainActivity(),Home_screen.this,"Home");
                   }
                   else {
                       if (Roomid!= null&&teachername!=null&&calltype!=null&&callid!=null){
                           callfrontclassFragment_Start_Class(Roomid,teacherimage,teachername,calltype,callid);
                       }else {
                           Singleton.getInstance().CallFragment(new MainActivity(),Home_screen.this,"Home");
                       }
                   }
                   if (type.equals(ConstantCommand.Profile_Type_T)) {
                       CourseItem.setVisible(false);
                       nav_learner.setVisible(false);

                   }
                   else {
                       nav_teacher.setVisible(false);
                   }

                   txt_Usertype.setText("( "+ type +" )");
               }
                       else {
                   Singleton.getInstance().CallFragment(new MainActivity(),Home_screen.this,"Home");
               }
           }


    if (userimage !=null){
            if (!(userimage.equals(""))) {
                String image = AppConst.photoconstant.concat(userimage);
                Glide.with(this).load(image).fitCenter().centerCrop().into(img_userImage);
            } else {
                Glide.with(this).load(R.drawable.userimagewhite).fitCenter().centerCrop().into(img_userImage);
            }

        }
        else {

            GetProfilePic(userid);
            Glide.with(this).load(R.drawable.userimagewhite).fitCenter().centerCrop().into(img_userImage);
        }


        if (username1 != null) {
            txt_UserName.setText(username1);
        } else {
            txt_UserName.setText("Username");
           }

        if (notificationlist==null){
            notificationCount.setVisibility(View.GONE);

        }

        else if (notificationlist.size()==0){
            notificationCount.setVisibility(View.GONE);
        }
        else {
            setnotificationcount(notificationlist.size());
        }
    }

    private void FindView() {
        drawer = findViewById(R.id.drawer_layout);
        toolbar = findViewById(R.id.toolbar);
        notificationIcon = findViewById(R.id.notification);
        notificationCount= findViewById(R.id.notif_count);
        HomeIcon= findViewById(R.id.HomeIcon);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        int menuToUse = R.menu.right_menu_btn;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(menuToUse, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @SuppressLint("RtlHardcoded")
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item != null && item.getItemId() == R.id.btnMyMenu) {
            if (drawer.isDrawerOpen(Gravity.LEFT)) {
                drawer.closeDrawer(Gravity.RIGHT);
            } else {
                drawer.openDrawer(Gravity.RIGHT);
            }
            return true;
        }
        return false;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
          int id = item.getItemId();
          Class fragmentClass = null;
            if (id == R.id.nav_learner) {
                fragmentClass = Fragment_course_learning.class;
            }
            else if (id == R.id.nav_Teacher) {
                fragmentClass = Fragment_course_teaching.class;

            }
            else if (id == R.id.nav_signIn) {
                GoToLogin();

            }
            else if (id == R.id.nav_signOut) {
                Signout();

            } else if (id == R.id.nav_slideshow) {
                GoToCourse();

            } else if (id == R.id.nav_manage) {
                if (list[0] != null) {
                    fragmentClass = Fragment_setting.class;
                }
            } else if (id == R.id.nav_writeus) {

                fragmentClass = Fragment_WriteUs.class;

            } else if (id == R.id.nav_invitefriend) {

                InviteFriend();
            }

            try {
                assert fragmentClass != null;
                mFragment = (Fragment) fragmentClass.newInstance();
            }catch (Exception e)
            { e.printStackTrace(); }
            if (mFragment!=null)
            {
                 Singleton.getInstance().CallFragment(mFragment, Home_screen.this, "bbhbhd");
            }
            drawer.closeDrawer(GravityCompat.END);
            return true;
        }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        //--------------------- Camera / Gallery Video  Upload-----------------

        String gallerypath_navigation;
        String selectImage_navigation;
        String filePath_navigation;
        Uri gelleryUri;
        TypedFile fileTypeFile1;
        if (requestCode == Teacher_Detail_Screen.PAYPAL_REQUEST_CODE)
        {
            if (resultCode==RESULT_OK)
            {
                Teacher_Detail_Screen.teacher_detail_screen.getPaymentData(data);
            }
        }
       else if (requestCode == Fragment_UpdateBioData.GALLERY_CODE)
       {
           if (resultCode == RESULT_OK)
           {
                gelleryUri = Uri.parse(String.valueOf(Picture_method.getInstance().getPathFromUri(data.getData(),Home_screen.this)));
                gallerypath_navigation = gelleryUri.getPath();
                if (gallerypath_navigation != null)
                {
                    selectImage_navigation = gallerypath_navigation;
                    Fragment_UpdateBioData.setSelectedVideoPath(selectImage_navigation);
                    Toast.makeText(Home_screen.this, "Picture Selected", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(Home_screen.this," Sorry , file path is missing..! ", Toast.LENGTH_LONG).show();
                }
           }
       }
       else if (requestCode == Fragment_UpdateBioData.CAMERA_CODE) {
            if (resultCode == RESULT_OK) {
                filePath_navigation = Fragment_UpdateBioData.current;
                if (filePath_navigation != null)
                {
                    selectImage_navigation = filePath_navigation;
                    Fragment_UpdateBioData.setSelectedVideoPath(selectImage_navigation);
                    Toast.makeText(this, " Video is Selected", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(Home_screen.this,"Sorry, file path is missing!", Toast.LENGTH_LONG).show();
                }
            }
        }


        //------------------- Camera/ Gallery Photo Upload-------------

        else if(requestCode == Selectbatch_fragment.CAMERACODE) {

            if (resultCode == RESULT_OK) {

                filePath_navigation = Selectbatch_fragment.navigation_current;

                if (filePath_navigation != null)
                {
                    selectImage_navigation = filePath_navigation;


                    String filePath = SiliCompressor.with(Home_screen.this).compress(selectImage_navigation,Picture_method.getInstance().getDestinationPath(Home_screen.this), true);

                    Selectbatch_fragment.selectbatch_fragment.setSelectedImagePath(filePath);
                    Selectbatch_fragment.selectbatch_fragment.BatchImage.setImageURI(Uri.parse(filePath_navigation));
                    Toast.makeText(Home_screen.this, "Picture is Selected", Toast.LENGTH_SHORT).show();
                }
            }
        }
        else if (requestCode == CAMERACODE) {
                if (resultCode == RESULT_OK)
                {
                    filePath_navigation = navigation_current;
                    if (filePath_navigation != null)
                    {
                        selectImage_navigation = filePath_navigation;
                        Toast.makeText(Home_screen.this, "Picture is Selected", Toast.LENGTH_SHORT).show();
                        String userid = list[0];
                        String filePath = SiliCompressor.with(Home_screen.this).compress(selectImage_navigation,Picture_method.getInstance().getDestinationPath(Home_screen.this), true);
                        fileTypeFile1 = new TypedFile("image/*", new File(filePath));
                        UploadProflePic(userid, fileTypeFile1);
                    }
                    else
                        {
                        Toast.makeText(Home_screen.this, "Sorry, file path is missing!", Toast.LENGTH_LONG).show();
                    }
                }
            }
            else if (requestCode == Selectbatch_fragment.GALLERYCODE) {

                if (resultCode == RESULT_OK) {

                    gelleryUri = Uri.parse(String.valueOf(Picture_method.getInstance().getPathFromUri(data.getData(),Home_screen.this)));
                    gallerypath_navigation = gelleryUri.getPath();

                    if (gallerypath_navigation != null)
                    {
                        selectImage_navigation = gallerypath_navigation;

                        String filePath = SiliCompressor.with(Home_screen.this).compress(selectImage_navigation,Picture_method.getInstance().getDestinationPath(Home_screen.this), true);
                        Selectbatch_fragment.selectbatch_fragment.setSelectedImagePath(filePath);
                        Selectbatch_fragment.selectbatch_fragment.BatchImage.setImageURI(data.getData());
                        Toast.makeText(Home_screen.this,"Picture is Selected", Toast.LENGTH_SHORT).show();
                    }
                    else
                        {
                           Toast.makeText(Home_screen.this,"Sorry, file path is missing!", Toast.LENGTH_LONG).show();
                        }
                }
            }

            else if (requestCode == GALLERYCODE) {

                if (resultCode == RESULT_OK) {

                    gelleryUri = Uri.parse(String.valueOf(Picture_method.getInstance().getPathFromUri(data.getData(),Home_screen.this)));
                    gallerypath_navigation = gelleryUri.getPath();

                    if (gallerypath_navigation != null)
                    {

                        selectImage_navigation = gallerypath_navigation;
                        String userid = list[0];
                        Toast.makeText(Home_screen.this, "Picture is Selected", Toast.LENGTH_SHORT).show();
                        String filePath = SiliCompressor.with(Home_screen.this).compress(selectImage_navigation,Picture_method.getInstance().getDestinationPath(Home_screen.this), true);
                        fileTypeFile1 = new TypedFile("image/*", new File(filePath));
                        UploadProflePic(userid, fileTypeFile1);

                    }
                    else {
                        Toast.makeText(Home_screen.this,
                                "Sorry, file path is missing!", Toast.LENGTH_LONG).show();
                    }
                }
            }
        }


    private void UploadProflePic(final String userid, TypedFile file_navigation)
    {

        if (NetworkStateCheck.isNetworkAvaliable(Home_screen.this))
        {
            headerprogressBar.setVisibility(View.VISIBLE);
            img_userImage.setVisibility(View.GONE);
            Singleton.getInstance().getApi().UploadProfilePic(userid, file_navigation, new Callback<UploadProfilePic_modle>() {
                @Override
                public void success(UploadProfilePic_modle uploadProfilePic_modle, Response response) {
                    if (uploadProfilePic_modle.getSuccess().equals("1")) {

                        headerprogressBar.setVisibility(View.GONE);

                        GetProfilePic(userid);


                        Toast.makeText(Home_screen.this, "Image Update Successfully", Toast.LENGTH_SHORT).show();
                    } else {
                        img_userImage.setVisibility(View.VISIBLE);
                        headerprogressBar.setVisibility(View.GONE);
                        Toast.makeText(Home_screen.this, "Image Not Successfully Update", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    img_userImage.setVisibility(View.VISIBLE);
                    headerprogressBar.setVisibility(View.GONE);
                    Toast.makeText(Home_screen.this, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }else {
            Toast.makeText(Home_screen.this, "Internet connection problem", Toast.LENGTH_SHORT).show();
        }
    }

    public void callfrontclassFragment_Start_Class(String Roomid,String teacherimage,
                                                   String teachername,String calltype,String callid)
    {
        Bundle bundle = new Bundle();
        Fragment mFragment = null;
        Class fragmentClass = Fragment_Start_Class.class;
        try
        {
            mFragment = (Fragment)fragmentClass.newInstance();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        FragmentManager fragmentManager = Home_screen.this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        bundle.putString("Roomid", Roomid);
        bundle.putString("teacherimage", teacherimage);
        bundle.putString("teachername", teachername);
        bundle.putString("calltype", calltype);
        bundle.putString("callid", callid);
        assert mFragment != null;
        mFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.container, mFragment).addToBackStack(null).commit();

    }

    private void GetProfilePic(final String userid)
    {
        if (NetworkStateCheck.isNetworkAvaliable(Home_screen.this))
        {
            Singleton.getInstance().getApi().GetProfilePic(userid, new Callback<GetProfilePic_modle>()
            {
                @Override
                public void success(GetProfilePic_modle getProfilePic_modle, Response response)
                {
                    if (getProfilePic_modle.getSuccess().equals("1"))
                    {

                        String update_Image = getProfilePic_modle.getThumb();
                        String txt_username = txt_UserName.getText().toString();

                        if (update_Image != null)
                        {

                            if (!update_Image.equals(""))
                            {

                                MySharePrafranceClass.ClearAll(Home_screen.this);

                                String image = AppConst.photoconstant.concat(update_Image);
                                Glide.with(Home_screen.this).load(image).fitCenter().centerCrop().into(img_userImage);
                                 if (profilelargephotoview!=null)
                                 {
                                     Glide.with(Home_screen.this).load(image).into(profilelargephotoview);

                                 }
                            }
                            else
                                {
                                   if (profilelargephotoview != null)
                                   {
                                       Glide.with(Home_screen.this).load(R.drawable.userimagewhite).into(profilelargephotoview);
                                   }
                                   Glide.with(Home_screen.this).load(R.drawable.userimagewhite).fitCenter().centerCrop().into(img_userImage);
                                }

                        }
                        else {
                            Glide.with(Home_screen.this).load(R.drawable.userimagewhite).fitCenter().centerCrop().into(img_userImage);
                        }
                        MySharePrafranceClass.Share(Home_screen.this, userid, txt_username, update_Image);
                        img_userImage.setVisibility(View.VISIBLE);
                }
                else{
                    img_userImage.setVisibility(View.VISIBLE);
                    Toast.makeText(Home_screen.this, "Get Profile Pic " + getProfilePic_modle.getSuccess(), Toast.LENGTH_SHORT).show();
                    }
                }
                @Override
                public void failure(RetrofitError error) {
                    img_userImage.setVisibility(View.VISIBLE);
                    //Toast.makeText(Home_screen.this, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
        else {
            Toast.makeText(Home_screen.this, "Internet connection problem", Toast.LENGTH_SHORT).show();
        }
    }


    @SuppressLint("RtlHardcoded")
    @Override
    public void onBackPressed() {
        super.onBackPressed();

            if (drawer.isDrawerOpen(Gravity.LEFT)) {
                drawer.closeDrawer(Gravity.RIGHT);

            }
            else {

                getSupportFragmentManager().getBackStackEntryCount();
            }
    }
//---------------------------------------------------- hellper  Method -----------------------------------------------

    public void setnotificationcount(int mNotifCount){
      Home_screen.mNotifCount = mNotifCount;
        if (mNotifCount == 0)
        { notificationCount.setVisibility(View.GONE);
        }
        else
            { notificationCount.setVisibility(View.VISIBLE);
              this.notificationCount.setText(String.valueOf(mNotifCount));
            }
    }
    //================================  Dialog Method ======================


    public void showAlert(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Home_screen.this);
        builder.setMessage(message).setTitle("Response from Servers")
                .setCancelable(false)
                .setPositiveButton("OK", (dialog, id) -> dialog.dismiss());
         AlertDialog alert = builder.create();
         alert.show();
    }
    public void getExtravalues() {

        if( getIntent().getExtras() != null)
        {   String message = getIntent().getExtras().getString("msg1");
            Roomid = getIntent().getExtras().getString("room1");
            teachername = getIntent().getExtras().getString("teachername1");
            teacherimage = getIntent().getExtras().getString("teacherimage1");
            calltype= getIntent().getExtras().getString("calltype1");
            callid= getIntent().getExtras().getString("callid1");
        }
    }
    private void Signout() {
        MySharePrafranceClass.ClearAll(Home_screen.this);
        MySharePrafranceClass.ClearAllLoginData(Home_screen.this);
        MySharePrafranceClass.ClearEditTeacherDetail(Home_screen.this);
        startActivity(new Intent(Home_screen.this, Login_screen.class));
        finish();
    }
    private void GoToLogin() {
        Intent intent = new Intent(Home_screen.this,Login_screen.class);
        startActivity(intent);
    }
    private void GoToCourse() {
        Intent mIntent = new Intent(Home_screen.this, Home_screen.class);
        mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(mIntent);
        finish();
    }
    private void InviteFriend() {
     String applicationname  =  getApplicationName(Home_screen.this);

        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT,applicationname);
            String sAux = "\n Hey, This live tutoring app is amazing. It helped us a lot. I thought you are  going to love it too.\n\n";
            sAux = sAux + "https://play.google.com/store/apps/details?id=com.tutionize.androidstudionavigationdrawer\n\n";
            i.putExtra(Intent.EXTRA_TEXT, sAux);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
            startActivity(Intent.createChooser(i, "Choose One Of These :"));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    public static String getApplicationName(Context context)
    {
        return context.getApplicationInfo().loadLabel(context.getPackageManager()).toString();
    }

    static public void ShowToast(String msg){
        Toast.makeText(home_screen,msg, Toast.LENGTH_SHORT).show();
    }


}

