package com.tutionize.androidstudionavigationdrawer.Adapter;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.rey.material.app.BottomSheetDialog;
import com.rey.material.widget.Button;
import com.rey.material.widget.ImageView;
import com.rey.material.widget.LinearLayout;
import com.rey.material.widget.TextView;
import com.tutionize.androidstudionavigationdrawer.Constant.AppConst;
import com.tutionize.androidstudionavigationdrawer.Constant.Singleton;
import com.tutionize.androidstudionavigationdrawer.Fragment.Live_Video_Screen;
import com.tutionize.androidstudionavigationdrawer.Fragment.Selectbatch_fragment;
import com.tutionize.androidstudionavigationdrawer.Model.All_Batch_Result_modle;
import com.tutionize.androidstudionavigationdrawer.Model.Delete_batch_modle;
import com.tutionize.androidstudionavigationdrawer.Model.GetToken_model;
import com.tutionize.androidstudionavigationdrawer.Model.Start_class_modle;
import com.tutionize.androidstudionavigationdrawer.R;
import com.tutionize.androidstudionavigationdrawer.Sharepool.MySharePrafranceClass;
import com.tutionize.androidstudionavigationdrawer.Sharepool.NetworkStateCheck;
import com.tutionize.androidstudionavigationdrawer.command.ConstantCommand;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * Created by Vishal Sandhu on 11/30/17.
 *
 *
 */

public class Adapter_AllBatch extends RecyclerView.Adapter<Adapter_AllBatch.MyViewHolder> {
    private Activity mActivity;
    private LayoutInflater mInflater;
    private List<All_Batch_Result_modle> Batchlist = new ArrayList<>();
    public String[] loginlist,list;


    public Adapter_AllBatch(Activity mActivity, List<All_Batch_Result_modle> Batchlist)
    {
        this.mActivity = mActivity;
        this.Batchlist = Batchlist;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView startTime, endTme, batch_name,batchduration;
        CircleImageView circleImageView;
        Button StartClass;
        ImageView editBatch,deletebatch;
        LinearLayout option_icon;
        MyViewHolder(View itemView)
        {
            super(itemView);
            startTime = itemView.findViewById(R.id.in_list_batch_startTime);
            batch_name = itemView.findViewById(R.id.batch_name);
            batchduration = itemView.findViewById(R.id.batchduration);
            endTme = itemView.findViewById(R.id.in_list_batch_endTime);
            StartClass = itemView.findViewById(R.id.Batchlist_startClass);
            circleImageView = itemView.findViewById(R.id.Batchlist_image);
            option_icon = itemView.findViewById(R.id.layoutOPtion);

            editBatch = itemView.findViewById(R.id.editBatch);
            deletebatch = itemView.findViewById(R.id.deletebatch);
            loginlist= MySharePrafranceClass.LoginDataGetSharePrefrance(mActivity);
            list=MySharePrafranceClass.GetSharePrefrance(mActivity);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.batch_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") int position) {
        All_Batch_Result_modle batchdata = Batchlist.get(position);
        String Picture = batchdata.getBatchimage();
        if (!(Picture == null))
        {
            if (!(Picture.equals("")))
            {
                String pic = AppConst.batchphotoconstant.concat(Picture);
                Glide.with(mActivity).load(pic).into(holder.circleImageView);
            }
            else {
                  Glide.with(mActivity).load(R.drawable.userimagewhite).into(holder.circleImageView);
            }
        } else {
            Glide.with(mActivity).load(R.drawable.userimagewhite).into(holder.circleImageView);
        }

        holder.startTime.setText(batchdata.getStarttime());
        holder.endTme.setText(batchdata.getEndtime());
        holder.batch_name.setText(batchdata.getBatchname());
        holder.batchduration.setText("Duration : "+batchdata.getDuration());

        holder.editBatch.setOnClickListener(v->
            callfrontclass(batchdata.getBatchname(), batchdata.getId(), batchdata.getStarttime(),
                    batchdata.getEndtime(), batchdata.getTeacherid(), batchdata.getDuration(),
                    batchdata.getBatchimage())
        );
        holder.deletebatch.setOnClickListener(v-> StartBottemDeletemenuMenu(batchdata, position));

        holder.StartClass.setOnClickListener((View v) ->GoingStartClass(batchdata));
    }


    @SuppressLint("SetTextI18n")
    private void StartBottemDeletemenuMenu(All_Batch_Result_modle batchdata, int position) {
        int styleId = R.style.MyAlertDialogStyle1;
        BottomSheetDialog mDialog = new BottomSheetDialog(mActivity);
        int view = R.layout.delete_batch_bottem_list;
        mDialog.applyStyle(styleId)
                .contentView(view)
                .heightParam(ViewGroup.LayoutParams.WRAP_CONTENT)
                .inDuration(300)
                .cancelable(false);
        TextView message = mDialog.findViewById(R.id.message);
        com.rey.material.widget.Button canceldialog = mDialog.findViewById(R.id.canceldialog);
        com.rey.material.widget.Button deleteBatchdialog = mDialog.findViewById(R.id.deleteBatchdialog);

        message.setText("Do you want to delete "+batchdata.getBatchname()+" Batch ?\n");
        canceldialog.setOnClickListener(v->mDialog.dismiss());
        deleteBatchdialog.setOnClickListener(v-> {DeleteBatch(batchdata.getId(), position);
        mDialog.dismiss();
        });

        mDialog.show();
    }


    private void GoingStartClass(All_Batch_Result_modle batchdata) {
        Random rand = new Random();
        int minRange = 1000, maxRange = 5000;
        int RoomId = rand.nextInt(maxRange - minRange) + minRange;
        String Student = batchdata.getStudent();
        String teachername = list[1];
        String teacherimage =list[2];
        if (Student!=null && teachername != null)
        {
            SetClassAddress(Student, "" + RoomId, teachername, teacherimage, ConstantCommand.callType_Video, list[0], batchdata.getStudent());
        }
        else
        {
            Toast.makeText(mActivity, "It's must be a Student into a batch", Toast.LENGTH_SHORT).show();
        }
    }
    private void SetClassAddress(String student, String roomId,String teacher_name,String img_name,String Calltype,String caller_id,String AllStudent) {
        Singleton.getInstance().getApi().SetClassAddress(student, roomId,teacher_name,img_name,Calltype,caller_id,new Callback<Start_class_modle>() {
            @Override
            public void success(Start_class_modle start_class_modle, Response response) {
                if (start_class_modle.getSuccess().equals("1")) {


                   // String roomidentity = start_class_modle.getResult();

                    String roomidentity = roomId;

                    //-----------------------Here  is CHECK  THE PERMISSION---------------------------------------

                    TelephonyManager telephonyManager = (TelephonyManager) mActivity.getSystemService(Context.TELEPHONY_SERVICE);

                    if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(mActivity, "Allow MicroPhone permission first from App info", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    assert telephonyManager != null;
                    @SuppressLint("HardwareIds")
                    String deviceId = telephonyManager.getDeviceId();
                    String type = loginlist[2];

                    if (roomidentity != null && type !=null && deviceId!=null) {
                        GoToClass(roomidentity,type.concat(deviceId),AllStudent);
                    }
            }
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(mActivity, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }
    @Override
    public int getItemCount() {
        return Batchlist.size();
    }


    private void DeleteBatch(String batchid, Integer position){
        if (NetworkStateCheck.isNetworkAvaliable(mActivity)) {
            Singleton.getInstance().getApi().DeleteBatch(batchid, new Callback<Delete_batch_modle>() {
                @Override
                public void success(Delete_batch_modle delete_batch_modle, Response response) {
                    if (delete_batch_modle.getSuccess().equals("1")) {
                        removeAt(position);
                        Toast.makeText(mActivity, "Batch is deleted", Toast.LENGTH_SHORT).show();

                    }
                    else {
                        Toast.makeText(mActivity, "" + delete_batch_modle.getResult(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(RetrofitError error) {

                }
            });
        }else {
            Toast.makeText(mActivity, "Internet connection problem", Toast.LENGTH_SHORT).show();
        }
    }
    @SuppressLint("ResourceAsColor")
    private void GoToClass(final String roomidentity, String userIdentity ,String Allstudent) {
        if (NetworkStateCheck.isNetworkAvaliable(mActivity)) {
            Singleton.getInstance().getApi().getToken(roomidentity, userIdentity, new Callback<GetToken_model>() {
                @Override
                public void success(GetToken_model getToken_model, Response response) {

                    if (getToken_model.getResult() == null) {
                        Toast.makeText(mActivity, "Unable to connnect", Toast.LENGTH_SHORT).show();
                    } else if (getToken_model.getResult().equals("")) {
                        Singleton.getInstance().Progress(mActivity).dismiss();
                        Toast.makeText(mActivity, "Unable to connnect", Toast.LENGTH_SHORT).show();
                    } else {
                        String Token = getToken_model.getResult();
                        callfrontclass2(Token, roomidentity, Allstudent);
                    }
                }
                @Override
                public void failure(RetrofitError error) {
                    Toast.makeText(mActivity, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }else {
            Toast.makeText(mActivity, "Internet connection problem", Toast.LENGTH_SHORT).show();
        }
    }

    private void callfrontclass2(String token, String room, String AllStudent) {
        Bundle bundle = new Bundle();
        Fragment mFragment = null;
        Class fragmentClass = Live_Video_Screen.class;
        try {
            mFragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        FragmentManager fragmentManager = ((FragmentActivity)mActivity).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);

        bundle.putString("Room", room);
        bundle.putString("Token", token);
        bundle.putString("calltype",ConstantCommand.callType_Video);
        bundle.putString("AllStudent",AllStudent);

        assert mFragment != null;
        mFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.container, mFragment).addToBackStack("jnsbnbs").commit();
    }
    private void callfrontclass(String batchName, String batchid,
                                String starttime, String endtime, String teacherid, String duration,
                                String image)
    {
        Bundle bundle = new Bundle();
        Fragment mFragment = null;
        Class fragmentClass = Selectbatch_fragment.class;
        try {
            mFragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        FragmentManager fragmentManager = ((FragmentActivity) mActivity).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        bundle.putString("batchName", batchName);
        bundle.putString("batchid", batchid);
        bundle.putString("starttime", starttime);
        bundle.putString("endtime", endtime);
        bundle.putString("teacherid", teacherid);
        bundle.putString("duration", duration);
        bundle.putString("image", image);

        assert mFragment != null;
        mFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.container, mFragment).addToBackStack("jnsbnbs").commit();
    }

    private void removeAt(int position)
    {
        Batchlist.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position,Batchlist.size());
    }
}
