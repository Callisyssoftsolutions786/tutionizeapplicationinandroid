package com.tutionize.androidstudionavigationdrawer.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.tutionize.androidstudionavigationdrawer.Constant.AppConst;
import com.tutionize.androidstudionavigationdrawer.Constant.Singleton;
import com.tutionize.androidstudionavigationdrawer.Model.PandingStudentListofTeacherDetail;
import com.tutionize.androidstudionavigationdrawer.Model.Result;
import com.tutionize.androidstudionavigationdrawer.Model.StudentListOfTeacherDetail;
import com.tutionize.androidstudionavigationdrawer.Model.TeacherDetailModle;
import com.tutionize.androidstudionavigationdrawer.Model.TeacherDetailResult;
import com.tutionize.androidstudionavigationdrawer.ProgressDialog;
import com.tutionize.androidstudionavigationdrawer.R;
import com.tutionize.androidstudionavigationdrawer.Sharepool.NetworkStateCheck;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * Created by hp on 01-03-2017.
 */
public class adapter_course_learning extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    private Activity mActivity;
    private LayoutInflater mInflater;
    private ValueFilter valueFilter;
    private List<Result> teacherlist = new ArrayList<>();
    private List<Result> teacherlist1 = new ArrayList<>();
    private List<StudentListOfTeacherDetail> Student;
    private List<PandingStudentListofTeacherDetail> studentpendinglist;
    private ArrayList<String> TeacherActiveStudent = new ArrayList<>();
    private ArrayList<String> TeacherPandingStudentList = new ArrayList<>();

    public adapter_course_learning(Activity mActivity, List<Result> teacherlist) {
        this.mActivity = mActivity;
        this.teacherlist = teacherlist;
        this.teacherlist1 = teacherlist;
        getFilter();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_code_teaching, parent, false);
        return new adapter_course_learning.FindHolder(view);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof FindHolder) {
            final Result teacherResult = teacherlist.get(position);
            Integer fee = teacherResult.getCourseprice();
             NumberFormat nf  = NumberFormat.getCurrencyInstance(Locale.US);
            if (fee!=null) {
                String amt = nf.format(fee);
           ((FindHolder) holder).txt_coursefee.setText(amt);
            }

            else {
                String amount1 = nf.format(0);
                ((FindHolder) holder).txt_coursefee.setText(amount1);
            }
            ((FindHolder) holder).txt_more.setOnClickListener(v->TeacherDetail(teacherResult.getId())

            );
            ((FindHolder) holder).Text_Username.setText(teacherResult.getFirst().concat(" " + teacherResult.getLast()));
            ((FindHolder) holder).txt_bio.setText(teacherResult.getBio());
            ((FindHolder) holder).txt_Qualification.setText(teacherResult.getQualification());

            if (teacherResult.getReview()!= null)
            {
                ((FindHolder) holder).text_number.setText(""+teacherResult.getReview());
            }

            holder.itemView.setOnClickListener(view -> TeacherDetail(teacherResult.getId()));


            String picUrl = teacherResult.getImage();

            if (!(picUrl == null)) {
                if (!(picUrl.equals(""))) {
                    String pic = AppConst.photoconstant.concat(picUrl);
                    Glide.with(mActivity).load(pic).into(((FindHolder) holder).picture);
                } else {
                    Glide.with(mActivity).load(R.drawable.userimagewhite).into(((FindHolder) holder).picture);
                }
            } else {
                Glide.with(mActivity).load(R.drawable.userimagewhite).into(((FindHolder) holder).picture);
            }

        }

    }

    @Override
    public int getItemCount() {

        return teacherlist.size();
    }

    @Override
    public Filter getFilter() {

        if (valueFilter == null){
           valueFilter = new ValueFilter();
        }
        return valueFilter;
    }

    public class FindHolder extends RecyclerView.ViewHolder {

        CircleImageView picture;
        TextView Text_Username, txt_more, txt_bio, txt_coursefee,txt_Qualification,text_number;
        FindHolder(View itemView) {
            super(itemView);

            txt_more = (TextView) itemView.findViewById(R.id.more_discription);
            Text_Username = (TextView) itemView.findViewById(R.id.txt_teachername);
            picture = (CircleImageView) itemView.findViewById(R.id.img_teacher);
            txt_bio = (TextView) itemView.findViewById(R.id.txt_bio);
            txt_Qualification = (TextView) itemView.findViewById(R.id.txt_qualification);
            txt_coursefee = (TextView)itemView.findViewById(R.id.txt_coursefeee);
            text_number = (TextView)itemView.findViewById(R.id.text_number);
        }
    }

    @SuppressLint("ResourceAsColor")
    private void TeacherDetail(final String teacherid) {
        if (NetworkStateCheck.isNetworkAvaliable(mActivity)) {
            KProgressHUD knprogress = ProgressDialog.getInstance().getKNProgressDialog(mActivity);
            knprogress.show();
            Singleton.getInstance().getApi().TeacherDetail(teacherid, new Callback<TeacherDetailModle>() {
                @Override
                public void success(TeacherDetailModle teacherDetailModle, Response response) {
                    if (teacherDetailModle.getSuccess() == 1) {
                        List<TeacherDetailResult> teacherdetail = teacherDetailModle.getResult();
                        studentpendinglist = teacherDetailModle.getStudentpendinglist();
                        String Teacher_name = teacherdetail.get(0).getFirst().concat(" " + teacherdetail.get(0).getLast());
                        String Teacher_id = teacherdetail.get(0).getId();
                        String teacher_course = teacherdetail.get(0).getCourses();
                        String amount = teacherdetail.get(0).getCourseprice();
                        String review = (String) teacherdetail.get(0).getReview();
                        String languages = (String) teacherdetail.get(0).getLanguage();
                        String country = (String) teacherdetail.get(0).getCountry();
                        String fee;
                        if (amount != null) {
                            fee = amount;
                        } else {
                            fee = "" + 0;
                        }
                        Student = teacherDetailModle.getStudentlist();
                        if (Student != null) {
                            for (int e = 0; e < Student.size(); e++) {
                                TeacherActiveStudent.add(Student.get(e).getStudent());
                            }
                        }
                        if (studentpendinglist != null) {
                            for (int o = 0; o < studentpendinglist.size(); o++) {
                                TeacherPandingStudentList.add(studentpendinglist.get(o).getStudent());
                            }
                        }
                        Singleton.getInstance().callfrontclass(mActivity, Teacher_id, Teacher_name, teacherdetail.get(0).getBio(), teacherdetail.get(0).getQualification(),
                                teacherdetail.get(0).getImage(), teacher_course, fee, review, languages, country, TeacherActiveStudent, TeacherPandingStudentList);
                        knprogress.dismiss();

                    } else {
                        knprogress.dismiss();
                        Toast.makeText(mActivity, "" + teacherDetailModle.getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    knprogress.dismiss();
                    Toast.makeText(mActivity, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }else {
            Toast.makeText(mActivity, "Internet connection problem", Toast.LENGTH_SHORT).show();
        }
    }
    class ValueFilter extends Filter
    {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if(constraint!=null && constraint.length()>0)
            {
                ArrayList<Result> filterList=new ArrayList<>();
                for(int i=0;i<teacherlist1.size();i++){
                    if(teacherlist1.get(i).getFirst().contains(constraint)
                            || teacherlist1.get(i).getEmail().contains(constraint)) {
                        filterList.add(teacherlist1.get(i));
                    }
                }
                results.count=filterList.size();
                results.values=filterList;
            }else{
                results.count=teacherlist1.size();
                results.values=teacherlist1;
            }
            return results;
        }
        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint,FilterResults results)
        {
            teacherlist =(ArrayList<Result>) results.values;
            notifyDataSetChanged();
        }
    }

}

