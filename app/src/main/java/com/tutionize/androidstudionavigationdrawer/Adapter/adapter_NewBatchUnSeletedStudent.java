package com.tutionize.androidstudionavigationdrawer.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.rey.material.widget.Button;
import com.rey.material.widget.TextView;
import com.tutionize.androidstudionavigationdrawer.Constant.AppConst;
import com.tutionize.androidstudionavigationdrawer.Fragment.Selectbatch_fragment;
import com.tutionize.androidstudionavigationdrawer.Model.SeletedStudent;
import com.tutionize.androidstudionavigationdrawer.Model.UnseletedStudent;
import com.tutionize.androidstudionavigationdrawer.R;
import com.tutionize.androidstudionavigationdrawer.Sharepool.MySharePrafranceClass;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by user on 31/1/18.
 */

public class adapter_NewBatchUnSeletedStudent  extends RecyclerView.Adapter<adapter_NewBatchUnSeletedStudent.MyViewHolder>{

    private List<UnseletedStudent> hiredStudentList=new ArrayList<>();
    private Activity activity;


    public adapter_NewBatchUnSeletedStudent(Activity activity,List<UnseletedStudent> hiredStudentList){
        this.activity=activity;
        this.hiredStudentList.clear();
        this.hiredStudentList=hiredStudentList;
        String[] list = MySharePrafranceClass.GetSharePrefrance(activity);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.hire_student_list,parent, false);
        return new adapter_NewBatchUnSeletedStudent.MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        final UnseletedStudent batchdata = hiredStudentList.get(position);

        holder.txt_name.setText(batchdata.getFirst() +" "+batchdata.getLast());
        holder.txt_country.setText(batchdata.getCountry());
        holder.txt_listtype.setText(batchdata.getType());
        String Picture= batchdata.getThumb();

        if (!(Picture == null)) {
            if (!(Picture.equals(""))) {
                String pic= AppConst.photoconstant.concat(Picture);
                Glide.with(activity).load(pic)
                        .into(holder.img_student);
            } else {
                Glide.with(activity).load(R.drawable.userimagewhite).into(holder.img_student);
            }
        } else {
            Glide.with(activity).load(R.drawable.userimagewhite).into(holder.img_student);
        }

        holder.btn_startclass.setOnClickListener((View v)->{
            Selectbatch_fragment.AddSelectedStudent(new SeletedStudent(batchdata.getId(),batchdata.getFirst(),batchdata.getLast(),
                    batchdata.getEmail(),batchdata.getImage(),batchdata.getThumb(),batchdata.getCountry(),batchdata.getType()));
            for (int i=0;i< Selectbatch_fragment.UnselectedStudentId.size();i++){
                if (batchdata.getId().equals(Selectbatch_fragment.UnselectedStudentId.get(i).getId())){
                    Selectbatch_fragment.UnselectedStudentId.remove(i);
                    break;
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return hiredStudentList.size();
    }

    public void removeAt(int position)
    {
        hiredStudentList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, hiredStudentList.size());
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txt_name,txt_country,txt_listtype;
        Button btn_startclass;
        CircleImageView img_student;
        MyViewHolder(View itemView) {
            super(itemView);
            txt_name= itemView.findViewById(R.id.hire_Student_list_nameofstudent);
            txt_country= itemView.findViewById(R.id.hire_Student_list_country);
            txt_listtype= itemView.findViewById(R.id.hire_Student_list_Type);
            btn_startclass= itemView.findViewById(R.id.hire_Student_list_add_student);
            img_student= itemView.findViewById(R.id.hire_Student_list_image);

        }
    }
}

