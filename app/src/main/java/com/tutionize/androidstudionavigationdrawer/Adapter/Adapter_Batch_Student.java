package com.tutionize.androidstudionavigationdrawer.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.rey.material.widget.Button;
import com.rey.material.widget.TextView;
import com.tutionize.androidstudionavigationdrawer.Constant.AppConst;
import com.tutionize.androidstudionavigationdrawer.Constant.Singleton;
import com.tutionize.androidstudionavigationdrawer.Fragment.Selectbatch_fragment;
import com.tutionize.androidstudionavigationdrawer.Model.All_Student_of_batch_result;
import com.tutionize.androidstudionavigationdrawer.Model.DeleteStudentToBatch;
import com.tutionize.androidstudionavigationdrawer.R;
import com.tutionize.androidstudionavigationdrawer.Sharepool.MySharePrafranceClass;
import com.tutionize.androidstudionavigationdrawer.Sharepool.NetworkStateCheck;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**0
 * Created by android on 12/5/17.
 */

public class Adapter_Batch_Student extends RecyclerView.Adapter<Adapter_Batch_Student.MyViewHolder> {

    private List<All_Student_of_batch_result> StudentofBatch = new ArrayList<>();
    private Activity activity;
    private String[] list;

    public Adapter_Batch_Student(Activity activity, List<All_Student_of_batch_result> StudentofBatch) {
        this.activity = activity;
        this.StudentofBatch = StudentofBatch;
        this.list= MySharePrafranceClass.GetSharePrefrance(activity);
    }

    @NonNull
    @Override
    public Adapter_Batch_Student.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.batch_student_list,parent, false);
        return new Adapter_Batch_Student.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull Adapter_Batch_Student.MyViewHolder holder, @SuppressLint("RecyclerView") int position) {
        All_Student_of_batch_result batchdata = StudentofBatch.get(position);
        if (batchdata!=null) {
            String first = batchdata.getFirst();
            if (first != null)
            {
                holder.txt_name.setText(first);
            }
            String country = batchdata.getCountry();
            if (country != null) {
                holder.txt_country.setText(country);
            }
            String type = batchdata.getType();
            if (type != null) {
                holder.txt_listtype.setText(type);
            }
            String Picture = batchdata.getThumb();
            if (!(Picture == null)) {
                if (!(Picture.equals(""))) {
                    String pic = AppConst.photoconstant.concat(Picture);
                    Glide.with(activity).load(pic).into(holder.img_student);
                } else {
                    Glide.with(activity).load(R.drawable.userimagewhite).into(holder.img_student);
                }
            } else {
                Glide.with(activity).load(R.drawable.userimagewhite).into(holder.img_student);
            }

            holder.btn_startclass.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.btn_startclass.setOnClickListener(null);
                    String batchid = Selectbatch_fragment.selectbatch_fragment.getBatchid();
                    String Studentid = batchdata.getId();
                    String Teacherid = list[0];
                    RemoveStudentToBatch(Teacherid, Studentid, batchid, position);
                }
            });
        }
    }

    private void RemoveStudentToBatch(String teacherid, String studentid, String batchid, final int position) {
      if(NetworkStateCheck.isNetworkAvaliable(activity)) {
          Singleton.getInstance().getApi().RemoveStudentToBatch(teacherid, studentid, batchid, new Callback<DeleteStudentToBatch>() {
              @Override
              public void success(DeleteStudentToBatch deleteStudentToBatch, Response response) {
                  if (deleteStudentToBatch.getSuccess().equals("1"))
                      removeAt(position);
                  Toast.makeText(activity, "" + deleteStudentToBatch.getMessage(), Toast.LENGTH_SHORT).show();
              }

              @Override
              public void failure(RetrofitError error) {
                  Toast.makeText(activity, "" + error, Toast.LENGTH_SHORT).show();
              }
          });
      }else {
          Toast.makeText(activity, "Internet connection problem", Toast.LENGTH_SHORT).show();
      }
    }


    @Override
    public int getItemCount() {
        return StudentofBatch.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txt_name, txt_country, txt_listtype;
        Button btn_startclass;
        CircleImageView img_student;

        MyViewHolder(View itemView) {
            super(itemView);

            txt_name = itemView.findViewById(R.id.batch_Student_list_nameofstudent);
            txt_country = itemView.findViewById(R.id.batch_Student_list_country);
            txt_listtype = itemView.findViewById(R.id.batch_Student_list_Type);
            btn_startclass = itemView.findViewById(R.id.batch_Student_list_add_student);
            img_student = itemView.findViewById(R.id.batch_Student_list_image);

        }
    }
    private void removeAt(int position) {
        StudentofBatch.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, StudentofBatch.size());
    }

}
