package com.tutionize.androidstudionavigationdrawer.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tutionize.androidstudionavigationdrawer.Activity_Screen.Student_List;
import com.tutionize.androidstudionavigationdrawer.Constant.AppConst;
import com.tutionize.androidstudionavigationdrawer.Model.HireStudentList_Result;
import com.tutionize.androidstudionavigationdrawer.R;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by hp on 01-03-2017.
 */
public class Adapter_course_teaching extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private Activity mActivity;
    private LayoutInflater mInflater;
    private List<HireStudentList_Result> StudentList= new ArrayList<>();
    public Adapter_course_teaching(Activity mActivity ,List<HireStudentList_Result> StudentList) {
        this.mActivity = mActivity;
        this.StudentList=StudentList;
        mInflater = LayoutInflater.from(mActivity);
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.hire_student_list, parent, false);
        return new FindHolder(view);
    }
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof FindHolder) {
            final HireStudentList_Result Student = StudentList.get(position);
            ((FindHolder) holder).txt_StudentName.setText(Student.getFirst()+" "+Student.getLast());
            ((FindHolder) holder).txt_Student_Country.setText(Student.getCountry());
            ((FindHolder) holder).txt_StudentType.setText(Student.getType());
            String Picture= Student.getThumb();
            if (!(Picture == null))
            {
                if (!(Picture.equals(""))) {
                    String pic=AppConst.photoconstant.concat(Picture);
                    Glide.with(mActivity).load(pic)
                            .into(((FindHolder) holder).studentImage);
                } else {
                    Glide.with(mActivity).load(R.drawable.userimagewhite).into(((FindHolder) holder).studentImage);
                }
            }
            else {
                 Glide.with(mActivity).load(R.drawable.userimagewhite).into(((FindHolder) holder).studentImage);
            }
        }
    }

    @Override
    public int getItemCount() {

        return StudentList.size();
    }


    public class FindHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        Button add_student;
        CircleImageView studentImage;
        TextView txt_StudentName,txt_Student_Country,txt_StudentType;
        FindHolder(View v) {
            super(v);
            studentImage= (CircleImageView) v.findViewById(R.id.hire_Student_list_image);
            txt_StudentName= (TextView) v.findViewById(R.id.hire_Student_list_nameofstudent);
            txt_Student_Country= (TextView) v.findViewById(R.id.hire_Student_list_country);
            txt_StudentType= (TextView) v.findViewById(R.id.hire_Student_list_Type);
            add_student=(Button)v.findViewById(R.id.hire_Student_list_add_student);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent mIntent=new Intent(mActivity, Student_List.class);
            mActivity.startActivity(mIntent);


        }
    }

}
