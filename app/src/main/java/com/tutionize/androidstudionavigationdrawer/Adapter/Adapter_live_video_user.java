package com.tutionize.androidstudionavigationdrawer.Adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rey.material.widget.Button;
import com.tutionize.androidstudionavigationdrawer.Model.TwelioView_Model;
import com.tutionize.androidstudionavigationdrawer.R;
import com.twilio.video.AudioTrack;
import com.twilio.video.Participant;
import com.twilio.video.VideoTrack;
import com.twilio.video.VideoView;

import java.util.ArrayList;
import java.util.List;



/**
 * Created by hp on 12-02-2017.
 */
public class Adapter_live_video_user extends RecyclerView.Adapter<Adapter_live_video_user.ViewHolder>
{
    private List<TwelioView_Model>twelioViewModels = new ArrayList<>();
    private Activity mActivity;
    private ArrayList<String> Text;
    private LayoutInflater mInflater;

    public Adapter_live_video_user(Activity mActivity, List<TwelioView_Model> twelioViewModels) {
        this.mActivity = mActivity;
        this.twelioViewModels=twelioViewModels;
        mInflater = LayoutInflater.from(mActivity);
    }
    @NonNull
    @Override
    public Adapter_live_video_user.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_live_user, parent, false);
        return new Adapter_live_video_user.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        TwelioView_Model twelioViewModel=twelioViewModels.get(position);
        holder.Thumb_two.setMirror(false);
        twelioViewModel.getPaticipant().setListener(new Participant.Listener() {
            @Override
            public void onAudioTrackAdded(Participant participant, AudioTrack audioTrack) {

            }

            @Override
            public void onAudioTrackRemoved(Participant participant, AudioTrack audioTrack) {

            }

            @Override
            public void onVideoTrackAdded(Participant participant, VideoTrack videoTrack) {

            }

            @Override
            public void onVideoTrackRemoved(Participant participant, VideoTrack videoTrack) {

            }

            @Override
            public void onAudioTrackEnabled(Participant participant, AudioTrack audioTrack) {

            }

            @Override
            public void onAudioTrackDisabled(Participant participant, AudioTrack audioTrack) {

            }

            @Override
            public void onVideoTrackEnabled(Participant participant, VideoTrack videoTrack) {

                if (holder.disable_view.getVisibility()==View.VISIBLE){
                    holder.disable_view.setVisibility(View.GONE);
                }

            }

            @Override
            public void onVideoTrackDisabled(Participant participant, VideoTrack videoTrack) {

               if (holder.disable_view.getVisibility()==View.GONE){
                   holder.disable_view.setVisibility(View.VISIBLE);
               }

            }
        });
        twelioViewModel.getVideoTrack().addRenderer(holder.Thumb_two);
    }
    @Override
    public int getItemCount() {
        return twelioViewModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private VideoView Thumb_two;
        private Button disable_view;
        public ViewHolder(View itemView) {
            super(itemView);
            Thumb_two = itemView.findViewById(R.id.thumbnail_vidio_view2);
            disable_view = itemView.findViewById(R.id.disable_view);
        }
    }
}
