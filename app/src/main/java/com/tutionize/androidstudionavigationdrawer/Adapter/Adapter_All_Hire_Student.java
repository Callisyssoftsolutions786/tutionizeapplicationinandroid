package com.tutionize.androidstudionavigationdrawer.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.rey.material.widget.Button;
import com.rey.material.widget.TextView;
import com.tutionize.androidstudionavigationdrawer.Constant.AppConst;
import com.tutionize.androidstudionavigationdrawer.Constant.Singleton;
import com.tutionize.androidstudionavigationdrawer.Fragment.Selectbatch_fragment;
import com.tutionize.androidstudionavigationdrawer.Model.AddStudentToBatch_modle;
import com.tutionize.androidstudionavigationdrawer.Model.Get_UnSelectedSudentResult;
import com.tutionize.androidstudionavigationdrawer.R;
import com.tutionize.androidstudionavigationdrawer.Sharepool.MySharePrafranceClass;
import com.tutionize.androidstudionavigationdrawer.Sharepool.NetworkStateCheck;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * Created by android on 12/4/17.
 */

public class Adapter_All_Hire_Student  extends RecyclerView.Adapter<Adapter_All_Hire_Student.MyViewHolder>{
    private Activity activity;
    private String[] list;
    private List<Get_UnSelectedSudentResult> StudentList = new ArrayList<Get_UnSelectedSudentResult>();
    public Adapter_All_Hire_Student(Activity activity ,List<Get_UnSelectedSudentResult> StudentList)
    {
       this.activity=activity;
       this.StudentList=StudentList;
       this.list= MySharePrafranceClass.GetSharePrefrance(activity);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.hire_student_list,parent, false);
        return new Adapter_All_Hire_Student.MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        final Get_UnSelectedSudentResult batchdata = StudentList.get(position);
        if (batchdata!=null)
        {
            holder.txt_name.setText(batchdata.getFirst() + " " + batchdata.getLast());
            holder.txt_country.setText(batchdata.getCountry());
            holder.txt_listtype.setText(batchdata.getType());
            String Picture = batchdata.getThumb();

            if (!(Picture == null)) {
                if (!(Picture.equals(""))) {
                    String pic = AppConst.photoconstant.concat(Picture);
                    Glide.with(activity).load(pic)
                            .into(holder.img_student);
                } else {
                    Glide.with(activity).load(R.drawable.userimagewhite).into(holder.img_student);
                }
            } else {
                Glide.with(activity).load(R.drawable.userimagewhite).into(holder.img_student);
            }

            holder.btn_startclass.setOnClickListener((View v) -> {

                String batchid = Selectbatch_fragment.selectbatch_fragment.getBatchid();
                String Studentid = batchdata.getId();

                String Teacherid = list[0];
                AddStudentToBatch(Teacherid, Studentid, batchid, position);
            });
        }

    }

    private void AddStudentToBatch(String teacherid, String studentid, String batchid,final int position) {
        if (NetworkStateCheck.isNetworkAvaliable(activity)) {
            Singleton.getInstance().getApi().AddStudentToBatch(teacherid, studentid, batchid, new Callback<AddStudentToBatch_modle>() {

                @Override
                public void success(AddStudentToBatch_modle addStudentToBatch_modle, Response response) {
                    if (addStudentToBatch_modle.getSuccess().equals("1")) {

                        removeAt(position);
                        Toast.makeText(activity, "" + addStudentToBatch_modle.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Toast.makeText(activity, "" + error.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });
        }else {
            Toast.makeText(activity, "Internet connection problem", Toast.LENGTH_SHORT).show();
        }
    }

    private void removeAt(int position) {
        StudentList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, StudentList.size());
    }


    @Override
    public int getItemCount() {
        return StudentList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txt_name,txt_country,txt_listtype;
        Button btn_startclass,btn_getclass;
        CircleImageView img_student;
        MyViewHolder(View itemView) {
            super(itemView);
            txt_name= itemView.findViewById(R.id.hire_Student_list_nameofstudent);
            txt_country= itemView.findViewById(R.id.hire_Student_list_country);
            txt_listtype= itemView.findViewById(R.id.hire_Student_list_Type);
            btn_startclass= itemView.findViewById(R.id.hire_Student_list_add_student);
            img_student= itemView.findViewById(R.id.hire_Student_list_image);

        }
    }
}
