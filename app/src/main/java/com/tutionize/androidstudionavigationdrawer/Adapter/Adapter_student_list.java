package com.tutionize.androidstudionavigationdrawer.Adapter;

import android.app.Activity;
import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tutionize.androidstudionavigationdrawer.R;

import java.util.ArrayList;


/**
 * Created by hp on 01-03-2017.
 */
public class Adapter_student_list extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private static final int TYPE_ITEM = 1;
    private ArrayList<Integer> Image;
    private ArrayList<String> Text;
    private LayoutInflater mInflater;
    MediaPlayer mediaplayer;
    public Adapter_student_list(Activity mActivity) {
        Activity mActivity1 = mActivity;
        mInflater = LayoutInflater.from(mActivity);
    }
    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_ITEM;
        } else {
            return TYPE_ITEM;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.custom_student_list, parent, false);
        return new FindHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
    }

    @Override
    public int getItemCount() {

        return 20;
    }


    public class FindHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView picture;
        TextView Text_Religion;
        FindHolder(View v) {
            super(v);

            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {


        }
    }

}
