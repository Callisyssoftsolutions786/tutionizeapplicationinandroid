package com.tutionize.androidstudionavigationdrawer.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.rey.material.widget.Button;
import com.rey.material.widget.TextView;
import com.tutionize.androidstudionavigationdrawer.Constant.AppConst;
import com.tutionize.androidstudionavigationdrawer.Fragment.Selectbatch_fragment;
import com.tutionize.androidstudionavigationdrawer.Model.HireStudentList_Result;
import com.tutionize.androidstudionavigationdrawer.Model.SeletedStudent;
import com.tutionize.androidstudionavigationdrawer.Model.UnseletedStudent;
import com.tutionize.androidstudionavigationdrawer.R;
import com.tutionize.androidstudionavigationdrawer.Sharepool.MySharePrafranceClass;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by user on 29/1/18.
 */

public class Adapter_NewBatchStudent  extends RecyclerView.Adapter<Adapter_NewBatchStudent.MyViewHolder>{
    private List<HireStudentList_Result> hiredStudentList=new ArrayList<>();
    private Activity activity;
    private String[] list;

    public Adapter_NewBatchStudent( Activity activity,List<HireStudentList_Result> hiredStudentList){
        this.activity=activity;
        this.hiredStudentList=hiredStudentList;
        this.list= MySharePrafranceClass.GetSharePrefrance(activity);
        for (HireStudentList_Result e : hiredStudentList){
            if (e!=null) {
                Selectbatch_fragment.AddUnSelectedStudent(new UnseletedStudent(e.getId(), e.getFirst(), e.getLast(),
                        e.getEmail(), e.getImage(), e.getThumb(), e.getCountry(), e.getType()));
            }
        }
    }
    @NonNull
    @Override
    public Adapter_NewBatchStudent.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.hire_student_list,parent, false);
        return new Adapter_NewBatchStudent.MyViewHolder(itemView);
    }
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull Adapter_NewBatchStudent.MyViewHolder holder, int position) {
        final HireStudentList_Result batchdata = hiredStudentList.get(position);
        if (batchdata!=null)
        {
            String first = batchdata.getFirst();
            String last = batchdata.getLast();
            String Country = batchdata.getCountry();
            String type = batchdata.getType();

            holder.txt_name.setText(first + " " + last);
            holder.txt_country.setText(Country);
            holder.txt_listtype.setText(type);
            String Picture = batchdata.getThumb();


            if (!(Picture == null)) {
                if (!(Picture.equals(""))) {
                    String pic = AppConst.photoconstant.concat(Picture);
                    Glide.with(activity).load(pic)
                            .into(holder.img_student);
                } else {
                    Glide.with(activity).load(R.drawable.userimagewhite).into(holder.img_student);
                }
            } else {
                Glide.with(activity).load(R.drawable.userimagewhite).into(holder.img_student);
            }

            holder.btn_startclass.setOnClickListener((View v) -> {

                Selectbatch_fragment.AddSelectedStudent(new SeletedStudent(batchdata.getId(), batchdata.getFirst(), batchdata.getLast(),
                        batchdata.getEmail(), batchdata.getImage(), batchdata.getThumb(), batchdata.getCountry(), batchdata.getType()));

                for (int i = 0; i < Selectbatch_fragment.UnselectedStudentId.size(); i++)
                {
                    if (batchdata.getId().equals(Selectbatch_fragment.UnselectedStudentId.get(i).getId()))
                    {
                        Selectbatch_fragment.UnselectedStudentId.remove(i);
                        break;
                    }
                }
                removeAt(position);
            });

        }

    }

    @Override
    public int getItemCount() {
        return hiredStudentList.size();
    }

    private void removeAt(int position) {
        hiredStudentList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, hiredStudentList.size());
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txt_name,txt_country,txt_listtype;
        Button btn_startclass;
        CircleImageView img_student;
        MyViewHolder(View itemView) {
            super(itemView);
            txt_name= itemView.findViewById(R.id.hire_Student_list_nameofstudent);
            txt_country= itemView.findViewById(R.id.hire_Student_list_country);
            txt_listtype= itemView.findViewById(R.id.hire_Student_list_Type);
            btn_startclass= itemView.findViewById(R.id.hire_Student_list_add_student);
            img_student= itemView.findViewById(R.id.hire_Student_list_image);
        }
    }
}
