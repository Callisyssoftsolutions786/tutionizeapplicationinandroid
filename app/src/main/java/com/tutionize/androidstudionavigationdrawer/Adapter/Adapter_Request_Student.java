package com.tutionize.androidstudionavigationdrawer.Adapter;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.tutionize.androidstudionavigationdrawer.Constant.AppConst;
import com.tutionize.androidstudionavigationdrawer.Constant.Singleton;
import com.tutionize.androidstudionavigationdrawer.Model.Requested_student_result;
import com.tutionize.androidstudionavigationdrawer.Model.Student_Notification_modle;
import com.tutionize.androidstudionavigationdrawer.R;
import com.tutionize.androidstudionavigationdrawer.Sharepool.MySharePrafranceClass;
import com.tutionize.androidstudionavigationdrawer.Sharepool.NetworkStateCheck;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * Created by android on 11/24/17.
 */

public class Adapter_Request_Student extends RecyclerView.Adapter<Adapter_Request_Student.MyViewHolder>{
    private Activity activity;
    private String[] list;
    private RelativeLayout New_RequestMenu;
    private LinearLayout recycleLayout;
    private ImageView Teacher_main_view_request;
    private List<Requested_student_result> StudentList = new ArrayList<Requested_student_result>();
    public Adapter_Request_Student (Activity activity , List<Requested_student_result> StudentList,
                                    RelativeLayout  New_RequestMenu, LinearLayout recycleLayout , ImageView Teacher_main_view_request){
        this.activity=activity;
        this.StudentList=StudentList;
        this.New_RequestMenu = New_RequestMenu;
        this.recycleLayout = recycleLayout;
        this.Teacher_main_view_request = Teacher_main_view_request;
    }

    @NonNull
    @Override
    public Adapter_Request_Student.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.requested_student_list,parent, false);
        return new Adapter_Request_Student.MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull Adapter_Request_Student.MyViewHolder holder,@SuppressLint("RecyclerView") final int position) throws IllegalArgumentException
    {
        list = MySharePrafranceClass.GetSharePrefrance(activity);
        Requested_student_result batchdata = StudentList.get(position);
        if (batchdata!=null)
        {
            holder.txt_name.setText(batchdata.getFirst());
            holder.txt_listtype.setText(batchdata.getType());
            String Picture = batchdata.getThumb();
            if (Picture != null) {
                if (!(Picture.equals(""))) {
                    String pic = AppConst.photoconstant.concat(Picture);
                    try {
                        Glide.with(activity).load(pic).into(holder.img_student);
                    } catch (IllegalArgumentException ignored) {
                    }
                } else {
                    try {
                        Glide.with(activity).load(R.drawable.userimagewhite).into(holder.img_student);
                    } catch (IllegalArgumentException ignored) {
                    }
                }
            } else {
                try {
                    Glide.with(activity).load(R.drawable.userimagewhite).into(holder.img_student);
                } catch (IllegalArgumentException ignored) {
                }
            }
            holder.btn_add.setOnClickListener(v -> AcceptRequest(list[0], batchdata.getId(), position));
            holder.btn_reject.setOnClickListener(v -> RejectRequest(list[0], batchdata.getId(), position));
        }
    }
    @Override
    public int getItemCount() {
        return StudentList.size();
    }
    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txt_name;
        TextView txt_listtype;
        Button btn_add,btn_reject;
        CircleImageView img_student;
        MyViewHolder(View itemView)
        {
            super(itemView);
            txt_name= itemView.findViewById(R.id.Student_list_nameofstudent);
            txt_listtype= itemView.findViewById(R.id.Student_list_Type);
            btn_add= itemView.findViewById(R.id.btn_add);
            btn_reject= itemView.findViewById(R.id.btn_reject);
            img_student= itemView.findViewById(R.id.Student_list_image);
        }
    }
    private void AcceptRequest(String teacher,String student,int position){
        if (NetworkStateCheck.isNetworkAvaliable(activity)) {
            Singleton.getInstance().getApi().AcceptRequest(teacher, student, new Callback<Student_Notification_modle>() {
                @Override
                public void success(Student_Notification_modle student_notification_modle, Response response) {
                    if (!student_notification_modle.getSuccess().trim().equals("")) {
                        RemoveAt(position);
                    }
                }
                @Override
                public void failure(RetrofitError error) {
                    Toast.makeText(activity, "" + error, Toast.LENGTH_SHORT).show();
                }
            });
        }else {
            Toast.makeText(activity, "Internet connection problem", Toast.LENGTH_SHORT).show();
        }
    }
    private void RejectRequest(String teacher,String student,int position){
        if (NetworkStateCheck.isNetworkAvaliable(activity)) {
            Singleton.getInstance().getApi().RejectRequest(teacher, student, new Callback<Student_Notification_modle>() {
                @Override
                public void success(Student_Notification_modle student_notification_modle, Response response) {
                    if (!student_notification_modle.getSuccess().trim().equals("")) {
                        RemoveAt(position);
                    }
                }
                @Override
                public void failure(RetrofitError error) {
                    Toast.makeText(activity, "" + error, Toast.LENGTH_SHORT).show();
                }
            });
        }else {
            Toast.makeText(activity, "Internet connection problem", Toast.LENGTH_SHORT).show();
        }
    }
    private  void RemoveAt(int position){
        try {
            StudentList.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, StudentList.size());
            float startDegress = -90;
            float endDegress = 0;
            RotateAnimation anim = new RotateAnimation(startDegress, endDegress, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
            anim.setInterpolator(new LinearInterpolator());
            anim.setRepeatCount(0);
            anim.setFillAfter(true);
            anim.setDuration(200);
            Teacher_main_view_request.startAnimation(anim);
            New_RequestMenu.setVisibility(View.GONE);
            recycleLayout.setVisibility(View.GONE);
        }catch (IndexOutOfBoundsException ignored){}
        catch (Exception ignored){}
    }
}

