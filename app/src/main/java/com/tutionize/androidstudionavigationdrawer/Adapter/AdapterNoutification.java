package com.tutionize.androidstudionavigationdrawer.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.tutionize.androidstudionavigationdrawer.Activity_Screen.Home_screen;
import com.tutionize.androidstudionavigationdrawer.Constant.Singleton;
import com.tutionize.androidstudionavigationdrawer.Fragment.Fragment_notification_Dialog;
import com.tutionize.androidstudionavigationdrawer.Model.Dtabase_Notification_modle;
import com.tutionize.androidstudionavigationdrawer.Model.PandingStudentListofTeacherDetail;
import com.tutionize.androidstudionavigationdrawer.Model.StudentListOfTeacherDetail;
import com.tutionize.androidstudionavigationdrawer.Model.TeacherDetailModle;
import com.tutionize.androidstudionavigationdrawer.Model.TeacherDetailResult;
import com.tutionize.androidstudionavigationdrawer.ProgressDialog;
import com.tutionize.androidstudionavigationdrawer.R;
import com.tutionize.androidstudionavigationdrawer.Sharepool.MySharePrafranceClass;
import com.tutionize.androidstudionavigationdrawer.Sharepool.NetworkStateCheck;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by user on 22/12/17.
 */

public class AdapterNoutification extends RecyclerView.Adapter<AdapterNoutification.MyViewHolder> {

    private Activity activity;
    private List<StudentListOfTeacherDetail> Student = new ArrayList<>();
    private List<Dtabase_Notification_modle> notificationlist = new ArrayList<>();
    private List<PandingStudentListofTeacherDetail> studentpendinglist =  new ArrayList<>();
    private ArrayList<String> TeacherActiveStudent = new ArrayList<>();
    private ArrayList<String> TeacherPandingStudentList = new ArrayList<>();

    public AdapterNoutification(Activity activity, List<Dtabase_Notification_modle> notificationlist) {
        this.activity = activity;
        this.notificationlist = notificationlist;
        String[] loginlist = MySharePrafranceClass.LoginDataGetSharePrefrance(activity);
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        CircleImageView notificationImage;
        TextView notification_Msg;
        public TextView notification_time;
        MyViewHolder(View itemView) {
            super(itemView);
            notification_Msg = itemView.findViewById(R.id.notification_msg);
            notification_time = itemView.findViewById(R.id.notification_Time);
            notificationImage = itemView.findViewById(R.id.notification_image);
        }
    }
    @NonNull
    @Override
    public AdapterNoutification.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_list, parent, false);
        return new AdapterNoutification.MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(@NonNull AdapterNoutification.MyViewHolder holder, int position) {
        Dtabase_Notification_modle database = notificationlist.get(position);
        holder.notification_Msg.setText(database.getNotification());
        holder.notification_time.setText(database.getNotification_time());
        holder.itemView.setOnClickListener(v->{
            if (!database.getCaller_id().equals("null")) {
                TeacherDetail(database.getCaller_id());
            }
            else {
                if (!database.getRoom_id().equals("null")) {
                    if (!database.getCaller_id().equals("null")) {
                        TeacherDetail(database.getCaller_id());
                    }
                } else {
                    Intent intent = new Intent(this.activity, Home_screen.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    activity.startActivity(intent);
                }
            }
        });
}
    @Override
    public int getItemCount() {
        return notificationlist.size();
    }
    @SuppressLint("ResourceAsColor")
    private void TeacherDetail(final String teacherid) {
        if (NetworkStateCheck.isNetworkAvaliable(activity)) {
            KProgressHUD knprogress = ProgressDialog.getInstance().getKNProgressDialog(activity);
            knprogress.show();
            Singleton.getInstance().getApi().TeacherDetail(teacherid, new Callback<TeacherDetailModle>() {
                @Override
                public void success(TeacherDetailModle teacherDetailModle, Response response) {
                    if (teacherDetailModle.getSuccess() == 1) {

                        List<TeacherDetailResult> teacherdetail = teacherDetailModle.getResult();
                        studentpendinglist = teacherDetailModle.getStudentpendinglist();
                        String Teacher_name = teacherdetail.get(0).getFirst().concat(" " + teacherdetail.get(0).getLast());
                        String Teacher_id = teacherdetail.get(0).getId();
                        String teacher_course = teacherdetail.get(0).getCourses();
                        String amount = teacherdetail.get(0).getCourseprice();
                        String Review = (String) teacherdetail.get(0).getReview();
                        String languages = (String) teacherdetail.get(0).getLanguage();
                        String country = (String) teacherdetail.get(0).getCountry();
                        String fee;
                        if (amount != null) {
                            fee = amount;
                        } else {
                            fee = "" + 0;
                        }
                        Student = teacherDetailModle.getStudentlist();
                        if (Student != null) {
                            for (int e = 0; e < Student.size(); e++) {
                                TeacherActiveStudent.add(Student.get(e).getStudent());
                            }
                        }
                        if (studentpendinglist != null) {
                            for (int o = 0; o < studentpendinglist.size(); o++) {
                                TeacherPandingStudentList.add(studentpendinglist.get(o).getStudent());
                            }
                        }
                        Fragment_notification_Dialog.fragment_notification_Dialog.dismiss();
                        Singleton.getInstance().callfrontclass(activity, Teacher_id, Teacher_name, teacherdetail.get(0).getBio(), teacherdetail.get(0).getQualification(),
                                teacherdetail.get(0).getImage(), teacher_course, fee, Review, languages, country, TeacherActiveStudent, TeacherPandingStudentList);
                        knprogress.dismiss();

                    } else {
                        knprogress.dismiss();
                        Toast.makeText(activity, "" + teacherDetailModle.getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    knprogress.dismiss();
                    Toast.makeText(activity, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }else {
            Toast.makeText(activity, "Internet connection problem", Toast.LENGTH_SHORT).show();
        }
    }
}
