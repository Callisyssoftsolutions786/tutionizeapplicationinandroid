package com.tutionize.androidstudionavigationdrawer.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tutionize.androidstudionavigationdrawer.Constant.AppConst;
import com.tutionize.androidstudionavigationdrawer.Fragment.Selectbatch_fragment;
import com.tutionize.androidstudionavigationdrawer.Model.SeletedStudent;
import com.tutionize.androidstudionavigationdrawer.R;
import com.tutionize.androidstudionavigationdrawer.Sharepool.MySharePrafranceClass;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;



/**
 * Created by user on 30/1/18.
 */

public class Adapter_NewBatchSelectedStudent  extends RecyclerView.Adapter<Adapter_NewBatchSelectedStudent.MyViewHolder> {
    private List<SeletedStudent> hiredStudentList=new ArrayList<>();
    private Activity activity;

    public Adapter_NewBatchSelectedStudent(Activity activity,List<SeletedStudent> hiredStudentList){
        this.activity=activity;
        this.hiredStudentList.clear();
        this.hiredStudentList=hiredStudentList;
        String[] list = MySharePrafranceClass.GetSharePrefrance(activity);
    }

    @NonNull
    @Override
    public Adapter_NewBatchSelectedStudent.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.batch_student_list,parent, false);

        return new Adapter_NewBatchSelectedStudent.MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull Adapter_NewBatchSelectedStudent.MyViewHolder holder, int position) {
        final SeletedStudent batchdata = hiredStudentList.get(position);
        holder.txt_name.setText(batchdata.getFirst() +" "+batchdata.getLast());
        holder.txt_country.setText(batchdata.getCountry());
        holder.txt_listtype.setText(batchdata.getType());
        String Picture= batchdata.getThumb();
        if (!(Picture == null)) {
            if (!(Picture.equals(""))) {
                String pic= AppConst.photoconstant.concat(Picture);
                Glide.with(activity).load(pic).into(holder.img_student);
            } else {
                Glide.with(activity).load(R.drawable.userimagewhite).into(holder.img_student);
            }
        } else {
            Glide.with(activity).load(R.drawable.userimagewhite).into(holder.img_student);
        }

        holder.btn_startclass.setOnClickListener(v -> {
            for (int i =0;i <=Selectbatch_fragment.selectedStudentId.size();i++){
                if (Selectbatch_fragment.selectedStudentId.get(i).getId().equals(batchdata.getId())){
                    Selectbatch_fragment.selectedStudentId.remove(i);
                    notifyDataSetChanged();
                    return;
                }
                }
        });

    }

    @Override
    public int getItemCount() {
        return hiredStudentList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txt_name, txt_country, txt_listtype;
        Button btn_startclass;
        CircleImageView img_student;
        MyViewHolder(View itemView) {
            super(itemView);

            txt_name = (TextView) itemView.findViewById(R.id.batch_Student_list_nameofstudent);
            txt_country = (TextView) itemView.findViewById(R.id.batch_Student_list_country);
            txt_listtype = (TextView) itemView.findViewById(R.id.batch_Student_list_Type);
            btn_startclass = (Button) itemView.findViewById(R.id.batch_Student_list_add_student);
            img_student = (CircleImageView) itemView.findViewById(R.id.batch_Student_list_image);
        }
    }
    public void removeAt(int position) {

        hiredStudentList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, hiredStudentList.size());
    }
}
