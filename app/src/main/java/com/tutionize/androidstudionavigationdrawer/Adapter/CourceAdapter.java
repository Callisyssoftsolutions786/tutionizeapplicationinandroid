package com.tutionize.androidstudionavigationdrawer.Adapter;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tutionize.androidstudionavigationdrawer.Constant.AppConst;
import com.tutionize.androidstudionavigationdrawer.Fragment.Fragment_course_learning;
import com.tutionize.androidstudionavigationdrawer.Model.CourseData;
import com.tutionize.androidstudionavigationdrawer.R;

import java.util.ArrayList;
import java.util.List;

public class CourceAdapter extends RecyclerView.Adapter<CourceAdapter.ViewHolder> {
 private Activity activity;
 private List<CourseData> courcedat =  new ArrayList<>();

    public CourceAdapter(Activity activity, List<CourseData> courcedata) {
        this.activity = activity;
        this.courcedat= courcedata;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cource_list_view, parent, false);
        return new CourceAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CourseData  data =  courcedat.get(position);

        holder.course_Name.setText(data.getCourse_category());
        String Picture =  data.getPic();
        if (!(Picture == null))
        {
            if (!(Picture.equals(""))) {
                String pic = AppConst.Ap_image_constant.concat(Picture);
                Glide.with(activity).load(pic).into(holder.cource_image);
            }
        }
        holder.itemView.setOnClickListener(v-> FragmentCall(new Fragment_course_learning(),"Tag",data.getId()));
    }

    @Override
    public int getItemCount() {
        return courcedat.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView course_Name;
        ImageView cource_image;
        public ViewHolder(View itemView) {
            super(itemView);
            course_Name = itemView.findViewById(R.id.course_Name);
            cource_image = itemView.findViewById(R.id.cource_image);
        }
    }

    private void FragmentCall(Fragment fragment, String Tag, String course) {
        Bundle bundle= new Bundle();
        FragmentManager fragmentManager = ((FragmentActivity)activity).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction= fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        bundle.putString("course", course);
        fragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.container, fragment).addToBackStack(Tag).commit();
    }

}
