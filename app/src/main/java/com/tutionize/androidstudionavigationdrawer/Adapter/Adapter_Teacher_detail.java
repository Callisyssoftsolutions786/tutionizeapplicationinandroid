package com.tutionize.androidstudionavigationdrawer.Adapter;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.tutionize.androidstudionavigationdrawer.Constant.AppConst;
import com.tutionize.androidstudionavigationdrawer.Fragment.Fragment_video_lacture;
import com.tutionize.androidstudionavigationdrawer.Model.GetVideo_Thumb_modle;
import com.tutionize.androidstudionavigationdrawer.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by hp on 10-02-2017.
 */
public class Adapter_Teacher_detail extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Activity mActivity;
    private List<com.tutionize.androidstudionavigationdrawer.Model.GetVideo_Thumb_modle> GetVideo_Thumb_modle= new ArrayList<>();
    public Adapter_Teacher_detail(Activity mActivity,List<GetVideo_Thumb_modle> GetVideo_Thumb_modle) {
        this.mActivity = mActivity;
        this.GetVideo_Thumb_modle = GetVideo_Thumb_modle;
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_teacher_detail, parent, false);
        return new FindHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof FindHolder) {
            final GetVideo_Thumb_modle getVideoThumbmodle=GetVideo_Thumb_modle.get(position);
            String thumb=getVideoThumbmodle.getThumb();
            String picturePath=AppConst.videoconstant.concat(thumb);
            if (thumb != null)
            {
                if (! thumb.equals("")){
                    Glide.with(mActivity).load(picturePath).placeholder(R.drawable.video_camera_icon).into(((FindHolder) holder).picture);
                }
            }
            ((FindHolder) holder).picture.setOnClickListener(v -> {
                 String video_id=getVideoThumbmodle.getId();
                 String video_title=getVideoThumbmodle.getVideo_title();
                 String video_url=getVideoThumbmodle.getVideo_url();
                 String video_description=getVideoThumbmodle.getVideo_description();
                 String teacher=getVideoThumbmodle.getTeacher();

                callfrontclass(video_id,teacher,video_description,video_title,video_url);
            });
        }
    }
    @Override
    public int getItemCount() {

        return GetVideo_Thumb_modle.size();
    }
    public class FindHolder extends RecyclerView.ViewHolder {

        ImageView picture;
        FindHolder(View v) {
            super(v);
            picture= (ImageView) v.findViewById(R.id.video_thumb);
        }
    }

    private void callfrontclass(String video_id, String teacher_name, String video_description,String video_title, String video_url)
    {
        Bundle bundle = new Bundle();
        Fragment mFragment = null;
        Class fragmentClass = Fragment_video_lacture.class;
        try {
            mFragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        FragmentManager fragmentManager = ((FragmentActivity) mActivity).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        bundle.putString("video_id", video_id);
        bundle.putString("teacher_name", teacher_name);
        bundle.putString("video_description", video_description);
        bundle.putString("video_title", video_title);
        bundle.putString("video_url", video_url);
        assert mFragment != null;
        mFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.container, mFragment).addToBackStack("jnsbnbs").commit();
    }




}
