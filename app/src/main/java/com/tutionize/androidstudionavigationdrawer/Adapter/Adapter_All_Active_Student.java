package com.tutionize.androidstudionavigationdrawer.Adapter;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.rey.material.widget.ImageView;
import com.rey.material.widget.TextView;
import com.tutionize.androidstudionavigationdrawer.Constant.AppConst;
import com.tutionize.androidstudionavigationdrawer.Constant.Singleton;
import com.tutionize.androidstudionavigationdrawer.Fragment.Live_Video_Screen;
import com.tutionize.androidstudionavigationdrawer.Model.GetToken_model;
import com.tutionize.androidstudionavigationdrawer.Model.HireStudentList_Result;
import com.tutionize.androidstudionavigationdrawer.Model.Start_class_modle;
import com.tutionize.androidstudionavigationdrawer.ProgressDialog;
import com.tutionize.androidstudionavigationdrawer.R;
import com.tutionize.androidstudionavigationdrawer.Sharepool.MySharePrafranceClass;
import com.tutionize.androidstudionavigationdrawer.Sharepool.NetworkStateCheck;
import com.tutionize.androidstudionavigationdrawer.command.ConstantCommand;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * Created by user on 2/1/18.
 */

public class Adapter_All_Active_Student  extends RecyclerView.Adapter<Adapter_All_Active_Student.MyViewHolder> {
    private List<HireStudentList_Result> StudentList=new ArrayList<>();
    private Activity activity;
    private String[] list,loginlist;

    public Adapter_All_Active_Student(Activity activity, List<HireStudentList_Result> StudentList ){
        this.activity= activity;
        this.StudentList=StudentList;

    }
    @NonNull
    @Override
    public Adapter_All_Active_Student.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.active_student_list,parent, false);
        return new Adapter_All_Active_Student.MyViewHolder(itemView);

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull Adapter_All_Active_Student.MyViewHolder holder, int position) {
        HireStudentList_Result data = StudentList.get(position);
        if (data != null)
        {
            holder.txt_name.setText(data.getFirst() + " " + data.getLast());
            holder.txt_country.setText(data.getCountry());
            holder.txt_listtype.setText(data.getType());
            String Picture = data.getThumb();
            if (!(Picture == null)) {
            if (!(Picture.equals(""))) {
                String pic = AppConst.photoconstant.concat(Picture);
                Glide.with(activity).load(pic).into(holder.img_student);
            } else {
                Glide.with(activity).load(R.drawable.userimagewhite).into(holder.img_student);
            }
        } else
            {
            Glide.with(activity).load(R.drawable.userimagewhite).into(holder.img_student);
        }

        holder.CallButton.setOnClickListener((View v) -> {
            Random rand = new Random();
            int minRange = 1000, maxRange = 5000;
            int RoomId = rand.nextInt(maxRange - minRange) + minRange;
            String Student = data.getId();
            String teachername = list[1];
            String teacherimage = list[2];
            if (Student != null && teachername != null && teacherimage != null) {
                SetClassAddress1(Student, "" + RoomId, teachername, teacherimage, ConstantCommand.callType_Audio, list[0], data.getId(), data.getImage(), data.getFirst());
                Toast.makeText(activity, "Call to " + data.getFirst(), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(activity, " Please Add Student ", Toast.LENGTH_SHORT).show();
            }
        });

        holder.VideoCallButton.setOnClickListener(v -> {

            Random rand = new Random();
            int minRange = 1000, maxRange = 5000;
            int RoomId = rand.nextInt(maxRange - minRange) + minRange;
            String Student = data.getId();
            String teachername = list[1];
            String teacherimage = list[2];

            if (Student != null && teachername != null && teacherimage != null) {
                Toast.makeText(activity, "Call to " + data.getFirst(), Toast.LENGTH_SHORT).show();
                SetClassAddress1(Student, "" + RoomId, teachername, teacherimage, ConstantCommand.callType_Video_1, list[0], data.getId(), data.getImage(), data.getFirst());
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setTitle("Message");
                builder.setMessage("Please Add Student First");
                builder.setPositiveButton("OK", (dialogInterface, i) -> dialogInterface.dismiss());
                builder.show();
            }
        });
    }
    }

    @Override
    public int getItemCount()
    {
        return StudentList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txt_name, txt_country, txt_listtype;
        ImageView CallButton,VideoCallButton;
        CircleImageView img_student;
        MyViewHolder(View itemView)
        {
            super(itemView);
            txt_name = itemView.findViewById(R.id.active_Student_list_nameofstudent);
            txt_country = itemView.findViewById(R.id.active_Student_list_country);
            txt_listtype = itemView.findViewById(R.id.active_Student_list_Type);
            CallButton = itemView.findViewById(R.id.active_Student_list_call_image);
            img_student = itemView.findViewById(R.id.active_Student_list_image);
            VideoCallButton= itemView.findViewById(R.id.active_Student_list_Video_call_image);
            loginlist = MySharePrafranceClass.LoginDataGetSharePrefrance(activity);
            list = MySharePrafranceClass.GetSharePrefrance(activity);
        }
    }
    private void SetClassAddress1(String student, String roomId,String teacher_name,String img_name, String Calltype,String caller_id,String AllStudent,String image,String name) {
        if (NetworkStateCheck.isNetworkAvaliable(activity)) {
            Singleton.getInstance().getApi().SetClassAddress(student, roomId, teacher_name, img_name, Calltype, caller_id, new Callback<Start_class_modle>() {
                @Override
                public void success(Start_class_modle start_class_modle, Response response) {
                   if (start_class_modle.getSuccess().equals("1")) {
                        String roomidentity = start_class_modle.getResult();

                    //String roomidentity = roomId;

                        //-----------------------Here  is CHECK  THE PERMISSION---------------------------------------

                        TelephonyManager telephonyManager = (TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE);
                        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                            Toast.makeText(activity, "Allow MicroPhone permission first from App info", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        assert telephonyManager != null;
                        @SuppressLint("HardwareIds")


                        String deviceId = telephonyManager.getDeviceId();



                        String type = loginlist[2];
                        if (roomidentity != null && type != null && deviceId != null) {
                            GoToClass1(roomidentity, type.concat(deviceId), AllStudent, image, name,Calltype);
                        }
                    }
                }
                @Override
                public void failure(RetrofitError error) {
                    Toast.makeText(activity, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }else {
            Toast.makeText(activity, "Internet connection problem", Toast.LENGTH_SHORT).show();
        }
    }
    @SuppressLint("ResourceAsColor")
    private void GoToClass1(final String roomidentity, String userIdentity,String AllStudent,String Image,String Name,String calltype) {
        if (NetworkStateCheck.isNetworkAvaliable(activity)) {
            KProgressHUD knprogress = ProgressDialog.getInstance().getKNProgressDialog(activity);
            knprogress.show();

            Singleton.getInstance().getApi().getToken(roomidentity, userIdentity, new Callback<GetToken_model>() {
                @Override
                public void success(GetToken_model getToken_model, Response response) {

                    if (getToken_model.getResult() == null) {
                        knprogress.dismiss();
                        Toast.makeText(activity, "Unable to connnect " + Name, Toast.LENGTH_SHORT).show();
                    } else if (getToken_model.getResult().equals("")) {
                        knprogress.dismiss();
                        Toast.makeText(activity, "Unable to connnect " + Name, Toast.LENGTH_SHORT).show();
                    } else {
                        String Token = getToken_model.getResult();
                        knprogress.dismiss();
                        callfrontclass3(Token, roomidentity,calltype, AllStudent, Image, Name);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Singleton.getInstance().Progress(activity).dismiss();
                    Toast.makeText(activity, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }else {
            Toast.makeText(activity, "Internet Connection problem", Toast.LENGTH_SHORT).show();
        }
    }
private void callfrontclass3(String token, String room, String Calltype, String AllStudent, String Image, String Name) {

        Bundle bundle = new Bundle();

        Fragment mFragment = null;
        Class fragmentClass = Live_Video_Screen.class;
    try {
            mFragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        FragmentManager fragmentManager = ((FragmentActivity)activity).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);

        bundle.putString("Room", room);
        bundle.putString("Token", token);
        bundle.putString("calltype",Calltype);
        bundle.putString("AllStudent",AllStudent);
        bundle.putString("teacherimage",Image);
        bundle.putString("teachername",Name);
        assert mFragment != null;
        mFragment.setArguments(bundle);

        fragmentTransaction.replace(R.id.container, mFragment).addToBackStack("jnsbnbs").commit();

    }
}
