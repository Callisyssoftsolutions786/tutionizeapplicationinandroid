package com.tutionize.androidstudionavigationdrawer.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.tutionize.androidstudionavigationdrawer.Fragment.Fragment_course_learning;
import com.tutionize.androidstudionavigationdrawer.Fragment.Fragment_course_teaching;

/**
 * Created by hp on 01-03-2017.
 */
public class Adapter_course extends FragmentStatePagerAdapter
{


    int mNumOfTabs;

    public Adapter_course(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs =NumOfTabs ;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                Fragment_course_learning Tab1 = new Fragment_course_learning();
                return Tab1;
            case 1:
                Fragment_course_teaching Tab2 = new Fragment_course_teaching();
                return Tab2;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }



}
