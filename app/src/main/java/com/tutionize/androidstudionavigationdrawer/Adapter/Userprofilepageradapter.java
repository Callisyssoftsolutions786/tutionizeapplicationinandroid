package com.tutionize.androidstudionavigationdrawer.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.tutionize.androidstudionavigationdrawer.Fragment.Fragment_profile_learning;
import com.tutionize.androidstudionavigationdrawer.Fragment.Fragment_profile_teaching;

/**
 * Created by hp on 23-02-2017.
 */
public class Userprofilepageradapter extends FragmentStatePagerAdapter
{
    int mNumOfTabs;

    public Userprofilepageradapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs =NumOfTabs ;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                Fragment_profile_learning Tab1 = new Fragment_profile_learning();
                return Tab1;
            case 1:
                Fragment_profile_teaching Tab2 = new Fragment_profile_teaching();
                return Tab2;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }


}
